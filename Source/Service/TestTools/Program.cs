﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;


namespace TestTools
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = new DirectoryInfo(@"C:\Users\hongnguyenx\Desktop\Research");
            var fileList = path.GetFiles("*.xrl");
            
            if (fileList.Any())
            {
                var desFile = XDocument.Load(fileList[0].FullName);

                for (int i = 1; i < fileList.Count(); i++)
                {
                    var sourceFile = XDocument.Load(fileList[i].FullName);
                    XmlMultiMerge(ref desFile, sourceFile);
                }

                desFile.Save(@"C:\Users\hongnguyenx\Desktop\Research\Total.xrl");
            }
        }

        private static XDocument XmlMultiMerge(ref XDocument detination, XDocument source)
        {
            detination.Descendants("PageList").FirstOrDefault().Add(source.Descendants("PageList").FirstOrDefault().Nodes());
            return detination;
        }
    }
}
