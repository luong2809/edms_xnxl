﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Globalization;

namespace TestFolderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using Aspose.Cells;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Data.Entities;

    /// <summary>
    /// The program.
    /// </summary>
    class Program
    {
        /// <summary>
        /// The is create.
        /// </summary>
        private bool isCreate = false;

        /// <summary>
        /// The file icon.
        /// </summary>
        private static Dictionary<string, string> fileIcon = new Dictionary<string, string>()
                {
                    { "doc", "images/wordfile.png" },
                    { "docx", "images/wordfile.png" },
                    { "dotx", "images/wordfile.png" },
                    { "xls", "images/excelfile.png" },
                    { "xlsx", "images/excelfile.png" },
                    { "pdf", "images/pdffile.png" },
                    { "7z", "images/7z.png" },
                    { "dwg", "images/dwg.png" },
                    { "dxf", "images/dxf.png" },
                    { "rar", "images/rar.png" },
                    { "zip", "images/zip.png" },
                    { "txt", "images/txt.png" },
                    { "xml", "images/xml.png" },
                    { "xlsm", "images/excelfile.png" },
                    { "bmp", "images/bmp.png" },
                };

        /// <summary>
        /// The folder service.
        /// </summary>
        private static FolderService folderService = new FolderService();

        /// <summary>
        /// The document service.
        /// </summary>
        private static DocumentService documentService = new DocumentService();
        private static DocumentPackageService documentPackageService = new DocumentPackageService();
        private static RevisionService revisionService = new RevisionService();
        private static StatusService statusService = new StatusService();
        private static DisciplineService disciplineService = new DisciplineService();
        private static DocumentTypeService documentTypeService = new DocumentTypeService();
        private static ReceivedFromService receivedFromService = new ReceivedFromService();

        private static PermissionWorkgroupService PermissionWorkgroupService = new PermissionWorkgroupService();

        private static AttachFileService attachFileService = new AttachFileService();

        /// <summary>
        /// The file extensions.
        /// </summary>
        private static List<string> fileExtensions = ConfigurationSettings.AppSettings.Get("Extension").Split(',').ToList();

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        static void Main(string[] args)
        {
            var workbook = new Workbook();
            workbook.Open(@"C:\Users\hongnguyen\Desktop\EDMS BLQDA\Import EDMR.xls");
            var wsData = workbook.Worksheets[0];
            var dataTable = wsData.Cells.ExportDataTable(2, 1, wsData.Cells.MaxRow - 1, 12)
                .AsEnumerable().Where(t => !string.IsNullOrEmpty(t["Column1"].ToString())).CopyToDataTable();

            foreach (DataRow dataRow in dataTable.Rows)
            {
                var disciplineObj = disciplineService.GetByName(dataRow["Column8"].ToString(), 21);
                var docObj = new DocumentPackage()
                {
                    ProjectId = 21,
                    ProjectName = "NCS2 - Phase 1",
                    DocNo = dataRow["Column1"].ToString(),
                    DocTitle = dataRow["Column2"].ToString(),
                    RevisionId = 1,
                    RevisionName = "A",
                    DisciplineId = disciplineObj != null ? disciplineObj.ID : 0,
                    DisciplineName = disciplineObj != null ? disciplineObj.Name : string.Empty,
                    IsCriticalDoc = dataRow["Column4"].ToString() == "x",
                    IsPriorityDoc = dataRow["Column5"].ToString() == "x",
                    IsVendorDoc = dataRow["Column6"].ToString() == "x",
                    ReferenceFromID = dataRow["Column7"].ToString() == "FEED" ? 1 : 2,
                    ReferenceFromName = dataRow["Column7"].ToString(),
                    Weight = Math.Round(Convert.ToDouble(dataRow["Column9"].ToString()),2),
                    IsDelete = false,
                    IsLeaf = true,
                    IsEMDR = true,

                    RevisionPlanedDate = DateTime.ParseExact(dataRow["Column10"].ToString(), "dd/MM/yyyy", null),
                    RevisionActualDate = DateTime.ParseExact(dataRow["Column11"].ToString(), "dd/MM/yyyy", null),
                    RevisionReceiveTransNo = dataRow["Column12"].ToString(),
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };

                documentPackageService.Insert(docObj);
            }
            Console.WriteLine("Completed!!!");
            Console.ReadLine();
        }

        /// <summary>
        /// The fsw_ renamed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                    e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.OldName;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var oldFileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var oldPath = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var newFileName = e.Name.Substring(lastPosition + 1, e.Name.Length - lastPosition - 1);

                        var listDocRename = documentService.GetSpecificDocument(oldFileName, oldPath);
                        foreach (var document in listDocRename)
                        {
                            ////var oldRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            document.Name = newFileName;
                            document.FileNameOriginal = newFileName;
                            document.LastUpdatedDate = DateTime.Now;

                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + newFileName;
                            }
                            else
                            {
                                document.RevisionFileName = document.DocIndex + "_" + newFileName;
                            }

                            document.FilePath = document.FilePath.Replace(oldFileName, newFileName);
                            ////document.RevisionFilePath = document.RevisionFilePath.Replace(oldFileName, newFileName);

                            ////var newRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            ////File.Move(oldRevisionPath, newRevisionPath);

                            documentService.Update(document);
                        }

                        ////Console.WriteLine("Renamed: FileName - {0}, ChangeType - {1}, Old FileName - {2}", e.Name, e.ChangeType, e.OldName);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ deleted.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".", StringComparison.Ordinal) + 1, fullPath.Length - fullPath.LastIndexOf(".", StringComparison.Ordinal) - 1);

                if (fileExtensions.Contains(fileExt))
                {
                    try
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var listDocDelete = documentService.GetSpecificDocument(fileName, path);
                        foreach (var document in listDocDelete)
                        {
                            document.IsDelete = true;
                            ////documentService.Update(document);

                            ////var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                   + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;
                            ////var fileTemp = new FileInfo(revisionPath);
                            ////fileTemp.Delete();
                        }

                        Console.WriteLine("Deleted: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.InnerException.Message);
                    }
                }
            }
        }

        /// <summary>
        /// The fsw_ changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                if (fileExtensions.Contains(fileExt.ToLower()))
                {
                    var lastPosition = fullPath.LastIndexOf(@"\");
                    var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                    var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                    var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);
                    if (objFolder != null)
                    {
                        var docLeaf = documentService.GetSpecificDocument(objFolder.ID, fileName);

                        var objDoc = new Document()
                            {
                                Name = docLeaf.Name,
                                DocumentNumber = docLeaf.DocumentNumber,
                                Title = docLeaf.Title,
                                DocumentTypeID = docLeaf.DocumentTypeID,
                                StatusID = docLeaf.StatusID,
                                DisciplineID = docLeaf.DisciplineID,
                                ReceivedFromID = docLeaf.ReceivedFromID,
                                ReceivedDate = docLeaf.ReceivedDate,
                                TransmittalNumber = docLeaf.TransmittalNumber,
                                LanguageID = docLeaf.LanguageID,
                                Well = docLeaf.Well,
                                Remark = docLeaf.Remark,
                                KeyWords = docLeaf.KeyWords,
                                
                                IsLeaf = true,
                                DocIndex = docLeaf.DocIndex + 1,
                                IsDelete = false,
                                FolderID = docLeaf.FolderID,
                                DirName = docLeaf.DirName,
                                FileExtension = docLeaf.FileExtension,
                                FileExtensionIcon = docLeaf.FileExtensionIcon,
                                FileNameOriginal = docLeaf.FileNameOriginal,
                                FilePath = docLeaf.FilePath,
                                CreatedDate = DateTime.Now
                            };

                        if (docLeaf.ParentID == null)
                        {
                            objDoc.ParentID = docLeaf.ID;
                        }
                        else
                        {
                            objDoc.ParentID = docLeaf.ParentID;
                        }

                        objDoc.RevisionFileName = objDoc.DocIndex + "_" + objDoc.FileNameOriginal;
                        objDoc.RevisionFilePath = docLeaf.RevisionFilePath.Replace(
                            docLeaf.RevisionFileName, objDoc.RevisionFileName);
                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";
                        var tempFile = new FileInfo(e.FullPath);


                        tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);

                        docLeaf.IsLeaf = false;

                        documentService.Update(docLeaf);
                        documentService.Insert(objDoc);
                    }

                    Console.WriteLine("Changed: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                }
            }
        }

        /// <summary>
        /// The fsw_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Created(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 && 
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                if (fileExtensions.Contains(fileExt.ToLower()))
                {
                    try
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);

                        var objDoc = new Document()
                            {
                                Name = fileName,
                                DocIndex = 1,
                                CreatedDate = DateTime.Now,
                                IsLeaf = true,
                                IsDelete = false
                            };
                        objDoc.RevisionFileName = objDoc.DocIndex + "_" + fileName;
                        objDoc.FilePath = "/bdpocedms/DocumentLibrary/" + path + "/" + fileName;
                        objDoc.RevisionFilePath = "/bdpocedms/DocumentLibrary/RevisionHistory/" + objDoc.DocIndex + "_" + fileName;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                                       ? fileIcon[fileExt.ToLower()]
                                                       : "images/otherfile.png";
                        objDoc.FileNameOriginal = fileName;
                        objDoc.DirName = "DocumentLibrary/" + path;

                        if (objFolder != null)
                        {
                            objDoc.FolderID = objFolder.ID;
                        }

                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";
                        var tempFile = new FileInfo(e.FullPath);


                        tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);
                        documentService.Insert(objDoc);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.InnerException.Message);
                    }

                    Console.WriteLine("Created: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                }
            }
        }

        /// <summary>
        /// The create folder.
        /// </summary>
        static void CreateFolder()
        {
            var rootPath = ConfigurationSettings.AppSettings.Get("rootPath");
            var listFolder = File.ReadAllLines(ConfigurationSettings.AppSettings.Get("listFolder")).ToList();

            ////Directory.CreateDirectory(rootPath + listFolder[1]);
            foreach (var folder in listFolder)
            {
                if (!string.IsNullOrEmpty(folder))
                {
                    Directory.CreateDirectory(rootPath + folder);
                    var listSubfolder = folder.Split('/').Where(t => !string.IsNullOrEmpty(t)).ToList();
                    var dirFather = listSubfolder[0] + "/" + listSubfolder[1];

                    for (int i = 2; i < listSubfolder.Count; i++)
                    {
                        var folFa = folderService.GetByDirName(dirFather);
                        var folChild = folderService.GetByDirName(dirFather + "/" + listSubfolder[i]);
                        if (folChild == null)
                        {
                            folChild = new Folder
                            {
                                Name = listSubfolder[i],
                                DirName = dirFather + "/" + listSubfolder[i],
                                ParentID = folFa.ID,
                                CategoryID = folFa.CategoryID,
                                Description = listSubfolder[i]
                            };

                            folderService.Insert(folChild);
                        }

                        dirFather += "/" + listSubfolder[i];
                    }
                }
            }

        }
    }
}
