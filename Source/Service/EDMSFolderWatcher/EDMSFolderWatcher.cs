﻿using System.ServiceProcess;
using System.Configuration;

namespace EDMSFolderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Document;
    using EDMs.Data.Entities;

    using Temp.IO;

    public partial class EDMSFolderWatcher : ServiceBase
    {
        /// <summary>
        /// The monitor path.
        /// </summary>
        private readonly string monitorPath = ConfigurationSettings.AppSettings.Get("Directory");

        /// <summary>
        /// The file icon.
        /// </summary>
        private static Dictionary<string, string> fileIcon = new Dictionary<string, string>()
                {
                    { "doc", "images/wordfile.png" },
                    { "docx", "images/wordfile.png" },
                    { "dotx", "images/wordfile.png" },
                    { "xls", "images/excelfile.png" },
                    { "xlsx", "images/excelfile.png" },
                    { "pdf", "images/pdffile.png" },
                    { "7z", "images/7z.png" },
                    { "dwg", "images/dwg.png" },
                    { "dxf", "images/dxf.png" },
                    { "rar", "images/rar.png" },
                    { "zip", "images/zip.png" },
                    { "txt", "images/txt.png" },
                    { "xml", "images/xml.png" },
                    { "xlsm", "images/excelfile.png" },
                    { "bmp", "images/bmp.png" },
                };

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The file extensions.
        /// </summary>
        private readonly List<string> fileExtensions = ConfigurationSettings.AppSettings.Get("Extension").Split(',').ToList();

        /// <summary>
        /// The folder watcher.
        /// </summary>
        private MyFileSystemWatcher folderWatcher;

        /// <summary>
        /// The is action.
        /// </summary>
        private bool isAction = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="EDMSFolderWatcher"/> class.
        /// </summary>
        public EDMSFolderWatcher()
        {
            InitializeComponent();

            folderService = new FolderService();
            documentService = new DocumentService();
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            this.isAction = true;
            this.folderWatcher = new MyFileSystemWatcher(this.monitorPath, "*.*")
                {
                    Interval = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("WatcherInterval")),
                    InternalBufferSize = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("WatcherBufferSize"))
                };

            this.folderWatcher.Created += new System.IO.FileSystemEventHandler(fsw_Created);
            this.folderWatcher.Changed += new System.IO.FileSystemEventHandler(fsw_Changed);
            this.folderWatcher.Deleted += new System.IO.FileSystemEventHandler(fsw_Deleted);
            this.folderWatcher.Renamed += new System.IO.RenamedEventHandler(fsw_Renamed);
            this.folderWatcher.EnableRaisingEvents = true;
            EventLog.WriteEntry("EDMS folder watcher service is started. Watching on '" + this.monitorPath + "'");
        }

        protected override void OnStop()
        {
            this.folderWatcher.Dispose();
            
            EventLog.WriteEntry("EDMS folder watcher service is stoped.");
        }

        protected override void OnCustomCommand(int command)
        {
            switch (command)
            {
                case 129:
                    this.isAction = true;
                    break;
                case 128:
                    this.isAction = false;
                    break;
            }

            this.EventLog.WriteEntry(this.isAction ? "Action is enabled." : "Action is disabled.");
        }

        /// <summary>
        /// The fsw_ renamed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                    e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.OldName;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var oldFileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var oldPath = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var newFileName = e.Name.Substring(lastPosition + 1, e.Name.Length - lastPosition - 1);

                        var listDocRename = documentService.GetSpecificDocument(oldFileName, oldPath);
                        foreach (var document in listDocRename)
                        {
                            ////var oldRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            document.Name = newFileName;
                            document.FileNameOriginal = newFileName;
                            document.LastUpdatedDate = DateTime.Now;

                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + newFileName;
                            }
                            else
                            {
                                document.RevisionFileName = document.DocIndex + "_" + newFileName;
                            }

                            document.FilePath = document.FilePath.Replace(oldFileName, newFileName);
                            ////document.RevisionFilePath = document.RevisionFilePath.Replace(oldFileName, newFileName);

                            ////var newRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            ////File.Move(oldRevisionPath, newRevisionPath);

                            this.documentService.Update(document);
                        }

                        ////Console.WriteLine("Renamed: FileName - {0}, ChangeType - {1}, Old FileName - {2}", e.Name, e.ChangeType, e.OldName);
                    }
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ deleted.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                    e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.Name;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".", StringComparison.Ordinal) + 1, fullPath.Length - fullPath.LastIndexOf(".", StringComparison.Ordinal) - 1);

                    if (this.fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\", StringComparison.Ordinal);
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var listDocDelete = this.documentService.GetSpecificDocument(fileName, path);
                        foreach (var document in listDocDelete)
                        {
                            document.IsDelete = true;
                            documentService.Update(document);

                            ////var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                   + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;
                            ////var fileTemp = new FileInfo(revisionPath);
                            ////fileTemp.Delete();
                        }

                        ////Console.WriteLine("Deleted: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                    }
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                    e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.Name;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");
                        var revisionFileName = DateTime.Now.ToString("ddMMyyhhmmss") + "_" + fileName;

                        var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);

                        var objDoc = new Document()
                            {
                                Name = fileName,
                                DocIndex = 1,
                                CreatedDate = DateTime.Now,
                                IsLeaf = true,
                                IsDelete = false
                            };
                        objDoc.RevisionFileName = fileName;
                        objDoc.FilePath = "/DocumentLibrary/" + path + "/" + fileName;
                        objDoc.RevisionFilePath = "/DocumentLibrary/RevisionHistory/" + revisionFileName;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                                       ? fileIcon[fileExt.ToLower()]
                                                       : "images/otherfile.png";
                        objDoc.FileNameOriginal = fileName;
                        objDoc.DirName = "DocumentLibrary/" + path;

                        if (objFolder != null)
                        {
                            objDoc.FolderID = objFolder.ID;
                            objDoc.CategoryID = objFolder.CategoryID;
                        }

                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";

                        ////var tempFile = new FileInfo(e.FullPath);
                        ////tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);

                        ////File.Copy(e.FullPath, revisionPath + objDoc.RevisionFileName, true);

                        documentService.Insert(objDoc);
                        ////Console.WriteLine("Created: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                    }
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }
    }
}
