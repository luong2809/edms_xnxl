﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Workflow;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class DistributionMatrixDetailService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DistributionMatrixDetailDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributionMatrixDetailService"/> class.
        /// </summary>
        public DistributionMatrixDetailService()
        {
            this.repo = new DistributionMatrixDetailDAO();
        }

        #region Get (Advances)
        public List<Data.Entities.DistributionMatrixDetail> GetAllByDM(int dmId)
        {
            return this.repo.GetAll().Where(t => t.DistributionMatrixId == dmId).ToList();
        }

        public Data.Entities.DistributionMatrixDetail GetByName(string name)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.Name.Trim() == name.Trim());
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Data.Entities.DistributionMatrixDetail> GetAll()
        {
            return this.repo.GetAll().OrderBy(t => t.Name).ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Data.Entities.DistributionMatrixDetail GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Data.Entities.DistributionMatrixDetail bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Data.Entities.DistributionMatrixDetail bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Data.Entities.DistributionMatrixDetail bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
