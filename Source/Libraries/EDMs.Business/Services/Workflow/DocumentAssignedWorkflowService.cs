﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Workflow;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class DocumentAssignedWorkflowService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DocumentAssignedWorkflowDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentAssignedWorkflowService"/> class.
        /// </summary>
        public DocumentAssignedWorkflowService()
        {
            this.repo = new DocumentAssignedWorkflowDAO();
        }

        #region Get (Advances)
        public Data.Entities.DocumentAssignedWorkflow GetLeafBydoc(int docId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.DocumentID == docId && t.IsLeaf.GetValueOrDefault());
        }
        
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Data.Entities.DocumentAssignedWorkflow> GetAll()
        {
            return this.repo.GetAll().ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Data.Entities.DocumentAssignedWorkflow GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Data.Entities.DocumentAssignedWorkflow bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Data.Entities.DocumentAssignedWorkflow bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Data.Entities.DocumentAssignedWorkflow bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
