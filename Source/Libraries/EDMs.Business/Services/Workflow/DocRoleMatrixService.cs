﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Workflow;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class DocRoleMatrixService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DocRoleMatrixDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocRoleMatrixService"/> class.
        /// </summary>
        public DocRoleMatrixService()
        {
            repo = new DocRoleMatrixDAO();
        }

        #region Get (Advances)
        public List<DocRoleMatrix> GetAllByDoc(int docId)
        {
            return repo.GetAll().Where(t => t.DocId == docId).ToList();
        }

        public DocRoleMatrix GetDocRoleMatrixByGroup(int docId, int roleId)
        {
            return repo.GetAll().FirstOrDefault(t => t.DocId == docId && t.RoleId == roleId);
        }
        public List<DocRoleMatrix> GetDocRoleMatrixByDeparment(int roleId)
        {
            return repo.GetAll().Where(t => t.RoleId == roleId).ToList();
        }

        public List<DocRoleMatrix> GetAllDocRoleMatrixByGroup(int roleId, int projectId)
        {
            return repo.GetAll().Where(t => t.RoleId == roleId && t.ProjectId == projectId).ToList();
        }
        public List<DocRoleMatrix> GetAllDocRoleMatrixByGroup(int roleId, DateTime? fromDate, DateTime? toDate, int projectId, int distributeStatus)
        {
            return repo.GetAll().Where(t => t.RoleId == roleId
                && (fromDate != null && t.CreatedDate >= fromDate)
                && (toDate != null && t.CreatedDate < toDate.Value.AddDays(1))
                && (projectId == 0 || t.ProjectId == projectId)
                && (distributeStatus == 0 || t.IsDistributed == (distributeStatus == 2))
                ).ToList();
        }

        public List<DocRoleMatrix> GetAllDocRoleMatrixDistributedBy(int userId, DateTime? fromDate, DateTime? toDate)
        {
            return repo.GetAll().Where(t => t.CreatedBy == userId
                && (fromDate != null && t.CreatedDate >= fromDate)
                && (toDate != null && t.CreatedDate < toDate.Value.AddDays(1))).ToList();
        }

        public List<int> GetProjectIdsInPermissionByRole(int roleId)
        {
            return repo.GetAll().Where(t => t.RoleId == roleId).Select(t => t.ProjectId.GetValueOrDefault()).ToList();
        }

        public List<DocRoleMatrix> GetAll(DateTime? fromDate, DateTime? todate)
        {
            return repo.GetAll().Where(t => (fromDate != null && t.CreatedDate >= fromDate)
                && (todate != null && t.CreatedDate < todate.Value.AddDays(1))).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<DocRoleMatrix> GetAll()
        {
            return repo.GetAll().ToList();
        }



        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public DocRoleMatrix GetById(int id)
        {
            return repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(DocRoleMatrix bo)
        {
            return repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(DocRoleMatrix bo)
        {
            try
            {
                return repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(DocRoleMatrix bo)
        {
            try
            {
                return repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
