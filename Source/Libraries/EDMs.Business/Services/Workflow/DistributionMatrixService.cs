﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Workflow;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class DistributionMatrixService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DistributionMatrixDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributionMatrixService"/> class.
        /// </summary>
        public DistributionMatrixService()
        {
            this.repo = new DistributionMatrixDAO();
        }

        #region Get (Advances)

        public Data.Entities.DistributionMatrix GetByName(string name)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.Name.Trim() == name.Trim());
        }

        public List<Data.Entities.DistributionMatrix> GetAllGroup(int roleId)
        {
            return this.repo.GetAll().Where(t => t.RoleId == roleId || t.RoleId == 0).OrderBy(t => t.Name).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Data.Entities.DistributionMatrix> GetAll()
        {
            return this.repo.GetAll().OrderBy(t => t.Name).ToList();
        }

        public List<Data.Entities.DistributionMatrix> GetByProject(int projectId)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId).OrderBy(t => t.Name).ToList();
        }

        public List<Data.Entities.DistributionMatrix> GetByType(int projectId, int roleId, int typeId)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId
                && (t.RoleId == 0 || t.RoleId == roleId)
                && (typeId == 0 || t.TypeId == typeId)).OrderBy(t => t.Name).ToList();
        }

        public Data.Entities.DistributionMatrix GetByType(int projectId, int typeEngineerId, int typeId, int roleId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.ProjectId == projectId 
                                                            && t.TypeEngineerId == typeEngineerId 
                                                            && t.TypeId == typeId
                                                            && t.RoleId == roleId);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Data.Entities.DistributionMatrix GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Data.Entities.DistributionMatrix bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Data.Entities.DistributionMatrix bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Data.Entities.DistributionMatrix bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
