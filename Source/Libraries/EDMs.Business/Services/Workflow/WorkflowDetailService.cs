﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Workflow;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class WorkflowDetailService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly WorkflowDetailDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowDetailService"/> class.
        /// </summary>
        public WorkflowDetailService()
        {
            this.repo = new WorkflowDetailDAO();
        }

        #region Get (Advances)
        public WorkflowDetail GetByCurrentStep(int wfStepId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.CurrentWorkflowStepID == wfStepId);
        }

        public List<WorkflowDetail> GetAllByWorkflow(int wfId)
        {
            return this.repo.GetAll().Where(t => t.WorkflowID == wfId).OrderBy(t => t.CurrentWorkflowStepName).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<WorkflowDetail> GetAll()
        {
            return this.repo.GetAll().OrderBy(t => t.CurrentWorkflowStepName).ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public WorkflowDetail GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(WorkflowDetail bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(WorkflowDetail bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(WorkflowDetail bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
