﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Workflow;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class DistributionMatrixDetailByUserService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DistributionMatrixDetailByUserDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributionMatrixDetailByUserService"/> class.
        /// </summary>
        public DistributionMatrixDetailByUserService()
        {
            this.repo = new DistributionMatrixDetailByUserDAO();
        }

        #region Get (Advances)
        public List<DistributionMatrixDetailByUser> GetAllByDoc(int docId, int dmId)
        {
            return this.repo.GetAll().Where(t => t.DocId == docId && t.DMId == dmId).ToList();
        }

        public List<DistributionMatrixDetailByUser> GetAllByDoc(int docId)
        {
            return this.repo.GetAllByDoc(docId);
        }
        public List<DistributionMatrixDetailByUser> GetAllReceivedDocByUser(int userId, DateTime? fromDate, DateTime? toDate, int typeId, int projectId, int distributeStatus)
        {
            return this.repo.GetAll().Where(t => t.UserId == userId
                && (fromDate != null && t.CreatedDate >= fromDate)
                && (toDate != null && t.CreatedDate < toDate.Value.AddDays(1))
                && (typeId == 0 || t.TypeId == typeId)
                && (projectId == 0 || t.ProjectId == projectId)
                && (distributeStatus == 0 || t.IsDistributed == (distributeStatus == 2))
                ).ToList();
        }
        public List<DistributionMatrixDetailByUser> GetAllReceivedDocByUser(int userId)
        {
            return this.repo.GetAll().Where(t => t.UserId == userId).ToList();
        }
        public List<DistributionMatrixDetailByUser> GetAllDistributedDocByUser(int userId, DateTime? fromDate, DateTime? toDate, int typeId)
        {
            return this.repo.GetAll().Where(t => t.CreatedBy == userId
                && (fromDate != null && t.CreatedDate >= fromDate)
                && (toDate != null && t.CreatedDate < toDate.Value.AddDays(1))
                && (typeId == 0 || t.TypeId == typeId)).ToList();
        }

        public DistributionMatrixDetailByUser GetDocRoleMatrixByUser(int docId, int userid, int dmId)
        {
            return this.repo.GetDocRoleMatrixByUser(docId, userid, dmId);
        }
        public List<DistributionMatrixDetailByUser> GetDMByUserAndDoc(int docId, int userid, int dmId)
        {
            return this.repo.GetAll().Where(t => t.DocId == docId && t.UserId == userid && t.DMId == dmId).ToList();
        }

        public List<DistributionMatrixDetailByUser> GetDM(int dmId)
        {
            return this.repo.GetAll().Where(t => t.DMId == dmId).ToList();
        }

        public List<DistributionMatrixDetailByUser> GetAllDMDetailByUser(int userid, int projectId)
        {
            return this.repo.GetAllDMDetailByUser(userid, projectId);
        }
        public List<DistributionMatrixDetailByUser> GetAllDMDetailByUser(int userid)
        {
            return this.repo.GetAllDMDetailByUser(userid);
        }
        public List<DistributionMatrixDetailByUser> GetAllDMDetailByUser(int userid, List<int> projectids)
        {
            return this.repo.GetAll().Where(t => t.UserId == userid && projectids.Contains(t.ProjectId.GetValueOrDefault())).ToList();
        }
        public DistributionMatrixDetailByUser GetDMDetailByUser(int userid, int docId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.UserId == userid && t.DocId == docId);
        }

        public DistributionMatrixDetailByUser GetDistributionMatrixDetailByDM(int dmId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.DMId == dmId);
        }

        public List<DistributionMatrixDetailByUser> GetAllByDM(int docId, int dmId)
        {
            return this.repo.GetAllByDM(docId, dmId);
        }

        public List<int> GetProjectIdListInPermission(int userId)
        {
            return this.repo.GetProjectIdListInPermission(userId);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<DistributionMatrixDetailByUser> GetAll()
        {
            return this.repo.GetAll().ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public DistributionMatrixDetailByUser GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Data.Entities.DistributionMatrixDetailByUser bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Data.Entities.DistributionMatrixDetailByUser bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Data.Entities.DistributionMatrixDetailByUser bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
