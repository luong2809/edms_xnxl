﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Workflow;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class DocumentAssignedUserService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DocumentAssignedUserDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentAssignedUserService"/> class.
        /// </summary>
        public DocumentAssignedUserService()
        {
            this.repo = new DocumentAssignedUserDAO();
        }

        #region Get (Advances)
        public List<Data.Entities.DocumentAssignedUser> GetAllIncompleteByUser(int userid)
        {
            return this.repo.GetAll().Where(t => t.UserID == userid 
                                                && !t.IsComplete.GetValueOrDefault()).ToList();
        }
        public List<Data.Entities.DocumentAssignedUser> GetAllIncomplete()
        {
            return this.repo.GetAll().Where(t => !t.IsComplete.GetValueOrDefault()).ToList();
        }

        public List<Data.Entities.DocumentAssignedUser> GetAllIncompleteByDoc(int docId)
        {
            return this.repo.GetAll().Where(t => t.DocumentID == docId
                                                && !t.IsComplete.GetValueOrDefault()).ToList();
        }

        public Data.Entities.DocumentAssignedUser GetCurrentInCompleteByDocUser(int userId, int docId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.DocumentID == docId 
                                                        && t.UserID == userId 
                                                        && !t.IsComplete.GetValueOrDefault());
        }

        public List<Data.Entities.DocumentAssignedUser> GetCurrentCanReject(int docId)
        {
            return this.repo.GetAll().Where(t => t.DocumentID == docId
                                                        && !t.IsComplete.GetValueOrDefault()).ToList();
        }

        public List<Data.Entities.DocumentAssignedUser> GetAllByDoc(int docId)
        {
            return this.repo.GetAll().Where(t => t.DocumentID == docId).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Data.Entities.DocumentAssignedUser> GetAll()
        {
            return this.repo.GetAll().ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Data.Entities.DocumentAssignedUser GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Data.Entities.DocumentAssignedUser bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Data.Entities.DocumentAssignedUser bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Data.Entities.DocumentAssignedUser bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
