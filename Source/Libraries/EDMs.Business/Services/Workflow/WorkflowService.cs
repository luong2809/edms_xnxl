﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Business.Services.Library;
using EDMs.Data.DAO.Workflow;

namespace EDMs.Business.Services.Workflow
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class WorkflowService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly WorkflowDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowService"/> class.
        /// </summary>
        public WorkflowService()
        {
            this.repo = new WorkflowDAO();
        }

        #region Get (Advances)
        public List<Data.Entities.Workflow> GetAllGroup(int roleId)
        {
            return this.repo.GetAll().Where(t => t.RoleId == roleId || t.RoleId == 0).OrderBy(t => t.Name).ToList();
        }
        public List<Data.Entities.Workflow> GetAllByProject(int projectId)
        {
            return this.repo.GetAll().Where(t => t.ProjectID == projectId).ToList();
        }

        public List<Data.Entities.Workflow> GetAllByProject(int projectId, int typeId)
        {
            return this.repo.GetAll().Where(t => t.ProjectID == projectId && t.TypeId == typeId).ToList();
        }

        public List<Data.Entities.Workflow> GetAllByProjectAndRole(int projectId, int roleId)
        {
            return this.repo.GetAll().Where(t => t.ProjectID == projectId 
                && (t.RoleId == 0 || t.RoleId == roleId)).ToList();
        }
        public List<Data.Entities.Workflow> GetAllByProjectAndRole(int projectId, int roleId, int typeId)
        {
            return this.repo.GetAll().Where(t => t.ProjectID == projectId 
                && (t.RoleId == 0 || t.RoleId == roleId)
                && t.TypeId == typeId).ToList();
        }

        public Data.Entities.Workflow GetByName(string name)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.Name.Trim() == name.Trim());
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Data.Entities.Workflow> GetAll()
        {
            return this.repo.GetAll().OrderBy(t => t.Name).ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Data.Entities.Workflow GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Data.Entities.Workflow bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Data.Entities.Workflow bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Data.Entities.Workflow bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
