﻿
namespace EDMs.Business.Services.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using EDMs.Data.DAO.Security;

  public  class UserAccessSystemService
    {
       private readonly UserAccesSystemDAO repo;

       public UserAccessSystemService()
        {
            this.repo = new UserAccesSystemDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// 
      /////
       public List<UsersAccessSystem> GetAllCoutID(int countid)
       {
           return this.repo.GetAllByCopuntIds(countid).ToList();
       }


       public List<UsersAccessSystem> GetSpecialListCountID(List<int> countids)
       {
           return this.repo.GetSpecialListUser(countids).ToList();
       }

       public UsersAccessSystem GetId(int id)
       {
           return this.repo.GetByID(id);
       }
       public List<UsersAccessSystem> GetAllOfDate(DateTime date)
       {
           return this.repo.GetAll().Where(t => t.DateAccess.Value == date.Date).ToList();
       }
        #endregion

       public int? Insert(UsersAccessSystem bo)
       {
           return this.repo.Insert(bo);
       }


       public bool Update(UsersAccessSystem bo)
       {
           try
           {
               return this.repo.Update(bo);
           }
           catch (Exception)
           {
               return false;
           }
       }

       public bool Delete(UsersAccessSystem bo)
       {
           try
           {
               return this.repo.Delete(bo);
           }
           catch (Exception)
           {
               return false;
           }
       }

       /// <summary>
       /// Delete Resource By ID
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public bool Delete(int id)
       {
           try
           {
               return this.repo.Delete(id);
           }
           catch (Exception)
           {
               return false;
           }
       }

    }
}
