﻿namespace EDMs.Business.Services.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using EDMs.Data.DAO.Security;

    public class RoleService
    {      
        private readonly RoleDAO repo;

        public RoleService()
        {
            this.repo = new RoleDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <param name="isHost">
        /// The is Host.
        /// </param>
        /// <returns>
        /// </returns>
        public List<Role> GetAll(bool isHost)
        {
            return isHost
                       ? this.repo.GetAll()
                       : this.repo.GetAll().Where(t => t.Id != 1).ToList();
        }

        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        public List<Role> GetAllSpecial()
        {
            return this.repo.GetAllSpecial();
        }

        /// <summary>
        /// Get Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Role GetByID(int ID)
        {
            return this.repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Get (Advances)

        public Role GetByName(string name)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.Name.Trim() == name.Trim());
        }

        public Role GetByFullName(string name)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.FullName == name.Trim());
        }

        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(Role bo)
        {
            try
            {
                return this.repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Role bo)
        {
            try
            {
                return this.repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Role bo)
        {
            try
            {
                return this.repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return this.repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
