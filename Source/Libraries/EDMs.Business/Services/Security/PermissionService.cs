﻿namespace EDMs.Business.Services.Security
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;
    using EDMs.Data.DAO.Security;

    public class PermissionService
    {      
        private readonly PermissionDAO repo;

        public PermissionService()
        {
            this.repo = new PermissionDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        public List<Permission> GetAll()
        {
            return this.repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Permission GetByID(int id)
        {
            return this.repo.GetById(id);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Get (Advances)

        public List<Permission> GetByRoleId(int roleId)
        {
            return this.repo.GetByRoleId(roleId);
        }

        public List<Permission> GetByRoleId(int roleId, int specialParent)
        {
            return this.repo.GetByRoleId(roleId, specialParent).Where(t => t.Menu.Active == true).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool DeletePermissions(List<Permission> permissions)
        {
            return this.repo.DeletePermissions(permissions);
        }

        /// <summary>
        /// Adds the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool AddPermissions(List<Permission> permissions)
        {
            return this.repo.AddPermissions(permissions);
        }

        #endregion
        
    }
}
