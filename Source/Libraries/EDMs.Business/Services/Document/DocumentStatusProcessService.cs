﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentStatusProcessService.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class DocumentStatusProcessService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DocumentStatusProcessDAO repo;


        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentStatusProcessService"/> class.
        /// </summary>
        public DocumentStatusProcessService()
        {
            this.repo = new DocumentStatusProcessDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentStatusProcess> GetSpecific(int tranId)
        {
            return this.repo.GetSpecific(tranId);
        }

        public List<DocumentStatusProcess> GetAllByDoc(int docId)
        {
            return this.repo.GetAll().Where(t => t.DocId == docId).ToList();
        }
        public DocumentStatusProcess GetOneByDocAndStatus(int docId, int statusId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.DocId == docId && t.StatusId == statusId);
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<DocumentStatusProcess> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

       
        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public DocumentStatusProcess GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(DocumentStatusProcess bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(DocumentStatusProcess bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(DocumentStatusProcess bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
