﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Data.DAO.Document;
using EDMs.Data.Entities;

namespace EDMs.Business.Services.Document
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class DocumentPackageService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DocumentPackageDAO repo;


        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentPackageService"/> class.
        /// </summary>
        public DocumentPackageService()
        {
            this.repo = new DocumentPackageDAO();
        }

        #region Get (Advances)
        public List<DocumentPackage> GetAllDocMaster(int projectId)
        {
            return this.repo.GetAll().Where(t => t.ParentId == null && !t.IsDelete.GetValueOrDefault() && t.ProjectId == projectId).ToList();
        }
        public List<DocumentPackage> GetAllDocInWFProccess()
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.IsLeaf.GetValueOrDefault()
                && t.IsInWFProcess.GetValueOrDefault()).ToList();
        }

        public DocumentPackage GetOneByDocNo_(string docNo, string revName, int Project)
        {
            return this.repo.GetAll().FirstOrDefault(t => !t.IsDelete.GetValueOrDefault()
                && (t.DocNo.ToLower().Trim() == docNo.ToLower().Trim())
                && (t.RevisionName.Trim() == revName.Trim())
                && t.ProjectId.GetValueOrDefault() == Project);
        }
        public DocumentPackage GetOneByDocNo(string docNo, string revName, int projectId)
        {
            return this.repo.GetAll().FirstOrDefault(t =>
                    (t.DocNo.Trim().Replace(" ", string.Empty) == docNo.Trim().Replace(" ", string.Empty))
                    && (t.RevisionName == revName)
                    && (t.ProjectId == projectId)
                    && (!t.IsDelete.GetValueOrDefault()));
        }
        public DocumentPackage GetOneByDocNo(string docNo, int projectId)
        {
            return this.repo.GetAll().FirstOrDefault(t => !t.IsDelete.GetValueOrDefault()
                && t.DocNo.Trim().Replace(" ", string.Empty) == docNo.Trim().Replace(" ", string.Empty)
                && t.IsLeaf.GetValueOrDefault()
                && t.ProjectId == projectId
                && t.TypeId == 1);
        }
        public List<DocumentPackage> GetAllOfProject(int projectId, int type)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.IsLeaf.GetValueOrDefault()
                && t.ProjectId == projectId
                && t.TypeId == type).ToList();
        }

        public List<DocumentPackage> GetAllOfProject(int projectId, int type, bool isGetAll)
        {
            return this.repo.GetAllOfProject(projectId, type, isGetAll);
        }

        public List<DocumentPackage> GetAllOfProjectNotIsLeaf(int projectId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.ProjectId == projectId
                && t.TypeId == 1).ToList();
        }

        public DocumentPackage GetOneByDocVendor(string docNo, string revName, int projectId)
        {
            return this.repo.GetAll().FirstOrDefault(t => !t.IsDelete.GetValueOrDefault()
                && t.DocNo.ToLower().Trim().Replace(" ", string.Empty) == docNo.ToLower().Trim().Replace(" ", string.Empty)
                && (t.RevisionName.Trim().ToLower() == revName.ToLower().Trim())
                && t.ProjectId == projectId
                && t.TypeId == 3);
        }

        /// <summary>
        /// The get all by package.
        /// </summary>
        /// <param name="packageId">
        /// The package id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentPackage> GetAllByPackage(int packageId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.IsLeaf.GetValueOrDefault()
                && t.PackageId == packageId).ToList();
        }

        public List<DocumentPackage> GetAllByWorkgroup(int workgroupId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault() && t.IsLeaf.GetValueOrDefault() && t.WorkgroupId == workgroupId).ToList();
        }

        public List<DocumentPackage> GetAllByDiscipline(int disciplineID, bool isGetAll)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && (isGetAll || t.IsLeaf.GetValueOrDefault())
                && t.DisciplineId == disciplineID).ToList();
        }

        public List<DocumentPackage> SearchForAutoDistributionProject(DateTime? fromDate, DateTime? toDate, int projectId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.IsLeaf.GetValueOrDefault()
                && t.ProjectId == projectId
                && (fromDate != null && t.UpdatedAttachFile >= fromDate)
                && (toDate != null && t.UpdatedAttachFile < toDate.Value.AddDays(1))
                ).ToList();
        }
        public List<DocumentPackage> SearchForAutoDistributionProject(DateTime? fromDate, DateTime? toDate, List<int> projectIds)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.IsLeaf.GetValueOrDefault()
                && projectIds.Contains(t.ProjectId.GetValueOrDefault())
                && (fromDate != null && t.UpdatedAttachFile >= fromDate)
                && (toDate != null && t.UpdatedAttachFile < toDate.Value.AddDays(1))
                ).ToList();
        }

        public List<DocumentPackage> GetAllByDocType(int docTypeid, bool isGetAll)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && (isGetAll || t.IsLeaf.GetValueOrDefault())
                && t.DocumentTypeId == docTypeid).ToList();
        }

        public List<DocumentPackage> GetAllByWorkgroupInPermission(List<int> listworkgroupId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault() && t.IsLeaf.GetValueOrDefault() && listworkgroupId.Contains(t.WorkgroupId.GetValueOrDefault())).ToList();
        }

        public List<DocumentPackage> GetAllByDisciplineInPermission(List<int> listDisciplineId, bool isGetAll)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && (isGetAll || t.IsLeaf.GetValueOrDefault())
                && listDisciplineId.Contains(t.DisciplineId.GetValueOrDefault())).ToList();
        }

        public List<DocumentPackage> GetAllEMDRByWorkgroup(int workgroupId, bool isGetAll)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault() && t.IsLeaf.GetValueOrDefault() && t.WorkgroupId == workgroupId && (isGetAll || t.IsEMDR.GetValueOrDefault())).ToList();
        }

        public List<DocumentPackage> GetAllEMDRByDiscipline(int disciplineId, bool isGetAll)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                    && t.IsLeaf.GetValueOrDefault()
                    && t.DisciplineId == disciplineId
                    && (isGetAll || t.IsEMDR.GetValueOrDefault())).ToList();
        }

        public bool IsExistByDocNo(string docNo)
        {
            return this.repo.GetAll().Any(t => t.IsLeaf.GetValueOrDefault() && !t.IsDelete.GetValueOrDefault() && t.DocNo == docNo);
        }

        /// <summary>
        /// The get all related document.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentPackage> GetAllRelatedDocument(int docId)
        {
            return this.repo.GetAll().Where(t => (t.ID == docId || t.ParentId == docId) && !t.IsDelete.GetValueOrDefault()).ToList();
        }

        public List<DocumentPackage> GetAllRevDoc(int parentId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault() && (t.ID == parentId || t.ParentId == parentId) && t.RevisionId != 0).OrderByDescending(t => t.RevisionId).ToList();
        }

        public List<DocumentPackage> SearchDocument(int projectId, int workfgroupId, string docNo, string docTitle, string searchFullFields, bool isGetAllRevision)
        {
            return this.repo.GetAll().Where(
                t =>
                (isGetAllRevision || t.IsLeaf.GetValueOrDefault())
                && !t.IsDelete.GetValueOrDefault()
                //&& t.IsEMDR.GetValueOrDefault()
                && (workfgroupId == 0 || t.WorkgroupId == workfgroupId)
                && (projectId == 0 || t.ProjectId == projectId)
                && (string.IsNullOrEmpty(docNo) || t.DocNo.ToLower().Contains(docNo.ToLower()))
                && (string.IsNullOrEmpty(docTitle) || t.DocTitle.ToLower().Contains(docTitle.ToLower()))
                && (string.IsNullOrEmpty(searchFullFields)
                    || (!string.IsNullOrEmpty(t.DocNo) && t.DocNo.ToLower().Contains(searchFullFields.ToLower()))
                    || (!string.IsNullOrEmpty(t.DocTitle) && t.DocTitle.ToLower().Contains(searchFullFields.ToLower())))).OrderBy(t => t.DocNo).ToList();
        }

        public List<DocumentPackage> SearchEMDR(string searchAll, string docNo, string docTitle, int projectId, int docTypeId, int disciplineId, int departmentId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.IsLeaf.GetValueOrDefault()
                && t.TypeId == 1
                && (string.IsNullOrEmpty(docNo) || t.DocNo.ToLower().Contains(docNo.ToLower()))
                && (string.IsNullOrEmpty(docTitle) || t.DocTitle.ToLower().Contains(docTitle.ToLower()))
                && (projectId == 0 || t.ProjectId == projectId)
                && (docTypeId == 0 || t.DocumentTypeId == docTypeId)
                && (disciplineId == 0 || t.DisciplineId == disciplineId)
                && (departmentId == 0 || t.DeparmentId == departmentId)
                && (string.IsNullOrEmpty(searchAll)
                    || (!string.IsNullOrEmpty(t.DocNo) && t.DocNo.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DocTitle) && t.DocTitle.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DeparmentName) && t.DeparmentName.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DocumentTypeName) && t.DocumentTypeName.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DisciplineName) && t.DisciplineName.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.IncomingTransNo) && t.IncomingTransNo.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.OutgoingTransNo) && t.OutgoingTransNo.ToLower().Contains(searchAll.ToLower()))
                    )
                ).ToList();
        }

        public List<DocumentPackage> SearchShopDrawing(string searchAll, string docNo, string docTitle, int projectId, int docTypeId, int disciplineId, int departmentId)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && t.IsLeaf.GetValueOrDefault()
                && t.TypeId == 2
                && (string.IsNullOrEmpty(docNo) || t.DocNo.ToLower().Contains(docNo.ToLower()))
                && (string.IsNullOrEmpty(docTitle) || t.DocTitle.ToLower().Contains(docTitle.ToLower()))
                && (projectId == 0 || t.ProjectId == projectId)
                && (docTypeId == 0 || t.DocumentTypeId == docTypeId)
                && (disciplineId == 0 || t.DisciplineId == disciplineId)
                && (departmentId == 0 || t.DeparmentId == departmentId)
                && (string.IsNullOrEmpty(searchAll)
                    || (!string.IsNullOrEmpty(t.DocNo) && t.DocNo.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DocTitle) && t.DocTitle.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DeparmentName) && t.DeparmentName.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DocumentTypeName) && t.DocumentTypeName.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.DisciplineName) && t.DisciplineName.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.IncomingTransNo) && t.IncomingTransNo.ToLower().Contains(searchAll.ToLower()))
                    || (!string.IsNullOrEmpty(t.OutgoingTransNo) && t.OutgoingTransNo.ToLower().Contains(searchAll.ToLower()))
                    )
                ).ToList();
        }

        public List<DocumentPackage> GetNewReceivedDoc(int projectId, int typeId, DateTime? fromDate, DateTime? toDate)
        {
            return this.repo.GetAll().Where(t => (fromDate != null && t.UpdatedAttachFile >= fromDate)
                && (toDate != null && t.UpdatedAttachFile < toDate.Value.AddDays(1))
                && (projectId == 0 || t.ProjectId == projectId)
                && t.TypeId == typeId
                && !t.IsDelete.GetValueOrDefault()).ToList();
        }

        public List<DocumentPackage> GetNewReceivedDoc(List<int> projectIds, int typeId, DateTime? fromDate, DateTime? toDate)
        {
            return this.repo.GetAll().Where(t => (fromDate != null && t.UpdatedAttachFile >= fromDate)
                && (toDate != null && t.UpdatedAttachFile < toDate.Value.AddDays(1))
                && projectIds.Contains(t.ProjectId.GetValueOrDefault())
                && t.TypeId == typeId
                && !t.IsDelete.GetValueOrDefault()).ToList();
        }
        public List<DocumentPackage> GetNewSpecialReceivedDoc(List<int> docIds, int typeId, DateTime? fromDate, DateTime? toDate)
        {
            return this.repo.GetAll().Where(t => (fromDate != null && t.UpdatedAttachFile >= fromDate)
                && (toDate != null && t.UpdatedAttachFile < toDate.Value.AddDays(1))
                && docIds.Contains(t.ID)
                && t.TypeId == typeId
                && !t.IsDelete.GetValueOrDefault()).ToList();
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<DocumentPackage> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        public List<DocumentPackage> GetTypeId(int Typeid, bool isGetAll)
        {
            return this.repo.GetAll().Where(t => !t.IsDelete.GetValueOrDefault()
                && (isGetAll || t.IsLeaf.GetValueOrDefault())
                && t.DocumentTypeId == Typeid).ToList();
        }
        public List<DocumentPackage> GetSpecialListAll(List<int> docIds)
        {
            return this.repo.GetAll().Where(t => t.IsDelete == false && docIds.Contains(t.ID)).ToList();
        }

        public List<DocumentPackage> GetSpecialList(List<int> docIds)
        {
            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false && docIds.Contains(t.ID)).ToList();
        }
        public List<DocumentPackage> GetSpecialList(List<int> docIds, int typeId)
        {
            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false && docIds.Contains(t.ID) && t.TypeId == typeId).ToList();
        }

        public List<DocumentPackage> GetAll(int projectId)
        {
            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false && t.ProjectId == projectId).ToList();
        }

        public List<DocumentPackage> GetAllRev(int projectId)
        {
            return this.repo.GetAll().Where(t => t.IsDelete == false && t.ProjectId == projectId).ToList();
        }

        public List<DocumentPackage> GetAllRev(int projectId, bool isGetAll)
        {
            return this.repo.GetAll().Where(t => (isGetAll || t.IsLeaf.GetValueOrDefault()) && t.IsDelete == false && t.ProjectId == projectId).ToList();
        }
        public List<DocumentPackage> GetAll(int projectId, int typeId)
        {
            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false && t.ProjectId == projectId && t.TypeId == typeId).ToList();
        }

        public List<DocumentPackage> GetAllLatestRev(int projectId)
        {
            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false && t.ProjectId == projectId).ToList();
        }
        public List<DocumentPackage> GetAllNewReceived(DateTime? fromDate, DateTime? toDate, int typeId, int projectId, int distributeStatus)
        {
            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false
                && (fromDate != null && t.UpdatedAttachFile >= fromDate)
                && (toDate != null && t.UpdatedAttachFile < toDate.Value.AddDays(1))
                && t.TypeId == typeId
                && (projectId == 0 || t.ProjectId == projectId)
                && (distributeStatus == 0 || t.IsDistributed == (distributeStatus == 2))
                ).ToList();
        }
        public List<DocumentPackage> GetAllLatestRev(List<int> projectIds)
        {
            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false && projectIds.Contains(t.ProjectId.GetValueOrDefault())).ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public DocumentPackage GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(DocumentPackage bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(DocumentPackage bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(DocumentPackage bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
