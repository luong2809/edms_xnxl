﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoDistributionMatriceservice.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class AutoDistributionMatriceService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly AutoDistributionMatrixDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoDistributionMatriceService"/> class.
        /// </summary>
        public AutoDistributionMatriceService()
        {
            this.repo = new AutoDistributionMatrixDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get specific AutoDistributionMatrix.
        /// </summary>
        /// <param name="listFolId">
        /// The list fol id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AutoDistributionMatrix> GetSpecificAutoDistributionMatrix(List<int> listFolId)
        {
            return this.repo.GetSpecificAutoDistributionMatrix(listFolId);
        }
        public List<AutoDistributionMatrix> GetAllByProject(int projectId)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId).ToList();
        }

        public List<AutoDistributionMatrix> GetSpecificAutoDistributionMatricestatic(List<int> listFolId)
        {
            return this.repo.GetSpecificAutoDistributionMatricestatic(listFolId);
        }

        public List<AutoDistributionMatrix> GetAllRelatedPermittedAutoDistributionMatrixItems(List<int> listFolId)
        {
            return this.repo.GetAllRelatedPermittedAutoDistributionMatrixItems(listFolId);
        }

        public List<AutoDistributionMatrix> GetAllSpecificAutoDistributionMatrix(List<int> listFolId)
        {
            return this.repo.GetAllSpecificAutoDistributionMatrix(listFolId);
        }

        public List<AutoDistributionMatrix> GetAllByUser(int userId)
        {
            return this.repo.GetAll().Where(t => t.UserId == userId).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<AutoDistributionMatrix> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public AutoDistributionMatrix GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(AutoDistributionMatrix bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(AutoDistributionMatrix bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(AutoDistributionMatrix bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
