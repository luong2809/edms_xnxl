﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarkupFileService.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class MarkupFileService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly MarkupFileDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MarkupFileService"/> class.
        /// </summary>
        public MarkupFileService()
        {
            this.repo = new MarkupFileDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<MarkupFile> GetSpecific(int tranId)
        {
            return this.repo.GetSpecific(tranId);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<MarkupFile> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// The get all by doc id.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<MarkupFile> GetAllByDocId(int docId)
        {
            return this.repo.GetAll().Where(t => t.DocId == docId).ToList();
        }
        public List<MarkupFile> GetAllByDocAssignUser(int docAssignUserId)
        {
            return this.repo.GetAll().Where(t => t.DocAssignUserId == docAssignUserId).ToList();
        }

        public List<MarkupFile> GetAllByDocIdAndUser(int docId, int userId)
        {
            return this.repo.GetAll().Where(t => t.DocId == docId && t.MarkupBy == userId).ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public MarkupFile GetById(int id)
        {
            return this.repo.GetById(id);
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="MarkupFile"/>.
        /// </returns>
        public MarkupFile GetByNameServer(string nameServer)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.FilePath.ToLower().Contains(nameServer.ToLower()));
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(MarkupFile bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(MarkupFile bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(MarkupFile bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
