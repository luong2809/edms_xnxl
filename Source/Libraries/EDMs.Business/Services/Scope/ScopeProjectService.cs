﻿using System;
using System.Collections.Generic;
using System.Linq;
using EDMs.Business.Services.Library;
using EDMs.Data.DAO.Scope;
using EDMs.Data.Entities;
using EDMs.Business.Services.Security;
namespace EDMs.Business.Services.Scope
{
    /// <summary>
    /// The category service.
    /// </summary>
    public class ScopeProjectService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly ScopeProjectDAO repo;

        /// <summary>
        /// The permission package service.
        /// </summary>
        private readonly PermissionWorkgroupService PermissionWorkgroupService;
        private readonly PermissionDisciplineService permissionDisciplineService;

        private readonly ProjectUserService projectUserService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScopeProjectService"/> class.
        /// </summary>
        public ScopeProjectService()
        {
            this.repo = new ScopeProjectDAO();
            this.PermissionWorkgroupService = new PermissionWorkgroupService();
            this.permissionDisciplineService = new PermissionDisciplineService();
            this.projectUserService = new ProjectUserService();
        }

        #region Get (Advances)

        /// <summary>
        /// The get all in permission.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ScopeProject> GetAllInPermission(int userId)
        {
            var projectIdInPermission = this.permissionDisciplineService.GetProjectInPermission(userId);
            return this.repo.GetAll().Where(t => projectIdInPermission.Contains(t.ID)).ToList();
        }

        public List<ScopeProject> GetAllByUser(int userId)
        {
            var projectIds = this.projectUserService.GetAllByUser(userId).Select(t => t.ProjectId.GetValueOrDefault()).ToList();
            return this.repo.GetAll().Where(t => t.DCId == userId || t.PMId == userId || projectIds.Contains(t.ID)).ToList();
        }
        public List<ScopeProject> GetAllByUser(int userId, List<int> projectAutoDistribution)
        {
            var projectIds = this.projectUserService.GetAllByUser(userId).Select(t => t.ProjectId.GetValueOrDefault()).ToList();
            return this.repo.GetAll().Where(t => t.DCId == userId 
                                            || t.PMId == userId 
                                            || projectIds.Contains(t.ID)
                                            || projectAutoDistribution.Contains(t.ID)).ToList();
        }

        public ScopeProject GetByName(string name)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.Name.Trim() == name.Trim());
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<ScopeProject> GetAll()
        {
            return this.repo.GetAll().OrderBy(t => t.Name).ToList();
        }


        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public ScopeProject GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(ScopeProject bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(ScopeProject bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(ScopeProject bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
