﻿
namespace EDMs.Data.DAO.Security
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

 public  class UserAccesSystemDAO: BaseDAO
    {
     public UserAccesSystemDAO() : base() { }

        #region GET (Basic)
        public List<UsersAccessSystem> GetAll()
        {
            return this.EDMsDataContext.UsersAccessSystems.ToList<UsersAccessSystem>();
        }

        public UsersAccessSystem GetByID(int ID)
        {
            return this.EDMsDataContext.UsersAccessSystems.FirstOrDefault(ob => ob.Id == ID);
        }
       
        #endregion

        #region Get (Advances)

        /// <summary>
        /// Gets the user by username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        //public UsersAccessSystem GetUserByUsername(string username)
        //{
        //    return this.EDMsDataContext.UsersAccessSystems.FirstOrDefault(ob => ob.Username == username);
        //}

        /// <summary>
        /// The get all by role id.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<UsersAccessSystem> GetAllByCopuntIds(int countid)
        {
            return this.EDMsDataContext.UsersAccessSystems.Where(t => t.CoutID == countid).ToList();
        }

        public List<UsersAccessSystem> GetSpecialListUser(List<int> counids)
        {
            return
                this.EDMsDataContext.UsersAccessSystems.ToArray().Where(t => counids.Contains(t.CoutID.GetValueOrDefault())).ToList();
        } 

        /// <summary>
        /// Gets the by resource id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        //public UsersAccessSystem GetByResourceId(int id)
        //{
        //    return this.EDMsDataContext.UsersAccessSystems.FirstOrDefault(x => x.ResourceId == id);
        //}

        #endregion

        #region Insert, Update, Delete
        public int? Insert(UsersAccessSystem ob)
        {
            try
            {
                this.EDMsDataContext.AddToUsersAccessSystems(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.Id;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
       
        public bool Update(UsersAccessSystem src)
        {
            try
            {
                UsersAccessSystem des;

                des = (from rs in this.EDMsDataContext.UsersAccessSystems
                       where rs.Id == src.Id
                       select rs).First();
                des.UserId = src.UserId;
                des.CoutID = src.CoutID;
                des.UserName = src.UserName;
                des.UserFullName = src.UserFullName;
                des.DateAccess = src.DateAccess;
                des.TimeAccess = src.TimeAccess;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(UsersAccessSystem ob)
        {
            try
            {
                UsersAccessSystem _ob = this.GetByID(ob.Id);
                if (_ob != null)
                {
                    this.EDMsDataContext.DeleteObject(_ob);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                UsersAccessSystem _ob = this.GetByID(ID);
                if (_ob != null)
                {
                    this.EDMsDataContext.DeleteObject(_ob);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

       
       
    }
}
