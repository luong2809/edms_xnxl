﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class RoomDAO : BaseDAO
    {
        public RoomDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Room> GetIQueryable()
        {
            return EDMsDataContext.Rooms;
        }
        
        public List<Room> GetAll()
        {
            return EDMsDataContext.Rooms.ToList<Room>();
        }

        public Room GetByID(int ID)
        {
            return EDMsDataContext.Rooms.Where(ob => ob.Id == ID).FirstOrDefault<Room>();
        }
       
        #endregion

        #region GET ADVANCE
        
        #endregion

        #region Insert, Update, Delete
        public bool Insert(Room ob)
        {
            try
            {
                EDMsDataContext.AddToRooms(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Room ob)
        {
            try
            {
                //Room _ob = GetByID(ob.Id);
                //if (_ob != null)
                //{
                //    EDMsDataContext.ApplyCurrentValues(_ob.EntityKey.EntitySetName, ob);
                //    EDMsDataContext.SaveChanges();
                //    return true;
                //}
                //else
                //    return false;

                Room _ob;

                _ob = (from room in EDMsDataContext.Rooms
                       where room.Id == ob.Id
                       select room).First();

                //_ob.Id = ob.Id;
                _ob.Name = ob.Name;
                _ob.Description = ob.Description;               
                _ob.LastUpdate = ob.LastUpdate;               
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Room ob)
        {
            try
            {
                Room _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Room _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
