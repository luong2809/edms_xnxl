﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoDistributionMatrixDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class AutoDistributionMatrixDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutoDistributionMatrixDAO"/> class.
        /// </summary>
        public AutoDistributionMatrixDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<AutoDistributionMatrix> GetIQueryable()
        {
            return this.EDMsDataContext.AutoDistributionMatrices;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AutoDistributionMatrix> GetAll()
        {
            return this.EDMsDataContext.AutoDistributionMatrices.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public AutoDistributionMatrix GetById(int id)
        {
            return this.EDMsDataContext.AutoDistributionMatrices.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(AutoDistributionMatrix ob)
        {
            try
            {
                this.EDMsDataContext.AddToAutoDistributionMatrices(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(AutoDistributionMatrix src)
        {
            try
            {
                AutoDistributionMatrix des = (from rs in this.EDMsDataContext.AutoDistributionMatrices
                                where rs.ID == src.ID
                                select rs).First();

                des.UserId = src.UserId;
                des.UserFullName = src.UserFullName;
                des.ProjectId = src.ProjectId;
                des.ProjectName = des.ProjectName;
                des.IsSendEmail = src.IsSendEmail;
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(AutoDistributionMatrix src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Gets all related permitted menu items.
        /// </summary>
        /// <param name="categoryId">
        /// The category Id.
        /// </param>
        /// <returns>
        /// </returns>
        public List<AutoDistributionMatrix> GetAllRelatedPermittedAutoDistributionMatrixItems(List<int> listFolId)
        {
            //Get all menu have permitted for current role.
            var categories = this.GetSpecificAutoDistributionMatrix(listFolId);

            if (categories != null)
            {

                //Gets and adds all parent menu items into the list.
                ////categories.AddRange(GetAllParentsAutoDistributionMatrixItemsByChildren(categories));
                return categories;
            }
            return new List<AutoDistributionMatrix>();
        }

        /// <summary>
        /// The get specific document.
        /// </summary>
        /// <param name="listDocId">
        /// The list doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AutoDistributionMatrix> GetSpecificAutoDistributionMatrix(List<int> listFolId)
        {
            return this.EDMsDataContext.AutoDistributionMatrices.ToArray().Where(t =>listFolId.Contains(t.ID)).ToList();
        }

        public List<AutoDistributionMatrix> GetSpecificAutoDistributionMatricestatic(List<int> listFolId)
        {
            return this.EDMsDataContext.AutoDistributionMatrices.ToArray().Where(t => listFolId.Contains(t.ID)).ToList();
        }

        public List<AutoDistributionMatrix> GetAllSpecificAutoDistributionMatrix(List<int> listFolId)
        {
            return this.EDMsDataContext.AutoDistributionMatrices.ToArray().Where(t => listFolId.Contains(t.ID)).ToList();
        }
    }
}
