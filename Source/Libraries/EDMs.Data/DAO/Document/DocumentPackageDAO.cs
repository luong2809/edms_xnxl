﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentPackageDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class DocumentPackageDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentPackageDAO"/> class.
        /// </summary>
        public DocumentPackageDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<DocumentPackage> GetIQueryable()
        {
            return this.EDMsDataContext.DocumentPackages;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentPackage> GetAll()
        {
            return this.EDMsDataContext.DocumentPackages.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public DocumentPackage GetById(int id)
        {
            return this.EDMsDataContext.DocumentPackages.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE

        public List<DocumentPackage> GetAllOfProject(int projectId, int type, bool isGetAll)
        {
            return this.EDMsDataContext.DocumentPackages.Where(t => !t.IsDelete.Value
                && (isGetAll || t.IsLeaf.Value)
                && t.ProjectId == projectId
                && t.TypeId == type).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(DocumentPackage ob)
        {
            try
            {
                this.EDMsDataContext.AddToDocumentPackages(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(DocumentPackage src)
        {
            try
            {
                DocumentPackage des = (from rs in this.EDMsDataContext.DocumentPackages
                                where rs.ID == src.ID
                                select rs).First();

                des.DocNo = src.DocNo;
                des.DocTitle = src.DocTitle;
                des.WorkgroupId = src.WorkgroupId;
                des.WorkgroupName = src.WorkgroupName;
                des.DeparmentId = src.DeparmentId;
                des.DeparmentName = src.DeparmentName;
                des.StartDate = src.StartDate;
                des.PlanedDate = src.PlanedDate;
                des.RevisionId = src.RevisionId;
                des.RevisionName = src.RevisionName;
                des.RevisionActualDate = src.RevisionActualDate;
                des.RevisionPlanedDate = src.RevisionPlanedDate;
                des.RevisionCommentCode = src.RevisionCommentCode;
                des.RevisionReceiveTransNo = src.RevisionReceiveTransNo;
                des.Complete = src.Complete;
                des.Weight = src.Weight;
                des.OutgoingTransNo = src.OutgoingTransNo;
                des.OutgoingTransDate = src.OutgoingTransDate;
                des.IncomingTransDate = src.IncomingTransDate;
                des.IncomingTransNo = src.IncomingTransNo;
                des.ICAReviewCode = src.ICAReviewCode;
                des.ICAReviewOutTransNo = src.ICAReviewOutTransNo;
                des.ICAReviewReceivedDate = src.ICAReviewReceivedDate;
                des.Notes = src.Notes;
                des.DocumentTypeId = src.DocumentTypeId;
                des.DocumentTypeName = src.DocumentTypeName;
                des.DisciplineId = src.DisciplineId;
                des.DisciplineName = src.DisciplineName;
                des.PackageId = src.PackageId;
                des.PackageName = src.PackageName;
                des.ProjectId = src.ProjectId;
                des.ProjectName = src.ProjectName;
                des.IsLeaf = src.IsLeaf;
                des.IsDelete = src.IsDelete;
                des.IsEMDR = src.IsEMDR;
                des.IndexNumber = src.IndexNumber;

                des.IsCriticalDoc = src.IsCriticalDoc;
                des.IsPriorityDoc = src.IsPriorityDoc;
                des.IsVendorDoc = src.IsVendorDoc;

                des.ReferenceFromID = src.ReferenceFromID;
                des.ReferenceFromName = src.ReferenceFromName;
                des.StatusID = src.StatusID;
                des.StatusName = src.StatusName;
                des.StatusEffectDate = src.StatusEffectDate;
                des.FinalCodeID = src.FinalCodeID;
                des.FinalCodeName = src.FinalCodeName;
                des.HasAttachFile = src.HasAttachFile;
                des.IsInWFProcess = src.IsInWFProcess;
                des.IsWFComplete = src.IsWFComplete;
                des.CurrentWorkflowName = src.CurrentWorkflowName;
                des.CurrentWorkflowStepName = src.CurrentWorkflowStepName;
                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;

                des.TypeId = src.TypeId;
                des.ReferenceDocId = src.ReferenceDocId;
                des.ReferenceDocNo = src.ReferenceDocNo;
                des.ReferenceDocTitle = src.ReferenceDocTitle;
                des.DocPath = src.DocPath;

                des.AnotherDocNo = src.AnotherDocNo;
                des.ActualDate = src.ActualDate;

                des.UpdatedAttachFile = src.UpdatedAttachFile;
                des.IncomingHardCopyTransDate = src.IncomingHardCopyTransDate;
                des.IncomingHardCopyTransNo = src.IncomingHardCopyTransNo;
                des.CommentCode = src.CommentCode;

                des.IsOpened = src.IsOpened;
                des.IsDistributed = src.IsDistributed;

                des.CurrenCodeName = src.CurrenCodeName;
                des.CurrentCodeID = src.CurrentCodeID;
                des.IssuePlanDate = src.IssuePlanDate;
                des.VendorPackagesId = src.VendorPackagesId;
                des.VendorPackagesName = src.VendorPackagesName;
                des.TransInFromVendorDate = src.TransInFromVendorDate;
                des.TransInFromVendorNo = src.TransInFromVendorNo;
                des.Organization = src.Organization;
                des.VendorResponseDate = src.VendorResponseDate;
                des.TransOutToOrganizationDate = src.TransOutToOrganizationDate;
                des.TransOutToOrganizationNo = src.TransOutToOrganizationNo;
                des.DeadlineReview1 = src.DeadlineReview1;
                des.TransInFromReviewNo1 = src.TransInFromReviewNo1;
                des.TransInFromReviewDate1 = src.TransInFromReviewDate1;
                des.StatusReview1 = src.StatusReview1;
                des.CodeReview1 = src.CodeReview1;
                des.DeadlineReview2 = src.DeadlineReview2;
                des.TransInFromReviewNo2 = src.TransInFromReviewNo2;
                des.TransInFromReviewDate2 = src.TransInFromReviewDate2;
                des.StatusReview2 = src.StatusReview2;
                des.CodeReview2 = src.CodeReview2;
                des.DeadlineReview3 = src.DeadlineReview3;
                des.TransInFromReviewNo3 = src.TransInFromReviewNo3;
                des.TransInFromReviewDate3 = src.TransInFromReviewDate3;
                des.StatusReview3 = src.StatusReview3;
                des.CodeReview3 = src.CodeReview3;

                des.AreaId = src.AreaId;
                des.AreaName = src.AreaName;
                des.VendorNameOfCutting = src.VendorNameOfCutting;

                des.DeadlineIssue = src.DeadlineIssue;
                des.DeadlineComment = src.DeadlineComment;
                des.DeadlineResponse = src.DeadlineResponse;
                des.EnddateResponse = src.EnddateResponse;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(DocumentPackage src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
