﻿using System;
using System.Collections.Generic;

namespace EDMs.Data.DAO
{
    public interface IRepository<T> : IDisposable where T : class
    {
        T GetByID(int Id);
        IEnumerable<T> GetAll();
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);
        void Save();
    }
}
