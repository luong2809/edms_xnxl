﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class AppointmentStatusDAO : BaseDAO
    {
        public AppointmentStatusDAO() : base() { }

        #region GET (Basic)
        public IQueryable<AppointmentStatus> GetIQueryable()
        {
            return EDMsDataContext.AppointmentStatuses;
        }
        
        public List<AppointmentStatus> GetAll()
        {
            return EDMsDataContext.AppointmentStatuses.ToList<AppointmentStatus>();
        }

        public AppointmentStatus GetByID(int ID)
        {
            return EDMsDataContext.AppointmentStatuses.Where(ob => ob.Id == ID).FirstOrDefault<AppointmentStatus>();
        }
       
        #endregion

        #region GET ADVANCE
        
        #endregion

        #region Insert, Update, Delete
        public bool Insert(AppointmentStatus ob)
        {
            try
            {
                EDMsDataContext.AddToAppointmentStatuses(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(AppointmentStatus ob)
        {
            try
            {
                //AppointmentStatus _ob = GetByID(ob.Id);
                //if (_ob != null)
                //{
                //    EDMsDataContext.ApplyCurrentValues(_ob.EntityKey.EntitySetName, ob);
                //    EDMsDataContext.SaveChanges();
                //    return true;
                //}
                //else
                //    return false;

                AppointmentStatus _ob;

                _ob = (from rs in EDMsDataContext.AppointmentStatuses
                       where rs.Id == ob.Id
                       select rs).First();

                //_ob.Id = ob.Id;
                _ob.Description = ob.Description;
                _ob.LastUpdate = ob.LastUpdate;
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(AppointmentStatus ob)
        {
            try
            {
                AppointmentStatus _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                AppointmentStatus _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
