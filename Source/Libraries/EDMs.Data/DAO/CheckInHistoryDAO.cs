﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class CheckInHistoryDAO : BaseDAO
    {
        public CheckInHistoryDAO() : base() { }

        #region GET (Basic)
        public IQueryable<CheckInHistory> GetIQueryable()
        {
            return EDMsDataContext.CheckInHistories;
        }
        
        public List<CheckInHistory> GetAll()
        {
            return EDMsDataContext.CheckInHistories.ToList<CheckInHistory>();
        }

        public CheckInHistory GetByID(int ID)
        {
            return EDMsDataContext.CheckInHistories.Where(ob => ob.Id == ID).FirstOrDefault<CheckInHistory>();
        }
       
        #endregion

        #region GET ADVANCE
        
        #endregion

        #region Insert, Update, Delete
        public bool Insert(CheckInHistory ob)
        {
            try
            {
                EDMsDataContext.AddToCheckInHistories(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(CheckInHistory ob)
        {
            try
            {
                //CheckInHistory _ob = GetByID(ob.Id);
                //if (_ob != null)
                //{
                //    EDMsDataContext.ApplyCurrentValues(_ob.EntityKey.EntitySetName, ob);
                //    EDMsDataContext.SaveChanges();
                //    return true;
                //}
                //else
                //    return false;

                CheckInHistory _ob;

                _ob = (from rs in EDMsDataContext.CheckInHistories
                       where rs.Id == ob.Id
                       select rs).First();

                //_ob.Id = ob.Id;
                _ob.AppointmentId = ob.AppointmentId;
                _ob.CheckInDate = ob.CheckInDate;
                _ob.CheckInTime = ob.CheckInTime;
                _ob.Description = ob.Description;
                _ob.EmergencyContactName = ob.EmergencyContactName;
                _ob.EmergencyContactPhone = ob.EmergencyContactPhone;
                _ob.IsClosed = ob.IsClosed;
                _ob.IsIn = ob.IsIn;
                _ob.ResourceId = ob.ResourceId;
                _ob.RoomId = ob.RoomId;
                _ob.ServiceTypeId = ob.ServiceTypeId;

                _ob.LastUpdate = ob.LastUpdate;
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(CheckInHistory ob)
        {
            try
            {
                CheckInHistory _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                CheckInHistory _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
