﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistributionMatrixDetailDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO.Workflow
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class DistributionMatrixDetailDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DistributionMatrixDetailDAO"/> class.
        /// </summary>
        public DistributionMatrixDetailDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Entities.DistributionMatrixDetail> GetIQueryable()
        {
            return this.EDMsDataContext.DistributionMatrixDetails;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Entities.DistributionMatrixDetail> GetAll()
        {
            return this.EDMsDataContext.DistributionMatrixDetails.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public Entities.DistributionMatrixDetail GetById(int id)
        {
            return this.EDMsDataContext.DistributionMatrixDetails.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(Entities.DistributionMatrixDetail ob)
        {
            try
            {
                this.EDMsDataContext.AddToDistributionMatrixDetails(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(Entities.DistributionMatrixDetail src)
        {
            try
            {
                Entities.DistributionMatrixDetail des = (from rs in this.EDMsDataContext.DistributionMatrixDetails
                                where rs.ID == src.ID
                                select rs).First();

                des.Name = src.Name;
                des.ProjectID = src.ProjectID;
                des.ProjectName = src.ProjectName;
                des.DisciplineID = src.DisciplineID;
                des.DisciplineName = src.DisciplineName;
                des.DocumentTypeID = src.DocumentTypeID;
                des.DocumentTypeName = src.DocumentTypeName;
                des.InformationOnlyRoleIds = src.InformationOnlyRoleIds;
                des.WorkAssignRoleIds = src.WorkAssignRoleIds;
                des.InformationOnlyUserIds = src.InformationOnlyUserIds;
                des.WorkAssignUserIds = src.WorkAssignUserIds;
                des.DistributionMatrixId = src.DistributionMatrixId;
                des.DistributionMatrixName = src.DistributionMatrixName;

                des.InformationOnlyRoleNames = src.InformationOnlyRoleNames;
                des.InformationOnlyUserNames = src.InformationOnlyUserNames;
                des.WorkAssignRoleNames = src.WorkAssignRoleNames;
                des.WorkAssignUserNames = src.WorkAssignUserNames;

                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(Entities.DistributionMatrixDetail src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
