﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DistributionMatrixDetailByUserDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO.Workflow
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class DistributionMatrixDetailByUserDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DistributionMatrixDetailByUserDAO"/> class.
        /// </summary>
        public DistributionMatrixDetailByUserDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Entities.DistributionMatrixDetailByUser> GetIQueryable()
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Entities.DistributionMatrixDetailByUser> GetAll()
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public Entities.DistributionMatrixDetailByUser GetById(int id)
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.FirstOrDefault(ob => ob.ID == id);
        }

        #endregion

        #region GET ADVANCE

        public List<DistributionMatrixDetailByUser> GetAllDMDetailByUser(int userid)
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.Where(t => t.UserId == userid).ToList();
        }

        public List<int> GetProjectIdListInPermission(int userId)
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.Where(t => t.UserId == userId).Select(t => t.ProjectId.Value).Distinct().ToList();
        }
        public List<DistributionMatrixDetailByUser> GetAllByDoc(int docId)
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.Where(t => t.DocId == docId).ToList();
        }

        public DistributionMatrixDetailByUser GetDocRoleMatrixByUser(int docId, int userid, int dmId)
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.FirstOrDefault(t => t.DocId == docId && t.UserId == userid && t.DMId == dmId);
        }

        public List<DistributionMatrixDetailByUser> GetAllDMDetailByUser(int userid, int projectId)
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.Where(t => t.UserId == userid && t.ProjectId == projectId).ToList();
        }

        public List<DistributionMatrixDetailByUser> GetAllByDM(int docId, int dmId)
        {
            return this.EDMsDataContext.DistributionMatrixDetailByUsers.Where(t => t.DocId == docId && t.DMId == dmId).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(Entities.DistributionMatrixDetailByUser ob)
        {
            try
            {
                this.EDMsDataContext.AddToDistributionMatrixDetailByUsers(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(Entities.DistributionMatrixDetailByUser src)
        {
            try
            {
                Entities.DistributionMatrixDetailByUser des = (from rs in this.EDMsDataContext.DistributionMatrixDetailByUsers
                                where rs.ID == src.ID
                                select rs).First();

                des.DMId = src.DMId;
                des.DMName = src.DMName;
                des.ProjectId = src.ProjectId;
                des.ProjectName = src.ProjectName;
                des.DocId = src.DocId;
                des.DocNo = src.DocNo;
                des.DocTitle = src.DocTitle;
                des.RevisionId = src.RevisionId;
                des.RevisionName = src.RevisionName;
                des.RoleId = src.RoleId;
                des.RoleName = src.RoleName;
                des.UserId = src.UserId;
                des.UserName = src.UserName;
                des.CreatedBy = src.CreatedBy;
                des.CreatedByName = src.CreatedByName;
                des.CreatedDate = src.CreatedDate;
                des.IsOpened = src.IsOpened;
                des.IsDistributed = src.IsDistributed;
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(Entities.DistributionMatrixDetailByUser src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
