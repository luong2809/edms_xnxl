﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentAssignedWorkflowDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO.Workflow
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class DocumentAssignedWorkflowDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentAssignedWorkflowDAO"/> class.
        /// </summary>
        public DocumentAssignedWorkflowDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<DocumentAssignedWorkflow> GetIQueryable()
        {
            return EDMsDataContext.DocumentAssignedWorkflows;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentAssignedWorkflow> GetAll()
        {
            return EDMsDataContext.DocumentAssignedWorkflows.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public DocumentAssignedWorkflow GetById(int id)
        {
            return EDMsDataContext.DocumentAssignedWorkflows.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(DocumentAssignedWorkflow ob)
        {
            try
            {
                EDMsDataContext.AddToDocumentAssignedWorkflows(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(DocumentAssignedWorkflow src)
        {
            try
            {
                DocumentAssignedWorkflow des = (from rs in EDMsDataContext.DocumentAssignedWorkflows
                                where rs.ID == src.ID
                                select rs).First();

                des.DocumentID = src.DocumentID;
                des.WorkflowID = src.WorkflowID;
                des.CurrentWorkflowStepID = src.CurrentWorkflowStepID;
                des.NextWorkflowStepID = src.NextWorkflowStepID;
                des.RejectWorkflowStepID = src.RejectWorkflowStepID;
                des.IsComplete = src.IsComplete;
                des.IsReject = src.IsReject;
                des.IsLeaf = src.IsLeaf;
                des.AssignedBy = src.AssignedBy;
                des.WorkflowName = src.WorkflowName;
                des.CurrentWorkflowStepName = src.CurrentWorkflowStepName;
                des.NextWorkflowStepName = src.NextWorkflowStepName;
                des.RejectWorkflowStepName = src.RejectWorkflowStepName;
                des.CanReject = src.CanReject;
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(DocumentAssignedWorkflow src)
        {
            try
            {
                var des = GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
