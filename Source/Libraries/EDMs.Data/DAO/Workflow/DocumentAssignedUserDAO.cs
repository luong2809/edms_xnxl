﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentAssignedUserDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO.Workflow
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class DocumentAssignedUserDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentAssignedUserDAO"/> class.
        /// </summary>
        public DocumentAssignedUserDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Entities.DocumentAssignedUser> GetIQueryable()
        {
            return this.EDMsDataContext.DocumentAssignedUsers;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Entities.DocumentAssignedUser> GetAll()
        {
            return this.EDMsDataContext.DocumentAssignedUsers.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public Entities.DocumentAssignedUser GetById(int id)
        {
            return this.EDMsDataContext.DocumentAssignedUsers.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(Entities.DocumentAssignedUser ob)
        {
            try
            {
                this.EDMsDataContext.AddToDocumentAssignedUsers(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(Entities.DocumentAssignedUser src)
        {
            try
            {
                Entities.DocumentAssignedUser des = (from rs in this.EDMsDataContext.DocumentAssignedUsers
                                where rs.ID == src.ID
                                select rs).First();

                des.DocumentAssignedWorkflowID = src.DocumentAssignedWorkflowID;
                des.DocumentID = src.DocumentID;
                des.UserID = src.UserID;
                des.ReceivedDate = src.ReceivedDate;
                des.PlanCompleteDate = src.PlanCompleteDate;
                des.IsOverDue = src.IsOverDue;
                des.IsComplete = src.IsComplete;
                des.AssignedBy = src.AssignedBy;
                des.CurrentWorkflowStepName = src.CurrentWorkflowStepName;
                des.CurrentWorkflowStepId = src.CurrentWorkflowStepId;
                des.IsReject = src.IsReject;
                des.ActualCompleteDate = src.ActualCompleteDate;
                des.UserName = src.UserName;
                des.AssignedByName = src.AssignedByName;
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(Entities.DocumentAssignedUser src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
