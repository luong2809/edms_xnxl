﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentNew.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the DocumentNew type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace EDMs.Data.Entities
{
    /// <summary>
    /// The document new.
    /// </summary>
    public partial class DocumentPackage
    {

        //public bool CanReject { get; set; }
        //public bool CanConsolidate { get; set; }
        //public bool IsInStep { get; set; }
        //public int DocumentAssignUserId { get; set; }

        //public DateTime? CurrentStatusPlanDate { get; set; }
        //public DateTime? CurrentStatusActualDate { get; set; }
        //public string CurrentStatusCommentCode { get; set; }
    }
}
