﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Security;

    public partial class Consulting //Patient_ConsultingDAO class, Patient.Consulting SQL table
    {
        public string UserFullName
        {
            get
            {
                try
                {
                    UserDAO dao = new UserDAO();
                    var u = dao.GetByID(this.UserID.Value);
                    return u != null ? u.Resource.FullName : "";
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public List<ServiceType> ListOfServiceType
        {
            get
            {
                if (String.IsNullOrEmpty(ServiceTypeID))
                    return null;
                else
                {
                    var listID = ServiceTypeID.Split(',');
                    ServiceTypeDAO _serviceTypeDAO = new ServiceTypeDAO();
                    var listConsultingHistory = _serviceTypeDAO.GetAll();

                    ////Old version
                    //var result = from p in listConsultingHistory
                    //         where listID.Any(val => p.Id.ToString().Equals(val))
                    //         select p;

                    ////New version
                    var result = listConsultingHistory.Where(c => listID.Any(id => c.Id.ToString().Equals(id)));

                    return result.ToList();
                }
            }
        }
        public String ListOfServiceTypeView
        {
            get
            {
                if (String.IsNullOrEmpty(ServiceTypeID))
                    return string.Empty;
                else
                {
                    StringBuilder services = new StringBuilder();
                    for (int i = 0; i < ListOfServiceType.Count; i++)
                    {
                        if (i == 0)
                            services.Append(" - " + ListOfServiceType[i].Name);
                        else
                            services.Append("<br/>" + " - " + ListOfServiceType[i].Name);
                    }
                    return services.ToString();
                }
            }
        }
    }
}
