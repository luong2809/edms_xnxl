﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Library;
    using EDMs.Data.DAO.Security;

    public partial class User
    {
        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <value>
        /// The role.
        /// </value>
        public Role Role
        {
            get
            {
                var dao = new RoleDAO();
                return RoleId != null ? dao.GetByID(RoleId.Value) : null;
            }
        }

        /// <summary>
        /// Gets the resource.
        /// </summary>
        /// <value>
        /// The resource.
        /// </value>
        public Resource Resource
        {
            get
            {
                var dao = new ResourceDAO();
                return ResourceId != null ? dao.GetByID(ResourceId.Value) : null;
            }
        }

        public string UserNameWithFullName
        {
            get
            {
                return  !string.IsNullOrEmpty(this.FullName) ? this.Username + "/" + this.FullName : this.Username;
            }
        }
        public string FullNameWithRole
        {
            get
            {
                var role = string.Empty;
                if (this.IsDC.GetValueOrDefault())
                {
                    role = "(DC) - ";
                }
                else if (this.IsManager.GetValueOrDefault())
                {
                    role = "(Manager) - ";
                }
                else if (this.IsLeader.GetValueOrDefault())
                {
                    role = "(Leader) - ";
                }
                else if (this.IsEngineer.GetValueOrDefault())
                {
                    role = "(Engineer) - ";
                }
                else if (this.IsAdmin.GetValueOrDefault())
                {
                    role = "(System Admin) - ";
                }
                return  role + (!string.IsNullOrEmpty(this.FullName) ? this.Username + "/" + this.FullName : this.Username);
            }
        }

        public string FullNameWithRoleDepartment
        {
            get
            {
                var role = string.Empty;
                if (this.IsDC.GetValueOrDefault())
                {
                    role = "("+ this.Role.Name +" - DC) - ";
                }
                else if (this.IsManager.GetValueOrDefault())
                {
                    role = "(" + this.Role.Name + " - Manager) - ";
                }
                else if (this.IsLeader.GetValueOrDefault())
                {
                    role = "(" + this.Role.Name + " - Leader) - ";
                }
                else if (this.IsEngineer.GetValueOrDefault())
                {
                    role = "(" + this.Role.Name + " - Engineer) - ";
                }
                else if (this.IsAdmin.GetValueOrDefault())
                {
                    role = "(" + this.Role.Name + " - System Admin) - ";
                }
                return role + (!string.IsNullOrEmpty(this.FullName) ? this.Username + "/" + this.FullName : this.Username);
            }
        }
    }
}
