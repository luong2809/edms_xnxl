﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Security;

    public partial class MemberCard //Patient_MemberCardDAO class, Patient.MemberCard SQL table
    {
        public string UserFullName
        {
            get
            {
                try
                {
                    UserDAO dao = new UserDAO();
                    var u = dao.GetByID(this.UserID.Value);
                    return u != null ? u.Resource.FullName : "";
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }
    }
}
