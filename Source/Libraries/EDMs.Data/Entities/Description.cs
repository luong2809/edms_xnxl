﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Security;

    public partial class Description //Patient_DescriptionDAO class, Patient.Description SQL table
    {
        public string UserFullName
        {
            get
            {
                try
                {
                    UserDAO dao = new UserDAO();
                    var u = dao.GetByID(this.UserID.Value);
                    return u != null ? u.Resource.FullName : "";
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }
    }
}
