﻿
namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Library;

    public partial class Code

    {
        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Description) ?
                    this.Name + " - " + this.Description :
                    this.Name;
            }
        }
    }
}
