﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Appointment.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Appointment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.Entities
{
    using DAO;

    using EDMs.Data.DAO.Library;
    using EDMs.Data.DAO.Security;

    /// <summary>
    /// The appointment.
    /// </summary>
    public partial class Appointment
    {
        /// <summary>
        /// Gets ResourceGroup.
        /// </summary>
        public Resource Resource
        {
            get
            {
                var userDao = new UserDAO();
                var resourceDao = new ResourceDAO();
                
                return resourceDao.GetByID(userDao.GetByID(this.CreatedBy.GetValueOrDefault()).ResourceId.GetValueOrDefault());
            }
        }
    }
}