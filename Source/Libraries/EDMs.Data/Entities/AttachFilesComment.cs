﻿
using EDMs.Data.DAO.Security;
using System;

namespace EDMs.Data.Entities
{

    /// <summary>
    /// The package.
    /// </summary>
    public partial class AttachFilesComment
    {
        /// <summary>
        /// Gets the project obj.
        /// </summary>
        public string CreatedByUser
        {
            get
            {
                return "1";
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}
