﻿using EDMs.Data.DAO.Scope;

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Document;

    public partial class Discipline
    {
        public Category Category
        {
            get
            {
                var dao = new CategoryDAO();
                return dao.GetById(this.CategoryId.GetValueOrDefault());
            }
        }

        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Description)
                        ? this.Name + " - " + this.Description
                        : this.Name;
            }
        }
    }
}
