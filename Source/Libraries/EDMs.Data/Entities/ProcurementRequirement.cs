﻿
namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Scope;

    /// <summary>
    /// The package.
    /// </summary>
    public partial class ProcurementRequirement
    {
        public string BiddingProcessTimePlan
        {
            get
            {
                var from = BiddingProcessTimePlanFrom != null
                    ? BiddingProcessTimePlanFrom.Value.ToString("MM/yyyy")
                    : string.Empty;

                var to = BiddingProcessTimePlanTo != null
                    ? " - " + BiddingProcessTimePlanTo.Value.ToString("MM/yyyy")
                    : string.Empty;

                return from + to;
            }
        }

        public string BiddingProcessTimeActual
        {
            get
            {
                var from = BiddingProcessTimeActualFrom != null
                    ? BiddingProcessTimeActualFrom.Value.ToString("MM/yyyy")
                    : string.Empty;

                var to = BiddingProcessTimeActualTo != null
                    ? " - " + BiddingProcessTimeActualTo.Value.ToString("MM/yyyy")
                    : string.Empty;

                return from + to;
            }
        }
    }
}
