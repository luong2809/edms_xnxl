﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Library;
    using EDMs.Data.DAO.Security;

    public partial class UsedService  //Patient_UsedServiceDAO class, Patient.UsedService SQL table
    {
        public  ServiceType ServiceType
        {
            get
            {
                ServiceTypeDAO dao = new ServiceTypeDAO();
                return dao.GetByID(ServiceTypeID.Value);
            }
        }

        public string UserFullName
        {
            get
            {
                try
                {
                    UserDAO dao = new UserDAO();
                    var u = dao.GetByID(this.UserID.Value);
                    return u != null ? u.Resource.FullName : "";
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ResourceFullName
        {
            get
            {
                if (this.ResourceID == null) return "";

                ResourceDAO dao = new ResourceDAO();
                var u = dao.GetByID(this.ResourceID.Value);
                return u != null ? u.FullName : "";
            }
        }
    }
}
