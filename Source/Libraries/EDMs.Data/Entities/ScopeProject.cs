﻿
namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO.Scope;

    /// <summary>
    /// The package.
    /// </summary>
    public partial class ScopeProject
    {
        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Description) 
                        ? this.Name + " - " + this.Description 
                        : this.Name;
            }
        }
    }
}
