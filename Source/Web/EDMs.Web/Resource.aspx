﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Resource.aspx.cs" Inherits="EDMs.Web._Resource" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Controls/Resources/List.ascx" TagPrefix="uc1" TagName="List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdKhachHangPanel
        {
            height: 100% !important;
        }
    </style>
    <telerik:RadTreeView ID="treeReourceType" runat="server" OnNodeClick="RadTreeView1_NodeClick">
        <Nodes>
            <telerik:RadTreeNode Text="Bác Sĩ" Expanded="true" Value="0">
                <Nodes>
                </Nodes>
            </telerik:RadTreeNode>
        </Nodes>
    </telerik:RadTreeView>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">
            <telerik:RadToolBar ID="ResourceMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <telerik:RadToolBarButton runat="server" Text="Thêm mới" ImageUrl="~/Images/addNew.png">
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true">
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton runat="server">
                        <ItemTemplate>
                            <asp:Label ID="lblSearchLabel" runat="server" Text="  Tìm kiếm:  " />
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton Value="searchTextBoxButton" CssClass="searchtextbox" CommandName="searchText">
                        <ItemTemplate>
                            <telerik:RadTextBox
                                runat="server" ID="txtSearch"
                                EmptyMessage="Nội dung cần tìm" />
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton ImageUrl="~/Images/search.gif" Value="search" CommandName="doSearch" />
                </Items>
            </telerik:RadToolBar>
        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadGrid ID="grdKhachHang" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" CssClass="abcabc" GridLines="None" Height="100%" OnDeleteCommand="grdKhachHang_DeleteCommand" OnItemCreated="grdKhachHang_ItemCreated" OnNeedDataSource="grdKhachHang_OnNeedDataSource" PageSize="30" Style="outline: none">
                <MasterTableView ClientDataKeyNames="Id" DataKeyNames="Id" Width="100%">
                    <NoRecordsTemplate>
                        Chưa có dữ liệu.
                    </NoRecordsTemplate>
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="Trang đầu" LastPageToolTip="Trang cuối" NextPagesToolTip="Trang sau" NextPageToolTip="Trang sau" PagerTextFormat="Đổi trang: {4} &amp;nbsp;Trang &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Tổng:  &lt;strong&gt;{5}&lt;/strong&gt; bác sĩ." PageSizeLabelText="Số dòng trên mỗi trang: " PrevPagesToolTip="Trang trước" PrevPageToolTip="Trang trước" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="STT">
                            <HeaderStyle HorizontalAlign="Center" Width="40" />
                            <ItemStyle Font-Bold="True" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblSTT" runat="server" align="center" Enabled="True" Text="<%# grdKhachHang.CurrentPageIndex*grdKhachHang.PageSize + grdKhachHang.Items.Count+1 %>" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Id" HeaderText="Mã" UniqueName="Id" Visible="False" />
                        <telerik:GridBoundColumn DataField="SSN" HeaderText="Mã" UniqueName="SSN">
                            <HeaderStyle HorizontalAlign="Center" Width="60" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FullName" HeaderText="Tên KH" UniqueName="FullName" />
                        <telerik:GridBoundColumn DataField="Sex" HeaderText="Giới tính" UniqueName="Gender">
                            <HeaderStyle HorizontalAlign="Center" Width="40" />
                            <ItemStyle  HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CellPhone" HeaderText="Phone" UniqueName="Phone">
                            <HeaderStyle HorizontalAlign="Center" Width="100" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Address1" HeaderText="Địa chỉ" UniqueName="Address" />
                        <telerik:GridBoundColumn DataField="Email" HeaderText="Email" UniqueName="Email" />
                        <telerik:GridBoundColumn DataField="ResourceGroup.Name" HeaderText="Nhóm" UniqueName="ResourceGroup" >
                             <HeaderStyle HorizontalAlign="Center" Width="150" />
                            <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="DateOfBirth" HeaderText="Ngày sinh" UniqueName="Birthday">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblFileDinhKem" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "DateOfBirth")).ToString("dd/MM/yyyy")  %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn>
                            <HeaderStyle Width="36" />
                            <ItemTemplate>
                                <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="padding-left: 7px; cursor: pointer;" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn CommandName="Delete" Text="Xóa" ConfirmText="Bạn có muốn xóa bác sĩ này?">
                            <HeaderStyle Width="36px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <%--<CommandItemStyle Height="25px" />--%>
                    <%-- <CommandItemTemplate>
                        <a href="#" onclick="return ShowInsertForm();" style="padding-left: 7px">
                            <img src="././Images/addNew.png" />
                            Thêm mới bác sĩ</a>
                    </CommandItemTemplate>--%>
                </MasterTableView>
                <ClientSettings>
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="RowDblClick" />
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxResource" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxResource">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdKhachHang"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdKhachHang">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdKhachHang"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="treeResourceType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ResourceList" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="ResourceDialog" runat="server" Title="Thông tin bác sĩ" Height="500px" VisibleStatusbar="False"
                Width="630px" MaxHeight="500px" MaxWidth="630px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
                Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ResourceImportDialog" runat="server" Title="Import bác sĩ" Height="250px" VisibleStatusbar="False"
                Width="630px" MaxHeight="250px" MaxWidth="630px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
                Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />


    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                toolbar = $find("<%= ResourceMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxResource.ClientID %>");

                searchButton = toolbar.findButtonByCommandName("doSearch");

                $telerik.$(".searchtextbox")
							.bind("keypress", function (e) {
							    searchButton.set_imageUrl("images/search.gif");
							    searchButton.set_value("search");
							});
            }

            function ShowEditForm(id, rowIndex) {
                var grid = $find("<%= grdKhachHang.ClientID %>");

                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                grid.get_masterTableView().selectItem(rowControl, true);

                window.radopen("Controls/Resources/ResourceEditForm.aspx?resourceId=" + id, "ResourceDialog");
                return false;
            }
            function ShowInsertForm() {
                window.radopen("Controls/Resources/ResourceEditForm.aspx", "ResourceDialog");
                return false;
            }

            function ShowImportForm() {
                window.radopen("Controls/Resources/ResourceImportForm.aspx", "ResourceImportDialog");
                return false;
            }

            function refreshGrid(arg) {
                //alert(arg);
                if (!arg) {
                    ajaxManager.ajaxRequest("Rebind");
                }
                else {
                    ajaxManager.ajaxRequest("RebindAndNavigate");
                }
            }
            function RowDblClick(sender, eventArgs) {
                window.radopen("Controls/Resources/ResourceEditForm.aspx?resourceId=" + eventArgs.getDataKeyValue("Id"), "ResourceDialog");
            }
            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdKhachHang.ClientID %>");
                var customerId = null;
                var customerName = "";

                if (grid.get_masterTableView().get_selectedItems().length > 0) {
                    var selectedRow = grid.get_masterTableView().get_selectedItems()[0];
                    customerId = selectedRow.getDataKeyValue("Id");
                    //customerName = selectedRow.Items["FullName"]; 
                    customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML;
                }

                if (strText == "Thêm mới") {
                    return ShowInsertForm();
                }
                else if (strText == "Import dữ liệu") {
                    return ShowImportForm();
                }
                else {
                    var commandName = args.get_item().get_commandName();
                    if (commandName == "doSearch") {
                        var searchTextBox = sender.findButtonByCommandName("searchText").findControl("txtSearch");
                        if (searchButton.get_value() == "clear") {
                            searchTextBox.set_value("");
                            searchButton.set_imageUrl("images/search.gif");
                            searchButton.set_value("search");
                        }

                        performSearch(searchTextBox);
                    } else if (commandName == "reply") {
                        window.radopen(null, "Edit");
                    }
                }
            }
            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>

<%--Tan.Le Remove here--%>
<%--<uc1:List runat="server" ID="ResourceList"/>--%>
<%-- <div id="EDMsResources" runat="server" />--%>
