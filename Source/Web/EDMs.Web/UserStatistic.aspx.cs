﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Data.SqlClient;
using Aspose.Cells;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;
using Telerik.Web.UI.GridExcelBuilder;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.UI;
    using System.Drawing;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class UserStatistic : Page
    {
       
        private readonly UserService userService = new UserService();

        private readonly RoleService roleService = new RoleService();


        private readonly UserAccessSystemService useraccess = new UserAccessSystemService();
        private readonly CountUserAccessService countusers = new CountUserAccessService();
       // private List<int> ListUserId = new List<int>();
        
       
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {

                    
                    var monday = DateTime.Today;
                    int valueday = monday.DayOfWeek - DayOfWeek.Monday;
                    monday = monday.AddDays(-valueday);
                    var sunday = monday.AddDays(6);
                    this.txtChiefFromDate.SelectedDate = monday;
                    this.txtChiefToDate.SelectedDate = sunday;
                    ReloadDate();
                    LoadProcessReport();
                    // if(UserSession.Current.IsCheif || UserSession.Current.IsLeader)
                    //{
                   
                    //}    
            }
        }

        protected void LoadDocument()
        {
                  
            var doclist=new List<UsersAccessSystem>();

            doclist = this.useraccess.GetAllOfDate(DateTime.Now.Date);

            this.grdDocument.DataSource = doclist;
          
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
            }
        }

        protected void View_Click(object sender, EventArgs e)
        {
            if (this.txtChiefFromDate.SelectedDate != null && this.txtChiefToDate.SelectedDate != null)
            {

                LoadProcessReport();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Alert", 
                    "<script> alert('Please selected Date.')", false);
            }
           
        }

        protected void grdDocument_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocument();
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            
        }

        private void ReloadDate()
        {
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();

            this.txtuseronline.Text = Application.Get("userLoginCount").ToString();

             this.txtTotaluseronline.Text= this.countusers.GetAll().Sum(t => t.Number).ToString();
           

            this.txtuseronlineinday.Text= this.countusers.GetDate(DateTime.Now.Date).Number.ToString();

        }
        private void LoadProcessReport()
        {
            ColumnSeries columnSeries = this.ColumnsChart.PlotArea.Series[0] as ColumnSeries;

            var processReportList = new List<ProcessReport>();
            var listDays = this.countusers.GetAllFromOfDate(this.txtChiefFromDate.SelectedDate.Value, this.txtChiefToDate.SelectedDate.Value);
            for (int i = 0; i < listDays.Count; i++)
            {
                var processReport = new ProcessReport();
                processReport.Date = listDays[i].DateAccess.GetValueOrDefault().ToString("dd/MM/yyyy");
                processReport.Number = listDays[i].Number.GetValueOrDefault();

                processReportList.Add(processReport);
               // temp.Add(processReport);

                ColumnsChart.PlotArea.XAxis.DataLabelsField = "Date";
                ColumnsChart.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Auto;
                ColumnsChart.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Auto;
                ColumnsChart.PlotArea.XAxis.LabelsAppearance.DataFormatString = "{0}";
                ColumnsChart.PlotArea.XAxis.LabelsAppearance.RotationAngle = 0;
                ColumnsChart.PlotArea.XAxis.LabelsAppearance.Step = 1;
                ColumnsChart.PlotArea.XAxis.LabelsAppearance.Skip = 0;
                ColumnsChart.PlotArea.XAxis.TitleAppearance.Text = "Day";
            }

            this.ColumnsChart.ChartTitle.Text = "DETAIL Users Access System  Cut-off: " +
                                                DateTime.Now.ToString("dd/MM/yyyy");

            this.ColumnsChart.DataSource = processReportList;
            this.ColumnsChart.DataBind();
        }
        protected void Timer_Tick(object sender, EventArgs e)
        {
            ReloadDate();
            LoadProcessReport();
        }
    }
}