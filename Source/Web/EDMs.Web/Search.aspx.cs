﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using Aspose.Cells;
using EDMs.Business.Services.Scope;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using Telerik.Web.Zip;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class Search : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly DocPropertiesViewService docPropertiesViewService;

        private readonly  OptionalTypeDetailService optionalTypeDetailService;

        private readonly OriginatorService originatorService;

        private readonly DocumentNewService documentNewService;

        private readonly AttachFileService attachFileService;

        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly DocumentPackageService documentPackageService;
        private readonly ScopeProjectService scopeProjectService;

        private readonly RoleService roleService;


        public Search()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.documentService = new DocumentService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.categoryService = new CategoryService();
            this.docPropertiesViewService = new DocPropertiesViewService();
            this.optionalTypeDetailService = new OptionalTypeDetailService();
            this.originatorService = new OriginatorService();
            this.documentNewService = new DocumentNewService();
            this.attachFileService = new AttachFileService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.roleService = new RoleService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.DefaultButton = this.btnSearch.UniqueID;
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            var temp = (RadPane)this.Master.FindControl("leftPane");
            temp.Collapsed = true;
            if (!Page.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["text"]))
                {
                    this.txtSearchAll.Text = this.Request.QueryString["text"];
                }
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                ////grdDocument.MasterTableView.CurrentPageIndex = grdDocument.MasterTableView.PageCount - 1;
                grdDocument.Rebind();
            }
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.ddlProject.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var docTypeId = Convert.ToInt32(this.ddlDocumentType.SelectedValue);
                var disciplineId = Convert.ToInt32(this.ddlDiscipline.SelectedValue);
                var departmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue);
                var searchAll = this.txtSearchAll.Text.Trim();
                var docNo = this.txtDocNo.Text.Trim();
                var docTitle = this.txtTitle.Text.Trim();

                this.grdDocument.DataSource = this.documentPackageService.SearchEMDR(searchAll, docNo, docTitle, projectId,
                    docTypeId, disciplineId, departmentId);
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            var listRelateDoc = this.documentService.GetAllRelateDocument(docId);
            if (listRelateDoc != null)
            {
                foreach (var objDoc in listRelateDoc)
                {
                    objDoc.IsDelete = true;
                    objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                    objDoc.LastUpdatedDate = DateTime.Now;
                    this.documentService.Update(objDoc);
                }
            }


        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectList = scopeProjectService.GetAll();
            this.ddlProject.DataSource = projectList;
            this.ddlProject.DataTextField = "FullName";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            var departmentList = this.roleService.GetAll(false);
            departmentList.Insert(0, new Role() {Id = 0, Name = "All"});
            this.ddlDepartment.DataSource = departmentList;
            this.ddlDepartment.DataTextField = "FullName";
            this.ddlDepartment.DataValueField = "ID";
            this.ddlDepartment.DataBind();

            if (ddlProject.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var docTypeList = this.documentTypeService.GetAllByProject(projectId);
                var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId);

                docTypeList.Insert(0, new DocumentType() { ID = 0, Name = "All" });
                disciplineList.Insert(0, new Discipline() { ID = 0, Name = "All" });

                

                this.ddlDiscipline.DataSource = disciplineList;
                this.ddlDiscipline.DataTextField = "FullName";
                this.ddlDiscipline.DataValueField = "ID";
                this.ddlDiscipline.DataBind();

                this.ddlDocumentType.DataSource = docTypeList;
                this.ddlDocumentType.DataTextField = "FullName";
                this.ddlDocumentType.DataValueField = "ID";
                this.ddlDocumentType.DataBind();
            }

            
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var ddlStatus = item.FindControl("ddlStatus") as RadComboBox;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;
                var ddlReceivedFrom = item.FindControl("ddlReceivedFrom") as RadComboBox;
                var txtReceivedDate = item.FindControl("txtReceivedDate") as RadDatePicker;
                var txtName = item.FindControl("txtName") as TextBox;
                var txtDocumentNumber = item.FindControl("txtDocumentNumber") as TextBox;
                var txtTitle = item.FindControl("txtTitle") as TextBox;
                var txtRemark = item.FindControl("txtRemark") as TextBox;
                var txtWell = item.FindControl("txtWell") as TextBox;

                var txtTransmittalNumber = item.FindControl("txtTransmittalNumber") as TextBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentService.GetById(docId);
                var oldName = objDoc.Name;
                var currentRevision = objDoc.RevisionID;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                var watcherService = new ServiceController("EDMSFolderWatcher");
                if (currentRevision != 0 && currentRevision != null && currentRevision != newRevision)
                {
                    var newObjDoc = new Document
                    {
                        DocIndex = objDoc.DocIndex + 1,
                        ParentID = objDoc.ParentID,
                        FolderID = objDoc.FolderID,
                        DirName = objDoc.DirName,
                        FilePath = objDoc.FilePath,
                        RevisionFilePath = objDoc.RevisionFilePath,
                        FileExtension = objDoc.FileExtension,
                        FileExtensionIcon = objDoc.FileExtensionIcon,
                        FileNameOriginal = objDoc.FileNameOriginal,
                        LanguageID = objDoc.LanguageID,

                        KeyWords = objDoc.KeyWords,
                        IsDelete = false,
                        CreatedDate = DateTime.Now,
                        CreatedBy = UserSession.Current.User.Id,

                        Name = txtName.Text.Trim(),
                        DocumentNumber = txtDocumentNumber.Text.Trim(),
                        Title = txtTitle.Text.Trim(),
                        RevisionID = Convert.ToInt32(ddlRevision.SelectedValue),
                        RevisionName = ddlRevision.Text,
                        StatusID = Convert.ToInt32(ddlStatus.SelectedValue),
                        DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue),
                        DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue),
                        ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue),
                        ReceivedDate = txtReceivedDate.SelectedDate,
                        Remark = txtRemark.Text.Trim(),
                        Well = txtWell.Text.Trim(),

                        TransmittalNumber = txtTransmittalNumber.Text.Trim()
                    }; ////Utilities.Utility.Clone(objDoc);

                    if (!string.IsNullOrEmpty(newObjDoc.RevisionName))
                    {
                        newObjDoc.RevisionFileName = newObjDoc.RevisionName + "_" + newObjDoc.Name;
                    }
                    else
                    {
                        newObjDoc.RevisionFileName = newObjDoc.Name;
                    }


                    newObjDoc.RevisionFilePath = newObjDoc.RevisionFilePath.Substring(
                        0, newObjDoc.RevisionFilePath.LastIndexOf('/')) + "/" + DateTime.Now.ToString("ddMMyyhhmmss") + "_" + newObjDoc.RevisionFileName;

                    // Allway check revision file is exist when update
                    var filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var revisionFilePath = Server.MapPath(newObjDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                    {
                        File.Copy(filePath, revisionFilePath, true);
                    }
                    // End check

                    if (currentRevision < newRevision)
                    {
                        objDoc.IsLeaf = false;
                        newObjDoc.IsLeaf = true;
                        if (objDoc.ParentID == null)
                        {
                            newObjDoc.ParentID = objDoc.ID;
                        }

                        this.documentService.Update(objDoc);
                    }
                    else
                    {
                        newObjDoc.IsLeaf = false;

                        filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(revisionFilePath))
                        {
                            File.Copy(revisionFilePath, filePath, true);
                        }

                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }
                    }

                    this.documentService.Insert(newObjDoc);

                    if (objDoc.Name != txtName.Text.Trim())
                    {
                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(filePath))
                        {
                            File.Move(filePath, filePath.Replace(oldName, txtName.Text.Trim()));
                        }

                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }

                        var listDocRename = this.documentService.GetSpecificDocument(oldName, objDoc.DirName);
                        foreach (var document in listDocRename)
                        {
                            document.Name = txtName.Text.Trim();
                            document.FileNameOriginal = txtName.Text.Trim();
                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + txtName.Text.Trim();
                            }
                            else
                            {
                                document.RevisionFileName = txtName.Text.Trim();
                            }

                            document.FilePath = document.FilePath.Replace(oldName, txtName.Text.Trim());

                            this.documentService.Update(document);
                        }
                    }
                }
                else
                {
                    // Allway check revision file is exist when update
                    var filePath = Server.MapPath(objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                    {
                        File.Copy(filePath, revisionFilePath, true);
                    }
                    // End check

                    if (objDoc.Name != txtName.Text.Trim())
                    {
                        if (watcherService.Status == ServiceControllerStatus.Running)
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(filePath))
                        {
                            File.Move(filePath, filePath.Replace(oldName, txtName.Text.Trim()));
                        }

                        if (watcherService.Status == ServiceControllerStatus.Running)
                        {
                            watcherService.ExecuteCommand(129);
                        }

                        var listDocRename = this.documentService.GetSpecificDocument(oldName, objDoc.DirName);
                        foreach (var document in listDocRename)
                        {
                            document.Name = txtName.Text.Trim();
                            document.FileNameOriginal = txtName.Text.Trim();
                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + txtName.Text.Trim();
                            }
                            else
                            {
                                document.RevisionFileName = txtName.Text.Trim();
                            }

                            document.FilePath = document.FilePath.Replace(oldName, txtName.Text.Trim());

                            if (document.ID == objDoc.ID)
                            {
                                document.DocumentNumber = txtDocumentNumber.Text;
                                document.Title = txtTitle.Text;
                                document.RevisionID = Convert.ToInt32(ddlRevision.SelectedValue);
                                document.RevisionName = ddlRevision.SelectedItem.Text;
                                document.DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue);
                                document.StatusID = Convert.ToInt32(ddlStatus.SelectedValue);
                                document.DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue);
                                document.ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue);
                                document.ReceivedDate = txtReceivedDate.SelectedDate;
                                document.Remark = txtRemark.Text.Trim();
                                document.Well = txtWell.Text.Trim();
                                document.TransmittalNumber = txtTransmittalNumber.Text.Trim();

                                document.LastUpdatedBy = UserSession.Current.User.Id;
                                document.LastUpdatedDate = DateTime.Now;
                            }

                            this.documentService.Update(document);
                        }
                    }
                    else
                    {
                        objDoc.Name = txtName.Text.Trim();
                        objDoc.DocumentNumber = txtDocumentNumber.Text.Trim();
                        objDoc.Title = txtTitle.Text.Trim();
                        objDoc.RevisionID = Convert.ToInt32(ddlRevision.SelectedValue);
                        objDoc.RevisionName = ddlRevision.Text;
                        objDoc.StatusID = Convert.ToInt32(ddlStatus.SelectedValue);
                        objDoc.DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue);
                        objDoc.DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue);
                        objDoc.ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue);
                        objDoc.ReceivedDate = txtReceivedDate.SelectedDate;
                        objDoc.Remark = txtRemark.Text.Trim();
                        objDoc.Well = txtWell.Text.Trim();
                        objDoc.TransmittalNumber = txtTransmittalNumber.Text.Trim();
                        if (!string.IsNullOrEmpty(objDoc.RevisionName))
                        {
                            objDoc.RevisionFileName = objDoc.RevisionName + "_" + objDoc.Name;
                        }
                        else
                        {
                            objDoc.RevisionFileName = objDoc.Name;
                        }

                        objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                        objDoc.LastUpdatedDate = DateTime.Now;

                        this.documentService.Update(objDoc);
                    }
                }
            }
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item.BackColor = Color.Aqua;
                    item.BorderColor = Color.Aqua;
                }
            }
        }

        protected void grdDocument_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
           //// this.LoadDocuments(true);
        }

        /// <summary>
        /// The btn download_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnDownload_Click(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).Parent.Parent as GridDataItem;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentNewService.GetById(docId);
            var docPackName = string.Empty;
            if (docObj != null)
            {
                docPackName = docObj.Name;
                var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_" + docObj.Name + "_Pack.rar");

                var attachFiles = this.attachFileService.GetAllByDocId(docId);

                var temp = ZipPackage.CreateFile(serverDocPackPath);

                foreach (var attachFile in attachFiles)
                {
                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                    {
                        temp.Add(Server.MapPath(attachFile.FilePath));
                    }
                }

                this.DownloadByWriteByte(serverDocPackPath, docPackName + ".rar", true);
            }
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        

        

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx");
        }

        protected void btnExport_OnClick(object sender, EventArgs e)
        {
            if (this.ddlProject.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var docTypeId = Convert.ToInt32(this.ddlDocumentType.SelectedValue);
                var disciplineId = Convert.ToInt32(this.ddlDiscipline.SelectedValue);
                var departmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue);
                var searchAll = this.txtSearchAll.Text.Trim();
                var docNo = this.txtDocNo.Text.Trim();
                var docTitle = this.txtTitle.Text.Trim();

                var dataset = new DataSet();
                using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["SiteSqlServer"]))
                {
                    using (var cmd = new SqlCommand("sp_SearchEMDRInfo", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DocNo", docNo);
                        cmd.Parameters.AddWithValue("@Title", docTitle);
                        cmd.Parameters.AddWithValue("@DocTypeId", docTypeId);
                        cmd.Parameters.AddWithValue("@DisciplineId", disciplineId);
                        cmd.Parameters.AddWithValue("@ProjectId", projectId);
                        cmd.Parameters.AddWithValue("@DepartmentId", departmentId);
                        cmd.Parameters.AddWithValue("@SearchAll", searchAll);
                        conn.Open();
                        using (var da = new SqlDataAdapter())
                        {
                            da.SelectCommand = cmd;
                            da.Fill(dataset);
                        }

                        conn.Close();
                    }

                    if (dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    {
                        var filePath = Server.MapPath("Exports") + @"\";
                        var workbook = new Workbook();
                        workbook.Open(filePath + @"Template\DocumentListTemplate.xlsm");
                        var dataSheet = workbook.Worksheets[0];
                        dataSheet.Cells["J2"].PutValue(DateTime.Now);
                        dataSheet.Cells["D1"].PutValue(dataSheet.Cells["D1"].Value.ToString().Replace("<ProjectName>", this.ddlProject.SelectedItem.Text));
                        dataSheet.Cells["D3"].PutValue(dataSheet.Cells["D3"].Value.ToString().Replace("<SearchAll>", searchAll)
                                                                                        .Replace("<DocNo>", docNo)
                                                                                        .Replace("<DocTitle>", docTitle)
                                                                                        );

                        dataSheet.Cells["F3"].PutValue(dataSheet.Cells["F3"].Value.ToString().Replace("<Discipline>", this.ddlDiscipline.SelectedItem.Text)
                                                                                        .Replace("<DocType>", this.ddlDocumentType.SelectedItem.Text)
                                                                                        .Replace("<Department>", this.ddlDepartment.SelectedItem.Text)
                                                                                        );

                        dataSheet.Cells.ImportDataTable(dataset.Tables[0], false, 5, 2, dataset.Tables[0].Rows.Count, dataset.Tables[0].Columns.Count, true);
                        dataSheet.Cells.DeleteRow(11 + dataset.Tables[0].Rows.Count);

                        var filename = this.ddlProject.SelectedItem.Text + "$" + "DocumentList.xlsm";
                        workbook.Save(filePath + filename);
                        this.Download_File(filePath + filename);
                    }
                }
            }
        }
        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }
        protected void ddlProject_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProject.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var docTypeList = this.documentTypeService.GetAllByProject(projectId);
                var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId);

                docTypeList.Insert(0, new DocumentType() { ID = 0, Name = "All" });
                disciplineList.Insert(0, new Discipline() { ID = 0, Name = "All" });

                this.ddlDiscipline.DataSource = disciplineList;
                this.ddlDiscipline.DataTextField = "FullName";
                this.ddlDiscipline.DataValueField = "ID";
                this.ddlDiscipline.DataBind();

                this.ddlDocumentType.DataSource = docTypeList;
                this.ddlDocumentType.DataTextField = "FullName";
                this.ddlDocumentType.DataValueField = "ID";
                this.ddlDocumentType.DataBind();
            }
        }
    }
}

