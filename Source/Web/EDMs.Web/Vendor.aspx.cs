﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using Telerik.Web.Zip;

namespace EDMs.Web
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class Vendor : Page
    {
        private readonly OptionalTypeService optionalTypeService = new OptionalTypeService();

        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService = new RevisionService();

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService = new FolderService();


        private readonly DocumentNewService documentNewService = new DocumentNewService();

        private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly UserService userService = new UserService();

        private readonly AttachFileService attachFileService = new AttachFileService();

        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly PackageService packageService = new PackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly DisciplineService disciplineService = new   DisciplineService();

        private readonly RoleService roleService = new RoleService();

        private readonly TemplateManagementService templateManagementService = new TemplateManagementService();

        private readonly VendorPackageService vendorpackageService = new VendorPackageService();
        private readonly ContractorService contractorService = new ContractorService();


       // private readonly DocRoleMatrixService docRoleMatrixService = new DocRoleMatrixService();

        private readonly DistributionMatrixDetailByUserService dmDetailByUserService = new DistributionMatrixDetailByUserService();

        private readonly DocumentAssignedWorkflowService docAssignedWorkflowService = new DocumentAssignedWorkflowService();

        private readonly DocumentAssignedUserService docAssignedUserService = new DocumentAssignedUserService();

      //  private readonly WorkflowDetailService wfDetailService = new WorkflowDetailService();

      //  private readonly WorkflowStepService wfStepService = new WorkflowStepService();

       // private readonly DistributionMatrixService dmService = new DistributionMatrixService();


      //  private readonly WorkflowService wfService = new WorkflowService();
        private readonly MarkupFileService markupFileService = new MarkupFileService();

        private readonly ProjectUserService projectUserService = new ProjectUserService();

        private readonly ProjectDepartmentService projectDepartmentService = new ProjectDepartmentService();

        //private readonly AutoDistributionMatriceservice autoDistributionMatriceservice = new AutoDistributionMatriceservice();


        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
        private List<int> listFolderId = new List<int>();

        //private List<DocumentPackage> DocListInWFProccess
        //{
        //    get { return this.documentPackageService.GetAllDocInWFProccess(); }
        //}

        //private List<DocumentAssignedUser> WorkAssigned
        //{
        //    get { return this.docAssignedUserService.GetAllIncompleteByUser(UserSession.Current.User.Id); }
        //}

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                this.LoadObjectTree();
                Session.Add("IsListAll", false);
                var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
                if (projectObj != null)
                {
                    int review = 1;
                    if (!string.IsNullOrEmpty(projectObj.organizationComment1)) { review = 2; }
                    if (!string.IsNullOrEmpty(projectObj.organizationComment2)) { review = 3; }
                    if (!string.IsNullOrEmpty(projectObj.organizationComment3)) { review = 0; }
                    this.grdDocument.MasterTableView.ColumnGroups.FindGroupByName("Review1").HeaderText = projectObj.organizationComment1+" Review";
                    this.grdDocument.MasterTableView.ColumnGroups.FindGroupByName("Review2").HeaderText = projectObj.organizationComment2 + " Review";
                    this.grdDocument.MasterTableView.ColumnGroups.FindGroupByName("Review3").HeaderText = projectObj.organizationComment3 + " Review";
                    RefreshRagrid(review);
                }
                else
                {
                    this.RadPane3.Visible = false;
                }

                if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC)
                {
                    this.CustomerMenu.Items[0].Visible = false;
                    this.CustomerMenu.Items[1].Visible = false;
                    foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                    {
                        if (item.Value == "Adminfunc")
                        {
                            item.Visible = false;
                        }
                    }

                    //this.CustomerMenu.Items[3].Visible = false;

                    this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                    //this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                }
            }
        }

       
        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            var folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));
            var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
            temp.NavigateUrl = ConfigurationSettings.AppSettings.Get("ServerName") + folder.DirName;


            ////var originalURL = @"\\" + ConfigurationSettings.AppSettings.Get("ServerName") + @"\" + folder.DirName.Replace(@"/", @"\");
            ////var tempURI = new Uri(originalURL);/////

            ////var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
            ////temp.NavigateUrl = tempURI.AbsoluteUri;

            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(true, isListAll);
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false, bool isListAll = false)
        {
            var cbShowAll = (CheckBox)this.CustomerMenu.Items[6].FindControl("ckbShowAll");
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var docList = new List<DocumentPackage>();
          
            if (projectObj != null)
            {

                if (this.rtvDiscipline.SelectedNode != null)
                {
                    docList =
                        this.documentPackageService.GetAll(projectObj.ID)
                        .Where(t =>t.IsVendorDoc.GetValueOrDefault() && t.VendorPackagesId.GetValueOrDefault() == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value))
                        .OrderBy(t => t.DocNo)
                        .ToList();
                }
                else
                {
                    docList =
                      this.documentPackageService.GetAll(projectObj.ID).Where(t=>t.IsVendorDoc.GetValueOrDefault())
                      .OrderBy(t => t.DocNo)
                      .ToList();
                }
            }

            this.grdDocument.DataSource = docList;
        }

        protected void RefreshRagrid(int review)
        {
            if(review==0) return;
            for (int i = review; i <= 3; i++)
            {
                this.grdDocument.MasterTableView.GetColumn("DeadlineReview" + i).Visible = false;
                this.grdDocument.MasterTableView.GetColumn("TransInFromReviewNo" + i).Visible = false;
                this.grdDocument.MasterTableView.GetColumn("TransInFromReviewDate" + i).Visible = false;
                this.grdDocument.MasterTableView.GetColumn("StatusReview" + i).Visible = false;
                this.grdDocument.MasterTableView.GetColumn("CodeReview" + i).Visible = false;
            }
        }


        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
           
            else if (e.Argument.Contains("Complete"))
            {
                var docId = Convert.ToInt32(e.Argument.Split('_')[1]);
                var currentWorkAssignedUser = this.docAssignedUserService.GetCurrentInCompleteByDocUser(
                    UserSession.Current.User.Id, docId);


                if (currentWorkAssignedUser != null)
                {
                    currentWorkAssignedUser.IsComplete = true;
                    currentWorkAssignedUser.ActualCompleteDate = DateTime.Now;
                    this.docAssignedUserService.Update(currentWorkAssignedUser);

                    if (!this.docAssignedUserService.GetAllIncompleteByDoc(docId).Any())
                    {
                        var docObj = this.documentPackageService.GetById(docId);
                        if (docObj != null)
                        {
                            var currentWorkAssignedWF = this.docAssignedWorkflowService.GetById(currentWorkAssignedUser.DocumentAssignedWorkflowID.GetValueOrDefault());
                            if (currentWorkAssignedWF != null)
                            {
                                //var nextStep = this.wfStepService.GetById(currentWorkAssignedWF.NextWorkflowStepID.GetValueOrDefault());
                                //if (nextStep != null)
                                //{
                                //    //this.ProcessWorkflow(nextStep, currentWorkAssignedWF.WorkflowID.GetValueOrDefault(), docObj, currentWorkAssignedWF.AssignedBy.GetValueOrDefault());
                                //    this.ProcessWorkflow(nextStep, currentWorkAssignedWF.WorkflowID.GetValueOrDefault(), docObj, UserSession.Current.UserId);
                                //}
                            }
                        }
                    }
                }

                this.grdDocument.Rebind();
            }

            else if (e.Argument == "ExportEMDRReport_New")
            {
                this.ExportEMDRReportNew();
            }
            else if (e.Argument == "DeleteAllDoc")
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        if (docObj.ParentId == null)
                        {
                            docObj.IsDelete = true;
                            this.documentPackageService.Update(docObj);
                        }
                        else
                        {
                            var listRelateDoc =
                                this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                            if (listRelateDoc != null)
                            {
                                foreach (var objDoc in listRelateDoc)
                                {
                                    objDoc.IsDelete = true;
                                    this.documentPackageService.Update(objDoc);
                                }
                            }
                        }
                    }
                }

                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ClearEMDRData")
            {
                var listDocPack = this.documentPackageService.GetAll();
                foreach (var documentPackage in listDocPack)
                {
                    this.documentPackageService.Delete(documentPackage);
                }

                var attachFilePackage = this.attachFilesPackageService.GetAll();
                foreach (var attachFilesPackage in attachFilePackage)
                {
                    var filePath = Server.MapPath(attachFilesPackage.FilePath);
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    this.attachFilesPackageService.Delete(attachFilesPackage);
                }

                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ExportUpdateEDMR")
            {
                this.ExportUpdateEDMR();
            }
            else if (e.Argument == "ExportMasterList")
            {
                 var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
                 if (projectObj == null) return;
                var vendorpackage =
                    this.vendorpackageService.GetAllVendorPackageOfProject
                        (this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0).OrderBy(t => t.Name).ToList();

                var filePath = Server.MapPath("Exports") + @"\";
                if (vendorpackage.Count > 0)
                {
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\MasterListVendorTemplate.xls");

                    var sheets = workbook.Worksheets;
                    var tempSheet = sheets[1];
                    var readMeSheet = sheets[0];
                    var revisionList = this.revisionService.GetAll();
                    var documenttypeList = this.documentTypeService.GetAllByProject(this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0);
                    var discipline = this.disciplineService.GetAllDisciplineOfProject(this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0);

                    for (int i = 0; i < revisionList.Count; i++)
                    {
                        readMeSheet.Cells["B" + (7 + i)].PutValue(revisionList[i].Name);
                        readMeSheet.Cells["C" + (7 + i)].PutValue(revisionList[i].Description);
                    }

                    for (int i = 0; i < documenttypeList.Count; i++)
                    {
                        readMeSheet.Cells["E" + (7 + i)].PutValue(documenttypeList[i].Name);
                        readMeSheet.Cells["F" + (7 + i)].PutValue(documenttypeList[i].Description);
                    }

                    for (int i = 0; i < discipline.Count; i++)
                    {
                        readMeSheet.Cells["H" + (7 + i)].PutValue(discipline[i].Name);
                        readMeSheet.Cells["I" + (7 + i)].PutValue(discipline[i].Description);
                    }

                    var rangeRevisionList = readMeSheet.Cells.CreateRange("B7", "B" + (7 + (revisionList.Count == 0 ? 1 : revisionList.Count)));
                    rangeRevisionList.Name = "RevisionList";
                    var rangeDocTypeList = readMeSheet.Cells.CreateRange("E7", "E" + (7 + (documenttypeList.Count == 0 ? 1 : documenttypeList.Count)));
                    rangeDocTypeList.Name = "DocumentTypeList";
                    var rangDisciplinList = readMeSheet.Cells.CreateRange("H7", "H" + (7 + (discipline.Count == 0 ? 1 : discipline.Count)));
                    rangDisciplinList.Name = "Discipline";

                    tempSheet.Cells["A2"].PutValue(this.ddlProject.SelectedValue);
                    tempSheet.Cells["B1"].PutValue("Project: " + this.ddlProject.SelectedItem.Text);
                   

                    //int review = 16;
                    //if (!string.IsNullOrEmpty(projectObj.organizationComment1)) { review = 21; }
                    //if (!string.IsNullOrEmpty(projectObj.organizationComment2)) { review = 26; }
                    //if (!string.IsNullOrEmpty(projectObj.organizationComment3)) { review = 31; }

                    tempSheet.Cells["R2"].PutValue(projectObj.organizationComment1 + " Review");
                    tempSheet.Cells["W2"].PutValue(projectObj.organizationComment2 + " Review");
                    tempSheet.Cells["AB2"].PutValue(projectObj.organizationComment3 + " Review");
                       
                        for (int i = 0; i < vendorpackage.Count; i++)
                        {
                            sheets.AddCopy("DataList");
                            sheets[i + 2].Name = Utility.RemoveSpecialCharacter(vendorpackage[i].Name);
                            sheets[i + 2].Cells["A1"].PutValue(vendorpackage[i].ID);

                            var validations = sheets[i + 2].Validations;
                            this.CreateValidation(rangeRevisionList.Name, validations, 1, 1000, 3, 3);
                            this.CreateValidation(rangeDocTypeList.Name, validations, 1, 1000, 5, 5);
                            this.CreateValidation(rangDisciplinList.Name, validations, 1, 1000, 4, 4);

                            for (int j = 9; j <= 31; j++)
                            {
                                sheets[i + 2].Cells.HideColumn((byte)j);
                            }
                        }

                    workbook.Worksheets.RemoveAt(1);
                    //workbook.Worksheets.RemoveAt(2);    


                    var filename = this.ddlProject.SelectedItem.Text + "$" + "MasterListVendorTemplate.xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "ExportEMDRReport")
            {
                var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.ddlProject.SelectedValue);
                var contractorList = this.contractorService.GetAllByProject(projectID);
                dtFull.Columns.AddRange(new[]
                {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Department", typeof (String)),
                    new DataColumn("Start", typeof (String)),
                    new DataColumn("Planned", typeof (String)),
                    new DataColumn("RevName", typeof (String)),
                    new DataColumn("RevPlanned", typeof (String)),
                    new DataColumn("RevActual", typeof (String)),
                    new DataColumn("RevCommentCode", typeof (String)),
                    new DataColumn("Complete", typeof (Double)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("OutgoingNo", typeof (String)),
                    new DataColumn("OutgoingDate", typeof (String)),
                    new DataColumn("IncomingNo", typeof (String)),
                    new DataColumn("IncomingDate", typeof (String)),
                    new DataColumn("ICANo", typeof (String)),
                    new DataColumn("ICADate", typeof (String)),
                    new DataColumn("ICAReviewCode", typeof (String)),
                    new DataColumn("Notes", typeof (String)),
                    new DataColumn("IsEMDR", typeof (String)),
                    new DataColumn("HasAttachFile", typeof (String)),
                });

                foreach (var contractor in contractorList)
                {

                }

                var dtDiscipline = new DataTable();
                dtDiscipline.Columns.AddRange(new[]
                {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Start", typeof (DateTime)),
                    new DataColumn("RevName", typeof (String)),
                    new DataColumn("RevPlanned", typeof (DateTime)),
                    new DataColumn("RevActual", typeof (DateTime)),
                    new DataColumn("Complete", typeof (Double)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("Department", typeof (String)),
                    new DataColumn("Notes", typeof (String)),
                    new DataColumn("IsEMDR", typeof (String)),
                    new DataColumn("HasAttachFile", typeof (String)),
                    new DataColumn("OutgoingNo", typeof (String)),
                    new DataColumn("OutgoingDate", typeof (String)),
                });
                projectName = this.ddlProject.SelectedItem.Text;

                if (this.rtvDiscipline.SelectedNode != null)
                {

                    var templateManagement = this.templateManagementService.GetSpecial(1, projectID);
                    if (templateManagement != null)
                    {
                        workbook.Open(Server.MapPath(templateManagement.FilePath));

                        var sheets = workbook.Worksheets;

                        var DisciplineId = Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value);
                        var Discipline = this.disciplineService.GetById(DisciplineId);
                        if (Discipline != null)
                        {
                            sheets[0].Name = Discipline.Name;
                            sheets[0].Cells["C7"].PutValue(Discipline.Name);
                            sheets[0].Cells["B7"].PutValue(this.ddlProject.SelectedValue + "," + DisciplineId);
                            sheets[0].Cells["M4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            docList =
                                this.documentPackageService.GetAllEMDRByDiscipline(DisciplineId, false)
                                    .OrderBy(t => t.DocNo)
                                    .ToList();

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();
                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType =
                                    this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                var dataRow = dtDiscipline.NewRow();
                                dataRow["DocId"] = -1;
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtDiscipline.Rows.Add(dataRow);

                                var listDocByDocType =
                                    docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtDiscipline.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;
                                    dataRow["Start"] = (object) document.StartDate ?? DBNull.Value;
                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["RevPlanned"] = (object) document.RevisionPlanedDate ?? DBNull.Value;
                                    dataRow["RevActual"] = (object) document.RevisionActualDate ?? DBNull.Value;
                                    dataRow["Complete"] = document.Complete/100;
                                    dataRow["Weight"] = document.Weight/100;
                                    dataRow["Department"] = document.DeparmentName;
                                    dataRow["Notes"] = document.Notes;
                                    dataRow["IsEMDR"] = document.IsEMDR.GetValueOrDefault() ? "x" : string.Empty;
                                    dataRow["HasAttachFile"] = document.HasAttachFile.GetValueOrDefault()
                                        ? "x"
                                        : string.Empty;
                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    count += 1;
                                    dtDiscipline.Rows.Add(dataRow);
                                }
                            }

                            sheets[0].Cells["A7"].PutValue(dtDiscipline.Rows.Count);

                            sheets[0].Cells.ImportDataTable(dtDiscipline, false, 7, 1, dtDiscipline.Rows.Count, 16,
                                true);
                            sheets[1].Cells.ImportDataTable(dtDiscipline, false, 7, 1, dtDiscipline.Rows.Count, 16,
                                true);

                            sheets[0].Cells[7 + dtDiscipline.Rows.Count, 2].PutValue("Total");

                            var txtDisciplineComplete =
                                this.CustomerMenu.Items[3].FindControl("txtDisciplineComplete") as RadNumericTextBox;
                            var txtDisciplineWeight =
                                this.CustomerMenu.Items[3].FindControl("txtDisciplineWeight") as RadNumericTextBox;
                            if (txtDisciplineComplete != null)
                            {
                                sheets[0].Cells[7 + dtDiscipline.Rows.Count, 9].PutValue(
                                    txtDisciplineComplete.Value/100);
                            }

                            if (txtDisciplineWeight != null)
                            {
                                sheets[0].Cells[7 + dtDiscipline.Rows.Count, 10].PutValue(txtDisciplineWeight.Value/
                                                                                          100);
                            }
                            sheets[0].AutoFitRows();
                            sheets[1].IsVisible = false;

                            var filename = projectName + " - " + Discipline.Name + " EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.DownloadByWriteByte(filePath + filename, filename, true);

                        }
                    }
                }
                else
                {
                    var listDisciplineInPermission = this.disciplineService.GetAllDisciplineOfProject(
                            Convert.ToInt32(this.ddlProject.SelectedValue))
                            .OrderBy(t => t.ID)
                            .ToList();
                        

                    if (listDisciplineInPermission.Count > 0)
                    {
                        var templateManagement = this.templateManagementService.GetSpecial(2,
                            projectID);
                        if (templateManagement != null)
                        {
                            workbook.Open(Server.MapPath(templateManagement.FilePath));

                            var totalDoc = 0;
                            var totalDocIssues = 0;
                            var totalDocRev0Issues = 0;
                            var totalDocRev1Issues = 0;
                            var totalDocRev2Issues = 0;
                            var totalDocRev3Issues = 0;
                            var totalDocRev4Issues = 0;
                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;

                            var totalDocDontIssues = 0;

                            var sheets = workbook.Worksheets;
                            var wsSummary = sheets[0];
                            wsSummary.Cells.InsertRows(7, listDisciplineInPermission.Count - 1);

                            for (int i = 0; i < listDisciplineInPermission.Count; i++)
                            {
                                dtFull.Rows.Clear();

                                sheets.AddCopy(1);

                                sheets[i + 2].Name = listDisciplineInPermission[i].Name;
                                sheets[i + 2].Cells["V4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                                sheets[i + 2].Cells["C7"].PutValue(listDisciplineInPermission[i].Name);

                                // Add hyperlink
                                var linkName = listDisciplineInPermission[i].Name;
                                wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                                wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1,
                                    "'" + listDisciplineInPermission[i].Name + "'" + "!D7");

                                docList =
                                    this.documentPackageService.GetAllEMDRByDiscipline(
                                        listDisciplineInPermission[i].ID, false).OrderBy(t => t.DocNo).ToList();
                                var docListHasAttachFile = docList.Where(t => t.HasAttachFile.GetValueOrDefault());
                                var wgDoc = docList.Count;
                                var wgDocIssues = docListHasAttachFile.Count();
                                var wgDocRev0Issues = docListHasAttachFile.Count(t => t.RevisionName == "0");
                                var wgDocRev1Issues = docListHasAttachFile.Count(t => t.RevisionName == "1");
                                var wgDocRev2Issues = docListHasAttachFile.Count(t => t.RevisionName == "2");
                                var wgDocRev3Issues = docListHasAttachFile.Count(t => t.RevisionName == "3");
                                var wgDocRev4Issues = docListHasAttachFile.Count(t => t.RevisionName == "4");
                                var wgDocRev5Issues = docListHasAttachFile.Count(t => t.RevisionName == "5");
                                var wgTotalDocRev = wgDocRev0Issues + wgDocRev1Issues + wgDocRev2Issues +
                                                    wgDocRev3Issues + wgDocRev4Issues + wgDocRev5Issues;
                                var wgDocDontIssues = wgDoc - wgDocIssues;

                                totalDoc += wgDoc;
                                totalDocIssues += wgDocIssues;
                                totalDocRev0Issues += wgDocRev0Issues;
                                totalDocRev1Issues += wgDocRev1Issues;
                                totalDocRev2Issues += wgDocRev2Issues;
                                totalDocRev3Issues += wgDocRev3Issues;
                                totalDocRev4Issues += wgDocRev4Issues;
                                totalDocRev5Issues += wgDocRev5Issues;
                                totalDocRevIssues += wgTotalDocRev;
                                totalDocDontIssues = totalDoc - totalDocIssues;

                                wsSummary.Cells["C" + (7 + i)].PutValue(wgDoc);
                                wsSummary.Cells["D" + (7 + i)].PutValue(wgDocDontIssues);
                                wsSummary.Cells["E" + (7 + i)].PutValue(wgDocRev0Issues);
                                wsSummary.Cells["F" + (7 + i)].PutValue(wgDocRev1Issues);
                                wsSummary.Cells["G" + (7 + i)].PutValue(wgDocRev2Issues);
                                wsSummary.Cells["H" + (7 + i)].PutValue(wgDocRev3Issues);
                                wsSummary.Cells["I" + (7 + i)].PutValue(wgDocRev4Issues);
                                wsSummary.Cells["J" + (7 + i)].PutValue(wgDocRev5Issues);
                                wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                                wsSummary.Cells["L" + (7 + i)].PutValue(wgTotalDocRev);


                                var count = 1;

                                var listDocumentTypeId =
                                    docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                                double? complete = 0;
                                double? weight = 0;

                                foreach (var documentTypeId in listDocumentTypeId)
                                {
                                    var documentType =
                                        this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                    var dataRow = dtFull.NewRow();
                                    dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                    dtFull.Rows.Add(dataRow);

                                    var listDocByDocType =
                                        docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                    foreach (var document in listDocByDocType)
                                    {
                                        dataRow = dtFull.NewRow();
                                        dataRow["DocId"] = document.ID;
                                        dataRow["NoIndex"] = count;
                                        dataRow["DocNo"] = document.DocNo;
                                        dataRow["DocTitle"] = document.DocTitle;
                                        dataRow["Department"] = document.DeparmentName;
                                        dataRow["Start"] = document.StartDate != null
                                            ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["Planned"] = document.PlanedDate != null
                                            ? document.PlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevName"] = document.RevisionName;
                                        dataRow["RevPlanned"] = document.RevisionPlanedDate != null
                                            ? document.RevisionPlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevActual"] = document.RevisionActualDate != null
                                            ? document.RevisionActualDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevCommentCode"] = document.RevisionCommentCode;
                                        dataRow["Complete"] = document.Complete/100;
                                        dataRow["Weight"] = document.Weight/100;
                                        dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                        dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                            ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["IncomingNo"] = document.IncomingTransNo;
                                        dataRow["IncomingDate"] = document.IncomingTransDate != null
                                            ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                        dataRow["ICADate"] = document.ICAReviewReceivedDate != null
                                            ? document.ICAReviewReceivedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                        dataRow["Notes"] = document.Notes;
                                        dataRow["IsEMDR"] = document.IsEMDR.GetValueOrDefault() ? "x" : string.Empty;
                                        dataRow["HasAttachFile"] = document.HasAttachFile.GetValueOrDefault()
                                            ? "x"
                                            : string.Empty;

                                        count += 1;
                                        dtFull.Rows.Add(dataRow);

                                        complete += (document.Complete/100)*(document.Weight/100);
                                        weight += document.Weight/100;
                                    }
                                }

                                sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                                sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 23, true);

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 12].PutValue(complete);
                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 13].PutValue(weight);

                            }

                            wsSummary.Cells["H4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));

                            wsSummary.Cells["C" + (7 + listDisciplineInPermission.Count)].PutValue(totalDoc);
                            wsSummary.Cells["D" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocDontIssues);
                            wsSummary.Cells["E" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev0Issues);
                            wsSummary.Cells["F" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev1Issues);
                            wsSummary.Cells["G" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev2Issues);
                            wsSummary.Cells["H" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev3Issues);
                            wsSummary.Cells["I" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev4Issues);
                            wsSummary.Cells["J" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev5Issues);
                            wsSummary.Cells["K" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRevIssues);
                            wsSummary.Cells["L" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRevIssues);

                            sheets[1].IsVisible = false;

                            var filename = projectName + " - " + "EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.DownloadByWriteByte(filePath + filename, filename, true);
                        }
                    }
                }
            }
            else if (e.Argument == "UpdatePackageStatus")
            {
                var txtPackageComplete =
                    this.CustomerMenu.Items[2].FindControl("txtPackageComplete") as RadNumericTextBox;
                var txtPackageWeight =
                    this.CustomerMenu.Items[2].FindControl("txtPackageWeight") as RadNumericTextBox;

                var packageobj = this.packageService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                if (packageobj != null)
                {
                    if (txtPackageComplete != null)
                    {
                        packageobj.Complete = txtPackageComplete.Value.GetValueOrDefault();
                    }

                    if (txtPackageWeight != null)
                    {
                        packageobj.Weight = txtPackageWeight.Value.GetValueOrDefault();
                    }

                    this.packageService.Update(packageobj);
                }
            }
            else if (e.Argument.Contains("DeleteRev"))
            {
                string st = e.Argument.ToString();
                int docId = Convert.ToInt32(st.Replace("DeleteRev_", string.Empty));

                var docObj = this.documentPackageService.GetById(docId);
                var listRelateDoc =
                    this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                if (docObj != null && listRelateDoc.Count > 1)
                {

                    docObj.IsDelete = true;
                    docObj.IsLeaf = false;
                    this.documentPackageService.Update(docObj);
                    docId = 0;
                    listRelateDoc =
                        this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            if (docId < objDoc.ID)
                            {
                                docId = objDoc.ID;
                                docObj = objDoc;
                            }
                        }
                    }
                    if (docId != 0)
                    {
                        docObj.IsLeaf = true;
                        this.documentPackageService.Update(docObj);
                        this.grdDocument.Rebind();
                    }
                }
                else
                {
                    Response.Write(
                        "<script>window.alert('Can not be reduced, because this document is only one version.')</script>");
                }
            }
            else if (e.Argument == "DownloadMulti")
            {
                var serverTotalDocPackPath =
                    Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.rar");
                var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);

                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                {
                    var cboxSelected = (CheckBox) item["IsSelected"].FindControl("IsSelected");
                    if (cboxSelected.Checked)
                    {
                        var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                        var name = (Label) item["Index1"].FindControl("lblName");
                        var serverDocPackPath =
                            Server.MapPath("~/Exports/DocPack/" + name.Text + "_" +
                                           DateTime.Now.ToString("ddMMyyyhhmmss") + ".rar");

                        var attachFiles = this.attachFileService.GetAllByDocId(docId);

                        var temp = ZipPackage.CreateFile(serverDocPackPath);

                        foreach (var attachFile in attachFiles)
                        {
                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                            {
                                temp.Add(Server.MapPath(attachFile.FilePath));
                            }
                        }

                        docPack.Add(serverDocPackPath);
                    }
                }

                this.DownloadByWriteByte(serverTotalDocPackPath, "DocumentPackage.rar", true);

            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "SendNotification")
            {
                var listDisciplineId = new List<int>();
                var listSelectedDoc = new List<Document>();
                var count = 0;
                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                {
                    var cboxSelected = (CheckBox) item["IsSelected"].FindControl("IsSelected");
                    if (cboxSelected.Checked)
                    {
                        count += 1;
                        var docItem = new Document();
                        var disciplineId = item["DisciplineID"].Text != @"&nbsp;"
                            ? item["DisciplineID"].Text
                            : string.Empty;
                        if (!string.IsNullOrEmpty(disciplineId) && disciplineId != "0")
                        {
                            listDisciplineId.Add(Convert.ToInt32(disciplineId));

                            docItem.ID = count;
                            docItem.DocumentNumber = item["DocumentNumber"].Text != @"&nbsp;"
                                ? item["DocumentNumber"].Text
                                : string.Empty;
                            docItem.Title = item["Title"].Text != @"&nbsp;"
                                ? item["Title"].Text
                                : string.Empty;
                            docItem.RevisionName = item["Revision"].Text != @"&nbsp;"
                                ? item["Revision"].Text
                                : string.Empty;
                            docItem.FilePath = item["FilePath"].Text != @"&nbsp;"
                                ? item["FilePath"].Text
                                : string.Empty;
                            docItem.DisciplineID = Convert.ToInt32(disciplineId);
                            listSelectedDoc.Add(docItem);
                        }
                    }
                }

                listDisciplineId = listDisciplineId.Distinct().ToList();

                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials =
                        Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials =
                        new NetworkCredential(UserSession.Current.User.Email,
                            Utility.Decrypt(UserSession.Current.User.HashCode))
                };

                foreach (var disciplineId in listDisciplineId)
                {
                    var notificationRule = this.notificationRuleService.GetAllByDiscipline(disciplineId);

                    if (notificationRule != null)
                    {
                        var message = new MailMessage();
                        message.From = new MailAddress(UserSession.Current.User.Email,
                            UserSession.Current.User.FullName);
                        message.Subject = "Test send notification from EDMs";
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = @"******<br/>
                                        Dear users,<br/><br/>

                                        Please be informed that the following documents are now available on the BDPOC Document Library System for your information.<br/><br/>

                                        <table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:350px'>Document number</th>
		                                        <th style='text-align:center; width:350px'>Document title</th>
		                                        <th style='text-align:center; width:60px'>Revision</th>
	                                        </tr>";

                        if (!string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listUserId =
                                notificationRule.ReceiverListId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            foreach (var userId in listUserId)
                            {
                                var user = this.userService.GetByID(userId);
                                if (user != null)
                                {
                                    message.To.Add(new MailAddress(user.Email));
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(notificationRule.ReceiveGroupId) &&
                                 string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listGroupId =
                                notificationRule.ReceiveGroupId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            var listUser = this.userService.GetSpecialListUser(listGroupId);
                            foreach (var user in listUser)
                            {
                                message.To.Add(new MailAddress(user.Email));
                            }
                        }

                        var subBody = string.Empty;
                        foreach (var document in listSelectedDoc)
                        {
                            var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                            if (document.DisciplineID == disciplineId)
                            {
                                subBody += @"<tr>
                                <td>" + document.ID + @"</td>
                                <td><a href='http://" + Server.MachineName +
                                           (!string.IsNullOrEmpty(port) ? ":" + port : string.Empty)
                                           + document.FilePath + "' download='" + document.DocumentNumber + "'>"
                                           + document.DocumentNumber + @"</a></td>
                                <td>"
                                           + document.Title + @"</td>
                                <td>"
                                           + document.RevisionName + @"</td>";
                            }
                        }


                        message.Body += subBody + @"</table>
                                        <br/><br/>
                                        Thanks and regards,<br/>
                                        ******";

                        smtpClient.Send(message);
                    }
                }
            }

        }


        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(false, isListAll);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentPackageService.GetById(docId);
            if (docObj != null)
            {
                if (docObj.ParentId == null)
                {
                    docObj.IsDelete = true;
                    this.documentPackageService.Update(docObj);

                    // Delete Attach file
                    var attachDocList = this.attachFilesPackageService.GetAllDocId(docObj.ID);
                    foreach (var attachDoc in attachDocList)
                    {
                        var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath));
                        if (File.Exists(attachFilePath))
                        {
                            File.Delete(attachFilePath);
                        }
                    }
                    // -------------------------------------------
                }
                else
                {
                    var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            objDoc.IsDelete = true;
                            this.documentPackageService.Update(objDoc);
                        }
                    }

                    // Delete Attach file
                    var docRelatedList = this.documentPackageService.GetAllRevDoc(docObj.ParentId.Value).Where(t => t.ID != docObj.ID).Select(t => t.ID).ToList();
                    var attachDocList = this.attachFilesPackageService.GetAllDocId(docRelatedList);
                    foreach (var attachDoc in attachDocList)
                    {
                        var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath));
                        if (File.Exists(attachFilePath))
                        {
                            File.Delete(attachFilePath);
                        }
                    }
                    // -------------------------------------------

                }
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {

            }
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                this.CustomerMenu.Items[3].Visible = false;
                this.rtvDiscipline.UnselectAllNodes();
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {

            }
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {

                ////Populate Filters by binding the combo to datasource
                //var filteringItem = (GridFilteringItem)e.Item;
                //var myRadComboBox = (RadComboBox)filteringItem.FindControl("RadComboBoxCustomerProgramDescription");

                //myRadComboBox.DataSource = myDataSet;
                //myRadComboBox.DataTextField = "CustomerProgramDescription";
                //myRadComboBox.DataValueField = "CustomerProgramDescription";
                //myRadComboBox.ClearSelection();
                //myRadComboBox.DataBind();
            }
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item["No."].BackColor = Color.Aqua;// ColorTranslator.FromHtml("#00608f");// Color.Aqua;
                    item["No."].BorderColor = Color.Aqua;// ColorTranslator.FromHtml("#00608f"); //
                }
            }

            //if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            //{
            //    var item = e.Item as GridEditableItem;
            //    var lbldocNo = item.FindControl("lbldocNo") as Label;
            //    var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
            //    var ddlDepartment = item.FindControl("ddlDepartment") as RadComboBox;
            //    var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
            //    var txtPlanedDate = item.FindControl("txtPlanedDate") as RadDatePicker;

            //    var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
            //    var txtRevisionPlanedDate = item.FindControl("txtRevisionPlanedDate") as RadDatePicker;
            //    var txtRevisionActualDate = item.FindControl("txtRevisionActualDate") as RadDatePicker;
            //    var txtRevisionCommentCode = item.FindControl("txtRevisionCommentCode") as TextBox;

            //    var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
            //    var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

            //    var txtOutgoingTransNo = item.FindControl("txtOutgoingTransNo") as TextBox;
            //    var txtOutgoingTransDate = item.FindControl("txtOutgoingTransDate") as RadDatePicker;

            //    var txtIncomingTransNo = item.FindControl("txtIncomingTransNo") as TextBox;
            //    var txtIncomingTransDate = item.FindControl("txtIncomingTransDate") as RadDatePicker;

            //    var txtICAReviewOutTransNo = item.FindControl("txtICAReviewOutTransNo") as TextBox;
            //    var txtICAReviewReceivedDate = item.FindControl("txtICAReviewReceivedDate") as RadDatePicker;
            //    var txtICAReviewCode = item.FindControl("txtICAReviewCode") as TextBox;

            //    var cbIsEMDR = item.FindControl("cbIsEMDR") as CheckBox;


            //    var listRevision = this.revisionService.GetAll();
            //    listRevision.Insert(0, new Revision() { ID = 0 });
            //    if (ddlRevision != null)
            //    {
            //        ddlRevision.DataSource = listRevision;
            //        ddlRevision.DataTextField = "Name";
            //        ddlRevision.DataValueField = "ID";
            //        ddlRevision.DataBind();
            //    }

            //    if (txtStartDate != null)
            //    {
            //        txtStartDate.DatePopupButton.Visible = false;
            //    }

            //    if (txtPlanedDate != null)
            //    {
            //        txtPlanedDate.DatePopupButton.Visible = false;
            //    }

            //    if (txtRevisionPlanedDate != null)
            //    {
            //        txtRevisionPlanedDate.DatePopupButton.Visible = false;
            //    }

            //    if (txtRevisionActualDate != null)
            //    {
            //        txtRevisionActualDate.DatePopupButton.Visible = false;
            //    }

            //    if (txtOutgoingTransDate != null)
            //    {
            //        txtOutgoingTransDate.DatePopupButton.Visible = false;
            //    }

            //    if (txtIncomingTransDate != null)
            //    {
            //        txtIncomingTransDate.DatePopupButton.Visible = false;
            //    }

            //    if (txtICAReviewReceivedDate != null)
            //    {
            //        txtICAReviewReceivedDate.DatePopupButton.Visible = false;
            //    }



            //    var docNo = (item.FindControl("DocNo") as HiddenField).Value;
            //    var docTitle = (item.FindControl("DocTitle") as HiddenField).Value;
            //    var deparmentId = (item.FindControl("DeparmentId") as HiddenField).Value;
            //    var startDate = (item.FindControl("StartDate") as HiddenField).Value;
            //    var planedDate = (item.FindControl("PlanedDate") as HiddenField).Value;
            //    var revisionId = (item.FindControl("RevisionId") as HiddenField).Value;
            //    var revisionPlanedDate = (item.FindControl("RevisionPlanedDate") as HiddenField).Value;
            //    var revisionActualDate = (item.FindControl("RevisionActualDate") as HiddenField).Value;
            //    var revisionCommentCode = (item.FindControl("RevisionCommentCode") as HiddenField).Value;
            //    var complete = (item.FindControl("Complete") as HiddenField).Value;
            //    var weight = (item.FindControl("Weight") as HiddenField).Value;

            //    var OutgoingTransNo = (item.FindControl("OutgoingTransNo") as HiddenField).Value;
            //    var OutgoingTransDate = (item.FindControl("OutgoingTransDate") as HiddenField).Value;
            //    var IncomingTransNo = (item.FindControl("IncomingTransNo") as HiddenField).Value;
            //    var IncomingTransDate = (item.FindControl("IncomingTransDate") as HiddenField).Value;
            //    var ICAReviewOutTransNo = (item.FindControl("ICAReviewOutTransNo") as HiddenField).Value;
            //    var ICAReviewReceivedDate = (item.FindControl("ICAReviewReceivedDate") as HiddenField).Value;
            //    var ICAReviewCode = (item.FindControl("ICAReviewCode") as HiddenField).Value;


            //    var isEMDR = (item.FindControl("IsEMDR") as HiddenField).Value;

            //    if (!string.IsNullOrEmpty(startDate))
            //    {
            //        txtStartDate.SelectedDate = Convert.ToDateTime(startDate);
            //    }

            //    if (!string.IsNullOrEmpty(planedDate))
            //    {
            //        txtPlanedDate.SelectedDate = Convert.ToDateTime(planedDate);
            //    }

            //    if (!string.IsNullOrEmpty(revisionPlanedDate))
            //    {
            //        txtRevisionPlanedDate.SelectedDate = Convert.ToDateTime(revisionPlanedDate);
            //    }

            //    if (!string.IsNullOrEmpty(revisionActualDate))
            //    {
            //        txtRevisionActualDate.SelectedDate = Convert.ToDateTime(revisionActualDate);
            //    }

            //    lbldocNo.Text = docNo;
            //    txtDocTitle.Text = docTitle;

            //    var departmentList = this.roleService.GetAll(false);

            //    if (ddlDepartment != null)
            //    {
            //        departmentList.Insert(0, new Role { Id = 0 });
            //        ddlDepartment.DataSource = departmentList;
            //        ddlDepartment.DataTextField = "Name";
            //        ddlDepartment.DataValueField = "Id";
            //        ddlDepartment.DataBind();

            //        ddlDepartment.SelectedValue = deparmentId;
            //    }

            //    ddlRevision.SelectedValue = revisionId;
            //    txtRevisionCommentCode.Text = revisionCommentCode;
            //    txtComplete.Value = Convert.ToDouble(complete);
            //    txtWeight.Value = Convert.ToDouble(weight);

            //    txtOutgoingTransNo.Text = OutgoingTransNo;
            //    if (!string.IsNullOrEmpty(OutgoingTransDate))
            //    {
            //        txtOutgoingTransDate.SelectedDate = Convert.ToDateTime(OutgoingTransDate);
            //    }

            //    txtIncomingTransNo.Text = IncomingTransNo;
            //    if (!string.IsNullOrEmpty(IncomingTransDate))
            //    {
            //        txtIncomingTransDate.SelectedDate = Convert.ToDateTime(IncomingTransDate);
            //    }

            //    txtICAReviewOutTransNo.Text = ICAReviewOutTransNo;
            //    txtICAReviewCode.Text = ICAReviewCode;
            //    if (!string.IsNullOrEmpty(ICAReviewReceivedDate))
            //    {
            //        txtICAReviewReceivedDate.SelectedDate = Convert.ToDateTime(ICAReviewReceivedDate);
            //    }

            //    cbIsEMDR.Checked = Convert.ToBoolean(isEMDR);
            //}
        }

        protected void radTreeFolder_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack);
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var lbldocNo = item.FindControl("lbldocNo") as Label;
                var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
                var ddlDepartment = item.FindControl("ddlDepartment") as RadComboBox;
                var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
                var txtPlanedDate = item.FindControl("txtPlanedDate") as RadDatePicker;

                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var txtRevisionPlanedDate = item.FindControl("txtRevisionPlanedDate") as RadDatePicker;
                var txtRevisionActualDate = item.FindControl("txtRevisionActualDate") as RadDatePicker;
                var txtRevisionCommentCode = item.FindControl("txtRevisionCommentCode") as TextBox;

                var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
                var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

                var txtOutgoingTransNo = item.FindControl("txtOutgoingTransNo") as TextBox;
                var txtOutgoingTransDate = item.FindControl("txtOutgoingTransDate") as RadDatePicker;

                var txtIncomingTransNo = item.FindControl("txtIncomingTransNo") as TextBox;
                var txtIncomingTransDate = item.FindControl("txtIncomingTransDate") as RadDatePicker;

                var txtICAReviewOutTransNo = item.FindControl("txtICAReviewOutTransNo") as TextBox;
                var txtICAReviewReceivedDate = item.FindControl("txtICAReviewReceivedDate") as RadDatePicker;
                var txtICAReviewCode = item.FindControl("txtICAReviewCode") as TextBox;

                var cbIsEMDR = item.FindControl("cbIsEMDR") as CheckBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentPackageService.GetById(docId);

                var currentRevision = objDoc.RevisionId;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                var department = this.roleService.GetByID(Convert.ToInt32(ddlDepartment.SelectedValue));

                var projectid = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectname = this.ddlProject.SelectedItem.Text;

                if (newRevision > currentRevision)
                {
                    var docObjNew = new DocumentPackage();
                    docObjNew.ProjectId = projectid;
                    docObjNew.ProjectName = projectname;
                    docObjNew.DisciplineId = objDoc.DisciplineId;
                    docObjNew.DisciplineName = objDoc.DisciplineName;
                    docObjNew.DocNo = lbldocNo.Text;
                    docObjNew.DocTitle = txtDocTitle.Text.Trim();
                    docObjNew.DeparmentId = objDoc.DeparmentId;
                    docObjNew.DeparmentName = objDoc.DeparmentName;
                    docObjNew.StartDate = txtStartDate.SelectedDate;

                    docObjNew.PlanedDate = txtPlanedDate.SelectedDate;
                    docObjNew.RevisionId = newRevision;
                    docObjNew.RevisionName = ddlRevision.SelectedItem.Text;
                    docObjNew.RevisionActualDate = txtRevisionActualDate.SelectedDate;
                    docObjNew.RevisionCommentCode = txtRevisionCommentCode.Text.Trim();
                    docObjNew.RevisionPlanedDate = txtRevisionPlanedDate.SelectedDate;
                    docObjNew.Complete = txtComplete.Value.GetValueOrDefault();
                    docObjNew.Weight = txtWeight.Value.GetValueOrDefault();
                    docObjNew.OutgoingTransNo = txtOutgoingTransNo.Text.Trim();
                    docObjNew.OutgoingTransDate = txtOutgoingTransDate.SelectedDate;
                    docObjNew.IncomingTransNo = txtIncomingTransNo.Text.Trim();
                    docObjNew.IncomingTransDate = txtIncomingTransDate.SelectedDate;
                    docObjNew.ICAReviewOutTransNo = txtICAReviewOutTransNo.Text.Trim();
                    docObjNew.ICAReviewCode = txtICAReviewCode.Text.Trim();
                    docObjNew.ICAReviewReceivedDate = txtICAReviewReceivedDate.SelectedDate;

                    docObjNew.DocumentTypeId = objDoc.DocumentTypeId;
                    docObjNew.DocumentTypeName = objDoc.DocumentTypeName;
                    docObjNew.DisciplineId = objDoc.DisciplineId;
                    docObjNew.DisciplineName = objDoc.DisciplineName;
                    docObjNew.PackageId = objDoc.PackageId;
                    docObjNew.PackageName = objDoc.PackageName;
                    docObjNew.Notes = string.Empty;
                    docObjNew.PlatformId = objDoc.PlatformId;
                    docObjNew.PlatformName = objDoc.PlatformName;

                    docObjNew.IsLeaf = true;
                    docObjNew.IsEMDR = cbIsEMDR.Checked;
                    docObjNew.ParentId = objDoc.ParentId ?? objDoc.ID;
                    docObjNew.CreatedBy = UserSession.Current.User.Id;
                    docObjNew.CreatedDate = DateTime.Now;

                    this.documentPackageService.Insert(docObjNew);

                    objDoc.IsLeaf = false;
                }
                else
                {
                    ////objDoc.DocNo = lbldocNo.Text;
                    objDoc.DocTitle = txtDocTitle.Text.Trim();
                    ////objDoc.DeparmentName = department != null ? department.FullName : string.Empty;
                    ////objDoc.DeparmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
                    objDoc.StartDate = txtStartDate.SelectedDate;
                    objDoc.PlanedDate = txtPlanedDate.SelectedDate;
                    objDoc.RevisionId = newRevision;
                    objDoc.RevisionName = ddlRevision.SelectedItem.Text;
                    objDoc.RevisionActualDate = txtRevisionActualDate.SelectedDate;
                    objDoc.RevisionCommentCode = txtRevisionCommentCode.Text.Trim();
                    objDoc.RevisionPlanedDate = txtRevisionPlanedDate.SelectedDate;
                    objDoc.Complete = txtComplete.Value.GetValueOrDefault();
                    objDoc.Weight = txtWeight.Value.GetValueOrDefault();
                    ////objDoc.OutgoingTransNo = txtOutgoingTransNo.Text.Trim();
                    ////objDoc.OutgoingTransDate = txtOutgoingTransDate.SelectedDate;
                    ////objDoc.IncomingTransNo = txtIncomingTransNo.Text.Trim();
                    ////objDoc.IncomingTransDate = txtIncomingTransDate.SelectedDate;
                    ////objDoc.ICAReviewOutTransNo = txtICAReviewOutTransNo.Text.Trim();
                    ////objDoc.ICAReviewCode = txtICAReviewCode.Text.Trim();
                    ////objDoc.ICAReviewReceivedDate = txtICAReviewReceivedDate.SelectedDate;

                    objDoc.IsEMDR = cbIsEMDR.Checked;
                }

                objDoc.UpdatedBy = UserSession.Current.User.Id;
                objDoc.UpdatedDate = DateTime.Now;

                this.documentPackageService.Update(objDoc);
            }
        }

        protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "Images/folderdir16.png";
        }

        private void PopulateNodeOnDemand(RadTreeNodeEventArgs e, TreeNodeExpandMode expandMode)
        {
            var categoryId = this.lblCategoryId.Value;
            var folderPermission =
                this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Where(
                    t => t.CategoryIdList == categoryId).Select(t => Convert.ToInt32(t.FolderIdList)).ToList();

            var listFolChild = this.folderService.GetAllByParentId(Convert.ToInt32(e.Node.Value), folderPermission);
            foreach (var folderChild in listFolChild)
            {
                var nodeFolder = new RadTreeNode();
                nodeFolder.Text = folderChild.Name;
                nodeFolder.Value = folderChild.ID.ToString();
                nodeFolder.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                nodeFolder.ImageUrl = "Images/folderdir16.png";
                e.Node.Nodes.Add(nodeFolder);
            }

            e.Node.Expanded = true;
        }

        /// <summary>
        /// The get all child folder id.
        /// </summary>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<int> GetAllChildFolderId(int parentId, List<int> folderPermission)
        {
            if (!this.listFolderId.Contains(parentId))
            {
                this.listFolderId.Add(parentId);
            }


            var listFolder = this.folderService.GetAllByParentId(parentId, folderPermission);
            foreach (var folder in listFolder)
            {
                this.listFolderId.Add(folder.ID);
                this.GetAllChildFolderId(folder.ID, folderPermission);
            }

            return this.listFolderId;
        }

        /// <summary>
        /// The custom folder tree.
        /// </summary>
        /// <param name="radTreeView">
        /// The rad tree view.
        /// </param>
        private void CustomFolderTree(RadTreeNode radTreeView)
        {
            foreach (var node in radTreeView.Nodes)
            {
                var nodetemp = (RadTreeNode)node;
                if (nodetemp.Nodes.Count > 0)
                {
                    this.CustomFolderTree(nodetemp);
                }

                nodetemp.ImageUrl = "Images/folderdir16.png";
            }
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void LoadObjectTree()
        {
           

            var projectList = new List<ScopeProject>();

            if (UserSession.Current.IsAdmin || UserSession.Current.IsDC)
            {
                projectList = this.scopeProjectService.GetAll();
            }
            else
            {
                var doclist = this.documentPackageService.GetTypeId(3, true).Select(t=>t.ProjectId.GetValueOrDefault()).Distinct().ToList();
                projectList = projectList.Where(t => doclist.Contains(t.ID)).ToList();
            }
           

            if (projectList.Any())
            {
                this.ddlProject.DataSource = projectList;
                this.ddlProject.DataTextField = "FullName";
                this.ddlProject.DataValueField = "ID";
                this.ddlProject.DataBind();
                this.ddlProject.SelectedIndex = 0;
                if (this.ddlProject.SelectedItem != null)
                {
                    var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                    var projectObj = this.scopeProjectService.GetById(projectId);

                    this.lblProjectId.Value = projectId.ToString();
                    this.ProjectFolderPath.Value = projectObj.ProjectPath;
                    var vendorlist = this.vendorpackageService.GetAllVendorPackageOfProject(projectId);

                    this.rtvDiscipline.DataSource = vendorlist.OrderBy(t => t.Name);
                    this.rtvDiscipline.DataTextField = "Name";
                    this.rtvDiscipline.DataValueField = "ID";
                    this.rtvDiscipline.DataFieldID = "ID";
                    this.rtvDiscipline.DataBind();
                }
            }
        }

        /// <summary>
        /// The repair list.
        /// </summary>
        /// <param name="listOptionalTypeDetail">
        /// The list optional type detail.
        /// </param>
        private void RepairList(ref List<OptionalTypeDetail> listOptionalTypeDetail)
        {
            var temp = listOptionalTypeDetail.Where(t => t.ParentId != null).Select(t => t.ParentId).Distinct().ToList();
            var temp2 = listOptionalTypeDetail.Select(t => t.ID).ToList();
            var tempList = new List<OptionalTypeDetail>();
            foreach (var x in temp)
            {
                if (!temp2.Contains(x.Value))
                {
                    tempList.AddRange(listOptionalTypeDetail.Where(t => t.ParentId == x.Value).ToList());
                }
            }

            var listOptionalType = tempList.Where(t => t.OptionalTypeId != null).Select(t => t.OptionalTypeId).Distinct().ToList();

            foreach (var optionalTypeId in listOptionalType)
            {
                var optionalType = this.optionalTypeService.GetById(optionalTypeId.Value);
                var tempOptTypeDetail = new OptionalTypeDetail() { ID = optionalType.ID * 9898, Name = optionalType.Name + "s" };
                listOptionalTypeDetail.Add(tempOptTypeDetail);
                ////tempList.Add(tempOptTypeDetail);
                OptionalType type = optionalType;
                foreach (var optionalTypeDetail in tempList.Where(t => t.OptionalTypeId == type.ID).ToList())
                {
                    optionalTypeDetail.ParentId = tempOptTypeDetail.ID;
                }
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (this.ddlProject.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectObj = this.scopeProjectService.GetById(projectId);
                this.lblProjectId.Value = projectId.ToString();
                this.ProjectFolderPath.Value = projectObj.ProjectPath;
                var vendorlist = this.vendorpackageService.GetAllVendorPackageOfProject(this.ddlProject.SelectedItem != null
                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                : 0);


                this.rtvDiscipline.DataSource = vendorlist.OrderBy(t=>t.Name);
                this.rtvDiscipline.DataTextField = "Name";
                this.rtvDiscipline.DataValueField = "ID";
                this.rtvDiscipline.DataFieldID = "ID";
                this.rtvDiscipline.DataBind();

          
           }
            this.grdDocument.Rebind();
        }

        private void refeshMenu(ScopeProject projectObj)
        {
            // Get all User have permission with selected project
           
            // ------------------------------------------------------------------------------------------
            if (!UserSession.Current.IsAdmin
                && UserSession.Current.UserId != projectObj.DCId
                    && UserSession.Current.UserId != projectObj.PMId)
            {
                this.CustomerMenu.Items[0].Visible = false;
                this.CustomerMenu.Items[1].Visible = false;
                foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                {
                    if (item.Value == "Adminfunc")
                    {
                        item.Visible = false;
                    }
                }

           
                this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
            }
            else
            {
                this.CustomerMenu.Items[0].Visible = true;
                this.CustomerMenu.Items[1].Visible = true;
                foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                {
                    if (item.Value == "Adminfunc")
                    {
                        item.Visible = true;
                    }
                }

                //this.CustomerMenu.Items[3].Visible = false;

                this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = true;
                //this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = true;
            }

            if (UserSession.Current.UserId == projectObj.DCId
                    || UserSession.Current.UserId == projectObj.PMId)
            {
                ((RadToolBarDropDown)this.CustomerMenu.Items[4]).Buttons[0].Visible = true;
                ((RadToolBarDropDown)this.CustomerMenu.Items[4]).Buttons[1].Visible = true;
            }
            else
            {
                ((RadToolBarDropDown)this.CustomerMenu.Items[4]).Buttons[0].Visible = false;
                ((RadToolBarDropDown)this.CustomerMenu.Items[4]).Buttons[1].Visible = false;
            }


            if (UserSession.Current.UserId == projectObj.DCId
                    || UserSession.Current.UserId == projectObj.PMId)
            {
                ((RadToolBarDropDown)this.CustomerMenu.Items[4]).Buttons[2].Visible = true;
            }
            else
            {
                ((RadToolBarDropDown)this.CustomerMenu.Items[4]).Buttons[2].Visible = false;
            }

        }
        protected void grdDocument_Init(object sender, EventArgs e)
        {
        }

        protected void grdDocument_DataBound(object sender, EventArgs e)
        {
        }

        protected void rtvDiscipline_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The btn download_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnDownload_Click(object sender, ImageClickEventArgs e)
        {
            var item = ((ImageButton)sender).Parent.Parent as GridDataItem;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentNewService.GetById(docId);
            var docPackName = string.Empty;
            if (docObj != null)
            {
                docPackName = docObj.Name;
                var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_" + docObj.Name + "_Pack.rar");

                var attachFiles = this.attachFileService.GetAllByDocId(docId);

                var temp = ZipPackage.CreateFile(serverDocPackPath);

                foreach (var attachFile in attachFiles)
                {
                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                    {
                        temp.Add(Server.MapPath(attachFile.FilePath));
                    }
                }

                this.DownloadByWriteByte(serverDocPackPath, docPackName + ".rar", true);
            }
        }

        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
                var filterItem = (GridFilteringItem)e.Item;
                var selectedProperty = new List<string>();

                var ddlFilterRev = (RadComboBox)filterItem.FindControl("ddlFilterRev");
            }
        }

        protected DateTime? SetPublishDate(GridItem item)
        {
            if (item.OwnerTableView.GetColumn("Index27").CurrentFilterValue == string.Empty)
            {
                return new DateTime?();
            }
            else
            {
                return DateTime.Parse(item.OwnerTableView.GetColumn("Index27").CurrentFilterValue);
            }
        }

        /// <summary>
        /// The bind tree view combobox.
        /// </summary>
        /// <param name="optionalType">
        /// The optional type.
        /// </param>
        /// <param name="ddlObj">
        /// The ddl obj.
        /// </param>
        /// <param name="rtvName">
        /// The rtv name.
        /// </param>
        /// <param name="listOptionalTypeDetailFull">
        /// The list optional type detail full.
        /// </param>
        private void BindTreeViewCombobox(int optionalType, RadComboBox ddlObj, string rtvName, IEnumerable<OptionalTypeDetail> listOptionalTypeDetailFull)
        {
            var rtvobj = (RadTreeView)ddlObj.Items[0].FindControl(rtvName);
            if (rtvobj != null)
            {
                var listOptionalTypeDetail = listOptionalTypeDetailFull.Where(t => t.OptionalTypeId == optionalType).ToList();
                this.RepairList(ref listOptionalTypeDetail);

                rtvobj.DataSource = listOptionalTypeDetail;
                rtvobj.DataFieldParentID = "ParentId";
                rtvobj.DataTextField = "Name";
                rtvobj.DataValueField = "ID";
                rtvobj.DataFieldID = "ID";
                rtvobj.DataBind();
            }
        }

        protected void rtvDiscipline_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/package.png";
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void InitGridContextMenu(int projectId)
        {
            var contractorList = this.contractorService.GetAllByProject(projectId).OrderBy(t => t.TypeID).ThenBy(t => t.Name);
            foreach (var contractor in contractorList)
            {
                if (contractor.TypeID == 1)
                {
                    this.grdDocument.MasterTableView.ColumnGroups[0].HeaderText = contractor.Name + " - VSP";
                }

                this.radMenu.Items.Add(new RadMenuItem()
                {
                    Text = (contractor.TypeID == 1 ? "Response from ": "Comment from ") + contractor.Name,
                    ImageUrl = "~/Images/comment1.png",
                    Value = contractor.TypeID == 1 ? "Response_" + contractor.ID : "Comment_" + contractor.ID,
                    //NavigateUrl = "~/Controls/Document/CommentResponseForm.aspx?contId=" + contractor.ID
                });
            }
        }

        private void ExportEMDRReportNew()
        {
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var docList = new List<DocumentPackage>();
            if (projectObj != null)
            {

                var vendorpackage = this.vendorpackageService.GetAllVendorPackageOfProject(projectObj.ID);
                docList = this.documentPackageService.GetAll(projectObj.ID).Where(t => t.IsVendorDoc.GetValueOrDefault())
                      .OrderBy(t => t.DocNo)
                      .ToList();
                if (docList.Count > 0)
                {
                  

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\EMDRReportVendorTemplate.xlsm");

                    var workSheets = workbook.Worksheets;
                    var summarySheet = workSheets[0];
                    summarySheet.Cells["E1"].PutValue(summarySheet.Cells["E1"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name));
                    summarySheet.Cells["E3"].PutValue(summarySheet.Cells["E3"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name));

                    summarySheet.Cells["K4"].PutValue(DateTime.Now.ToString("dd-MM-yyyy"));
                    summarySheet.Cells["A2"].PutValue(vendorpackage.Count);


                    workSheets[1].Cells["E1"].PutValue(workSheets[1].Cells["E1"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name));
                    workSheets[1].Cells["E3"].PutValue(workSheets[1].Cells["E3"].Value );
                    workSheets[1].Cells["M4"].PutValue(workSheets[1].Cells["M4"].Value.ToString()
                                .Replace("<Date>", DateTime.Now.ToString("dd-MM-yyyy")));


                    for (int i = 0; i < vendorpackage.Count; i++)
                    {

                        var docListGroupByDisciplineList = docList.Where(t=> t.VendorPackagesId.GetValueOrDefault()==vendorpackage[i].ID);
                        // Fill data to Summary sheet
                        summarySheet.Cells[7 + i, 2].PutValue(i + 1);
                        summarySheet.Cells[7 + i, 3].PutValue(vendorpackage[i].Name);
                        summarySheet.Cells[7 + i, 4].PutValue(vendorpackage[i].VendorName);
                        summarySheet.Cells[7 + i, 5].PutValue(docListGroupByDisciplineList.Count());
                        summarySheet.Cells[7 + i, 6].PutValue(docListGroupByDisciplineList.Count(t => !string.IsNullOrEmpty(t.TransInFromVendorNo)));
                        summarySheet.Cells[7 + i, 7].PutValue(docListGroupByDisciplineList.Count(t => t.CurrenCodeName.Contains("1")));
                        summarySheet.Cells[7 + i, 8].PutValue(docListGroupByDisciplineList.Count(t => t.CurrenCodeName.Contains("2")));
                        summarySheet.Cells[7 + i, 9].PutValue(docListGroupByDisciplineList.Count(t => t.CurrenCodeName.Contains("3")));
                        summarySheet.Cells[7 + i, 10].PutValue(docListGroupByDisciplineList.Count(t => t.CurrenCodeName.Contains("4")));
                     

                        summarySheet.Hyperlinks.Add("D" + (8 + i), 1, 1, "'" + vendorpackage[i].Name.Replace('/', '-').Replace('\\', '-') + "'!C7");

                        // Fill data for discipline sheet Summary
                        var count = 1;
                        var docCount = 1;
                       
                        workSheets.AddCopy("DataSheet");
                        workSheets[i + 2].Name = vendorpackage[i].Name.Replace('/', '-').Replace('\\', '-');
                        workSheets[i + 2].Cells["E4"].PutValue(vendorpackage[i].Name);

                        int review = 19;
                        if (!string.IsNullOrEmpty(projectObj.organizationComment1)) { review = 24; }
                        if (!string.IsNullOrEmpty(projectObj.organizationComment2)) { review = 29; }
                        if (!string.IsNullOrEmpty(projectObj.organizationComment3)) { review = 34; }

                        workSheets[i + 2].Cells["T5"].PutValue(projectObj.organizationComment1 + " Review");
                        workSheets[i + 2].Cells["Y5"].PutValue(projectObj.organizationComment2 + " Review");
                        workSheets[i + 2].Cells["AD5"].PutValue(projectObj.organizationComment3 + " Review");

                        for (int j = review; j <= 33; j++)
                        {
                            workSheets[i + 2].Cells.HideColumn((byte)j);
                        }

                        workSheets[i + 2].Cells["O3"].PutValue("Summary");
                        workSheets[i + 2].Hyperlinks.Add("O3", 1, 1, "Summary!A3");

                        var docListGroupByDocTypeList = docListGroupByDisciplineList.GroupBy(t => t.DocumentTypeName);
                        foreach (var docListGroupByDocType in docListGroupByDocTypeList)
                        {
                            workSheets[i + 2].Cells[6 + count, 3].PutValue(docListGroupByDocType.Key);
                            workSheets[i + 2].Cells[6 + count, 1].PutValue(0);

                            count += 1;

                            foreach (var document in docListGroupByDocType)
                            {
                                workSheets[i + 2].Cells[6 + count, 1].PutValue(document.ID);
                                workSheets[i + 2].Cells[6 + count, 2].PutValue(docCount);
                                workSheets[i + 2].Cells[6 + count, 3].PutValue(document.DocNo);
                                workSheets[i + 2].Cells[6 + count, 4].PutValue(document.DocTitle);
                                workSheets[i + 2].Cells[6 + count, 5].PutValue(document.RevisionName);
                                workSheets[i + 2].Cells[6 + count, 6].PutValue(document.DisciplineName);
                                workSheets[i + 2].Cells[6 + count, 7].PutValue(document.DocumentTypeName);
                                workSheets[i + 2].Cells[6 + count, 8].PutValue(document.CurrenCodeName);
                                workSheets[i + 2].Cells[6 + count, 9].PutValue(document.StatusName);
                                workSheets[i + 2].Cells[6 + count, 10].PutValue(document.IssuePlanDate);
                                workSheets[i + 2].Cells[6 + count, 11].PutValue(document.TransInFromVendorNo);
                                workSheets[i + 2].Cells[6 + count, 12].PutValue(document.TransInFromVendorDate);
                                workSheets[i + 2].Cells[6 + count, 13].PutValue(document.VendorResponseDate);
                                workSheets[i + 2].Cells[6 + count, 14].PutValue(document.OutgoingTransNo);
                                workSheets[i + 2].Cells[6 + count, 15].PutValue(document.OutgoingTransDate);
                                workSheets[i + 2].Cells[6 + count, 16].PutValue(document.TransOutToOrganizationNo);
                                workSheets[i + 2].Cells[6 + count, 17].PutValue(document.TransOutToOrganizationDate);
                                workSheets[i + 2].Cells[6 + count, 18].PutValue(document.Organization);
                                workSheets[i + 2].Cells[6 + count, 19].PutValue(document.DeadlineReview1);
                                workSheets[i + 2].Cells[6 + count, 20].PutValue(document.TransInFromReviewNo1);
                                workSheets[i + 2].Cells[6 + count, 21].PutValue(document.TransInFromReviewDate1);
                                workSheets[i + 2].Cells[6 + count, 22].PutValue(document.StatusReview1);
                                workSheets[i + 2].Cells[6 + count, 23].PutValue(document.CodeReview1);
                                workSheets[i + 2].Cells[6 + count, 24].PutValue(document.DeadlineReview2);
                                workSheets[i + 2].Cells[6 + count, 25].PutValue(document.TransInFromReviewNo2);
                                workSheets[i + 2].Cells[6 + count, 26].PutValue(document.TransInFromReviewDate2);
                                workSheets[i + 2].Cells[6 + count, 27].PutValue(document.StatusReview2);
                                workSheets[i + 2].Cells[6 + count, 28].PutValue(document.CodeReview2);
                                workSheets[i + 2].Cells[6 + count, 29].PutValue(document.DeadlineReview3);
                                workSheets[i + 2].Cells[6 + count, 30].PutValue(document.TransInFromReviewNo3);
                                workSheets[i + 2].Cells[6 + count, 31].PutValue(document.TransInFromReviewDate3);
                                workSheets[i + 2].Cells[6 + count, 32].PutValue(document.StatusReview3);
                                workSheets[i + 2].Cells[6 + count, 33].PutValue(document.CodeReview3);

                                docCount += 1;
                                count += 1;
                            }
                        }

                        workSheets[i + 2].Cells["A2"].PutValue(count);
                    }

                   

                    //var style= new Aspose.Cells.Style();
                    //var flag= new StyleFlag();
                    //style=summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].Style;
                    //style.Font.IsBold=true;
                    ////style.BackgroundColor = Color.Orange;
                    //flag.FontBold=true;
                    
                    //summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].ApplyStyle(style, flag);

                    workSheets.RemoveAt(1);

                    var filename = Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "_" + "Vendor Report.xlsm";
                    workbook.Save(filePath + filename);
                    this.Download_File(filePath + filename);
                }
            }
        }

        protected void ckbShowAll_CheckedChange(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }







        private void ExportUpdateEDMR()
        {
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var docList = new List<DocumentPackage>();
            if (projectObj != null)
            {
                docList = this.documentPackageService.GetAll(projectObj.ID, 3).Where(t => t.IsVendorDoc.GetValueOrDefault()).ToList();

                if (docList.Count > 0)
                {
                    var docListGroupByDisciplineList =docList.GroupBy(t => t.VendorPackagesName).ToList();
                    if (this.rtvDiscipline.SelectedNode != null)
                    {
                        docListGroupByDisciplineList = docList.Where(t => t.VendorPackagesId.GetValueOrDefault() == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).GroupBy(t => t.VendorPackagesName).ToList();
                    }

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\UpdateVendorTemplate.xlsm");

                    var sheets = workbook.Worksheets;
                    var tempDataSheet = sheets[0];
                    var revisionList = this.revisionService.GetAll();
                    var documenttypeList = this.documentTypeService.GetAllByProject(this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0);
                    var discipline = this.disciplineService.GetAllDisciplineOfProject(this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0);

                    for (int i = 0; i < revisionList.Count; i++)
                    {
                        tempDataSheet.Cells["B" + (7 + i)].PutValue(revisionList[i].Name);
                        tempDataSheet.Cells["C" + (7 + i)].PutValue(revisionList[i].Description);
                    }

                    for (int i = 0; i < documenttypeList.Count; i++)
                    {
                        tempDataSheet.Cells["E" + (7 + i)].PutValue(documenttypeList[i].Name);
                        tempDataSheet.Cells["F" + (7 + i)].PutValue(documenttypeList[i].Description);
                    }

                    for (int i = 0; i < discipline.Count; i++)
                    {
                        tempDataSheet.Cells["H" + (7 + i)].PutValue(discipline[i].Name);
                        tempDataSheet.Cells["I" + (7 + i)].PutValue(discipline[i].Description);
                    }

                    var rangeRevisionList = tempDataSheet.Cells.CreateRange("B7", "B" + (7 + (revisionList.Count == 0 ? 1 : revisionList.Count)));
                    rangeRevisionList.Name = "RevisionList";
                    var rangeDocTypeList = tempDataSheet.Cells.CreateRange("E7", "E" + (7 + (documenttypeList.Count == 0 ? 1 : documenttypeList.Count)));
                    rangeDocTypeList.Name = "DocumentTypeList";
                    var rangDisciplinList = tempDataSheet.Cells.CreateRange("H7", "H" + (7 + (discipline.Count == 0 ? 1 : discipline.Count)));
                    rangDisciplinList.Name = "Discipline";
                    for (int i = 0; i < docListGroupByDisciplineList.Count; i++)
                    {

                        int review = 18;
                        if (!string.IsNullOrEmpty(projectObj.organizationComment1)) { review = 23; }
                        if (!string.IsNullOrEmpty(projectObj.organizationComment2)) { review = 28; }
                        if (!string.IsNullOrEmpty(projectObj.organizationComment3)) { review = 33; }

                        sheets[i + 1].Cells["S4"].PutValue(projectObj.organizationComment1 + " Review");
                        sheets[i + 1].Cells["X4"].PutValue(projectObj.organizationComment2 + " Review");
                        sheets[i + 1].Cells["AC4"].PutValue(projectObj.organizationComment3 + " Review");

                        for (int j = review; j <= 32; j++)
                        {
                            sheets[i + 1].Cells.HideColumn((byte)j);
                        }

                        var docListGroupByDis = docListGroupByDisciplineList[i].ToList();
                        //sheets.AddCopy("Sheet1");
                        sheets[i + 1].Name = docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-');
                        sheets[i + 1].Cells["A1"].PutValue(this.ddlProject.SelectedValue);
                        sheets[i + 1].Cells["A2"].PutValue(docListGroupByDis[0].VendorPackagesId);
                        sheets[i + 2].Cells["A3"].PutValue(docListGroupByDis.Count());

                        sheets[i + 1].Cells["D1"].PutValue(sheets[i + 2].Cells["D1"].Value.ToString()
                                .Replace("<ProjectName>", this.ddlProject.SelectedItem.Text));
                        sheets[i + 1].Cells["D3"].PutValue(docListGroupByDis[0].DisciplineName);
                        sheets[i + 1].Cells["J2"].PutValue(DateTime.Now);

                        var validations = sheets[i + 1].Validations;
                        this.CreateValidation(rangeRevisionList.Name, validations, 1, 1000, 4, 4);
                        this.CreateValidation(rangeDocTypeList.Name, validations, 1, 1000, 6, 6);
                        this.CreateValidation(rangDisciplinList.Name, validations, 1, 1000, 5, 5);

                        for (int j = 0; j < docListGroupByDis.Count; j++)
                        {
                            var docObj = docListGroupByDis[j];
                            sheets[i + 1].Cells[5 + j, 1].PutValue(docObj.ID);
                            sheets[i + 1].Cells[5 + j, 2].PutValue(docObj.DocNo);
                            sheets[i + 1].Cells[5 + j, 3].PutValue(docObj.DocTitle);
                            sheets[i + 1].Cells[5 + j, 4].PutValue(docObj.RevisionName);
                            sheets[i + 1].Cells[5 + j, 5].PutValue(docObj.DisciplineName);
                            sheets[i + 1].Cells[5 + j, 6].PutValue(docObj.DocumentTypeName);
                            sheets[i + 1].Cells[5 + j, 7].PutValue(docObj.CurrenCodeName);
                            sheets[i + 1].Cells[5 + j, 8].PutValue(docObj.StatusName);
                            sheets[i + 1].Cells[5 + j, 9].PutValue(docObj.IssuePlanDate != null ? docObj.IssuePlanDate.GetValueOrDefault().ToString("dd/MM/yyyy"): string.Empty);
                            sheets[i + 1].Cells[5 + j, 10].PutValue(docObj.TransInFromVendorNo);
                            sheets[i + 1].Cells[5 + j, 11].PutValue(docObj.TransInFromVendorDate != null ? docObj.TransInFromVendorDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 12].PutValue(docObj.VendorResponseDate != null ? docObj.VendorResponseDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 13].PutValue(docObj.OutgoingTransNo);
                            sheets[i + 1].Cells[5 + j, 14].PutValue(docObj.OutgoingTransDate != null ? docObj.OutgoingTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 15].PutValue(docObj.TransOutToOrganizationNo);
                            sheets[i + 1].Cells[5 + j, 16].PutValue(docObj.TransOutToOrganizationDate != null ? docObj.TransOutToOrganizationDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 17].PutValue(docObj.Organization);
                            sheets[i + 1].Cells[5 + j, 18].PutValue(docObj.DeadlineReview1 != null ? docObj.DeadlineReview1.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 19].PutValue(docObj.TransInFromReviewNo1);
                            sheets[i + 1].Cells[5 + j, 20].PutValue(docObj.TransInFromReviewDate1 != null ? docObj.TransInFromReviewDate1.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 21].PutValue(docObj.StatusReview1);
                            sheets[i + 1].Cells[5 + j, 22].PutValue(docObj.CodeReview1);
                            sheets[i + 1].Cells[5 + j, 23].PutValue(docObj.DeadlineReview2 != null ? docObj.DeadlineReview2.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 24].PutValue(docObj.TransInFromReviewNo2);
                            sheets[i + 1].Cells[5 + j, 25].PutValue(docObj.TransInFromReviewDate2 != null ? docObj.TransInFromReviewDate2.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 26].PutValue(docObj.StatusReview2);
                            sheets[i + 1].Cells[5 + j, 27].PutValue(docObj.CodeReview2);
                            sheets[i + 1].Cells[5 + j, 28].PutValue(docObj.DeadlineReview3 != null ? docObj.DeadlineReview3.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 29].PutValue(docObj.TransInFromReviewNo3);
                            sheets[i + 1].Cells[5 + j, 30].PutValue(docObj.TransInFromReviewDate3 != null ? docObj.TransInFromReviewDate3.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 31].PutValue(docObj.StatusReview3);
                            sheets[i + 1].Cells[5 + j, 32].PutValue(docObj.CodeReview3);
                        }
                    }

                    for (int i = docListGroupByDisciplineList.Count; i < 60; i++)
                    {
                        workbook.Worksheets.RemoveAt(docListGroupByDisciplineList.Count + 1);
                    }

                    workbook.Worksheets[0].IsVisible = false;

                    var filename = this.ddlProject.SelectedItem.Text + "$" + UserSession.Current.User.Username + "_UpdateVendor.xlsm";
                    workbook.Save(filePath + filename);
                    this.Download_File(filePath + filename);
                }
            }
        }
        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }
    }
}