﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Resource.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class Resource
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Library;

    using Telerik.Web.UI;

    /// <summary>
    /// Class Resource
    /// </summary>
    public partial class _Resource : Page
    {

        protected void SetListProperties()
        {
            if (treeReourceType.SelectedNode != null)
            {
                //if (!string.IsNullOrEmpty(treeReourceType.SelectedNode.Value))
                //ResourceList.ResourceType = treeReourceType.SelectedNode.Value;
            }
        }
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResourceGroupService resourceGroupService = new ResourceGroupService();
                var listGroup = resourceGroupService.GetAll();
                foreach(var group in listGroup)
                {
                    RadTreeNode node = new RadTreeNode(group.Name,group.Id.ToString());
                    (treeReourceType).Nodes[0].Nodes.Add(node);
                }
            }

            //var ResourceList = new List();
            //ResourceList = (List) LoadControl("~/Controls/Resources/List.ascx");
            //ResourceList.ResourceType = string.Empty;

            //loadControl(ResourceList);

            //if (!IsPostBack)
            //SetListProperties();
            //if (!Page.IsPostBack)
            //LoadResourceByResourceType(true);
        }

        /// <summary>
        /// The load Resource by Resource type.
        /// </summary>
        protected void LoadResourceByResourceType(bool isbind = false)
        {
            var resourceService = new ResourceService();
            if (treeReourceType.SelectedNode != null)
            {
                if (!string.IsNullOrEmpty(treeReourceType.SelectedNode.Value))
                {
                    var ResourceType = treeReourceType.SelectedNode.Value;

                    if (ResourceType == string.Empty || int.Parse(ResourceType) == 0)
                    {
                        grdKhachHang.DataSource = resourceService.GetAll(true);
                    }
                    else
                    {
                        grdKhachHang.DataSource = resourceService.GetByResourceGroup(int.Parse(ResourceType));
                    }
                    if (isbind)
                        grdKhachHang.DataBind();
                }
            }
            else
            {
                grdKhachHang.DataSource = resourceService.GetAll(true);

                if (isbind)
                    grdKhachHang.DataBind();
            }
        }
        protected void SearchResource(string search, bool isbind = false)
        {
            var resourceService = new ResourceService();
            if (treeReourceType.SelectedNode != null)
            {
                if (!string.IsNullOrEmpty(treeReourceType.SelectedNode.Value))
                {
                    var ResourceType = treeReourceType.SelectedNode.Value;

                    if (ResourceType == string.Empty || int.Parse(ResourceType) == 0)
                    {
                        grdKhachHang.DataSource = resourceService.FindByFullName(search, true);
                    }
                    else
                    {
                        int type = int.Parse(ResourceType);
                        grdKhachHang.DataSource = resourceService.FindByFullName(search,type,true);
                    }
                    if (isbind)
                        grdKhachHang.DataBind();
                }
            }
            else
            {
                grdKhachHang.DataSource = resourceService.FindByFullName(search,true);

                if (isbind)
                    grdKhachHang.DataBind();
            }
        }
        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadTreeView1_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Node.Value))
            {               
                LoadResourceByResourceType(true);
            }
        }

        /// <summary>
        /// The load control.
        /// </summary>
        /// <param name="ct">
        /// The ct.
        /// </param>
        //private void loadControl(Control ct)
        //{
        //    if (!EDMsResources.Controls.Contains(ct))
        //    {
        //        //EDMsResources.Controls.Remove(ct);
        //        EDMsResources.Controls.Add(ct);
        //    }


        //}

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdKhachHang_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            LoadResourceByResourceType();
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdKhachHang_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format("return ShowEditForm('{0}','{1}');",
                                                               e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex][
                                                                   "Id"], e.Item.ItemIndex);
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdKhachHang.MasterTableView.SortExpressions.Clear();
                grdKhachHang.MasterTableView.GroupByExpressions.Clear();
                grdKhachHang.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdKhachHang.MasterTableView.SortExpressions.Clear();
                grdKhachHang.MasterTableView.GroupByExpressions.Clear();
                grdKhachHang.MasterTableView.CurrentPageIndex = grdKhachHang.MasterTableView.PageCount - 1;
                grdKhachHang.Rebind();
            }            
            else
            {
                SearchResource(e.Argument, true);
            }
        }

        /// <summary>
        /// grdKhachHang Page Size Changed
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        //protected void grdKhachHang_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        //{
        //    grdKhachHang.PageSize = e.NewPageSize;
        //    var resourceService = new ResourceService();
        //    grdKhachHang.DataSource = resourceService.GetAll();
        //    grdKhachHang.DataBind();
        //}

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdKhachHang_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var resourceID = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
            var resourceService = new ResourceService();
            resourceService.Delete(resourceID);
        }

        protected void grdKhachHang_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }
    }
}
