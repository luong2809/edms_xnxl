﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewerFilePDF_discipline.aspx.cs" Inherits="EDMs.Web.ViewerFilePDF_discipline" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">--%>
<!DOCTYPE html>
<html ng-app="app" ng-controller="LayoutCtrl" class="ng-scope">
<head runat="server">
    <%--<link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        //function CloseAndRefreshGrid() {
        //    var oWin = GetRadWindow();
        //    var parentWindow = oWin.BrowserWindow;
        //    $(oWin).ready(function () {
        //        oWin.close();
        //    });
        //    parentWindow.refreshGrid();
        //}

        //function GetRadWindow() {
        //    var oWindow = null;
        //    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including classic dialog
        //    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

        //    return oWindow;
        //}
        $('#PDFIframe').ready(function () {
            setTimeout(function () {
                $('#PDFIframe').contents().find('#download').remove();
                $('#PDFIframe').contents().find('#print').remove();
            }, 100);
        });

        $(window).load(function () {

            $('#PDFIframe').ready(function () {

                $('#PDFIframe').contents().find('#download').css("display", "none");
                $('#PDFIframe').contents().find('#print').css("display", "none");

            });
        });

    </script>
    <style type="text/css">
        
      
    </style>--%>
     <meta charset="UTF-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script async="" src="https://www.google-analytics.com/analytics.js"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="~/Scripts/pdfobject.min.js"></script>
  <title>viewer using PDFObject and HTML5 </title>
  <style>
	.pdfobject-container { height: 500px;}
	.pdfobject { border: 1px solid #666; }
  </style>
</head>
<body>
<%--    <form id="form1" runat="server">--%>
   <%--     <div class="content">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
          <%-- 
            <div id="viewer" style="width: 100%; height: 100%;align-content:center;">
             <cc1:ShowPdf ID="ShowPdf1" runat="server" BorderStyle="Inset" BorderWidth="2px" FilePath="../../DocumentLibrary/SharedDoc/A1/CTC1-002-GEN-KM3-SP-01_0_SPECIFICATIONS FOR STEEL MATERIAL.PDF"
            Height="500px" Style="z-index: 50; left: 24px; position: absolute; top: 50px"
            Width="99%" />
      </div>--%>
            <%--<telerik:GridDropDownListColumnEditor ID="ddlRevision" runat="server"
                    DropDownStyle-Width="110px">
            </telerik:GridDropDownListColumnEditor>--%>
         
        <%--  <div id="Add" runat="server" style=" float: left; padding-left:5px; padding-top:5px; width:99%;">--%>
                           <%-- <asp:Panel ID="Panel1" runat="server">
                             <iframe id="PDFIframe" runat="server" frameborder="0"  style= "width :100%; height:600px;" ></iframe>
                             <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" > </telerik:RadAjaxLoadingPanel>
                             </asp:Panel>
                           </div>--%>
       <%-- <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />--%>
       <%-- <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                      
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>--%>
            

<%--            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
            <Windows>
                 <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Viewer File"
                VisibleStatusbar="false" Height="690" Width="650" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>
        </div>--%>
       <%-- <telerik:RadCodeBlock runat="server">
            <script type="text/javascript">
                
                //function ShowLabelContent() {
                //    var x = document.getElementById('print');
                //    x.hidden();
                //    var y = document.getElementById('download');
                //    y.hidden();

                //    $('#print').hide();
                //    $('#download').hidden();
                //}


                //onload = ShowLabelContent;

                   
                 
                
                //function refreshGrid(arg) {
                //    //alert(arg);
                //    if (!arg) {
                //       // ajaxManager.ajaxRequest("Rebind");
                //    }
                //    else {
                //       // ajaxManager.ajaxRequest("RebindAndNavigate");
                //    }
                //}
            </script>
        </telerik:RadCodeBlock>--%>
         <div class="container" style="padding:10px 10px;">
        <div id="pdf_view" runat="server" class=" pdfobject-container"></div>
             </div>
      
   <%-- </form>--%>
    <script type="text/javascript">
  $(document).ready(function(){
	$('#header').load('../header-ads.html');
    $('#footer').load('../footer-ads.html');
    PDFObject.embed("pdfobject_sample.pdf", "#pdf_view");
  });
</script>
</body>
</html>
