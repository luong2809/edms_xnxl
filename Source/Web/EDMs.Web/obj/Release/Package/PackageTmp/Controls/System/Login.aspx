﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="EDMs.Web.Login" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">--%>
<!DOCTYPE html>
<!-- saved from url=(0108)http://backup-server.local/ttconfigsvc/spfauthentication/oauth/login?signin=927e2d821274c03b992d8c11cb07d3d1 -->
<html> <%--ng-app="app" ng-controller="LayoutCtrl" class="ng-scope">--%>
<head runat="server"> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}
</style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../CSS/Login.css" rel="stylesheet" type="text/css" />
    
</head>
<body style="overflow:hidden;" lang="en">
  
     <div class="hi-wrapper">
        <div class="hi-branding"></div>
        <div class="hi-content page-login">
            <div class="current-user ng-hide" ng-show="model.currentUser">
                <%--You are currently logged in as: <a href="http://backup-server.local/ttconfigsvc/spfauthentication/oauth/login?signin=927e2d821274c03b992d8c11cb07d3d1#" class="dropdown-toggle ng-binding" data-toggle="dropdown"> <b class="caret"></b></a>
                <ul class="dropdown-menu pull-right" role="menu" style="margin-right:25px;">
                    <li><a href="http://backup-server.local/ttconfigsvc/spfauthentication/oauth/logout">Logout</a></li>
                    <li class="divider ng-hide" ng-show="model.loginWithDifferentAccountUrl"></li>
                    <li><a href="http://backup-server.local/ttconfigsvc/spfauthentication/oauth/login?signin=927e2d821274c03b992d8c11cb07d3d1" ng-show="model.loginWithDifferentAccountUrl" class="ng-hide">Login With Different Account</a></li>
                </ul>--%>
            </div>
            <div class="page-header">
   <%-- <h2>Intergraph Authorization Server</h2>--%>
                 <img src="../../Images/Logo_SPF.png" width="95%" height="100%" style="margin-top:15px;" alt="The A"/>
</div>
<div class="content">
    <div class="container-fluid">
        <div class="row ng-hide" ng-show="model.errorMessage">
            <div class="col-md-12 col-sm-12">
                <div ng-show="model.errorMessage" class="alert alert-danger ng-binding ng-hide">
                    <strong>Error:</strong>
                    <asp:Label ID="lblMessage_1" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12" ng-show="model.loginUrl">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        <form name="form" runat="server" method="post" class="ng-pristine ng-invalid ng-invalid-required">
                            <input type="hidden" name="idsrv.xsrf" value="j3zUNhPiO28hmp19AD6gHnS7cHNwxHS46ypUJylGayLqjHX6PbGOLd5BC9NtmBgaBl84SxCmZs_hklmC4y9wr-jcpmRjdRNKUbyoJrRmtR8" token="model.antiForgery" class="ng-isolate-scope">
                            <fieldset>
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <%--<input required="" name="username" autofocus="" id="username" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Username" ng-model="model.username" maxlength="100">--%>
                                     <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control ng-dirty ng-valid ng-valid-required" ></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <%--<input required="" id="password" name="password" type="password" class="form-control ng-pristine ng-invalid ng-invalid-required" placeholder="Password" ng-model="model.password" maxlength="100" autocomplete="off">--%>
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control ng-dirty ng-valid ng-valid-required" TextMode="Password"></asp:TextBox>
                                </div>
                               <%-- <div class="form-group login-remember" ng-show="model.allowRememberMe">
                                    <label for="rememberMe">
                                        <input type="checkbox" id="rememberMe" name="rememberMe" ng-model="model.rememberMe" value="true" class="ng-pristine ng-valid">
                                        <strong>Remember My Login</strong>
                                    </label>
                                </div>--%>
                                <div class="actions">
                                    <%--<button class="btn btn-primary">Login</button>--%>
                                      <asp:Button ID="btnLogin" runat="server" Text="Login"  CssClass="btn btn-primary" OnClick="btnLogin_Click" />
                                </div>
                                 <div><asp:Label ID="lblMessage" runat="server" CssClass="login-error"></asp:Label></div>
                            </fieldset>
                        </form>
                    </div>
                    <ul class="list-unstyled">
                        <!-- ngRepeat: link in model.additionalLinks -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 external-providers ng-hide" ng-show="model.externalProviders">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">External Login</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-inline">
                            <!-- ngRepeat: provider in model.externalProviders -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

            <div class="footer">
            </div>
        </div>
    </div>
   <%-- <script id="modelJson" type="application/json">
        {&quot;loginUrl&quot;:&quot;/ttconfigsvc/spfauthentication/oauth/login?signin=927e2d821274c03b992d8c11cb07d3d1&quot;,&quot;antiForgery&quot;:{&quot;name&quot;:&quot;idsrv.xsrf&quot;,&quot;value&quot;:&quot;j3zUNhPiO28hmp19AD6gHnS7cHNwxHS46ypUJylGayLqjHX6PbGOLd5BC9NtmBgaBl84SxCmZs_hklmC4y9wr-jcpmRjdRNKUbyoJrRmtR8&quot;},&quot;allowRememberMe&quot;:true,&quot;rememberMe&quot;:false,&quot;username&quot;:null,&quot;externalProviders&quot;:[],&quot;additionalLinks&quot;:null,&quot;errorMessage&quot;:null,&quot;requestId&quot;:&quot;e8a2893e-3840-4605-81d7-b7933a9c5946&quot;,&quot;siteUrl&quot;:&quot;http://backup-server.local/ttconfigsvc/spfauthentication/oauth/&quot;,&quot;siteName&quot;:&quot;Intergraph Authorization Server&quot;,&quot;currentUser&quot;:null,&quot;logoutUrl&quot;:&quot;http://backup-server.local/ttconfigsvc/spfauthentication/oauth/logout&quot;}
    </script>--%>
   <%-- <script src="../../Scripts/Script.js"></script>--%>
    

</body>
</html>
