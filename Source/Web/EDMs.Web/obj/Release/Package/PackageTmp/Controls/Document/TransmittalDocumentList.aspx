﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransmittalDocumentList.aspx.cs" Inherits="EDMs.Web.Controls.Document.TransmittalDocumentList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../../Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        #grdDocumentPanel {
            height: 100% !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
        <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True" Height="100%"
            AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
            GridLines="None"
            Skin="Windows7"
            OnNeedDataSource="grdDocument_OnNeedDataSource"
            OnDetailTableDataBind="grdDocument_DetailTableDataBind"
            OnDeleteCommand="grdDocument_DeleteCommand"
            PageSize="30" Style="outline: none">
            <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%">
                <%--<GroupByExpressions>
                                <telerik:GridGroupByExpression>
                                    <SelectFields>
                                        <telerik:GridGroupByField FieldAlias="-" FieldName="DocumentTypeName" FormatString="{0:D}"
                                            HeaderValueSeparator=""></telerik:GridGroupByField>
                                    </SelectFields>
                                    <GroupByFields>
                                        <telerik:GridGroupByField FieldName="DocumentTypeName" SortOrder="Ascending" ></telerik:GridGroupByField>
                                    </GroupByFields>
                                </telerik:GridGroupByExpression>
                            </GroupByExpressions>  --%>
                <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                <ColumnGroups>
                    <telerik:GridColumnGroup HeaderText="REVISION DETAILS" Name="RevisionDetails"
                        HeaderStyle-HorizontalAlign="Center" />
                    <telerik:GridColumnGroup HeaderText="OUTGOING TRANSMITTAL" Name="OutgoingTrans"
                        HeaderStyle-HorizontalAlign="Center" />
                    <telerik:GridColumnGroup HeaderText="INCOMING TRANSMITTAL" Name="IncomingTrans"
                        HeaderStyle-HorizontalAlign="Center" />
                    <telerik:GridColumnGroup HeaderText="ICA REVIEW DETAILS" Name="ICAReviews"
                        HeaderStyle-HorizontalAlign="Center" />
                </ColumnGroups>

                <DetailTables>
                    <telerik:GridTableView DataKeyNames="ID" Name="DocDetail" Width="100%"
                        AllowPaging="True" PageSize="10">
                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                        <Columns>
                            <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                <HeaderStyle Width="3%" />
                                <ItemStyle HorizontalAlign="Center" Width="3%" />
                                <ItemTemplate>
                                    <a href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>'
                                        download='<%# DataBinder.Eval(Container.DataItem, "FileName") %>' target="_blank">
                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ExtensionIcon") %>'
                                            Style="cursor: pointer;" ToolTip="Download document" />
                                    </a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridBoundColumn DataField="FileName" HeaderText="File name" UniqueName="FileName">
                                <HeaderStyle HorizontalAlign="Center" Width="50%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="CreatedByUser" HeaderText="Upload by" UniqueName="CreatedByUser">
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Upload time" UniqueName="CreatedDate"
                                DataFormatString="{0:dd/MM/yyyy hh:mm tt}">
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                </DetailTables>

                <Columns>
                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                    <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" HeaderTooltip="Delete document"
                        ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                        <HeaderStyle Width="25" />
                        <ItemStyle HorizontalAlign="Center" Width="25" />
                    </telerik:GridButtonColumn>
                    <telerik:GridTemplateColumn HeaderText="No." Groupable="False">
                        <HeaderStyle HorizontalAlign="Center" Width="2%" VerticalAlign="Middle"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblSoTT" runat="server" Text='<%# grdDocument.CurrentPageIndex * grdDocument.PageSize + grdDocument.Items.Count+1 %>'>
                            </asp:Label>

                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="DOC. No." UniqueName="DocNo"
                        DataField="DocNo" ShowFilterIcon="False" FilterControlWidth="97%"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                        <HeaderStyle HorizontalAlign="Center" Width="220" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Eval("DocNo") %>' Style="cursor: pointer" />

                            <asp:Image ID="newicon" runat="server" ImageUrl="~/Images/new.png" Visible='<%# (DateTime.Now - Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "UpdatedAttachFile"))).TotalHours < 24 %>' />

                            <%--<telerik:RadToolTip Skin="Simple" runat="server" ID="dirNameToolTip" RelativeTo="Element" 
                                            AutoCloseDelay="10000" ShowDelay="0" Position="BottomRight"
                                            Width="400px" Height="150px" HideEvent="LeaveTargetAndToolTip"  TargetControlID="lblDocNo" IsClientID="False"
                                            Animation="Fade" Text='<%# "<b >Transmittals Information </b> <br/>" 
                                                                    + "Incoming trans no.: " + Eval("IncomingTransNo") + "<br/>"
                                                                    + "Date: " + Eval("IncomingTransDate","{0:dd/MM/yyyy}") + "<hr/>" + "<br/>"
                                                                    + "Outgoing trans no.: " + Eval("OutgoingTransNo") + "<br/>"
                                                                    + "Date: " + Eval("OutgoingTransDate","{0:dd/MM/yyyy}") + "<hr/>" + "<br/>"
                                                                    + "ICA review in/out trans no.: " + Eval("ICAReviewOutTransNo") + "<br/>"
                                                                    + "Date: " + Eval("ICAReviewReceivedDate","{0:dd/MM/yyyy}") + "<br/>"
                                                                    + "Review code: " + Eval("ICAReviewCode","{0:dd/MM/yyyy}") + "<br/>"%>'>
                                         </telerik:RadToolTip>--%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:HiddenField ID="DocNo" runat="server" Value='<%# Eval("DocNo") %>' />
                            <asp:Label runat="server" ID="Label1"></asp:Label>
                            <%--<asp:TextBox ID="txtDocNo" runat="server" Width="100%"></asp:TextBox>--%>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Vendor Doc No." UniqueName="AnotherDocNo"
                        DataField="AnotherDocNo" ShowFilterIcon="False" FilterControlWidth="213"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                        <HeaderStyle HorizontalAlign="Center" Width="200" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <%# Eval("AnotherDocNo") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="DOC. Title" UniqueName="DocTitle"
                        DataField="DocTitle" ShowFilterIcon="False" FilterControlWidth="213"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                        <HeaderStyle HorizontalAlign="Center" Width="220" />
                        <ItemStyle HorizontalAlign="Left" Width="220" />
                        <ItemTemplate>
                            <%# Eval("DocTitle") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Rev." UniqueName="Rev"
                        DataField="RevisionName" ShowFilterIcon="False" FilterControlWidth="43"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo">
                        <HeaderStyle HorizontalAlign="Center" Width="50" />
                        <ItemStyle HorizontalAlign="Center" Width="50" />
                        <ItemTemplate>
                            <%# Eval("RevisionName") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Deadline" UniqueName="PlanedDate"
                        AllowFiltering="false">
                        <HeaderStyle HorizontalAlign="Center" Width="80" />
                        <ItemStyle HorizontalAlign="Center" Width="80" />
                        <ItemTemplate>
                            <%# Eval("PlanedDate","{0:dd/MM/yyyy}") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Department" UniqueName="DeparmentName"
                        DataField="DeparmentName" ShowFilterIcon="False" FilterControlWidth="97%"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                        <HeaderStyle HorizontalAlign="Center" Width="150" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <%# Eval("DeparmentName") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Discipline" UniqueName="DisciplineName" DataField="DisciplineName" ShowFilterIcon="False" FilterControlWidth="97%"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                        <HeaderStyle HorizontalAlign="Center" Width="150" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <%# Eval("DisciplineName")%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Document Type" UniqueName="DocumentTypeName" DataField="DocumentTypeName" ShowFilterIcon="False" FilterControlWidth="97%"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                        <HeaderStyle HorizontalAlign="Center" Width="150" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <%# Eval("DocumentTypeName")%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                <Selecting AllowRowSelect="true" />
                <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
            </ClientSettings>
        </telerik:RadGrid>

        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadCodeBlock runat="server">
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
