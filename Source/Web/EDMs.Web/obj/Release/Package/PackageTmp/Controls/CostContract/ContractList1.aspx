﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContractList1.aspx.cs" Inherits="EDMs.Web.Controls.CostContract.ContractList1" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <!--[if gte IE 8]>
        <style type="text/css">
            #ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important!important}
            #ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important!important}
        </style>
    <![endif]-->

    <style type="text/css">
        /*Custom CSS of Grid documents for FF browser*/
        #ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important!important}
        #ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important!important}
        /*End*/
        @-moz-document url-prefix() {
            #ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important!important}
            #ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important!important}
        }
        
        #RAD_SPLITTER_PANE_CONTENT_ctl00_rightPane {
            overflow: hidden !important!important
        }
        .RadGrid_Windows7 .rgGroupHeader {
            line-height: 19px !important!important
        }
        .rgExpandCol {
            width: 1% !important!important
        }

        .rgGroupCol {
            width: 1% !important!important
        }

        #ContentPlaceHolder2_grdDocument_ctl00_ctl02_ctl03_txtDate_popupButton {
            display: none!important
        }

        .rgExpandCol {
            width: 1% !important!important
        }

        .rgGroupCol {
            width: 1% !important!important
        }

        .RadGrid .rgSelectedRow
        {
            background-image : none !important!important
            background-color: coral !important!important
        }

        .rgMasterTable .rgClipCells .rgClipCells {
            table-layout: auto !important!important
        }

        .rgGroupCol
        {
            padding-left: 0 !important!important
            padding-right: 0 !important!important
            font-size:1px !important!important
        }
 
        div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none!important 
        }
        /*#RAD_SPLITTER_PANE_CONTENT_ctl00_leftPane {
            width: 250px !important!important
        }
        #RAD_SPLITTER_PANE_CONTENT_ctl00_topLeftPane {
            width: 250px !important!important
        }*/

        .RadAjaxPanel {
            height: 100% !important!important
        }

        .rpExpandHandle {
            display: none !important!important
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 0px !important!important
            padding-right: 0px !important!important
        }

        /*Hide change page size control*/
        /*div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none!important        sssssss
        }*/    

        a.tooltip
        {
            outline: none!important
            text-decoration: none!important
        }

            a.tooltip strong
            {
                line-height: 30px!important
            }

            a.tooltip:hover
            {
                text-decoration: none!important
            }

            a.tooltip span
            {
                z-index: 10!important
                display: none!important
                padding: 14px 20px!important
                margin-top: -30px!important
                margin-left: 5px!important
                width: 240px!important
                line-height: 16px!important
            }

            a.tooltip:hover span
            {
                display: inline!important
                position: absolute!important
                color: #111!important
                border: 1px solid #DCA!important
                background: #fffAF0!important
            }

        .callout
        {
            z-index: 20!important
            position: absolute!important
            top: 30px!important
            border: 0!important
            left: -12px!important
        }

        /*CSS3 extras*/
        a.tooltip span
        {
            border-radius: 4px!important
            -moz-border-radius: 4px!important
            -webkit-border-radius: 4px!important
            -moz-box-shadow: 5px 5px 8px #CCC!important
            -webkit-box-shadow: 5px 5px 8px #CCC!important
            box-shadow: 5px 5px 8px #CCC!important
        }

        .rgMasterTable {
            table-layout: auto!important
        }

        
        #ctl00_ContentPlaceHolder2_radTreeFolder {
            overflow: visible !important!important
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel
        {
            height: 100% !important!important
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5
        {
            height: 100% !important!important
        }

        #divContainerLeft
        {
            width: 25%!important
            float: left!important
            margin: 5px!important
            height: 99%!important
            border-right: 1px dotted green!important
            padding-right: 5px!important
        }

        #divContainerRight
        {
            width: 100%!important
            float: right!important
            margin-top: 5px!important
            height: 99%!important
        }

        .dotted
        {
            border: 1px dotted #000!important
            border-style: none none dotted!important
            color: #fff!important
            background-color: #fff!important
        }

        .exampleWrapper
        {
            width: 100%!important
            height: 100%!important
            /*background: transparent url(images/background.png) no-repeat top left!important*/
            position: relative!important
        }

        .tabStrip
        {
            position: absolute!important
            top: 0px!important
            left: 0px!important
        }

        .multiPage
        {
            position: absolute!important
            top: 30px!important
            left: 0px!important
            color: white!important
            width: 100%!important
            height: 100%!important
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow
        {
            z-index: 8000 !important!important
        }

        .TemplateMenu
        {
            z-index: 10!important
        }

        /*div.RadComboBox .rcbInputCell .rcbInput
        {
            padding-left: 22px!important
        }*/
    </style>
    <div style="width: 98%!important padding-top: 10px!important padding-left: 5px">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/project.png" /> Selected project:<br />
        
        <telerik:RadComboBox ID="ddlProject" runat="server" 
            Skin="Windows7" Width="100%" AutoPostBack="True" 
            OnItemDataBound="ddlProject_ItemDataBound"
            OnSelectedIndexChanged="ddlProject_SelectedIndexChanged"/>
        <br />
        <hr/>
        Procurement Requirement:
        <telerik:RadTreeView ID="rtvPR" runat="server" 
            Width="100%" Height="100%" ShowLineImages="False"
            OnNodeClick="rtvPR_NodeClick" 
            OnNodeDataBound="rtvPR_NodeDataBound"
            OnClientNodeClicking="rtvPR_ClientNodeClicking">
        </telerik:RadTreeView>
    </div>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal">
        <telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">
            
            <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <telerik:RadToolBarDropDown runat="server" Text="Add" ImageUrl="~/Images/addNew.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Contract" Value="1" ImageUrl="~/Images/contract.png"></telerik:RadToolBarButton>
                            <%--<telerik:RadToolBarButton runat="server" Text="Multi documents" Value="2" ImageUrl="~/Images/addmulti.png"></telerik:RadToolBarButton>--%>
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true"/>
                    
                    <telerik:RadToolBarDropDown runat="server" Text="Action" ImageUrl="~/Images/action.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Send mail" Value="3" ImageUrl="~/Images/email.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" Text="Download multi documents" Value="4" ImageUrl="~/Images/download.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                            <telerik:RadToolBarButton runat="server" Text="Export master list template" Value="Adminfunc" ImageUrl="~/Images/export.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" Text="Import document master list" Value="Adminfunc" ImageUrl="~/Images/import.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true"  Value="Adminfunc"/>
                            <telerik:RadToolBarButton runat="server" Text="Export report" Value="7" ImageUrl="~/Images/emdrreport.png"/>
                            <telerik:RadToolBarButton runat="server" Text="Import report" Value="Adminfunc" ImageUrl="~/Images/upload.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="False"/>
                            
                            <telerik:RadToolBarButton runat="server" Text="Attach multi document file" Value="Adminfunc" ImageUrl="~/Images/uploadmulti.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Value="Adminfunc" />
                            <telerik:RadToolBarButton runat="server" Text="Share documents" Value="Adminfunc" ImageUrl="~/Images/share.png" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Value="Adminfunc" Visible="False"/>
                            <telerik:RadToolBarButton runat="server" Text="Delete all selected documents" Value="Adminfunc" ImageUrl="~/Images/delete.png"  Visible="False"/>
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    
                    <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="False"/>
                    <telerik:RadToolBarButton runat="server" Value="ShowAll" Visible="False">
                        <ItemTemplate>
                            <asp:CheckBox ID="ckbShowAll" runat="server" Text="Show all Document revision." AutoPostBack="True" OnCheckedChanged="ckbShowAll_CheckedChange"/>
                        </ItemTemplate>
                    </telerik:RadToolBarButton>

                    <telerik:RadToolBarButton runat="server" Visible="False">
                        <ItemTemplate>
                            Work status: Complete 
                            <telerik:radnumerictextbox type="Percent" id="txtDisciplineComplete" runat="server" Width="60px" CssClass="min25Percent" ReadOnly="True">
                                <NumberFormat DecimalDigits="2"></NumberFormat>
                            </telerik:radnumerictextbox>
                            | Weight 
                            <telerik:radnumerictextbox type="Percent" id="txtDisciplineWeight" runat="server" Width="60px" CssClass="min25Percent" ReadOnly="True">
                                <NumberFormat DecimalDigits="2"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton ImageUrl="~/Images/save.png" Text="Update" Value="search" CommandName="UpdatePackageStatus" Visible="False" />
                    
                    <telerik:RadToolBarDropDown runat="server" Text="Clear" ImageUrl="~/Images/clearEMDR.png" Visible="False">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Clear EMDR data" Value="12" ImageUrl="~/Images/clearEMDR.png" />
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                </Items>
            </telerik:RadToolBar>
            
        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None" >
                    <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                        <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                            <telerik:RadGrid AllowCustomPaging="False" AllowPaging="True" AllowSorting="True" 
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None" 
                                Height="100%" ID="grdDocument"  AllowFilteringByColumn="True" AllowMultiRowSelection="False"
                                OnDeleteCommand="grdDocument_DeleteCommand" 
                                OnItemCommand="grdDocument_ItemCommand" 
                                OnItemDataBound="grdDocument_ItemDataBound" 
                                OnNeedDataSource="grdDocument_OnNeedDataSource" 
                                OnUpdateCommand="grdDocument_UpdateCommand"
                                PageSize="100" runat="server" Style="outline: none" Width="100%">
                                <SortingSettings SortedBackColor="#FFF6D6"></SortingSettings>
                                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                <MasterTableView AllowMultiColumnSorting="false"
                                    ClientDataKeyNames="ID" DataKeyNames="ID" CommandItemDisplay="Top" 
                                    EditMode="InPlace" Font-Size="8pt">
                                    <%--<GroupByExpressions>
                                            <telerik:GridGroupByExpression>
                                                <SelectFields>
                                                    <telerik:GridGroupByField FieldAlias="_" FieldName="ProcurementRequirementNumber" FormatString="{0:D}"
                                                        HeaderValueSeparator=""></telerik:GridGroupByField>
                                                </SelectFields>
                                                <GroupByFields>
                                                    <telerik:GridGroupByField FieldName="ProcurementRequirementNumber" SortOrder="Ascending" ></telerik:GridGroupByField>
                                                </GroupByFields>
                                            </telerik:GridGroupByExpression>
                                        </GroupByExpressions> --%>   
                                    <CommandItemSettings  ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false"/>
                                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp!importantnbsp!importantPage &lt!importantstrong&gt!important{0}&lt!important/strong&gt!important / &lt!importantstrong&gt!important{1}&lt!important/strong&gt!important, Total:  &lt!importantstrong&gt!important{5}&lt!important/strong&gt!important Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ColumnGroups>
                                        <telerik:GridColumnGroup HeaderText="Contract Value (Not include 10% VAT)" Name="ContractValue"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                        <telerik:GridColumnGroup HeaderText="Payment Value" Name="Payment"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                        <telerik:GridColumnGroup HeaderText="Remain Value" Name="Remain"
                                             HeaderStyle-HorizontalAlign="Center"/>
                                    </ColumnGroups>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                        <telerik:GridBoundColumn DataField="HasAttachFile" UniqueName="HasAttachFile" Display="False" />
                                        <telerik:GridClientSelectColumn UniqueName="IsSelected" Display="False">
                                            <HeaderStyle Width="25"  />
                                            <ItemStyle HorizontalAlign="Center" Width="25"/>
                                        </telerik:GridClientSelectColumn>

                                        <telerik:GridTemplateColumn HeaderText="No." Groupable="False" AllowFiltering="false">
                                            <HeaderStyle HorizontalAlign="Center" Width="30" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Width="30"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSoTT" runat="server" Text='<%# grdDocument.CurrentPageIndex * grdDocument.PageSize + grdDocument.Items.Count+1 %>'>
                                                </asp:Label>
                                      
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="EditColumn" Display="False">
                                                <HeaderStyle Width="30"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>, <%# DataBinder.Eval(Container.DataItem, "ProjectId") %>)' style="text-decoration: none!important color:blue">
                                                    <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer!important" AlternateText="Edit properties" />
                                                        <a/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                    <telerik:GridButtonColumn Display="False" UniqueName="DeleteColumn" CommandName="Delete" HeaderTooltip="Delete document"
                                            ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                            <HeaderStyle Width="25" />
                                            <ItemStyle HorizontalAlign="Center" Width="25"  />
                                        </telerik:GridButtonColumn>
                                            
                                    <telerik:GridTemplateColumn HeaderText="PR Number" UniqueName="ProcurementRequirementNumber"
                                        DataField="ProcurementRequirementNumber" ShowFilterIcon="False" FilterControlWidth="97%" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                        <HeaderStyle HorizontalAlign="Center" Width="160" />
                                        <ItemStyle HorizontalAlign="Left" Width="160" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Eval("ProcurementRequirementNumber") %>' style="cursor: pointer!important color: green"/> 
                                            <telerik:RadToolTip Skin="Simple" runat="server" ID="dirNameToolTip" RelativeTo="Element" Title="Procurement Requirement Information"
                                            AutoCloseDelay="10000" ShowDelay="0" Position="BottomRight"
                                            Width="400px" Height="150px" HideEvent="LeaveTargetAndToolTip"  TargetControlID="lblDocNo" IsClientID="False"
                                            Animation="Fade" Text='<%# "<b>Number:</b> " + Eval("PRObj.Number") + "<br/>" +
                                                                       "<b>Description:</b> " + (!string.IsNullOrEmpty(Eval("PRObj.Description").ToString()) 
                                                                                        ? Eval("PRObj.Description").ToString().Replace("\n", "<br/>")
                                                                                        : string.Empty) + "<br/>"
                                                                    + "<b>Code:</b> " + Eval("PRObj.Code") + "<br/>"
                                                                    + "<b>Main Perform:</b> " + Eval("PRObj.MainOwnerName") + "<br/>"
                                                                    + "<b>Contractor Selection Type:</b> " + Eval("PRObj.ContractorChoiceTypeName") + "<br/>"
                                                                    + "<b>Procurement Plan:</b/> " + Eval("PRObj.ProcurementPlanValue", "{0:$ ###,##0.##}") + "<br/>"
                                                                    + "<b>Procurement Req: </b>" + Eval("PRObj.ProcurementRequirementValue", "{0:$ ###,##0.##}") + "<br/>"
                                                                    + "<b>USD Exchange: </b>" + Eval("PRObj.USDExchangeValue", "{0:$ ###,##0.##}") + "<br/>"%>'>
                                         </telerik:RadToolTip>

                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                            
                                    <telerik:GridTemplateColumn HeaderText="Contract Number" UniqueName="Number"
                                        DataField="Number" ShowFilterIcon="False" FilterControlWidth="97%" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                        <HeaderStyle HorizontalAlign="Center" Width="160" />
                                        <ItemStyle HorizontalAlign="Left"/>
                                        <ItemTemplate>
                                            <%# Eval("Number") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn HeaderText="Contractor Selected" UniqueName="ContractorSelectedName"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%# Eval("ContractorSelectedName") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                    <telerik:GridTemplateColumn HeaderText="Contract Content" UniqueName="ContractContent"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="220" />
                                        <ItemStyle HorizontalAlign="Left"  />
                                        <ItemTemplate>
                                            <%# Eval("ContractContent") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                    <telerik:GridTemplateColumn HeaderText="Delivery Date" UniqueName="DeliveryDate"
                                        AllowFiltering="false" >
                                        <HeaderStyle HorizontalAlign="Center" Width="80" />
                                        <ItemStyle HorizontalAlign="Center" Width="80"/>
                                        <ItemTemplate>
                                            <%# Eval("DeliveryDate","{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Delivery Status" UniqueName="DeliveryStatus"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="180" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%# Eval("DeliveryStatus") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Effected Date" UniqueName="EffectedDate"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="80" />
                                        <ItemStyle HorizontalAlign="Center" Width="80"/>
                                        <ItemTemplate>
                                            <%# Eval("EffectedDate","{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="End Date" UniqueName="EndDate"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="80" />
                                        <ItemStyle HorizontalAlign="Center" Width="80"/>
                                        <ItemTemplate>
                                            <%# Eval("EndDate","{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="Contract Type" UniqueName="ContractTypeName"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="100" />
                                        <ItemStyle HorizontalAlign="Center"/>
                                        <ItemTemplate>
                                            <%# !string.IsNullOrEmpty(Eval("ContractTypeName").ToString()) 
                                                    ? Eval("ContractTypeName").ToString().Replace("\n", "<br/>")
                                                    : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="Contract Status" UniqueName="ContractStausName"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="100" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%# Eval("ContractStausName") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                    <telerik:GridTemplateColumn HeaderText="Exchange Rate" UniqueName="ExchangeRate"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="100" />
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <%# Eval("ExchangeRate") != null && Convert.ToDouble(Eval("ExchangeRate")) != 0.0
                                                ? Eval("ExchangeRate", "{0:VND ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    

                                    <telerik:GridTemplateColumn HeaderText="VND" UniqueName="ContractValueVND"
                                        AllowFiltering="false" ColumnGroupName="ContractValue">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right"/>
                                        <ItemTemplate>
                                            <%# Eval("ContractValueVND") != null && Convert.ToDouble(Eval("ContractValueVND")) != 0.0
                                                ? Eval("ContractValueVND", "{0:VND ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="USD" UniqueName="ContractValueUSD"
                                        AllowFiltering="false" ColumnGroupName="ContractValue">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right"  />
                                        <ItemTemplate>
                                            <%# Eval("ContractValueUSD") != null && Convert.ToDouble(Eval("ContractValueUSD")) != 0.0
                                                ? Eval("ContractValueUSD", "{0:$ ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="Arising Value" UniqueName="ArisingTotalValue"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <%# Eval("ArisingTotalValue") != null && Convert.ToDouble(Eval("ArisingTotalValue")) != 0.0
                                                ? Eval("ArisingTotalValue", "{0:$ ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="VND" UniqueName="PaymentedValueVND"
                                        AllowFiltering="false" ColumnGroupName="Payment">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right"/>
                                        <ItemTemplate>
                                            <%# Eval("PaymentedValueVND") != null && Convert.ToDouble(Eval("PaymentedValueVND")) != 0.0
                                                ? Eval("PaymentedValueVND", "{0:VND ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="USD" UniqueName="PaymentedValueUSD"
                                        AllowFiltering="false" ColumnGroupName="Payment">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right"/>
                                        <ItemTemplate>
                                            <%# Eval("PaymentedValueUSD") != null && Convert.ToDouble(Eval("PaymentedValueUSD")) != 0.0
                                                ? Eval("PaymentedValueUSD", "{0:$ ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="USD Exchange" UniqueName="PaymentValueUSDExchange"
                                        AllowFiltering="false" ColumnGroupName="Payment">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right"/>
                                        <ItemTemplate>
                                            <%# Eval("PaymentValueUSDExchange") != null && Convert.ToDouble(Eval("PaymentValueUSDExchange")) != 0.0
                                                ? Eval("PaymentValueUSDExchange", "{0:$ ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="VND" UniqueName="RemainPaymentVND"
                                        AllowFiltering="false" ColumnGroupName="Remain">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right"/>
                                        <ItemTemplate>
                                            <%# Eval("RemainPaymentVND") != null && Convert.ToDouble(Eval("RemainPaymentVND")) != 0.0
                                                ? Eval("RemainPaymentVND", "{0:VND ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="USD" UniqueName="RemainPaymentUSD"
                                        AllowFiltering="false" ColumnGroupName="Remain">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right"/>
                                        <ItemTemplate>
                                            <%# Eval("RemainPaymentUSD") != null && Convert.ToDouble(Eval("RemainPaymentUSD")) != 0.0
                                                ? Eval("RemainPaymentUSD", "{0:$ ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="USD Exchange" UniqueName="RemainPaymentUSDExchange"
                                        AllowFiltering="false" ColumnGroupName="Remain">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <%# Eval("RemainPaymentUSDExchange") != null && Convert.ToDouble(Eval("RemainPaymentUSDExchange")) != 0.0
                                                ? Eval("RemainPaymentUSDExchange", "{0:$ ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="Difference with PR Value" UniqueName="DeferenceWithPRValue"
                                        AllowFiltering="false" >
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <%# Eval("DeferenceWithPRValue") != null && Convert.ToDouble(Eval("DeferenceWithPRValue")) != 0.0
                                                ? Eval("DeferenceWithPRValue", "{0:$ ###,##0.##}")
                                                : string.Empty%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="Note" UniqueName="Note"
                                        AllowFiltering="false" >
                                        <HeaderStyle HorizontalAlign="Center" Width="250" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Eval("Note") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridTemplateColumn HeaderText="Complete - %" UniqueName="Complete" AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="65" />
                                        <ItemStyle HorizontalAlign="Center" Width="65" />
                                        <ItemTemplate>
                                            <%# Eval("Complete")!=null && Convert.ToDouble(Eval("Complete")) != 0 ?  Eval("Complete") + "%" : "-"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Weight - %" UniqueName="Weight" AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="65" />
                                        <ItemStyle HorizontalAlign="Center" Width="65" />
                                        <ItemTemplate>
                                            <%# Eval("Weight")!=null ?  Eval("Weight") + "%" : "-"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                            <ClientEvents OnRowContextMenu="RowContextMenu" OnRowClick="RowClick"></ClientEvents>
                            <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </telerik:RadPane>

                </telerik:RadSplitter>       

            </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadContextMenu ID="radMenu" runat="server"
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking" OnClientShowing="gridContextMenuShowing">
        <Items>
            <telerik:RadMenuItem Text="Edit properties" ImageUrl="~/Images/edit.png" Value="EditProperties"/>
            <telerik:RadMenuItem Text="Share Control" ImageUrl="~/Images/sharecontrol.png" Value="ShareControl"/>
            <telerik:RadMenuItem IsSeparator="True"/>
            <telerik:RadMenuItem Text="Attach Files" ImageUrl="~/Images/attach.png" Value="AttachDocument"/>
            <telerik:RadMenuItem Text="Material List" ImageUrl="~/Images/material.png" Value="MaterialList" />
            <telerik:RadMenuItem IsSeparator="True"/>
            <telerik:RadMenuItem Text="Payment History" ImageUrl="~/Images/payment1.png" Value="PaymentHistory" />
        </Items>
    </telerik:RadContextMenu>

    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="rtvPR">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu" />
                        <telerik:AjaxUpdatedControl ControlID="IsFullPermission" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ckbShowAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="radMenu">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="rtvPR" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu" LoadingPanelID="RadAjaxLoadingPanel2"/>
                      
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ddlProject">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ChildProject" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="rtvPR" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu"/>
                        <telerik:AjaxUpdatedControl ControlID="lblProjectId"/>

                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ChildProject">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rtvPR" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu"/>
                    </UpdatedControls>

                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ckbShowAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>
    <%--OnClientClose="refreshGrid"--%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Contract Information"
                VisibleStatusbar="false" Height="700" Width="650" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="Payment" runat="server" Title="Payment History"
                VisibleStatusbar="false" Height="650" Width="900" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="MaterialList" runat="server" Title="Material List"
                VisibleStatusbar="false" Height="475" Width="1024" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="ShareControl" runat="server" Title="Share Control"
                VisibleStatusbar="false" Height="600" Width="700" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="CommentWnd" runat="server" 
                VisibleStatusbar="false" Height="700" Width="900" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="ResponseWnd" runat="server" 
                VisibleStatusbar="false" Height="650" Width="1000" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="UploadMulti" runat="server" Title="Create multiple documents"
                VisibleStatusbar="false" Height="520" MinHeight="520" MaxHeight="520" Width="640" MinWidth="640" MaxWidth="640"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="RevisionDialog" runat="server" Title="Revision History"
                VisibleStatusbar="false" Height="550" Width="1250" MinHeight="550"  
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="TransmittalList" runat="server" Title="Transmittal List"
                VisibleStatusbar="false" Height="400" Width="1250" MinHeight="400" MinWidth="1250" MaxHeight="400" MaxWidth="1250" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="SendMail" runat="server" Title="Send mail"
                VisibleStatusbar="false" Height="560" Width="992" MinHeight="560" MinWidth="992" MaxHeight="560" MaxWidth="992" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach Contract files"
                VisibleStatusbar="false" Height="500" Width="700" MinHeight="500" MinWidth="700" MaxHeight="500" MaxWidth="700" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="AttachMulti" runat="server" Title="Attach multi document files"
                VisibleStatusbar="false" Height="500" Width="700" MinHeight="500" MinWidth="700" MaxHeight="500" MaxWidth="700" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="AttachComment" runat="server" Title="Attach Comment/Response"
                VisibleStatusbar="false" Height="500" Width="900" MinHeight="500" MinWidth="900" MaxHeight="500" MaxWidth="900" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="ImportData" runat="server" Title="Import master list"
                VisibleStatusbar="false" Height="400" Width="520" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="ImportEMDRReport" runat="server" Title="Import EMDR Report"
                VisibleStatusbar="false" Height="400" Width="520" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="ShareDocument" runat="server" Title="Share documents"
                VisibleStatusbar="false" Height="600" Width="520" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            

        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction"/>
    <asp:HiddenField runat="server" ID="lblDisciplineId"/>
    <asp:HiddenField runat="server" ID="lblFolderId"/>
    <asp:HiddenField runat="server" ID="lblDocId"/>
    <asp:HiddenField runat="server" ID="lblCategoryId"/>
    <asp:HiddenField runat="server" ID="IsFullPermission"/>
    <asp:HiddenField runat="server" ID="lblProjectId"/>

    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex"/>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../../Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">
            
            function OnClientSelectedIndexChanging(sender, eventArgs) {
                var input = sender.get_inputDomElement()!important
                input.style.background = "url(" + eventArgs.get_item().get_imageUrl() + ") no-repeat"!important
            }
            function OnClientLoad(sender) {
                var input = sender.get_inputDomElement()!important
                var selectedItem = sender.get_selectedItem()!important
                input.style.background = "url(" + selectedItem.get_imageUrl() + ") no-repeat"!important
            }

            var radDocuments!important

            function ShowFilter(obj) {
                if (obj.checked) {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().showFilterItem()!important
                } else {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().hideFilterItem()!important
                }
            }

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView()!important
                masterTable.rebind()!important
            }


            function ExportGrid() {
                var masterTable = $find("<%=grdDocument.ClientID %>").get_masterTableView()!important
                masterTable.exportToExcel('PIN_list.xls')!important
                return false!important
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender!important
            }

            function onRequestStart(sender, args)
            {
                 ////alert(args.get_eventTarget())!important
                 if (args.get_eventTarget().indexOf("ExportTo") >= 0 || args.get_eventTarget().indexOf("btnDownloadPackage") >= 0 ||
                     args.get_eventTarget().indexOf("ajaxCustomer") >= 0)
                 {
                     args.set_enableAjax(false)!important
                 }
             }

            function onColumnHidden(sender) {
                
                var masterTableView = sender.get_masterTableView().get_element()!important
                masterTableView.style.tableLayout = "auto"!important
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"!important }, 0)!important
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value!important

                radDocuments.get_masterTableView().showColumn(8)!important
                radDocuments.get_masterTableView().showColumn(9)!important
                radDocuments.get_masterTableView().showColumn(10)!important
                radDocuments.get_masterTableView().showColumn(11)!important
                
                radDocuments.get_masterTableView().showColumn(12)!important
                radDocuments.get_masterTableView().showColumn(13)!important
                radDocuments.get_masterTableView().showColumn(14)!important
                radDocuments.get_masterTableView().showColumn(15)!important

                if (selectedFolder != "") {
                    ajaxManager.ajaxRequest("ListAllDocuments")!important
                }
            }
            
            function OnClientDocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value!important

                radDocuments.get_masterTableView().hideColumn(8)!important
                radDocuments.get_masterTableView().hideColumn(9)!important
                radDocuments.get_masterTableView().hideColumn(10)!important
                radDocuments.get_masterTableView().hideColumn(11)!important
                
                radDocuments.get_masterTableView().hideColumn(12)!important
                radDocuments.get_masterTableView().hideColumn(13)!important
                radDocuments.get_masterTableView().hideColumn(14)!important
                radDocuments.get_masterTableView().hideColumn(15)!important
                
                if (selectedFolder != "") {
                    ajaxManager.ajaxRequest("TreeView")!important
                }
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID")!important
                document.getElementById("<%= lblDocId.ClientID %>").value = Id!important
            }

            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value()!important
                var contractId = document.getElementById("<%= lblDocId.ClientID %>").value!important
                var isFullPermission = document.getElementById("<%= IsFullPermission.ClientID %>").value!important

                var packageId = document.getElementById("<%= lblDisciplineId.ClientID %>").value!important
                
                
                switch (itemValue) {
                
                case "AttachDocument":
                    var owd = $find("<%=AttachDoc.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("AttachContractDocument.aspx?contractId=" + contractId, "AttachDoc")!important
                    break!important

                case "PaymentHistory":
                    var owd = $find("<%=Payment.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("PaymentHistoryPage.aspx?contractId=" + contractId, "Payment")!important
                    break!important

                case "MaterialList":
                    var owd = $find("<%=MaterialList.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("MaterialListByContract.aspx?contractId=" + contractId, "MaterialList")!important
                    break!important
                
                case "EditProperties":
                    var projectId = document.getElementById("<%= lblProjectId.ClientID %>").value!important
                    var owd = $find("<%=CustomerDialog.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("ContractEditForm.aspx?contractId=" + contractId + "&projId=" + projectId, "CustomerDialog")!important
                    break!important
                    case "ShareControl":
                    var projectId = document.getElementById("<%= lblProjectId.ClientID %>").value!important
                    var owd = $find("<%=ShareControl.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("PermissionContractPage.aspx?contractId=" + contractId + "&projId=" + projectId, "ShareControl")!important
                    break!important
                }
            }
            
            function onClientContextMenuItemClicking(sender, args) {
                var menuItem = args.get_menuItem()!important
                var treeNode = args.get_node()!important
                menuItem.get_menu().hide()!important

                switch (menuItem.get_value()) {
                case "Rename":
                    treeNode.startEdit()!important
                    break!important
                case "Delete":
                    var result = confirm("Are you sure you want to delete the folder: " + treeNode.get_text())!important
                    args.set_cancel(!result)!important
                    break!important
                }
            }
            
            function rtvExplore_OnNodeExpandedCollapsed(sender, eventArgs) {
                var allNodes = eventArgs._node.get_treeView().get_allNodes()!important

                var i!important
                var selectedNodes = ""!important

                for (i = 0!important i < allNodes.length!important i++) {
                    if (allNodes[i].get_expanded())
                        selectedNodes += allNodes[i].get_value() + "*"!important
                }

                Set_Cookie("expandedNodesObjTree", selectedNodes, 30)!important
            }

            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date()!important
                today.setTime(today.getTime())!important

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24!important
                }
                var expires_date = new Date(today.getTime() + (expires))!important

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? "!importantexpires=" + expires_date.toGMTString() : "") +
                    ((path) ? "!importantpath=" + path : "") +
                    ((domain) ? "!importantdomain=" + domain : "") +
                    ((secure) ? "!importantsecure" : "")!important
            }

            function gridContextMenuShowing(menu, args) {
                var isfullpermission = document.getElementById("<%= IsFullPermission.ClientID %>").value!important
                if (isfullpermission == "true") {
                    //menu.get_allItems()[0].enable()!important
                    //menu.get_allItems()[1].enable()!important
                    //menu.get_allItems()[3].enable()!important
                    //menu.get_allItems()[4].enable()!important
                    //menu.get_allItems()[6].enable()!important
                }
                else {
                    //menu.get_allItems()[0].disable()!important
                    //menu.get_allItems()[1].disable()!important
                    //menu.get_allItems()[3].enable()!important
                    //menu.get_allItems()[4].enable()!important
                    //menu.get_allItems()[6].enable()!important
                }
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar!important
            var searchButton!important
            var ajaxManager!important

            function pageLoad() {
                toolbar = $find("<%= CustomerMenu.ClientID %>")!important
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>")!important
            }

            function ShowEditForm(docId, projId) {
                var owd = $find("<%=CustomerDialog.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("ContractEditForm.aspx?projId=" + projId + "&docId=" + docId, "CustomerDialog")!important
            }
            
            function ShowUploadForm(id) {
                var owd = $find("<%=AttachDoc.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("Controls/Document/UploadDragDrop.aspx?docId=" + id, "AttachDoc")!important
            }
            
            function ShowRevisionForm(id) {
                var owd = $find("<%=RevisionDialog.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + id, "RevisionDialog")!important
            }

            function refreshGrid(arg) {
                if (!arg) {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView()!important
                    masterTable.rebind()!important
                }
                else {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView()!important
                    masterTable.rebind()!important
                }
            }
            
            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'))!important
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog")!important
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog")!important
            }
            
            function rtvPR_ClientNodeClicking(sender, args) {
                var DisciplineValue = args.get_node().get_value()!important
                document.getElementById("<%= lblDisciplineId.ClientID %>").value = DisciplineValue!important
            }
            
            function radPbCategories_OnClientItemClicking(sender, args)
            {
                var item = args.get_item()!important
                var categoryId = item.get_value()!important
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId!important
                
            }

            function OnClientButtonClicking(sender, args) {
                var button = args.get_item()!important
                var strText = button.get_text()!important
                var strValue = button.get_value()!important

                var grid = $find("<%= grdDocument.ClientID %>")!important
                var customerId = null!important
                var customerName = ""!important

                //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                //    var selectedRow = grid.get_masterTableView().get_selectedItems()[0]!important
                //    customerId = selectedRow.getDataKeyValue("Id")!important
                //    //customerName = selectedRow.Items["FullName"]!important 
                //    //customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML!important
                //}Send notifications window.open('file://Shared Folder/Users')

                if (strText.toLowerCase() == "send notifications") {
                    var grid = $find("<%=grdDocument.ClientID %>")!important
                    var masterTable = grid.get_masterTableView()!important
                    var number = 0!important
                    for (var i = 0!important i < masterTable.get_dataItems().length!important i++) {
                        var gridItemElement = masterTable.get_dataItems()[i].findElement("IsSelected")!important
                        if (gridItemElement.checked) {
                            number++!important
                        }
                    }
                    if (number == 0) {
                        alert("Please select documents to send notification")!important
                    }
                    else {
                        ajaxManager.ajaxRequest("SendNotification")!important
                    }
                }
                
                if (strText.toLowerCase() == "import document master list") {
                    var owd = $find("<%=ImportData.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("Controls/Document/ImportDocMasterList.aspx", "ImportData")!important
                }

                if (strText.toLowerCase() == "import emdr report") {
                    var owd = $find("<%=ImportEMDRReport.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("Controls/Document/ImportEMDR.aspx", "ImportEMDRReport")!important
                }
                
                if (strText.toLowerCase() == "send mail") {
                    var grid = $find("<%=grdDocument.ClientID %>")!important
                    var masterTable = grid.get_masterTableView()!important
                    var listId = ""!important
                    var selectedRows = masterTable.get_selectedItems()!important
                    if (selectedRows.length == 0) {
                        alert("Please select documents to send mail")!important
                    } else {
                        for (var i = 0!important i < selectedRows.length!important i++) {
                            var row = selectedRows[i]!important
                            //alert(row.getDataKeyValue("ID"))!important
                            listId += row.getDataKeyValue("ID") + ","!important
                        }
                        var owd = $find("<%=SendMail.ClientID %>")!important
                        owd.Show()!important
                        owd.setUrl("Controls/Document/SendMail.aspx?listDoc=" + listId, "SendMail")!important
                    }
                }

                if (strText.toLowerCase() == "share documents") {
                    var grid = $find("<%=grdDocument.ClientID %>")!important
                    var masterTable = grid.get_masterTableView()!important
                    
                    var listId = ""!important
                    
                    var selectedRows = masterTable.get_selectedItems()!important
                    if (selectedRows.length == 0) {
                        alert("Please select documents to share for Document library.")!important
                    }
                    else {
                        for (var i = 0!important i < selectedRows.length!important i++) {
                            var row = selectedRows[i]!important
                            //alert(row.getDataKeyValue("ID"))!important
                            listId += row.getDataKeyValue("ID") + ","!important
                        }
                        var owd = $find("<%=ShareDocument.ClientID %>")!important
                        owd.Show()!important
                        owd.setUrl("Controls/Document/ShareDocument.aspx?listDoc=" + listId, "ShareDocument")!important
                    }
                }

                if (strText.toLowerCase() == "delete all selected documents") {
                    var grid = $find("<%=grdDocument.ClientID %>")!important
                    var masterTable = grid.get_masterTableView()!important

                    var listId = ""!important

                    var selectedRows = masterTable.get_selectedItems()!important
                    if (selectedRows.length == 0) {
                        alert("Please select documents to delete.")!important
                    }
                    else {
                        if (confirm("Do you want to delete all selected documents?") == false) return!important

                        for (var i = 0!important i < selectedRows.length!important i++) {
                            var row = selectedRows[i]!important
                            //alert(row.getDataKeyValue("ID"))!important
                            listId += row.getDataKeyValue("ID") + ","!important
                        }

                        ajaxManager.ajaxRequest("DeleteAllDoc")!important
                    }
                }
                
                if (strText.toLowerCase() == "download multi documents") {
                    var grid = $find("<%=grdDocument.ClientID %>")!important
                    var masterTable = grid.get_masterTableView()!important
                    var number = 0!important
                    var listId = ""!important
                    for (var i = 0!important i < masterTable.get_dataItems().length!important i++) {
                        var gridItemElement = masterTable.get_dataItems()[i].findElement("IsSelected")!important
                        if (gridItemElement.checked) {
                            number++!important
                            listId += masterTable.get_dataItems()[i].getDataKeyValue("ID") + ","!important
                        }
                    }
                    if (number == 0) {
                        alert("Please select documents to download")!important
                    }
                    else {
                        ajaxManager.ajaxRequest("DownloadMulti")!important
                    }
                }
                
                if (strText.toLowerCase() == "clear emdr data") {
                    ajaxManager.ajaxRequest("ClearEMDRData")!important
                }

                if (strText.toLowerCase() == "export master list template") {
                    ajaxManager.ajaxRequest("ExportMasterList")!important
                }

                if (strText.toLowerCase() == "export report") {
                    ajaxManager.ajaxRequest("ExportEMDRReport_New")!important
                }

                if (strText.toLowerCase() == "attach multi document file") {
                    var owd = $find("<%=AttachMulti.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("Controls/Document/UploadMultiDocumentFile.aspx", "AttachMulti")!important
                }
                
                if (strText.toLowerCase() == "update") {
                    ajaxManager.ajaxRequest("UpdatePackageStatus")!important
                }

                //Download multi documents Export template data file Attach multi document file
                ////if (strText == "View explorer") {
                ////    window.open("file://WIN-P7KS57HL1HG/DocumentLibrary")!important
                ////}

                if (strText.toLowerCase() == "contract") {
                    
                    <%--var selectedDiscipline = document.getElementById("<%= lblDisciplineId.ClientID %>").value!important
                    if (selectedDiscipline == "") {
                        alert("Please choice one Discipline to add new Document.")!important
                        return false!important
                    }
                    else {--%>
                    var projectId = document.getElementById("<%= lblProjectId.ClientID %>").value!important
                    var owd = $find("<%=CustomerDialog.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("ContractEditForm.aspx?projId=" + projectId, "CustomerDialog")!important
                    //}
                }
                
                if (strText.toLowerCase() == "multi documents") {
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value!important
                    if (selectedFolder == "") {
                        alert("Please choice one folder to create new document.")!important
                        return false!important
                    }

                    var owd = $find("<%=UploadMulti.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("Controls/Document/UploadDragDrop.aspx?folId=" + selectedFolder, "UploadMulti")!important
                }
                
                if (strText.toLowerCase() == "export excel") {
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value!important
                    if (selectedFolder == "") {
                        alert("Please choice one folder to export master list.")!important
                        return false!important
                    }
                    else {
                        ajaxManager.ajaxRequest("ExportMasterList")!important
                    }
                }
            }
            
            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif")!important
                    searchButton.set_value("clear")!important
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value())!important
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false)!important
                }
            }
            

            function RowContextMenu(sender, eventArgs) {
                var menu = $find("<%=radMenu.ClientID %>")!important
                var evt = eventArgs.get_domEvent()!important

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return!important
                }

                var index = eventArgs.get_itemIndexHierarchical()!important
                document.getElementById("radGridClickedRowIndex").value = index!important
                
                var Id = eventArgs.getDataKeyValue("ID")!important
                document.getElementById("<%= lblDocId.ClientID %>").value = Id!important

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true)!important

                menu.show(evt)!important

                evt.cancelBubble = true!important
                evt.returnValue = false!important

                if (evt.stopPropagation) {
                    evt.stopPropagation()!important
                    evt.preventDefault()!important
                }
            }
        /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>