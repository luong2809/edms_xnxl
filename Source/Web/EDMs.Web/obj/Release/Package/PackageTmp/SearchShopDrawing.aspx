﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchShopDrawing.aspx.cs" Inherits="EDMs.Web.SearchShopDrawing" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Content/styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
        html, body, form {
	        overflow:hidden;
        }
        
        #RAD_SPLITTER_PANE_CONTENT_ctl00_rightPane {
            overflow:hidden !important;
        }

        .RadAjaxPanel {
            height: 100% !important;
        }
        .accordion dt a
        {
            color: #085B8F;
            border-bottom: 2px solid #46A3D3;
            font-size: 1.5em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .RadGrid .rgSelectedRow
        {
            background-image : none !important;
            background-color: coral !important;
        }

        .accordion dt span
        {
            color: #085B8F;
            border-bottom: 2px solid #46A3D3;
            font-size: 1.5em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .qlcbTooltip {
            line-height: 1.8;
            padding-right: 5px;
            text-align: right;
        }
         .qlcbFormItem input[type="text"], .qlcbFormItem textarea, .qlcbFormItem select {
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
    
        }
         html body .riSingle .riTextBox, html body .riSingle .riTextBox[type="text"] {
   
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important;
            border-style: solid!important;
            border-width: 1px 1px 1px 5px!important;
            color: #000000!important;
            float: left!important;
            font: 12px "segoe ui"!important;
            margin: 0!important;
            padding: 2px 5px 3px!important;
            vertical-align: middle!important;
        }
         div.RadPicker input.qlcbFormRequired1[type="text"], div.RadPicker_Default input.qlcbFormRequired1[type="text"] {
            border-left-color: Red!important;
            border-left-width: 5px!important;
        }
         .qlcbFormItem input.min25Percent[type="text"], div.qlcbFormItem textarea.min25Percent {
            min-width: 235px;
        }
        .qlcbFormItem input.minFullWidth[type="text"], div.qlcbFormItem textarea.minFullWidth {
            min-width: 626px;
        }
        .qlcbFormItem select.min25Percent {
            min-width: 250px;
        }
        .qlcbFormItem input.min50Percent[type="text"], div.qlcbFormItem textarea.min50Percent, div.qlcbFormItem select.min50Percent {
            min-width: 50%;
        }
        .qlcbFormItem input.qlcbFormRequired[type="text"], div.qlcbFormItem textarea.qlcbFormRequired, div.qlcbFormItem select.qlcbFormRequired {
            border-left-color: #FF0000;
            border-left-width: 5px;
        }
        .qlcbFormItem input.qlcbFormUPPERCASE[type="text"], div.qlcbFormItem textarea.qlcbFormUPPERCASE {
            text-transform: uppercase;
        }
        .qlcbFormItem input[type="text"], div.qlcbFormItem textarea, div.qlcbFormItem select {
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
        }
        .qlcbFormItem input[type="text"]:hover, div.qlcbFormItem select:hover {
            border-color: #000000 #000000 #000000 #46A3D3;
        }
        .qlcbFormItem input.qlcbFormRequired[type="text"]:hover, div.qlcbFormItem select.qlcbFormRequired:hover {
            border-color: #000000 #000000 #000000 #FF0000;
        }
        .RadPicker, div.RadPicker_Default {
            display: inline !important;
            float: left !important;
        }
        .min25Percent {
            min-width: 217px;
        }
            
        a.tooltip
        {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong
            {
                line-height: 30px;
            }

            a.tooltip:hover
            {
                text-decoration: none;
            }

            a.tooltip span
            {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span
            {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout
        {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span
        {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel
        {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5
        {
            height: 100% !important;
        }

        #divContainerLeft
        {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight
        {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted
        {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper
        {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip
        {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage
        {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow
        {
            z-index: 8000 !important;
        }

        .TemplateMenu
        {
            z-index: 10;
        }

        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        
        }   

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 1px !important;
        }

        /*#RAD_SPLITTER_PANE_CONTENT_ctl00_leftPane {
            width: 350px !important;
        }
        #RAD_SPLITTER_PANE_CONTENT_ctl00_topLeftPane {
            width: 350px !important;
        }*/
        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
               margin: 0 0px;
           }
           .RadComboBox .rcbInputCell .rcbInput{
            /*border-left-color: #FF0000;*/
            /*border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;*/
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 96%;
           }
           .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
               padding-left: 0px !important;
           }
            div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
           }
           .rgEditForm {
               text-align: right;
           }
           .RadComboBox {
               border-bottom: none !important;
           }
    </style>
    
    <dl class="accordion">
        <dt style="width: 100%;">
            <span>Search conditions</span>
        </dt>
    </dl>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server" >
    
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane3" runat="server" Height="180" Scrollable="false" Scrolling="None">
            <dl class="accordion">
                <dt style="width: 100%;">
                    <span>Search Shop Drawing:</span>
                </dt>
            </dl>
            <table style="width: 98%;padding-bottom: 5px">
            <tr class="qlcbFormItem">
                <td width="10%"  class="qlcbTooltip">  <span id="Span2" style="color: red">Search all fields</span></td>
                <td width="23%"> <asp:TextBox ID="txtSearchAll" runat="server" CssClass="min25Percent" Width="234"/></td>
                <td width="10%" class="qlcbTooltip">Document No.</td>
                <td width="23%"><asp:TextBox ID="txtDocNo" runat="server" CssClass="min25Percent" Width="234"/></td>
                <td width="10%" class="qlcbTooltip">  <span id="Span3">Title</span></td>
                <td width="*"><asp:TextBox ID="txtTitle" runat="server" CssClass="min25Percent" Width="234"/></td>
        
            </tr>
    
            <tr><td height="1px" colspan="6"></td></tr>
            <tr class="qlcbFormItem">
                <td class="qlcbTooltip">Project</td>
                <td ><asp:DropDownList ID="ddlProject" runat="server" CssClass="min25Percent" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="ddlProject_OnSelectedIndexChanged"/></td>
                <td class="qlcbTooltip">  <span id="Span9">Discipline</span></td>
                <td>
                    <asp:DropDownList ID="ddlDiscipline" runat="server" CssClass="min25Percent" Width="250px" />
                </td> 
                <td class="qlcbTooltip">   <span id="Span10">Document Type</span></td>
                <td>
                    <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="min25Percent" Width="250px" /> 
                </td>
            </tr>

            <tr><td height="1px" colspan="6"></td></tr>
            <tr class="qlcbFormItem">
                <td class="qlcbTooltip">Department</td>
                <td><asp:DropDownList ID="ddlDepartment" runat="server" CssClass="min25Percent" Width="250px" /></td>
                <td class="qlcbTooltip" colspan="2" style="text-align: center">
                    <telerik:RadButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" Width="80px" style="text-align: center">
                        <Icon PrimaryIconUrl="Images/search.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnExport" runat="server" Text="Export Excel File" OnClick="btnExport_OnClick" Width="120px" style="text-align: center">
                        <Icon PrimaryIconUrl="Images/excelfile.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    
                </td>
        
            </tr>

             <tr><td height="1px" colspan="6"></td></tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True" Height="100%"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                GridLines="None" 
                OnNeedDataSource="grdDocument_OnNeedDataSource"
                OnItemDataBound="grdDocument_ItemDataBound" 
                PageSize="100" Style="outline: none">
        <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" EditMode="InPlace" Font-Size="8pt" >
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <SelectFields>
                        <telerik:GridGroupByField FieldAlias="." FieldName="ReferenceDocNo" FormatString="{0:D}"
                            HeaderValueSeparator=""></telerik:GridGroupByField>
                    </SelectFields>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="ReferenceDocNo" SortOrder="Ascending" ></telerik:GridGroupByField>
                    </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
            <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
            <ColumnGroups>
                <telerik:GridColumnGroup HeaderText="" Name="RevisionDetails"
                        HeaderStyle-HorizontalAlign="Center"/>
                <telerik:GridColumnGroup HeaderText="VIA EMAIL FOR INFORMATION" Name="OutgoingTrans"
                        HeaderStyle-HorizontalAlign="Center"/>
                <telerik:GridColumnGroup HeaderText="INCOMING HARD COPY" Name="IncomingTrans"
                        HeaderStyle-HorizontalAlign="Center"/>
                <telerik:GridColumnGroup HeaderText="ICA REVIEW DETAILS" Name="ICAReviews"
                        HeaderStyle-HorizontalAlign="Center"/>
            </ColumnGroups>

            <Columns>
                <telerik:GridBoundColumn DataField="HasAttachFile" UniqueName="HasAttachFile" Display="False" />
                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                
                <telerik:GridTemplateColumn HeaderText="Project" UniqueName="ProjectName"
                    AllowFiltering="false" Display="False">
                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                    <ItemStyle HorizontalAlign="Left"  />
                    <ItemTemplate>
                        <%# Eval("ProjectName") %>
                    </ItemTemplate>

                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="DOC. No." UniqueName="DocNo"
                    DataField="DocNo" ShowFilterIcon="False" FilterControlWidth="97%" 
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                    <HeaderStyle HorizontalAlign="Center" Width="180" />
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        
                        
                        <asp:Label ID="lblDocNo" runat="server" Text='<%# Eval("DocNo") %>' style="cursor: pointer"/> 
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="DocNo" runat="server" Value='<%# Eval("DocNo") %>'/>
                        <asp:Label runat="server" ID="Label1"></asp:Label>
                        <%--<asp:TextBox ID="txtDocNo" runat="server" Width="100%"></asp:TextBox>--%>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                                            
                <telerik:GridTemplateColumn HeaderText="DOC. Title" UniqueName="DocTitle"
                    DataField="DocTitle" ShowFilterIcon="False" FilterControlWidth="97%" 
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                    <HeaderStyle HorizontalAlign="Center" Width="220" />
                    <ItemStyle HorizontalAlign="Left" Width="220" />
                    <ItemTemplate>
                        <%# Eval("DocTitle") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                    
                <telerik:GridTemplateColumn HeaderText="Rev." UniqueName="Rev"
                    AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Center" Width="50" />
                    <ItemStyle HorizontalAlign="Center" Width="50"/>
                    <ItemTemplate>
                        <%# Eval("RevisionName") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                        
                <telerik:GridTemplateColumn HeaderText="Department" UniqueName="DeparmentName"
                    AllowFiltering="false" >
                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                    <ItemStyle HorizontalAlign="Left"  />
                    <ItemTemplate>
                        <%# Eval("DeparmentName") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn HeaderText="Discipline" UniqueName="DisciplineName" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                    <ItemStyle HorizontalAlign="Left"/>
                    <ItemTemplate>
                        <%# Eval("DisciplineName")%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Document Type" UniqueName="DocumentTypeName" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                    <ItemStyle HorizontalAlign="Left"/>
                    <ItemTemplate>
                        <%# Eval("DocumentTypeName")%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                        
                    <telerik:GridTemplateColumn HeaderText="Reference From" UniqueName="ReferenceFromName" AllowFiltering="false" Display="False">
                    <HeaderStyle HorizontalAlign="Center" Width="60" />
                    <ItemStyle HorizontalAlign="Center"   />
                    <ItemTemplate>
                        <%# Eval("ReferenceFromName")%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                        
                <telerik:GridTemplateColumn HeaderText="Planed" UniqueName="RevisionPlanedDate"
                    AllowFiltering="false" ColumnGroupName="RevisionDetails" Display="False">
                    <HeaderStyle HorizontalAlign="Center" Width="80" />
                    <ItemStyle HorizontalAlign="Center" Width="80"/>
                    <ItemTemplate>
                        <%# Eval("RevisionPlanedDate","{0:dd/MM/yyyy}") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Trans No." UniqueName="OutgoingTransNo"
                        AllowFiltering="false" ColumnGroupName="OutgoingTrans">
                    <HeaderStyle HorizontalAlign="Center" Width="120" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Eval("OutgoingTransNo") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn HeaderText="Date" UniqueName="OutgoingTransDate"
                        AllowFiltering="false" ColumnGroupName="OutgoingTrans" >
                    <HeaderStyle HorizontalAlign="Center" Width="80" />
                    <ItemStyle HorizontalAlign="Center" Width="80"/>
                    <ItemTemplate>
                        <%# Eval("OutgoingTransDate","{0:dd/MM/yyyy}") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                        
                                    
                                        
                <telerik:GridTemplateColumn HeaderText="Trans No." UniqueName="IncomingTransNo"
                        AllowFiltering="false" ColumnGroupName="IncomingTrans">
                    <HeaderStyle HorizontalAlign="Center" Width="120" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Eval("IncomingTransNo") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Date" UniqueName="IncomingTransDate"
                        AllowFiltering="false" ColumnGroupName="IncomingTrans" >
                    <HeaderStyle HorizontalAlign="Center" Width="80" />
                    <ItemStyle HorizontalAlign="Center" Width="80"/>
                    <ItemTemplate>
                        <%# Eval("IncomingTransDate","{0:dd/MM/yyyy}") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                        
                                    
                                        
                <telerik:GridTemplateColumn HeaderText="Final Code" UniqueName="FinalCodeName"
                        AllowFiltering="false" Display="False">
                    <HeaderStyle HorizontalAlign="Center" Width="10" />
                    <ItemStyle HorizontalAlign="Center" Width="40"/>
                    <ItemTemplate>
                        <%# Eval("FinalCodeName") %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                        
                <telerik:GridTemplateColumn HeaderText="Complete - %" UniqueName="Complete" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Center" Width="65" />
                    <ItemStyle HorizontalAlign="Center" Width="65" />
                    <ItemTemplate>
                        <%# Eval("Complete")!=null && Convert.ToDouble(Eval("Complete")) != 0 ?  Eval("Complete") + "%" : "-"%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn HeaderText="Weight - %" UniqueName="Weight" AllowFiltering="false">
                    <HeaderStyle HorizontalAlign="Center" Width="65" />
                    <ItemStyle HorizontalAlign="Center" Width="65" />
                    <ItemTemplate>
                        <%# Eval("Weight")!=null ?  Eval("Weight") + "%" : "-"%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                                    
                <telerik:GridTemplateColumn HeaderText="EMDR" UniqueName="IsEMDR" AllowFiltering="false" Display="False">
                    <HeaderStyle HorizontalAlign="Center" Width="50" />
                    <ItemStyle HorizontalAlign="Center" Width="50" />
                    <ItemTemplate>
                        <asp:Image ID="imgIsEMDR" runat="server" ImageUrl="~/Images/ok.png" Visible='<%# Eval("IsEMDR")%>'/>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings Selecting-AllowRowSelect="true"  AllowColumnHide="True">
            <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
            <Selecting AllowRowSelect="true" />
            <ClientEvents  OnGridCreated="GetGridObject" />
            <ClientEvents OnRowContextMenu="RowContextMenu" OnRowSelected="RowClick"></ClientEvents>
            <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
        </ClientSettings>
</telerik:RadGrid>
        </telerik:RadPane>
    </telerik:RadSplitter>
    
    <span style="display: none">
        
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
               <%-- <telerik:AjaxSetting AjaxControlID="btnClearSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
                
                <telerik:AjaxSetting AjaxControlID="ddlProject">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlRevision"/>
                        <telerik:AjaxUpdatedControl ControlID="ddlDiscipline"/>
                        <telerik:AjaxUpdatedControl ControlID="ddlDocumentType"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="rtvProject">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2" UpdatePanelRenderMode="Block" UpdatePanelHeight="100%"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rtvBlock">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2" UpdatePanelRenderMode="Block" UpdatePanelHeight="100%"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rtvField">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2" UpdatePanelRenderMode="Block" UpdatePanelHeight="100%"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rtvPlatform">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2" UpdatePanelRenderMode="Block" UpdatePanelHeight="100%"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>
    <telerik:RadContextMenu ID="radMenu" runat="server"
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking">
        <Items>
            <telerik:RadMenuItem Text="Attach document files" ImageUrl="~/Images/attach.png" Value="AttachDoc">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Revision History" ImageUrl="~/Images/revision.png" Value="RevisionHistory">
            </telerik:RadMenuItem>
        </Items>
    </telerik:RadContextMenu>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information"
                VisibleStatusbar="false" Height="690" Width="650" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="RevisionDialog" runat="server" Title="Revision History"
                VisibleStatusbar="false" Height="700" Width="1250" MinHeight="700" MinWidth="1250" MaxHeight="700" MaxWidth="1250" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="SendMail" runat="server" Title="Send mail"
                VisibleStatusbar="false" Height="560" Width="992" MinHeight="560" MinWidth="992" MaxHeight="560" MaxWidth="992" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach document files"
                VisibleStatusbar="false" Height="500" Width="700" MinHeight="500" MinWidth="700" MaxHeight="500" MaxWidth="700" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            

        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction"/>
    <asp:HiddenField runat="server" ID="lblFolderId"/>
    <asp:HiddenField runat="server" ID="lblDocId"/>
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex"/>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">
            
            var radDocuments;

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function ShowUploadForm(id) {
                var owd = $find("<%=AttachDoc.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/UploadDragDrop.aspx?docId=" + id, "AttachDoc");
            }

            function ShowRevisionForm(id) {
                var owd = $find("<%=RevisionDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + id, "RevisionDialog");
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;
            }

            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                switch (itemValue) {
                    case "RevisionHistory":
                        var owd = $find("<%=RevisionDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/DocumentPackageRevisionHistory.aspx?docId=" + docId, "RevisionDialog");
                        break;
                    case "AttachDoc":
                        var owd = $find("<%=AttachDoc.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/AttachDocument.aspx?docId=" + docId + "&isFullPermission=false", "AttachDoc");
                        break;
                }
            }
            
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnDownloadPackage") >= 0) {
                    args.set_enableAjax(false);
                }
            }


            function ShowEditForm(id, folId) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id + "&folId=" + folId, "CustomerDialog");
                
                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }
            function ShowInsertForm() {
                
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");

                //window.radopen("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");
                //return false;
            }
            
            

            function refreshGrid(arg) {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }
            
            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
            }
            
            function onNodeClicking(sender, args) {
                var folderValue = args.get_node().get_value();
                document.getElementById("<%= lblFolderId.ClientID %>").value = folderValue;
            }

            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                var customerId = null;
                var customerName = "";

                //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                //    var selectedRow = grid.get_masterTableView().get_selectedItems()[0];
                //    customerId = selectedRow.getDataKeyValue("Id");
                //    //customerName = selectedRow.Items["FullName"]; 
                //    //customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML;
                //}


                
                 
                if (strText == "Documents") {

                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to create new document");
                        return false;
                    }

                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.Show();
                    owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?folId=" + selectedFolder, "CustomerDialog");

                }

                if (strText == "Thêm mới") {
                    return ShowInsertForm();
                }
                else if (strText == "Import dữ liệu") {
                    return ShowImportForm();
                }
                else if (strText == "Dữ liệu thô") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_1_" + customerId);
                }
                else if (strText == "Tiềm năng") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_2_" + customerId);
                }
                else if (strText == "Chưa liên hệ được") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_3_" + customerId);
                }
                else if (strText == "Không tiềm năng") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_4_" + customerId);
                }
                else if (strText == "Thông tin sai") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_5_" + customerId);
                }
                else if (strText == "Liên hệ tư vấn") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_6_" + customerId);
                }
                else if (strText == "Hẹn tư vấn") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_7_" + customerId);
                }
                else if (strText == "Đã sử dụng dịch vụ") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_8_" + customerId);
                }
                else {
                    var commandName = args.get_item().get_commandName();
                    if (commandName == "doSearch") {
                        var searchTextBox = sender.findButtonByCommandName("searchText").findControl("txtSearch");
                        if (searchButton.get_value() == "clear") {
                            searchTextBox.set_value("");
                            searchButton.set_imageUrl("images/search.gif");
                            searchButton.set_value("search");
                        }

                        performSearch(searchTextBox);
                    } else if (commandName == "reply") {
                        window.radopen(null, "Edit");
                    }
                }
            }
            
            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }
            

            function RowContextMenu(sender, eventArgs) {
                var menu = $find("<%=radMenu.ClientID %>");
                var evt = eventArgs.get_domEvent();

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return;
                }

                var index = eventArgs.get_itemIndexHierarchical();
                document.getElementById("radGridClickedRowIndex").value = index;
                
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);

                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }
            
            function ddlSystemDDHandler(sender, eventArgs) {
                var tree = sender.get_items().getItem(0).findControl("rtvSystem");
                var selectedNode = tree.get_selectedNode();
                if (selectedNode) {
                    selectedNode.scrollIntoView();
                }
            }
        /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>

<%--Tan.Le Remove here--%>
<%--<uc1:List runat="server" ID="CustomerList"/>--%>
<%-- <div id="EDMsCustomers" runat="server" />--%>
