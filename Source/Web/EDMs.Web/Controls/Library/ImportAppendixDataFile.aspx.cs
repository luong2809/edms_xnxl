﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Library
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportAppendixDataFile : Page
    {
        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportAppendixDataFile()
        {
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentName = string.Empty;
            var currentValue = string.Empty;

            var disciplineList = new List<Discipline>();
            var errorDisciplineList = new List<Discipline>();
            var docTypeList = new List<DocumentType>();
            var errorDocTypeList = new List<DocumentType>();

            var type = string.Empty;
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var datasheet = workbook.Worksheets[1];

                        currentSheetName = datasheet.Name;
                        currentName = string.Empty;
                        if (datasheet.Cells["A1"].Value.ToString() == "AppendixFile")
                        {
                            type = datasheet.Cells["A2"].Value.ToString();
                            switch (type)
                            {
                                case "DisciplineList":
                                    var dtDiscipline = datasheet.Cells.ExportDataTable(4, 2, datasheet.Cells.MaxRow,
                                    5).AsEnumerable().Where(t => !string.IsNullOrEmpty(t["Column1"].ToString())).CopyToDataTable();
                                    foreach (DataRow  dr in dtDiscipline.Rows)
                                    {
                                        currentName = dr["Column1"].ToString();
                                        var disName = dr["Column1"].ToString();
                                        var projName = dr["Column2"].ToString();
                                        var projObj = this.scopeProjectService.GetByName(projName);
                                        var objDis = new Discipline();
                                        if (projObj != null)
                                        {
                                            objDis.Name = disName;
                                            objDis.ProjectId = projObj.ID;
                                            objDis.ProjectName = projObj.Name;
                                            objDis.Weight = !string.IsNullOrEmpty(dr["Column3"].ToString())
                                                ? Convert.ToDouble(dr["Column3"].ToString())*100
                                                : 0;
                                            objDis.Description = dr["Column4"].ToString();
                                            objDis.IsAutoCalculate = !string.IsNullOrEmpty(dr["Column5"].ToString());
                                            objDis.LastUpdatedBy = UserSession.Current.User.Id;
                                            objDis.LastUpdatedDate = DateTime.Now;

                                            disciplineList.Add(objDis);

                                            
                                        }
                                        else
                                        {
                                            objDis.Name = disName;
                                            objDis.ProjectName = projName;
                                            errorDisciplineList.Add(objDis);
                                        }

                                    }

                                    if (!errorDisciplineList.Any())
                                    {
                                        foreach (var disObj in disciplineList)
                                        {
                                            this.disciplineService.Insert(disObj);
                                            var projObj = this.scopeProjectService.GetById(disObj.ProjectId.GetValueOrDefault());
                                            // Create document Folder
                                            var validDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(disObj.Name).Replace('&', '-');
                                            var projectFolder = Server.MapPath("../.." + projObj.ProjectPath);

                                            Directory.CreateDirectory(projectFolder + "\\01.All Revision\\" + validDisciplineName);
                                            Directory.CreateDirectory(projectFolder + "\\02.Latest Revision\\" + validDisciplineName);
                                            Directory.CreateDirectory(projectFolder + "\\03.Shop Drawing\\01.All Revision\\" + validDisciplineName);
                                            Directory.CreateDirectory(projectFolder + "\\03.Shop Drawing\\02.Latest Revision\\" + validDisciplineName);
                                            Directory.CreateDirectory(projectFolder + "\\06.For Construction\\" + validDisciplineName);

                                            disObj.DisciplinePath = projObj.ProjectPath + "/01.All Revision/" + validDisciplineName;
                                            this.disciplineService.Update(disObj);
                                            // -------------------------------------------------
                                        }

                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document master list file is valid. System import successfull!";
                                    }
                                    else
                                    {

                                        this.blockError.Visible = true;
                                        this.lblError.Text = "Have error: Please re-check disciplines below <br/>";
                                        foreach (var errorDis in errorDisciplineList)
                                        {
                                            this.lblError.Text += errorDis.Name + "<br/>";
                                        }

                                        this.lblError.Text += "It already exist in System or Project info  not valid.";
                                    }

                                    break;
                                case "DocTypeList":
                                    var dtDocType= datasheet.Cells.ExportDataTable(4, 2, datasheet.Cells.MaxRow,
                                    3).AsEnumerable().Where(t => !string.IsNullOrEmpty(t["Column1"].ToString())).CopyToDataTable();
                                    foreach (DataRow dr in dtDocType.Rows)
                                    {
                                        currentName = dr["Column1"].ToString();
                                        var docTypeName = dr["Column1"].ToString();
                                        var projName = dr["Column2"].ToString();
                                        var projObj = this.scopeProjectService.GetByName(projName);
                                        var objDocType = new DocumentType();
                                        if (projObj != null)
                                        {
                                            objDocType.Name = docTypeName;
                                            objDocType.ProjectId = projObj.ID;
                                            objDocType.ProjectName = projObj.Name;
                                           
                                            objDocType.Description = dr["Column3"].ToString();
                                            objDocType.LastUpdatedBy = UserSession.Current.User.Id;
                                            objDocType.LastUpdatedDate = DateTime.Now;

                                            docTypeList.Add(objDocType);
                                        }
                                        else
                                        {
                                            objDocType.Name = docTypeName;
                                            objDocType.ProjectName = projName;
                                            errorDocTypeList.Add(objDocType);
                                        }

                                    }

                                    if (!errorDocTypeList.Any())
                                    {
                                        foreach (var docTypeObj in docTypeList)
                                        {
                                            this.documentTypeService.Insert(docTypeObj);
                                        }

                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document master list file is valid. System import successfull!";
                                    }
                                    else
                                    {

                                        this.blockError.Visible = true;
                                        this.lblError.Text = "Have error: Please re-check Document Type below <br/>";
                                        foreach (var errorDocType in errorDocTypeList)
                                        {
                                            this.lblError.Text += errorDocType.Name + "<br/>";
                                        }

                                        this.lblError.Text += "It already exist in System or Project info  not valid.";
                                    }

                                    break;
                            }
                        }
                        else
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text =
                                "Appendix Load file is invalid. Try export new template form System!";
                        }
                    }
                }
            }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentName + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}