﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using EDMs.Business.Services.Scope;

namespace EDMs.Web.Controls.Library
{
    using System;
    using System.Web.UI;
    using System.Linq;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DisciplineEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly  DocumentPackageService documentpackageService;
        private readonly AttachFilesPackageService attachfilepackageService;
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DisciplineEditForm()
        {
            this.userService = new UserService();
            this.disciplineService = new DisciplineService();
            this.scopeProjectService = new ScopeProjectService();
            this.documentpackageService = new DocumentPackageService();
            this.attachfilepackageService = new AttachFilesPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var objDis = this.disciplineService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objDis != null)
                    {

                        this.txtName.Text = objDis.Name;
                        this.txtDescription.Text = objDis.Description;
                        this.ddlProject.SelectedValue = objDis.ProjectId.ToString();
                        this.txtWeight.Value = objDis.Weight;
                        this.cbIsAutoCalculate.Checked = objDis.IsAutoCalculate.GetValueOrDefault();
                        var createdUser = this.userService.GetByID(objDis.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objDis.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objDis.LastUpdatedBy != null && objDis.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objDis.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objDis.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectObj = this.scopeProjectService.GetById(projectId);

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var disId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var objDis = this.disciplineService.GetById(disId);
                    if (objDis != null)
                    {
                        // Create document Folder
                        var validDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&','-');
                        var oldValidDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(objDis.Name.Replace('&', '-'));
                        var projectFolder = Server.MapPath("../.." + projectObj.ProjectPath);
                        var pathst = projectFolder + "/01.All Revision/" + oldValidDisciplineName;
                        if (Directory.Exists(pathst) && validDisciplineName != oldValidDisciplineName)
                        {


                            Directory.Move(projectFolder + "\\01.All Revision\\" + oldValidDisciplineName,
                                            projectFolder + "\\01.All Revision\\" + validDisciplineName);

                            this.ReplacePath(oldValidDisciplineName, validDisciplineName, objDis.ID);

                            Directory.Move(projectFolder + "\\02.Latest Revision\\" + oldValidDisciplineName,
                                            projectFolder + "\\02.Latest Revision\\" + validDisciplineName);
                            Directory.Move(projectFolder + "\\03.Shop Drawing\\01.All Revision\\" + oldValidDisciplineName,
                                            projectFolder + "\\03.Shop Drawing\\01.All Revision\\" + validDisciplineName);
                            Directory.Move(projectFolder + "\\03.Shop Drawing\\02.Latest Revision\\" + oldValidDisciplineName,
                                            projectFolder + "\\03.Shop Drawing\\02.Latest Revision\\" + validDisciplineName);
                            Directory.Move(projectFolder + "\\06.For Construction\\" + oldValidDisciplineName,
                                            projectFolder + "\\06.For Construction\\" + validDisciplineName);

                          

                        }
                        else if (!Directory.Exists(pathst))
                        {
                            Directory.CreateDirectory(projectFolder + "\\01.All Revision\\" + validDisciplineName);
                            Directory.CreateDirectory(projectFolder + "\\02.Latest Revision\\" + validDisciplineName);
                            Directory.CreateDirectory(projectFolder + "\\03.Shop Drawing\\01.All Revision\\" + validDisciplineName);
                            Directory.CreateDirectory(projectFolder + "\\03.Shop Drawing\\02.Latest Revision\\" + validDisciplineName);
                            Directory.CreateDirectory(projectFolder + "\\06.For Construction\\" + validDisciplineName);
                        }
                        // -------------------------------------------------

                        objDis.Name = this.txtName.Text.Trim();
                        objDis.Description = this.txtDescription.Text.Trim();
                        objDis.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                        objDis.ProjectName = this.ddlProject.SelectedItem.Text;
                        objDis.Weight = this.txtWeight.Value;
                        objDis.IsAutoCalculate = this.cbIsAutoCalculate.Checked;
                        objDis.LastUpdatedBy = UserSession.Current.User.Id;
                        objDis.LastUpdatedDate = DateTime.Now;
                        objDis.DisciplinePath = projectObj.ProjectPath + "/01.All Revision/" +
                                                Utilities.Utility.RemoveSpecialCharacterForFolder(
                                                    this.txtName.Text.Trim()).Replace('&', '-');
                        this.disciplineService.Update(objDis);



                        
                    }
                }
                else
                {
                    var objDis = new Discipline()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                        ProjectName = this.ddlProject.SelectedItem.Text,
                        Weight = this.txtWeight.Value,
                        IsAutoCalculate = this.cbIsAutoCalculate.Checked,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                       // DisciplinePath = projectObj.ProjectPath + "/01.All Revision/" + Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&', '-')
                    };

                    this.disciplineService.Insert(objDis);

                    // Create document Folder
                    var validDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(objDis.Name).Replace('&', '-');
                    var projectFolder = Server.MapPath("../.." + projectObj.ProjectPath);

                    Directory.CreateDirectory(projectFolder + "\\01.All Revision\\" + validDisciplineName);
                    Directory.CreateDirectory(projectFolder + "\\02.Latest Revision\\" + validDisciplineName);
                    Directory.CreateDirectory(projectFolder + "\\03.Shop Drawing\\01.All Revision\\" + validDisciplineName);
                    Directory.CreateDirectory(projectFolder + "\\03.Shop Drawing\\02.Latest Revision\\" + validDisciplineName);
                    Directory.CreateDirectory(projectFolder + "\\06.For Construction\\" + validDisciplineName);

                    objDis.DisciplinePath = projectObj.ProjectPath + "/01.All Revision/" + validDisciplineName;
                    this.disciplineService.Update(objDis);
                    // -------------------------------------------------
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        protected void ReplacePath(string oldvalues, string newvalues, int disID)
        {
            var listdocID = this.documentpackageService.GetAllByDiscipline(disID, true).Select(t => t.ID).ToList() ;
            var fileattachlist = this.attachfilepackageService.GetAllDocId(listdocID);

            oldvalues="/"+oldvalues+"/";
            newvalues="/"+newvalues+"/";
            foreach (var fileobj in fileattachlist)
            {
                fileobj.FilePath = fileobj.FilePath.Replace(oldvalues, newvalues);
                this.attachfilepackageService.Update(fileobj);
            }
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            var projectInPermission = this.scopeProjectService.GetAll();
            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
        }
    }
}