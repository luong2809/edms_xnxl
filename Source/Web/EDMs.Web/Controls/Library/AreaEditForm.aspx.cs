﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Library
{
    using System;
    using System.Configuration;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.IO;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Business.Services.Scope;
    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AreaEditForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly AreaService AreaService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        /// <summary>
        /// the ScopeProject
        /// </summary>
        private readonly  ScopeProjectService scopeProjectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AreaEditForm()
        {
            this.userService = new UserService();
            this.AreaService = new AreaService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.categoryService = new CategoryService();
            this.scopeProjectService=new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var objArea = this.AreaService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objArea != null)
                    {
                        this.txtName.Text = objArea.Name;
                        this.ddlProject.SelectedValue = objArea.ProjectId.ToString();
                        this.txtDescription.Text = objArea.Description;
                        var createdUser = this.userService.GetByID(objArea.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objArea.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objArea.UpdatedBy != null && objArea.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objArea.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objArea.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>s
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (this.Page.IsValid)
            //{
               var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectObj = this.scopeProjectService.GetById(projectId);

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var AreaId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.AreaService.GetById(AreaId);
                    if (obj != null)
                    {
                        // Create document Folder
                        var validDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&','-');
                        var oldValidDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name.Replace('&', '-'));
                        var projectFolder = Server.MapPath("../.." + projectObj.ProjectPath);

                        if (Directory.Exists(projectFolder) && validDisciplineName != oldValidDisciplineName)
                        {
                            Directory.Move(projectFolder + "\\07.Cutting\\" + oldValidDisciplineName,
                                       projectFolder + "\\07.Cutting\\" + validDisciplineName);
                        }
                        else
                        {
                            Directory.CreateDirectory(projectFolder + "\\07.Cutting\\" + validDisciplineName);
                        }


                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                        obj.ProjectName = this.ddlProject.SelectedItem.Text;
                        obj.AreaPath = projectObj.ProjectPath + "/07.Cutting/" +
                                                Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&', '-');
                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;

                        this.AreaService.Update(obj);
                    }
                }
                else
                {
                    var obj = new Area()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        ProjectId=Convert.ToInt32(this.ddlProject.SelectedValue),
                        ProjectName=this.ddlProject.SelectedItem.Text,
                        AreaPath= projectObj.ProjectPath + "/07.Cutting/" + Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&', '-'),
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    this.AreaService.Insert(obj);

                    var validDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name).Replace('&', '-');
                    var projectFolder = Server.MapPath("../.." + projectObj.ProjectPath);

                    Directory.CreateDirectory(projectFolder + "\\07.Cutting\\" + validDisciplineName);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                         "CloseAndRebind();", true);
           // }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }


        private void LoadComboData()
        {
            var projectInPermission = this.scopeProjectService.GetAll();
            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
        }
    }
}