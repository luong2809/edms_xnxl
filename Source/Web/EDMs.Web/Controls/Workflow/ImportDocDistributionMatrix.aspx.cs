﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocDistributionMatrix : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;


        private readonly DocumentPackageService documentPackageService;


        private readonly DocRoleMatrixService docRoleMatrixService;

        private readonly DistributionMatrixDetailByUserService dmdeDetailByUserService;

        private readonly DistributionMatrixService dmService;
        private readonly ScopeProjectService projectService;

        private readonly RoleService roleService;

        private readonly UserService userService;


        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocDistributionMatrix()
        {
            this.documentService = new DocumentService();
            this.documentPackageService = new DocumentPackageService();
            this.dmdeDetailByUserService = new DistributionMatrixDetailByUserService();
            this.docRoleMatrixService = new DocRoleMatrixService();
            this.dmService = new DistributionMatrixService();
            this.projectService = new ScopeProjectService();
            this.roleService = new RoleService();
            this.userService = new UserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var singleProject = !string.IsNullOrEmpty(this.Request.QueryString["singleProject"]);
            var typeId = Convert.ToInt32(this.Request.QueryString["type"]);
            
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        for (int x = 0; x < (singleProject ? workbook.Worksheets.Count : 1); x++)
                        {
                            var datasheet = workbook.Worksheets[x];
                            var addDocRoleMatrixList = new List<DocRoleMatrix>();
                            var addDMDetailByUserist = new List<DistributionMatrixDetailByUser>();
                            var removeDocRoleMatrixList = new List<DocRoleMatrix>();
                            var removeDMDetailByUserist = new List<DistributionMatrixDetailByUser>();
                            var updateDocList = new List<DocumentPackage>();
                            var updateDocRoleMatrixList = new List<DocRoleMatrix>();
                            var updateDMDetailByUserList = new List<DistributionMatrixDetailByUser>();

                            var projectId = Convert.ToInt32(datasheet.Cells["A4"].Value);
                            var dmType = datasheet.Cells["A1"].Value.ToString();
                            var rowCount = 0;
                            var docdataTable = new DataTable();
                            var groupDataTable = new DataTable();
                            var userCount = 0;
                            switch (dmType)
                            {
                                case "DMRole":
                                    currentSheetName = datasheet.Name;
                                    currentDocumentNo = string.Empty;
                                    rowCount = Convert.ToInt32(datasheet.Cells["A2"].Value);
                                    var groupCount = Convert.ToInt32(datasheet.Cells["A3"].Value);

                                    docdataTable = datasheet.Cells.ExportDataTable(5, 0, rowCount,
                                        6 + groupCount);
                                    groupDataTable = datasheet.Cells.ExportDataTable(2, 6, 3,
                                        groupCount);
                                    foreach (DataRow dataRow in docdataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            var docId = Convert.ToInt32(dataRow["Column2"].ToString());
                                            var docObj = this.documentPackageService.GetById(docId);
                                            if (docObj != null)
                                            {

                                                currentDocumentNo = docObj.DocNo;
                                                for (int i = 0; i < groupCount; i++)
                                                {
                                                    var roleId =
                                                        Convert.ToInt32(
                                                            groupDataTable.Rows[2]["Column" + (1 + i)].ToString());
                                                    var roleName = groupDataTable.Rows[0]["Column" + (1 + i)].ToString();
                                                    var docRoleMatrixObj =
                                                        this.docRoleMatrixService.GetDocRoleMatrixByGroup(docId, roleId);

                                                    if (docRoleMatrixObj == null &&
                                                        !string.IsNullOrEmpty(dataRow["Column" + (7 + i)].ToString()))
                                                    {
                                                        var docRoleMatrix = new DocRoleMatrix()
                                                        {
                                                            DocId = docObj.ID,
                                                            DocNo = docObj.DocNo,
                                                            DocTitle = docObj.DocTitle,
                                                            RevisionId = docObj.RevisionId,
                                                            RevisionName = docObj.RevisionName,
                                                            RoleId = roleId,
                                                            RoleName = roleName,
                                                            ProjectId = docObj.ProjectId,
                                                            ProjectName = docObj.ProjectName,
                                                            CreatedBy = UserSession.Current.User.Id,
                                                            CreatedByName = UserSession.Current.User.FullName,
                                                            CreatedDate = DateTime.Now,
                                                            Deadline = docObj.PlanedDate,
                                                            IsDistributed = false
                                                        };
                                                        addDocRoleMatrixList.Add(docRoleMatrix);

                                                        // Update distribution for department status from DC
                                                        docObj.IsDistributed = true;
                                                        updateDocList.Add(docObj);
                                                        // ------------------------------------------------
                                                    }
                                                    else if (docRoleMatrixObj != null && string.IsNullOrEmpty(dataRow["Column" + (7 + i)].ToString()))
                                                    {
                                                        removeDocRoleMatrixList.Add(docRoleMatrixObj);

                                                        // Update distribution for department status from DC
                                                        docObj.IsDistributed = false;
                                                        updateDocList.Add(docObj);
                                                        // ------------------------------------------------
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        // Add/Remove document distribute to department
                                        foreach (var docRoleMatrix in addDocRoleMatrixList)
                                        {
                                            this.docRoleMatrixService.Insert(docRoleMatrix);
                                        }

                                        foreach (var docRoleMatrix in removeDocRoleMatrixList)
                                        {
                                            this.docRoleMatrixService.Delete(docRoleMatrix);
                                        }

                                        // Email notification for Manager of department
                                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                                        {
                                            this.NotificationForManager(addDocRoleMatrixList);
                                        }

                                        // --------------------------------------------

                                        // Update distribution status for Document List
                                        foreach (var doc in updateDocList)
                                        {
                                            this.documentPackageService.Update(doc);
                                        }
                                        // --------------------------------------------

                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document distribution matrix init file is valid. System import successfull!";
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                    break;
                                case "DMLead":
                                case "DMEngineer":
                                    currentSheetName = datasheet.Name;
                                    currentDocumentNo = string.Empty;
                                    rowCount = Convert.ToInt32(datasheet.Cells["A2"].Value);
                                    userCount = Convert.ToInt32(datasheet.Cells["A3"].Value);

                                    docdataTable = datasheet.Cells.ExportDataTable(5, 0, rowCount, 6 + userCount);
                                    groupDataTable = datasheet.Cells.ExportDataTable(2, 6, 3, userCount);

                                    var projectObj = this.projectService.GetById(projectId);
                                    var dmObj = this.dmService.GetByType(projectId, dmType == "DMLead" ? 1 : 2, typeId, 0);
                                    if (dmObj == null)
                                    {
                                        dmObj = new DistributionMatrix()
                                        {
                                            Name = projectObj.Name + "-" + UserSession.Current.User.Role.Name +
                                                                (typeId == 1
                                                                ? (dmType == "DMLead" ? "-EMDR-DMatrix_Lead" : "-EMDR-DMatrix_Engineer")
                                                                : (dmType == "DMLead" ? "-ShopDrawing-DMatrix_Lead" : "-ShopDrawing-DMatrix_Engineer")),

                                            Description = (typeId == 1 ? "EMDR" : "Shop Drawing")
                                            + " Documents Distribution Matrix of \"" + UserSession.Current.User.Role.FullName + "\" for "
                                            + (dmType == "DMLead" ? "Leads" : "Engineers"),

                                            ProjectId = projectObj.ID,
                                            ProjectName = projectObj.Name,
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedDate = DateTime.Now,
                                            RoleId = 0,
                                            RoleName = "General",
                                            IsHaveDetail = false,
                                            TypeEngineerId = dmType == "DMLead" ? 1 : 2,
                                            TypeId = typeId,
                                            TypeName = typeId == 1 ? "EMDR" : "Shop Drawing",
                                            IsCanDelete = false,
                                            
                                        };
                                        this.dmService.Insert(dmObj);
                                    }

                                    foreach (DataRow dataRow in docdataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            var docId = Convert.ToInt32(dataRow["Column2"].ToString());
                                            var docObj = this.documentPackageService.GetById(docId);
                                            if (docObj != null)
                                            {
                                                // Update Document distribute status 
                                                if (dmType == "DMLead")
                                                {
                                                    var docRoleMatrixObj = this.docRoleMatrixService.GetDocRoleMatrixByGroup(docObj.ID, UserSession.Current.RoleId);
                                                    if (docRoleMatrixObj != null)
                                                    {
                                                        docRoleMatrixObj.IsDistributed = true;
                                                        updateDocRoleMatrixList.Add(docRoleMatrixObj);
                                                    }
                                                }
                                                else
                                                {
                                                    var dmLeadObj = this.dmService.GetByType(projectId, 1, typeId, 0);
                                                    var dmDetailLead = this.dmdeDetailByUserService.GetDocRoleMatrixByUser(docObj.ID, UserSession.Current.UserId, dmLeadObj.ID);
                                                    if (dmDetailLead != null)
                                                    {
                                                        dmDetailLead.IsDistributed = true;
                                                        updateDMDetailByUserList.Add(dmDetailLead);
                                                    }
                                                }
                                                

                                                currentDocumentNo = docObj.DocNo;
                                                for (int i = 0; i < userCount; i++)
                                                {
                                                    var userId = Convert.ToInt32(groupDataTable.Rows[2]["Column" + (1 + i)].ToString());
                                                    var userFullName = groupDataTable.Rows[0]["Column" + (1 + i)].ToString();
                                                    var dmDetailByUserObj = this.dmdeDetailByUserService.GetDocRoleMatrixByUser(docId, userId, dmObj.ID);

                                                    if (dmDetailByUserObj == null && !string.IsNullOrEmpty(dataRow["Column" + (7 + i)].ToString()))
                                                    {
                                                        var dmDetailByUser = new DistributionMatrixDetailByUser()
                                                        {
                                                            DMId = dmObj.ID,
                                                            DMName = dmObj.Name,
                                                            DocId = docObj.ID,
                                                            DocNo = docObj.DocNo,
                                                            DocTitle = docObj.DocTitle,
                                                            RevisionId = docObj.RevisionId,
                                                            RevisionName = docObj.RevisionName,
                                                            UserId = userId,
                                                            UserName = userFullName,
                                                            ProjectId = docObj.ProjectId,
                                                            ProjectName = docObj.ProjectName,
                                                            CreatedBy = UserSession.Current.User.Id,
                                                            CreatedByName = UserSession.Current.User.FullName,
                                                            CreatedDate = DateTime.Now,
                                                            Deadline = docObj.PlanedDate,
                                                            TypeId = typeId,
                                                            TypeName = typeId == 1 ? "EMDR" : "Shop Drawing",
                                                            IsDistributed = false
                                                        };
                                                        addDMDetailByUserist.Add(dmDetailByUser);
                                                    }
                                                    else if (dmDetailByUserObj != null &&
                                                             string.IsNullOrEmpty(dataRow["Column" + (7 + i)].ToString()))
                                                    {
                                                        removeDMDetailByUserist.Add(dmDetailByUserObj);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        foreach (var dmDetailByUser in addDMDetailByUserist)
                                        {
                                            this.dmdeDetailByUserService.Insert(dmDetailByUser);
                                        }

                                        foreach (var dmDetailByUser in removeDMDetailByUserist)
                                        {
                                            this.dmdeDetailByUserService.Delete(dmDetailByUser);
                                        }

                                        foreach (var docRoleMatrix in updateDocRoleMatrixList)
                                        {
                                            this.docRoleMatrixService.Update(docRoleMatrix);
                                        }

                                        foreach (var dmDetailUser in updateDMDetailByUserList)
                                        {
                                            this.dmdeDetailByUserService.Update(dmDetailUser);
                                        }

                                        // Email notification for Manager of department
                                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                                        {
                                            this.NotificationForLeadEngineer(addDMDetailByUserist);
                                        }

                                        // --------------------------------------------

                                        dmObj.IsHaveDetail = true;
                                        this.dmService.Update(dmObj);
                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document distribution matrix detail file is valid. System import successfull!";
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo +
                                     "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        private void NotificationForManager(List<DocRoleMatrix> docRoleMatrixList)
        {
            var smtpClient = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                Host = ConfigurationManager.AppSettings["Host"],
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
            };

            var docRoleMatrixGroupByRole = docRoleMatrixList.GroupBy(t => t.RoleId);
            foreach (var docRoleMatrixOfRole in docRoleMatrixGroupByRole)
            {
                var roleId = docRoleMatrixOfRole.Key.GetValueOrDefault();
                var roleObj = this.roleService.GetByID(roleId);
                var message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                message.Subject = roleObj.Name + "Receive New Documents";
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;

                message.Body = "EDMS System <br/>" +
                               "Action Time: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm") + "<br/>" +
                               "====================================================================================<br/>" +
                               "\"" + roleObj.FullName + "\" have new distribute Document<br/>" +
                               @"<table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:250px'>Document number</th>
		                                        <th style='text-align:center; width:300px'>Document title</th>
		                                        <th style='text-align:center; width:60px'>Revision</th>
		                                        <th style='text-align:center; width:150px'>Distributed By</th>
	                                        </tr>";
                var subBody = string.Empty;
                for (int i = 0; i < docRoleMatrixOfRole.Count(); i++)
                {
                    subBody += @"<tr>
                                <td>" + (i+1) + @"</td>
                                <td>" + docRoleMatrixOfRole.ToList()[i].DocNo + @"</td>
                                <td>" + docRoleMatrixOfRole.ToList()[i].DocTitle + @"</td>
                                <td>" + docRoleMatrixOfRole.ToList()[i].RevisionName + @"</td>
                                <td>" + docRoleMatrixOfRole.ToList()[i].CreatedByName + @"</td>
                                </tr>";
                }

                message.Body += subBody + @"</table><br/><br/>
                        Please access to <a style='text-decoration: none' hreh='" + ConfigurationManager.AppSettings["AppLink"] + @"'>EDMS System</a> for more information.<br/><br/>
                        ====================================================================================<br/>
                        ";

                var managerEmailList = this.userService.GetAllByRoleId(roleId).Where(t => t.IsManager.GetValueOrDefault() && !string.IsNullOrEmpty(t.Email)).Select(t => t.Email).ToList();

                foreach (var managerEmail in managerEmailList)
                {
                    message.To.Add(managerEmail);
                }

                smtpClient.Send(message);
            }
        }

        private void NotificationForLeadEngineer(List<DistributionMatrixDetailByUser> dmDetailByUserList)
        {
            var smtpClient = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                Host = ConfigurationManager.AppSettings["Host"],
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
            };

            var dmDetailGroupByUser = dmDetailByUserList.GroupBy(t => t.UserId);
            foreach (var dmDetailOfUser in dmDetailGroupByUser)
            {
                var userId = dmDetailOfUser.Key.GetValueOrDefault();
                var userObj = this.userService.GetByID(userId);
                if (userObj != null && !string.IsNullOrEmpty(userObj.Email))
                {
                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = userObj.FullName + "Receive New Documents";
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;

                    message.Body = "EDMS System <br/>" +
                                   "Action Time: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm") + "<br/>" +
                                   "====================================================================================<br/>" +
                                   "\"" + userObj.FullName + "\" have new distribute Document<br/>" +
                                   @"<table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:250px'>Document number</th>
		                                        <th style='text-align:center; width:300px'>Document title</th>
		                                        <th style='text-align:center; width:60px'>Revision</th>
		                                        <th style='text-align:center; width:150px'>Distributed By</th>
	                                        </tr>";
                    var subBody = string.Empty;
                    for (int i = 0; i < dmDetailOfUser.Count(); i++)
                    {
                        subBody += @"<tr>
                                <td>" + (i + 1) + @"</td>
                                <td>" + dmDetailOfUser.ToList()[i].DocNo + @"</td>
                                <td>" + dmDetailOfUser.ToList()[i].DocTitle + @"</td>
                                <td>" + dmDetailOfUser.ToList()[i].RevisionName + @"</td>
                                <td>" + dmDetailOfUser.ToList()[i].CreatedByName + @"</td>
                                </tr>";
                    }

                    message.Body += subBody + @"</table><br/><br/>
                        Please access to <a style='text-decoration: none' hreh='" + ConfigurationManager.AppSettings["AppLink"] + @"'>EDMS System</a> for more information.<br/><br/>
                        ====================================================================================<br/>
                        ";

                    message.To.Add(userObj.Email);

                    smtpClient.Send(message);
                }
            }
        }
    }
}