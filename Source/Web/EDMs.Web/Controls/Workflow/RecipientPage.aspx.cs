﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class RecipientPage : Page
    {

        private readonly WorkflowStepService wfStepService = new WorkflowStepService();
        private readonly WorkflowService wfService = new WorkflowService();
        private readonly WorkflowDetailService wfDetailService = new WorkflowDetailService();
        private readonly ScopeProjectService projectService = new ScopeProjectService();

        private readonly RoleService roleService = new RoleService();
        private readonly UserService userService = new UserService();
        private readonly DistributionMatrixService dmService = new DistributionMatrixService();
        private int WorkflowDetailId
        {
            get
            {
                return Convert.ToInt32(this.Request.QueryString["wfdId"]);
            }
        }

        private WorkflowDetail WorkflowDetailObj
        {
            get { return this.wfDetailService.GetById(this.WorkflowDetailId); }
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["wfdId"]))
                {
                    if (this.WorkflowDetailObj != null)
                    {
                        this.LoadListBoxData(this.WorkflowDetailObj);
                    }
                }
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }

        private void LoadListBoxData(WorkflowDetail wfDetailobj)
        {
            var wfObj = this.wfService.GetById(wfDetailobj.WorkflowID.GetValueOrDefault());
            var roleList = this.roleService.GetAll(false);
            var userlist = this.userService.GetAll();


            var dmList = new List<DistributionMatrix>();
            if (UserSession.Current.IsAdmin || UserSession.Current.IsDC)
            {
                dmList = this.dmService.GetByType(wfDetailobj.ProjectID.GetValueOrDefault(), 0,
                    wfObj.TypeId.GetValueOrDefault());
            }
            else
            {
                dmList = this.dmService.GetByType(wfDetailobj.ProjectID.GetValueOrDefault(), UserSession.Current.RoleId,
                    wfObj.TypeId.GetValueOrDefault());
            }
                
            
            var selectedDM = wfDetailobj.DistributionMatrixIDs.Split('$')
                                        .Where(t => !string.IsNullOrEmpty(t))
                                        .Select(t => Convert.ToInt32(t));
            var selectedAssignRole = wfDetailobj.AssignRoleIDs.Split('$')
                                        .Where(t => !string.IsNullOrEmpty(t))
                                        .Select(t => Convert.ToInt32(t));
            var selectedAssignUser = wfDetailobj.AssignUserIDs.Split('$')
                                        .Where(t => !string.IsNullOrEmpty(t))
                                        .Select(t => Convert.ToInt32(t));
            var selectedInfoOnlyRole = wfDetailobj.InformationOnlyRoleIDs.Split('$')
                                        .Where(t => !string.IsNullOrEmpty(t))
                                        .Select(t => Convert.ToInt32(t));
            var selectedInfoOnlyUser = wfDetailobj.InformationOnlyUserIDs.Split('$')
                                        .Where(t => !string.IsNullOrEmpty(t))
                                        .Select(t => Convert.ToInt32(t));
            
            // Fill data for Assignment
            foreach (var role in roleList.Where(t => !selectedAssignRole.Contains(t.Id)))
            {
                var roleItem = new RadListBoxItem()
                {
                    Value = "G-" + role.Id,
                    Text = role.FullName,
                    ImageUrl = "~/Images/recipGroup.png"
                };
                this.lbAssignment.Items.Add(roleItem);
            }

            foreach (var role in roleList.Where(t => selectedAssignRole.Contains(t.Id)))
            {
                var roleItem = new RadListBoxItem()
                {
                    Value = "G-" + role.Id,
                    Text = role.FullName,
                    ImageUrl = "~/Images/recipGroup.png"
                };
                this.lbAssignmentSelected.Items.Add(roleItem);
            }

            foreach (var user in userlist.Where(t => !selectedAssignUser.Contains(t.Id)))
            {
                var userItem = new RadListBoxItem()
                {
                    Value = "U-" + user.Id,
                    Text = user.UserNameWithFullName,
                    ImageUrl = "~/Images/recipUser.png"
                };

                this.lbAssignment.Items.Add(userItem);
            }

            foreach (var user in userlist.Where(t => selectedAssignUser.Contains(t.Id)))
            {
                var userItem = new RadListBoxItem()
                {
                    Value = "U-" + user.Id,
                    Text = user.UserNameWithFullName,
                    ImageUrl = "~/Images/recipUser.png"
                };

                this.lbAssignmentSelected.Items.Add(userItem);
            }

            // Fill data for Information Only
            foreach (var role in roleList.Where(t => !selectedInfoOnlyRole.Contains(t.Id)))
            {
                var roleItem = new RadListBoxItem()
                {
                    Value = "G-" + role.Id,
                    Text = role.FullName,
                    ImageUrl = "~/Images/recipGroup.png"
                };
                this.lbInfoOnly.Items.Add(roleItem);
            }

            foreach (var role in roleList.Where(t => selectedInfoOnlyRole.Contains(t.Id)))
            {
                var roleItem = new RadListBoxItem()
                {
                    Value = "G-" + role.Id,
                    Text = role.FullName,
                    ImageUrl = "~/Images/recipGroup.png"
                };
                this.lbInfoOnlySelected.Items.Add(roleItem);
            }

            foreach (var user in userlist.Where(t => !selectedInfoOnlyUser.Contains(t.Id)))
            {
                var userItem = new RadListBoxItem()
                {
                    Value = "U-" + user.Id,
                    Text = user.UserNameWithFullName,
                    ImageUrl = "~/Images/recipUser.png"
                };

                this.lbInfoOnly.Items.Add(userItem);
            }

            foreach (var user in userlist.Where(t => selectedInfoOnlyUser.Contains(t.Id)))
            {
                var userItem = new RadListBoxItem()
                {
                    Value = "U-" + user.Id,
                    Text = user.UserNameWithFullName,
                    ImageUrl = "~/Images/recipUser.png"
                };

                this.lbInfoOnlySelected.Items.Add(userItem);
            }

            // File data for Marix
            foreach (var distributionMatrix in dmList.Where(t => !selectedDM.Contains(t.ID)))
            {
                var dmItem = new RadListBoxItem()
                {
                    Value = distributionMatrix.ID.ToString(),
                    Text = distributionMatrix.Name,
                    ImageUrl = "~/Images/distribution.png"
                };

                this.lbMatrix.Items.Add(dmItem);
            }

            foreach (var distributionMatrix in dmList.Where(t => selectedDM.Contains(t.ID)))
            {
                var dmItem = new RadListBoxItem()
                {
                    Value = distributionMatrix.ID.ToString(),
                    Text = distributionMatrix.Name,
                    ImageUrl = "~/Images/distribution.png"
                };

                this.lbMatrixSelected.Items.Add(dmItem);
            }
        }

        protected void btnAssignment_OnClick(object sender, EventArgs e)
        {
             var selectedRoleIds = string.Empty;
             var selectedUserIds = string.Empty;
            selectedRoleIds = this.lbAssignmentSelected.Items.Where(t => t.Value.Contains("G-")).Aggregate(selectedRoleIds, (current, t) => current + t.Value.Split('-')[1] + "$");
            selectedUserIds = this.lbAssignmentSelected.Items.Where(t => t.Value.Contains("U-")).Aggregate(selectedUserIds, (current, t) => current + t.Value.Split('-')[1] + "$");

            this.WorkflowDetailObj.AssignRoleIDs = selectedRoleIds;
            this.WorkflowDetailObj.AssignUserIDs = selectedUserIds;

            if (this.lbAssignmentSelected.Items.Count > 0)
            {
                this.WorkflowDetailObj.Recipients = "W - " + this.lbAssignmentSelected.Items[0].Text;
            }

            this.wfDetailService.Update(this.WorkflowDetailObj);
        }

        protected void btnMatrix_OnClick(object sender, EventArgs e)
        {
            var selectedDMIds = string.Empty;
            selectedDMIds = this.lbMatrixSelected.Items.Aggregate(selectedDMIds, (current, t) => current + t.Value + "$");
            this.WorkflowDetailObj.DistributionMatrixIDs = selectedDMIds;

            if (this.lbMatrixSelected.Items.Count > 0)
            {
                this.WorkflowDetailObj.Recipients = "DM - " + this.lbMatrixSelected.Items[0].Text;
            }

            this.wfDetailService.Update(this.WorkflowDetailObj);
        }

        protected void btnInfoOnly_OnClick(object sender, EventArgs e)
        {
            var selectedRoleIds = string.Empty;
            var selectedUserIds = string.Empty;
            selectedRoleIds = this.lbInfoOnlySelected.Items.Where(t => t.Value.Contains("G-")).Aggregate(selectedRoleIds, (current, t) => current + t.Value.Split('-')[1] + "$");
            selectedUserIds = this.lbInfoOnlySelected.Items.Where(t => t.Value.Contains("U-")).Aggregate(selectedUserIds, (current, t) => current + t.Value.Split('-')[1] + "$");

            this.WorkflowDetailObj.InformationOnlyRoleIDs = selectedRoleIds;
            this.WorkflowDetailObj.InformationOnlyUserIDs = selectedUserIds;

            if (this.lbInfoOnlySelected.Items.Count > 0)
            {
                this.WorkflowDetailObj.Recipients = "I - " + this.lbInfoOnlySelected.Items[0].Text;
            }

            this.wfDetailService.Update(this.WorkflowDetailObj);
        }

        private void UpdateRecipientView(ref WorkflowDetail wfDetail)
        {
            var recipient = string.Empty;
            if (this.lbAssignmentSelected.Items.Count > 0)
            {
                recipient += "W - " + this.lbAssignmentSelected.Items[0].Text;
            }

            if (this.lbInfoOnlySelected.Items.Count > 0)
            {
                recipient += "<br/>I - " + this.lbInfoOnlySelected.Items[0].Text;
            }

            if (this.lbAssignmentSelected.Items.Count > 0)
            {
                recipient += "<br/>DM" + this.lbAssignmentSelected.Items[0].Text;
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            var selectedAssignRoleIds = string.Empty;
            var selectedAssignUserIds = string.Empty;
            var selectedIinfoRoleIds = string.Empty;
            var selectedInfoUserIds = string.Empty;
            var selectedDMIds = string.Empty;

            selectedAssignRoleIds = this.lbAssignmentSelected.Items.Where(t => t.Value.Contains("G-")).Aggregate(selectedAssignRoleIds, (current, t) => current + t.Value.Split('-')[1] + "$");
            selectedAssignUserIds = this.lbAssignmentSelected.Items.Where(t => t.Value.Contains("U-")).Aggregate(selectedAssignUserIds, (current, t) => current + t.Value.Split('-')[1] + "$");

            selectedIinfoRoleIds = this.lbInfoOnlySelected.Items.Where(t => t.Value.Contains("G-")).Aggregate(selectedIinfoRoleIds, (current, t) => current + t.Value.Split('-')[1] + "$");
            selectedInfoUserIds = this.lbInfoOnlySelected.Items.Where(t => t.Value.Contains("U-")).Aggregate(selectedInfoUserIds, (current, t) => current + t.Value.Split('-')[1] + "$");

            selectedDMIds = this.lbMatrixSelected.Items.Aggregate(selectedDMIds, (current, t) => current + t.Value + "$");

            this.WorkflowDetailObj.AssignRoleIDs = selectedAssignRoleIds;
            this.WorkflowDetailObj.AssignUserIDs = selectedAssignUserIds;

            this.WorkflowDetailObj.InformationOnlyRoleIDs = selectedIinfoRoleIds;
            this.WorkflowDetailObj.InformationOnlyUserIDs = selectedInfoUserIds;

            this.WorkflowDetailObj.DistributionMatrixIDs = selectedDMIds;

            if (this.lbMatrixSelected.Items.Count > 0)
            {
                this.WorkflowDetailObj.Recipients = "DM - " + this.lbMatrixSelected.Items[0].Text + " ...";
            }

            if (this.lbInfoOnlySelected.Items.Count > 0)
            {
                this.WorkflowDetailObj.Recipients = "I - " + this.lbInfoOnlySelected.Items[0].Text + " ...";
            }

            if (this.lbAssignmentSelected.Items.Count > 0)
            {
                this.WorkflowDetailObj.Recipients = "W - " + this.lbAssignmentSelected.Items[0].Text + " ...";
            }

            this.wfDetailService.Update(this.WorkflowDetailObj);

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }
    }
}