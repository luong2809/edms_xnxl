﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DistributionMatrixDetailByUserPage : Page
    {

        private readonly DistributionMatrixService dmService = new DistributionMatrixService();
        private readonly DistributionMatrixDetailService dmDetailService = new DistributionMatrixDetailService();
        private readonly ScopeProjectService projectService = new ScopeProjectService();

        private readonly DisciplineService disciplineService = new DisciplineService();
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        private readonly RoleService roleService = new RoleService();
        private readonly UserService userService = new UserService();

        private int DistributionMatrixId
        {
            get
            {
                return Convert.ToInt32(this.Request.QueryString["dmId"]);
            }
        }

        private Data.Entities.DistributionMatrix DistributionMatrixObj
        {
            get { return this.dmService.GetById(this.DistributionMatrixId); }
        }

        private ScopeProject ProjectObj
        {
            get { return this.projectService.GetById(this.DistributionMatrixObj.ProjectId.GetValueOrDefault()); }
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
            }
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var dmId = Convert.ToInt32(this.Request.QueryString["dmId"]);
            this.grdDocument.DataSource = this.dmDetailService.GetAllByDM(dmId);    
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }

        protected void grdDocument_OnBatchEditCommand(object sender, GridBatchEditingEventArgs e)
        {
            foreach (GridBatchEditingCommand command in e.Commands)
            {
                Hashtable newValues = command.NewValues;
                ////var wfDetailID = Convert.ToInt32(newValues["ID"].ToString());
                ////var wfDetailObj = this.wfDetailService.GetById(wfDetailID);

                ////if (wfDetailObj != null)
                ////{
                ////    var wfStepDefineID = Convert.ToInt32(newValues["StepDefinitionID"].ToString());
                ////    var nextWorkflowStepID = Convert.ToInt32(newValues["NextWorkflowStepID"].ToString());
                ////    var nextWorkflowStepObj = this.wfStepService.GetById(nextWorkflowStepID);
                ////    var rejectWorkflowStepID = Convert.ToInt32(newValues["RejectWorkflowStepID"].ToString());
                ////    var rejectWorkflowStepObj = this.wfStepService.GetById(rejectWorkflowStepID);

                ////    wfDetailObj.StepDefinitionID = wfStepDefineID;
                ////    wfDetailObj.StepDefinitionName = Utility.WorkflowStepDefine[wfStepDefineID];

                ////    wfDetailObj.NextWorkflowStepID = nextWorkflowStepObj != null
                ////        ? nextWorkflowStepObj.ID
                ////        : 0;
                ////    wfDetailObj.NextWorkflowStepName = nextWorkflowStepObj != null
                ////        ? nextWorkflowStepObj.Name
                ////        : string.Empty;

                ////    wfDetailObj.RejectWorkflowStepID = rejectWorkflowStepObj != null
                ////        ? rejectWorkflowStepObj.ID
                ////        : 0;
                ////    wfDetailObj.RejectWorkflowStepName = rejectWorkflowStepObj != null
                ////        ? rejectWorkflowStepObj.Name
                ////        : string.Empty;

                ////    wfDetailObj.Duration = Convert.ToInt32(newValues["Duration"].ToString());

                ////    this.wfDetailService.Update(wfDetailObj);
                ////}
            }
        }

        protected void grdDocument_OnItemUpdated(object sender, GridUpdatedEventArgs e)
        {
            var x = 0;
            var item = (GridEditableItem)e.Item;
            var id = item.GetDataKeyValue("ID").ToString();
        }

        protected void grdDocument_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            var temp = e.CommandName;
        }

        protected void grdDocument_OnPreRender(object sender, EventArgs e)
        {
            var disciplineList = this.disciplineService.GetAllDisciplineOfProject(this.ProjectObj.ID);
            var docTypeList = this.documentTypeService.GetAllByProject(this.ProjectObj.ID);
            var roleList = this.roleService.GetAll(UserSession.Current.User.Id == 1);
            var userList = this.userService.GetAll();

            disciplineList.Insert(0, new Discipline() {ID = 0});
            docTypeList.Insert(0, new DocumentType() {ID = 0});
            roleList.Insert(0, new Role() {Id = 0});
            userList.Insert(0, new User() {Id = 0});

            var ddlDiscipline = (this.grdDocument.MasterTableView.GetBatchEditorContainer("Discipline") as Panel).FindControl("ddlDiscipline") as RadComboBox;
            var ddlDocumentType = (this.grdDocument.MasterTableView.GetBatchEditorContainer("DocumentType") as Panel).FindControl("ddlDocumentType") as RadComboBox;
            var ddlInfoOnlyRole = (this.grdDocument.MasterTableView.GetBatchEditorContainer("InfoOnlyRoles") as Panel).FindControl("ddlInfoOnlyRole") as RadComboBox;
            var ddlWorkAssignRole = (this.grdDocument.MasterTableView.GetBatchEditorContainer("WorkAssignRoles") as Panel).FindControl("ddlWorkAssignRole") as RadComboBox;
            var ddlInfoOnlyUser = (this.grdDocument.MasterTableView.GetBatchEditorContainer("InfoOnlyUsers") as Panel).FindControl("ddlInfoOnlyUser") as RadComboBox;
            var ddlWorkAssignUser = (this.grdDocument.MasterTableView.GetBatchEditorContainer("WorkAssignUsers") as Panel).FindControl("ddlWorkAssignUser") as RadComboBox;

            if (ddlDiscipline != null)
            {
                ddlDiscipline.DataSource = disciplineList;
                ddlDiscipline.DataTextField = "FullName";
                ddlDiscipline.DataValueField = "ID";
                ddlDiscipline.DataBind();
            }

            if (ddlDocumentType != null)
            {
                ddlDocumentType.DataSource = docTypeList;
                ddlDocumentType.DataTextField = "FullName";
                ddlDocumentType.DataValueField = "ID";
                ddlDocumentType.DataBind();
            }

            if (ddlInfoOnlyRole != null)
            {
                ddlInfoOnlyRole.DataSource = roleList;
                ddlInfoOnlyRole.DataTextField = "FullName";
                ddlInfoOnlyRole.DataValueField = "ID";
                ddlInfoOnlyRole.DataBind();
            }

            if (ddlWorkAssignRole != null)
            {
                ddlWorkAssignRole.DataSource = roleList;
                ddlWorkAssignRole.DataTextField = "FullName";
                ddlWorkAssignRole.DataValueField = "ID";
                ddlWorkAssignRole.DataBind();
            }

            if (ddlInfoOnlyUser != null)
            {
                ddlInfoOnlyUser.DataSource = userList;
                ddlInfoOnlyUser.DataTextField = "UserNameWithFullName";
                ddlInfoOnlyUser.DataValueField = "ID";
                ddlInfoOnlyUser.DataBind();
            }

            if (ddlWorkAssignUser != null)
            {
                ddlWorkAssignUser.DataSource = userList;
                ddlWorkAssignUser.DataTextField = "UserNameWithFullName";
                ddlWorkAssignUser.DataValueField = "ID";
                ddlWorkAssignUser.DataBind();
            }
        }

        protected void grdDocument_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;
                var ddlInfoOnlyUser = item.FindControl("ddlInfoOnlyUser") as RadComboBox;
                var ddlWorkAssignUser = item.FindControl("ddlWorkAssignUser") as RadComboBox;
                var ddlInfoOnlyRole = item.FindControl("ddlInfoOnlyRole") as RadComboBox;
                var ddlWorkAssignRole = item.FindControl("ddlWorkAssignRole") as RadComboBox;

                var disciplineList = this.disciplineService.GetAllDisciplineOfProject(this.ProjectObj.ID);
                var docTypeList = this.documentTypeService.GetAllByProject(this.ProjectObj.ID);
                var roleList = this.roleService.GetAll(UserSession.Current.User.Id == 1);
                var userList = this.userService.GetAll();

                if (ddlDiscipline != null)
                {
                    ddlDiscipline.DataSource = disciplineList;
                    ddlDiscipline.DataTextField = "FullName";
                    ddlDiscipline.DataValueField = "ID";
                    ddlDiscipline.DataBind();
                }

                if (ddlDocumentType != null)
                {
                    ddlDocumentType.DataSource = docTypeList;
                    ddlDocumentType.DataTextField = "FullName";
                    ddlDocumentType.DataValueField = "ID";
                    ddlDocumentType.DataBind();
                }

                if (ddlInfoOnlyRole != null)
                {
                    ddlInfoOnlyRole.DataSource = roleList;
                    ddlInfoOnlyRole.DataTextField = "FullName";
                    ddlInfoOnlyRole.DataValueField = "ID";
                    ddlInfoOnlyRole.DataBind();
                }

                if (ddlWorkAssignRole != null)
                {
                    ddlWorkAssignRole.DataSource = roleList;
                    ddlWorkAssignRole.DataTextField = "FullName";
                    ddlWorkAssignRole.DataValueField = "ID";
                    ddlWorkAssignRole.DataBind();
                }

                if (ddlInfoOnlyUser != null)
                {
                    ddlInfoOnlyUser.DataSource = userList;
                    ddlInfoOnlyUser.DataTextField = "UserNameWithFullName";
                    ddlInfoOnlyUser.DataValueField = "ID";
                    ddlInfoOnlyUser.DataBind();
                }

                if (ddlWorkAssignUser != null)
                {
                    ddlWorkAssignUser.DataSource = userList;
                    ddlWorkAssignUser.DataTextField = "UserNameWithFullName";
                    ddlWorkAssignUser.DataValueField = "ID";
                    ddlWorkAssignUser.DataBind();
                }

                if (e.Item is GridEditableItem)
                {
                    var DisciplineID = (item.FindControl("DisciplineID") as HiddenField).Value;
                    var DocumentTypeID = (item.FindControl("DocumentTypeID") as HiddenField).Value;
                    var InformationOnlyUserIds = (item.FindControl("InformationOnlyUserIds") as HiddenField).Value;
                    var WorkAssignUserIds = (item.FindControl("WorkAssignUserIds") as HiddenField).Value;
                    var InformationOnlyRoleIds = (item.FindControl("InformationOnlyRoleIds") as HiddenField).Value;
                    var WorkAssignRoleIds = (item.FindControl("WorkAssignRoleIds") as HiddenField).Value;

                    if (ddlDiscipline != null)
                    {
                        ddlDiscipline.SelectedValue = DisciplineID;
                    }

                    if (ddlDocumentType != null)
                    {
                        ddlDocumentType.SelectedValue = DocumentTypeID;
                    }

                    if (ddlInfoOnlyUser != null)
                    {
                        foreach (RadComboBoxItem userItem in ddlInfoOnlyUser.Items)
                        {
                            userItem.Checked = InformationOnlyUserIds.Split('$').Contains(userItem.Value);
                        }
                    }

                    if (ddlWorkAssignUser != null)
                    {
                        foreach (RadComboBoxItem userItem in ddlWorkAssignUser.Items)
                        {
                            userItem.Checked = WorkAssignUserIds.Split('$').Contains(userItem.Value);
                        }
                    }

                    if (ddlInfoOnlyRole != null)
                    {
                        foreach (RadComboBoxItem roleItem in ddlInfoOnlyRole.Items)
                        {
                            roleItem.Checked = InformationOnlyRoleIds.Split('$').Contains(roleItem.Value);
                        }
                    }

                    if (ddlWorkAssignRole != null)
                    {
                        foreach (RadComboBoxItem roleItem in ddlWorkAssignRole.Items)
                        {
                            roleItem.Checked = WorkAssignRoleIds.Split('$').Contains(roleItem.Value);
                        }
                    }
                }
            }
        }

        protected void grdDocument_OnUpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var dmDetailId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                var dmDetailObj = this.dmDetailService.GetById(dmDetailId);

                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;

                var ddlInfoOnlyUser = item.FindControl("ddlInfoOnlyUser") as RadComboBox;
                var ddlWorkAssignUser = item.FindControl("ddlWorkAssignUser") as RadComboBox;
                var ddlInfoOnlyRole = item.FindControl("ddlInfoOnlyRole") as RadComboBox;
                var ddlWorkAssignRole = item.FindControl("ddlWorkAssignRole") as RadComboBox;

                var infoOnlyUserIds = string.Empty;
                var infoOnlyUserNames = string.Empty;

                var workAssignUserIds = string.Empty;
                var workAssignUserNames = string.Empty;

                var infoOnlyRoleIds = string.Empty;
                var infoOnlyRoleNames = string.Empty;

                var workAssignRoleIds = string.Empty;
                var workAssignRoleNames = string.Empty;

                if (ddlInfoOnlyUser != null)
                {
                    infoOnlyUserIds = ddlInfoOnlyUser.CheckedItems.Aggregate(infoOnlyUserIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    infoOnlyUserNames = ddlInfoOnlyUser.CheckedItems.Aggregate(infoOnlyUserNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                if (ddlWorkAssignUser != null)
                {
                    workAssignUserIds = ddlWorkAssignUser.CheckedItems.Aggregate(workAssignUserIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    workAssignUserNames = ddlWorkAssignUser.CheckedItems.Aggregate(workAssignUserNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                if (ddlInfoOnlyRole != null)
                {
                    infoOnlyRoleIds = ddlInfoOnlyRole.CheckedItems.Aggregate(infoOnlyRoleIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    infoOnlyRoleNames = ddlInfoOnlyRole.CheckedItems.Aggregate(infoOnlyRoleNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                if (ddlWorkAssignRole != null)
                {
                    workAssignRoleIds = ddlWorkAssignRole.CheckedItems.Aggregate(workAssignRoleIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    workAssignRoleNames = ddlWorkAssignRole.CheckedItems.Aggregate(workAssignRoleNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                if (dmDetailObj != null)
                {
                    dmDetailObj.DisciplineID = ddlDiscipline != null && ddlDiscipline.SelectedItem != null
                        ? Convert.ToInt32(ddlDiscipline.SelectedValue)
                        : 0;
                    dmDetailObj.DisciplineName = ddlDiscipline != null && ddlDiscipline.SelectedItem != null
                        ? ddlDiscipline.SelectedItem.Text
                        : string.Empty;

                    dmDetailObj.DocumentTypeID = ddlDocumentType != null && ddlDocumentType.SelectedItem != null
                        ? Convert.ToInt32(ddlDocumentType.SelectedValue)
                        : 0;
                    dmDetailObj.DocumentTypeName = ddlDocumentType != null && ddlDocumentType.SelectedItem != null
                        ? ddlDocumentType.SelectedItem.Text
                        : string.Empty;

                    dmDetailObj.InformationOnlyRoleIds = infoOnlyRoleIds;
                    dmDetailObj.InformationOnlyRoleNames = infoOnlyRoleNames;
                    dmDetailObj.InformationOnlyUserIds = infoOnlyUserIds;
                    dmDetailObj.InformationOnlyUserNames = infoOnlyUserNames;

                    dmDetailObj.WorkAssignRoleIds = workAssignRoleIds;
                    dmDetailObj.WorkAssignRoleNames = workAssignRoleNames;
                    dmDetailObj.WorkAssignUserIds = workAssignUserIds;
                    dmDetailObj.WorkAssignUserNames = workAssignUserNames;

                    this.dmDetailService.Update(dmDetailObj);
                }
            }
        }

        protected void grdDocument_OnInsertCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridDataInsertItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;

                var ddlInfoOnlyUser = item.FindControl("ddlInfoOnlyUser") as RadComboBox;
                var ddlWorkAssignUser = item.FindControl("ddlWorkAssignUser") as RadComboBox;
                var ddlInfoOnlyRole = item.FindControl("ddlInfoOnlyRole") as RadComboBox;
                var ddlWorkAssignRole = item.FindControl("ddlWorkAssignRole") as RadComboBox;

                var infoOnlyUserIds = string.Empty;
                var infoOnlyUserNames = string.Empty;

                var workAssignUserIds = string.Empty;
                var workAssignUserNames = string.Empty;

                var infoOnlyRoleIds = string.Empty;
                var infoOnlyRoleNames = string.Empty;

                var workAssignRoleIds = string.Empty;
                var workAssignRoleNames = string.Empty;

                if (ddlInfoOnlyUser != null)
                {
                    infoOnlyUserIds = ddlInfoOnlyUser.CheckedItems.Aggregate(infoOnlyUserIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    infoOnlyUserNames = ddlInfoOnlyUser.CheckedItems.Aggregate(infoOnlyUserNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                if (ddlWorkAssignUser != null)
                {
                    workAssignUserIds = ddlWorkAssignUser.CheckedItems.Aggregate(workAssignUserIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    workAssignUserNames = ddlWorkAssignUser.CheckedItems.Aggregate(workAssignUserNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                if (ddlInfoOnlyRole != null)
                {
                    infoOnlyRoleIds = ddlInfoOnlyRole.CheckedItems.Aggregate(infoOnlyRoleIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    infoOnlyRoleNames = ddlInfoOnlyRole.CheckedItems.Aggregate(infoOnlyRoleNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                if (ddlWorkAssignRole != null)
                {
                    workAssignRoleIds = ddlWorkAssignRole.CheckedItems.Aggregate(workAssignRoleIds, (current, checkedItem) => current + checkedItem.Value + "$");
                    workAssignRoleNames = ddlWorkAssignRole.CheckedItems.Aggregate(workAssignRoleNames, (current, checkedItem) => current + checkedItem.Text + "<br/>");
                }

                var dmDetailObj = new DistributionMatrixDetail()
                {
                    ProjectID = this.ProjectObj.ID,
                    ProjectName = this.ProjectObj.Name,

                    DisciplineID = ddlDiscipline != null && ddlDiscipline.SelectedItem != null 
                                    ? Convert.ToInt32(ddlDiscipline.SelectedValue)
                                    : 0,
                    DisciplineName = ddlDiscipline != null && ddlDiscipline.SelectedItem != null
                                    ? ddlDiscipline.SelectedItem.Text
                                    : string.Empty,

                    DocumentTypeID = ddlDocumentType != null && ddlDocumentType.SelectedItem != null
                                    ? Convert.ToInt32(ddlDocumentType.SelectedValue)
                                    : 0,
                    DocumentTypeName = ddlDocumentType != null && ddlDocumentType.SelectedItem != null
                                    ? ddlDocumentType.SelectedItem.Text
                                    : string.Empty,

                    InformationOnlyRoleIds = infoOnlyRoleIds,
                    InformationOnlyRoleNames = infoOnlyRoleNames,
                    InformationOnlyUserIds = infoOnlyUserIds,
                    InformationOnlyUserNames = infoOnlyUserNames,

                    WorkAssignRoleIds = workAssignRoleIds,
                    WorkAssignRoleNames = workAssignRoleNames,
                    WorkAssignUserIds = workAssignUserIds,
                    WorkAssignUserNames = workAssignUserNames,

                    DistributionMatrixId = this.DistributionMatrixObj.ID,
                    DistributionMatrixName = this.DistributionMatrixObj.Name
                };

                this.dmDetailService.Insert(dmDetailObj);
            }
        }

        protected void grdDocument_OnDeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var dmId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.dmDetailService.Delete(dmId);
            this.grdDocument.Rebind();
        }
    }
}