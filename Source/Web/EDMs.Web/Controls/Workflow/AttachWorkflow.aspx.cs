﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AttachWorkflow : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly WorkflowService wfService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly ScopeProjectService projectService;
        private readonly DocumentAssignedWorkflowService docAssignedWfService;
        private readonly DocumentAssignedUserService docAssignedUserService;
        private readonly DocumentPackageService documentPackageService;
        private readonly WorkflowStepService wfStepService;
        private readonly WorkflowDetailService wfDetailService;
        private readonly DistributionMatrixService dmService;
        private readonly DistributionMatrixDetailService dmDetailService;

        private int DocId
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["docId"]);
            }
        }

        private DocumentPackage DocObj
        {
            get { return documentPackageService.GetById(DocId); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AttachWorkflow()
        {
            this.userService = new UserService();
            this.wfService = new WorkflowService();
            this.projectService = new ScopeProjectService();
            this.docAssignedUserService = new DocumentAssignedUserService();
            this.docAssignedWfService = new DocumentAssignedWorkflowService();
            this.documentPackageService = new DocumentPackageService();
            this.wfStepService = new WorkflowStepService();
            this.wfDetailService = new WorkflowDetailService();
            this.dmDetailService = new DistributionMatrixDetailService();
            this.dmService = new DistributionMatrixService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                LoadComboData();
                
                if (!string.IsNullOrEmpty(Request.QueryString["disId"]))
                {
                    var objDocumentStatus = wfService.GetById(Convert.ToInt32(Request.QueryString["disId"]));
                    CreatedInfo.Visible = objDocumentStatus != null;

                    if (objDocumentStatus != null)
                    {
                        txtDescription.Text = objDocumentStatus.Description;
                        ddlWorkflow.SelectedValue = objDocumentStatus.ProjectID.GetValueOrDefault().ToString();
                        var createdUser = userService.GetByID(objDocumentStatus.CreatedBy.GetValueOrDefault());

                        lblCreated.Text = "Created at " + objDocumentStatus.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objDocumentStatus.UpdatedBy != null && objDocumentStatus.UpdatedDate != null)
                        {
                            lblCreated.Text += "<br/>";
                            var lastUpdatedUser = userService.GetByID(objDocumentStatus.UpdatedBy.GetValueOrDefault());
                            lblUpdated.Text = "Last modified at " + objDocumentStatus.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (DocObj != null)
                {
                    var wfObj = this.wfService.GetById(Convert.ToInt32(ddlWorkflow.SelectedValue));
                    if (wfObj != null)
                    {
                        var wfFirstStepObj = this.wfStepService.GetFirstStep(wfObj.ID);
                        if (wfFirstStepObj != null)
                        {
                            this.ProcessWorkflow(wfFirstStepObj, wfObj);
                        }
                    }
                }

                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        private void ProcessWorkflow(WorkflowStep wfStepObj, Data.Entities.Workflow wfObj)
        {
            var wfDetailObj = this.wfDetailService.GetByCurrentStep(wfStepObj.ID);
            if (wfDetailObj != null)
            {
                var assignWorkFlow = new DocumentAssignedWorkflow
                {
                    DocumentID = this.DocObj.ID,
                    DocumentNumber = this.DocObj.DocNo,
                    WorkflowID = wfObj.ID,
                    WorkflowName = wfObj.Name,
                    CurrentWorkflowStepID = wfDetailObj.CurrentWorkflowStepID,
                    CurrentWorkflowStepName = wfDetailObj.CurrentWorkflowStepName,
                    NextWorkflowStepID = wfDetailObj.NextWorkflowStepID,
                    NextWorkflowStepName = wfDetailObj.NextWorkflowStepName,
                    RejectWorkflowStepID = wfDetailObj.RejectWorkflowStepID,
                    RejectWorkflowStepName = wfDetailObj.RejectWorkflowStepName,
                    IsComplete = true,
                    IsReject = false,
                    IsLeaf = false,
                    AssignedBy = UserSession.Current.User.Id,
                    CanReject = wfStepObj.CanReject
                };

                var assignWorkflowId = this.docAssignedWfService.Insert(assignWorkFlow);

                if (wfDetailObj.StepDefinitionID != 1)
                {
                    assignWorkFlow.IsComplete = false;
                    assignWorkFlow.IsLeaf = true;
                    this.docAssignedWfService.Update(assignWorkFlow);
                    if (assignWorkflowId != null)
                    {
                        var assignUserIds = this.GetAssignUserList(wfDetailObj);

                        foreach (var assignUserId in assignUserIds)
                        {
                            var userObj = this.userService.GetByID(assignUserId);
                            var assignUser = new DocumentAssignedUser
                            {
                                DocumentAssignedWorkflowID = assignWorkflowId,
                                DocumentID = DocObj.ID,
                                UserID = assignUserId,
                                UserName = userObj.UserNameWithFullName,
                                ReceivedDate = DateTime.Now,
                                PlanCompleteDate = DateTime.Now.AddDays(wfDetailObj.Duration.GetValueOrDefault()),
                                IsOverDue = false,
                                IsComplete = false,
                                IsReject = false,
                                AssignedBy = UserSession.Current.User.Id,
                                AssignedByName = UserSession.Current.User.UserNameWithFullName,
                                WorkflowId = wfObj.ID,
                                WorkflowName = wfObj.Name,
                                CurrentWorkflowStepName = wfStepObj.Name,
                                CurrentWorkflowStepId = wfStepObj.ID,
                                CanReject = wfStepObj.CanReject
                            };
                            docAssignedUserService.Insert(assignUser);
                        }
                    }

                    this.DocObj.IsInWFProcess = true;
                    this.DocObj.IsWFComplete = false;
                    this.DocObj.CurrentWorkflowName = wfObj.Name;
                    this.DocObj.CurrentWorkflowStepName = assignWorkFlow.CurrentWorkflowStepName;
                    this.documentPackageService.Update(DocObj);
                }
                else
                {
                    // Implement send notification info for "Info Assign"
                    //----------------------------------------------------

                    // Goto next step have assign info.
                    var wfNextStepObj = this.wfStepService.GetById(wfDetailObj.NextWorkflowStepID.GetValueOrDefault());
                    if (wfNextStepObj != null)
                    {
                        this.ProcessWorkflow(wfNextStepObj, wfObj);
                    }
                }
            }
        }

        private List<int> GetAssignUserList(WorkflowDetail wfDetailObj)
        {
            var assignUserIds = new List<int>();
            assignUserIds.AddRange(wfDetailObj.AssignUserIDs.Split('$')
                                                .Where(t => !string.IsNullOrEmpty(t))
                                                .Select(t => Convert.ToInt32(t)));

            foreach (var roleId in wfDetailObj.AssignRoleIDs.Split('$')
                                                .Where(t => !string.IsNullOrEmpty(t))
                                                .Select(t => Convert.ToInt32(t)))
            {
                assignUserIds.AddRange(this.userService.GetAllByRoleId(roleId).Select(t => t.Id));
            }

            foreach (var dmId in wfDetailObj.DistributionMatrixIDs.Split('$')
                                                .Where(t => !string.IsNullOrEmpty(t))
                                                .Select(t => Convert.ToInt32(t)))
            {
                var dmObj = this.dmService.GetById(dmId);
                if (dmObj != null)
                {
                    var dmDetail = this.dmDetailService.GetAllByDM(dmObj.ID)
                                    .FirstOrDefault(t => t.DisciplineID == this.DocObj.DisciplineId
                                                    && t.DocumentTypeID == this.DocObj.DocumentTypeId);
                    if (dmDetail != null)
                    {
                        assignUserIds.AddRange(dmDetail.WorkAssignUserIds.Split('$')
                                                .Where(t => !string.IsNullOrEmpty(t))
                                                .Select(t => Convert.ToInt32(t)));

                        foreach (var roleId in dmDetail.WorkAssignRoleIds.Split('$')
                                                .Where(t => !string.IsNullOrEmpty(t))
                                                .Select(t => Convert.ToInt32(t)))
                        {
                            assignUserIds.AddRange(this.userService.GetAllByRoleId(roleId).Select(t => t.Id));
                        }
                    }
                }
            }

            assignUserIds = assignUserIds.Distinct().ToList();

            return assignUserIds;
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        private void LoadComboData()
        {
            if (DocObj != null)
            {
                var wfList = wfService.GetAllByProject(DocObj.ProjectId.GetValueOrDefault());

                wfList.Insert(0, new Data.Entities.Workflow {ID = 0});
                ddlWorkflow.DataSource = wfList;
                ddlWorkflow.DataTextField = "Name";
                ddlWorkflow.DataValueField = "ID";
                ddlWorkflow.DataBind();
            }
        }

        protected void ddlWorkflow_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var wfId = Convert.ToInt32(ddlWorkflow.SelectedValue);
            var wfObj = wfService.GetById(wfId);
            if (wfObj != null)
            {
                txtDescription.Text = wfObj.Description;
            }
        }
    }
}