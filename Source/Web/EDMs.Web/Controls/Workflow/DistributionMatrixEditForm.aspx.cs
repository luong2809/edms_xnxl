﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DistributionMatrixEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DistributionMatrixService dmService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly ScopeProjectService projectService;

        private readonly RoleService roleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DistributionMatrixEditForm()
        {
            this.userService = new UserService();
            this.dmService = new DistributionMatrixService();
            this.projectService = new ScopeProjectService();
            this.roleService = new RoleService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var dmObj = this.dmService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    this.CreatedInfo.Visible = dmObj != null;

                    if (dmObj != null)
                    {
                        this.txtName.Text = dmObj.Name;
                        this.txtDescription.Text = dmObj.Description;
                        this.ddlProject.SelectedValue = dmObj.ProjectId.GetValueOrDefault().ToString();
                        this.ddlRole.SelectedValue = dmObj.RoleId.GetValueOrDefault().ToString();
                        this.ddlType.SelectedValue = dmObj.TypeId.GetValueOrDefault().ToString();
                        var createdUser = this.userService.GetByID(dmObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + dmObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (dmObj.UpdatedBy != null && dmObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(dmObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + dmObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var documentStatusId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.dmService.GetById(documentStatusId);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                        obj.ProjectName = this.ddlProject.SelectedItem.Text;
                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;
                        obj.RoleId = this.ddlRole.SelectedItem != null 
                            ? Convert.ToInt32(this.ddlRole.SelectedValue) 
                            : 0;
                        obj.RoleName = this.ddlRole.SelectedItem != null 
                            ? this.ddlRole.SelectedItem.Text 
                            : string.Empty;
                        obj.TypeId = Convert.ToInt32(this.ddlType.SelectedValue);
                        obj.TypeName = this.ddlType.SelectedItem.Text;
                        this.dmService.Update(obj);
                    }
                }
                else
                {
                    var obj = new Data.Entities.DistributionMatrix()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                        ProjectName = this.ddlProject.SelectedItem.Text,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        RoleId = this.ddlRole.SelectedItem != null
                            ? Convert.ToInt32(this.ddlRole.SelectedValue)
                            : 0,
                        RoleName = this.ddlRole.SelectedItem != null
                            ? this.ddlRole.SelectedItem.Text
                            : string.Empty,
                        IsHaveDetail = false,
                        IsCanDelete = true,
                        TypeId = Convert.ToInt32(this.ddlType.SelectedValue),
                        TypeName = this.ddlType.SelectedItem.Text

                    };

                    this.dmService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Distribution Matrix name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            var projectList = this.projectService.GetAll();
            this.ddlProject.DataSource = projectList;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            var roleList = this.roleService.GetAll(false);
            roleList.Insert(0, new Role(){Id = 0, Name = "General"});
            this.ddlRole.DataSource = roleList;
            this.ddlRole.DataTextField = "FullName";
            this.ddlRole.DataValueField = "ID";
            this.ddlRole.DataBind();

            if (UserSession.Current.IsManager || UserSession.Current.IsLead || UserSession.Current.IsEngineer)
            {
                this.ddlRole.SelectedValue = UserSession.Current.RoleId.ToString();
            }
        }
    }
}