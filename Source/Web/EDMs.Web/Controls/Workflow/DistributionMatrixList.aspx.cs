﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class DistributionMatrixList : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly DistributionMatrixService dmService = new DistributionMatrixService();

        private readonly UserService userService = new UserService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly DocRoleMatrixService docRoleMatrixService = new DocRoleMatrixService();

        private readonly DistributionMatrixDetailByUserService dmDetailByUserService = new DistributionMatrixDetailByUserService();

        private readonly ProjectDepartmentService projectDepartmentService = new ProjectDepartmentService();
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();
        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.Page.IsPostBack)
            {
                this.LoadSystemPanel();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
            else if (e.Argument.Contains("ExportDistributionMatrix"))
            {
                var dmId = Convert.ToInt32(e.Argument.Split('_')[1]);
                this.ExportDistributionMatrix(dmId);
            }
            else if (e.Argument.Contains("Delete"))
            {
                var dmId = Convert.ToInt32(e.Argument.Split('_')[1]);
                this.dmService.Delete(dmId);

                this.grdDocument.Rebind();

            }
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var dmList = new List<DistributionMatrix>();
            if (UserSession.Current.IsAdmin || UserSession.Current.IsDC)
            {
                dmList = this.dmService.GetAll();
            }
            else
            {
                dmList = this.dmService.GetAllGroup(UserSession.Current.RoleId);
            }

            this.grdDocument.DataSource = dmList;
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format(
                    "return ShowEditForm('{0}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"]);
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var wfId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.dmService.Delete(wfId);

            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            //AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);

            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC && !UserSession.Current.IsManager)
            {
                permissions = permissions.Where(t => t.MenuId != 63).ToList();
            }

            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void ExportDistributionMatrix(int dmId)
        {
            var dmObj = this.dmService.GetById(dmId);
            if (dmObj != null)
            {
                // Get department joined in project
                var departmentJoinProject = this.projectDepartmentService.GetAllByProject(dmObj.ProjectId.GetValueOrDefault()).Select(t => t.DepartmentId.GetValueOrDefault()).ToList();
                // -----------------------------------
                var userList = new List<User>();
                if (dmObj.RoleId == 0)
                {
                    
                    if (departmentJoinProject.Count > 0)
                    {
                        userList = this.userService.GetAll().Where(t => t.Id != 1 && departmentJoinProject.Contains(t.RoleId.GetValueOrDefault())).ToList();
                    }
                    else
                    {
                        userList = this.userService.GetAll().Where(t => t.Id != 1).ToList();
                    }
                    
                }
                else
                {
                    userList = this.userService.GetAllByRoleId(UserSession.Current.RoleId);
                }

                var leadUsers = userList.Where(t => t.IsLeader.GetValueOrDefault()).OrderBy(t => t.Role.Name).ThenBy(t => t.Username).ToList();
                var engUsers = userList.Where(t => t.IsEngineer.GetValueOrDefault()).OrderBy(t => t.Role.Name).ThenBy(t => t.Username).ToList();

                var filePath = Server.MapPath("../../Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\DocDistributionmatrixByUser_OneProject_Template.xlsm");
                var dataSheet = workbook.Worksheets[0];
                var tempDataSheet = workbook.Worksheets[1];

                var dtFull = new DataTable();
                dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocID", typeof (String)),
                        new DataColumn("NoIndex", typeof (String)),
                        new DataColumn("DocNo", typeof (String)),
                        new DataColumn("DocTitle", typeof (String)),
                        new DataColumn("DocRev", typeof (String)),
                    });

                for (int i = 0; i < leadUsers.Count; i++)
                {
                    dataSheet.Cells[2, 6 + i].PutValue(leadUsers[i].FullName + "\n" + "(" + leadUsers[i].Role.Name + ")");
                    dataSheet.Cells[3, 6 + i].PutValue(i + 1);
                    dataSheet.Cells[4, 6 + i].PutValue(leadUsers[i].Id);

                    // Add dynamic matrix column
                    var colGroup = new DataColumn("Col_" + leadUsers[i].Id, typeof(String));
                    dtFull.Columns.Add(colGroup);
                }

                dataSheet.Cells.Merge(1, 6, 1, leadUsers.Count);
                dataSheet.Cells[1, 6].PutValue("Lead");

                for (int i = 0; i < engUsers.Count; i++)
                {
                    dataSheet.Cells[2, 6 + i + leadUsers.Count].PutValue(engUsers[i].FullName + "\n" + "(" + engUsers[i].Role.Name + ")");
                    dataSheet.Cells[3, 6 + i + leadUsers.Count].PutValue(i + 1 + leadUsers.Count);
                    dataSheet.Cells[4, 6 + i + leadUsers.Count].PutValue(engUsers[i].Id);

                    // Add dynamic matrix column
                    var colGroup = new DataColumn("Col_" + engUsers[i].Id, typeof(String));
                    dtFull.Columns.Add(colGroup);
                }

                dataSheet.Cells.Merge(1, 6 + leadUsers.Count, 1, engUsers.Count);
                dataSheet.Cells[1, 6 + leadUsers.Count].PutValue("Engineer");

                dataSheet.Cells["E1"].PutValue(dataSheet.Cells["E1"].Value.ToString()
                    .Replace("<ProjectName>", dmObj.ProjectName));

                var docList = new List<DocumentPackage>();
                if (dmObj.RoleId == 0)
                {
                    if (dmObj.TypeId == 1)
                    {
                        docList = this.documentPackageService.GetAllLatestRev(dmObj.ProjectId.GetValueOrDefault());
                    }
                    else
                    {
                        docList = this.documentPackageService.GetAllLatestRev(dmObj.ProjectId.GetValueOrDefault()).Where(t => t.TypeId == 2).ToList();
                    }
                }
                else
                {
                    if (dmObj.TypeId == 1)
                    {

                        var docRoleMatrix =
                            this.docRoleMatrixService.GetAllDocRoleMatrixByGroup(dmObj.RoleId.GetValueOrDefault(),
                                dmObj.ProjectId.GetValueOrDefault());
                        if (docRoleMatrix.Count > 0)
                        {
                            var docIds = docRoleMatrix.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
                            docList = this.documentPackageService.GetSpecialList(docIds);
                        }


                    }
                    else
                    {
                        var projectObj = this.scopeProjectService.GetById(dmObj.ProjectId.GetValueOrDefault());
                        var refdocList = new List<DocumentPackage>();
                        if (projectObj != null)
                        {
                            var fullDocList = this.documentPackageService.GetAllLatestRev(projectObj.ID);

                            fullDocList = fullDocList.Where(t => t.TypeId == 2).ToList();
                            if (UserSession.Current.IsAdmin || UserSession.Current.User.Id == projectObj.DCId ||
                                UserSession.Current.User.Id == projectObj.PMId)
                            {
                                refdocList = fullDocList;
                            }
                            else if (UserSession.Current.IsManager || UserSession.Current.IsDC)
                            {
                                var docRoleMatrix =
                                    this.docRoleMatrixService.GetAllDocRoleMatrixByGroup(UserSession.Current.RoleId,
                                        dmObj.ProjectId.GetValueOrDefault());
                                var docIds = docRoleMatrix.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();

                                refdocList = fullDocList.Where(t => docIds.Contains(t.ID)).ToList();
                            }
                            else if (UserSession.Current.IsLead || UserSession.Current.IsEngineer)
                            {
                                var dmDetailByUserList =
                                    this.dmDetailByUserService.GetAllDMDetailByUser(UserSession.Current.User.Id);
                                if (dmDetailByUserList.Count > 0)
                                {
                                    var docIds =
                                        dmDetailByUserList.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
                                    refdocList = fullDocList.Where(t => docIds.Contains(t.ID)).ToList();
                                }
                            }
                            var refDocIds = refdocList.Select(t => t.ID);


                            docList = this.documentPackageService.GetAllLatestRev(projectObj.ID)
                                .Where(t => t.TypeId == 2
                                            && refDocIds.Contains(t.ReferenceDocId.GetValueOrDefault())).ToList();
                        }
                    }
                }

                var count = 1;
                var docGroupBydocType = docList.GroupBy(t => t.DocumentTypeName);
                foreach (var docListOfDocType in docGroupBydocType)
                {
                    var dataRow = dtFull.NewRow();
                    dataRow["DocID"] = 0;
                    dataRow["DocNo"] = docListOfDocType.Key;
                    dtFull.Rows.Add(dataRow);
                    //count += 1;

                    foreach (var document in docListOfDocType.Where(t=> t.IsEMDR.GetValueOrDefault()).OrderBy(t => t.DocNo).ToList())
                    {
                        if (document.DocNo.Length > 0)
                        {
                            dataRow = dtFull.NewRow();
                            dataRow["DocID"] = document.ID;
                            dataRow["NoIndex"] = count;
                            dataRow["DocNo"] = document.DocNo;
                            dataRow["DocTitle"] = document.DocTitle;
                            dataRow["DocRev"] = document.RevisionName;

                            var dmDetailList = this.dmDetailByUserService.GetAllByDoc(document.ID, dmId);
                            foreach (var dmDetailItem in dmDetailList)
                            {
                                dataRow["Col_" + dmDetailItem.UserId] = "X";
                            }

                            dtFull.Rows.Add(dataRow);

                            count += 1;
                        }
                    }
                }

                dataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                tempDataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                if (dmObj.RoleId != 0)
                {
                    dataSheet.Cells["E2"].PutValue("Dept: " + UserSession.Current.User.Role.FullName);
                }
                dataSheet.Cells["A1"].PutValue(dmObj.ID);
                dataSheet.Cells["A2"].PutValue(dtFull.Rows.Count);
                dataSheet.Cells["A3"].PutValue(leadUsers.Count + engUsers.Count);

                tempDataSheet.IsVisible = false;

                var filename = dmObj.Name + "$" + "DocDistributionMatrixDetail.xlsm";
                workbook.Save(filePath + filename);
                this.Download_File(filePath + filename);
            }
        }

        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }
    }
}

