﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportAutoDistributionMatrixDetail : Page
    {
        private readonly DocumentPackageService documentPackageService;

        private readonly DistributionMatrixDetailByUserService dmdeDetailByUserService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly StatusService statusService;

        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportAutoDistributionMatrixDetail()
        {
            this.documentPackageService = new DocumentPackageService();
            this.dmdeDetailByUserService = new DistributionMatrixDetailByUserService();
            this.scopeProjectService = new ScopeProjectService();
            this.statusService = new StatusService();
            this.userService = new UserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var addDocRoleMatrixList = new List<DistributionMatrixDetailByUser>();
            var removeDocRoleMatrixList = new List<DistributionMatrixDetailByUser>();
            var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
            var projectObj = this.scopeProjectService.GetById(projectId);

            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);

                        var datasheet = workbook.Worksheets[0];

                        currentSheetName = datasheet.Name;
                        currentDocumentNo = string.Empty;
                        var rowCount = Convert.ToInt32(datasheet.Cells["A2"].Value);
                        var userCount = Convert.ToInt32(datasheet.Cells["A3"].Value);

                        var docdataTable = datasheet.Cells.ExportDataTable(5, 0, rowCount, 6 + userCount);
                        var groupDataTable = datasheet.Cells.ExportDataTable(2, 6, 3, userCount);


                        var DMList0 = this.dmdeDetailByUserService.GetDM(0);
                        foreach (DataRow dataRow in docdataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) &&
                                !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                            {
                                var docId = Convert.ToInt32(dataRow["Column2"].ToString());
                                var docObj = this.documentPackageService.GetById(docId);
                                if (docObj != null)
                                {
                                    currentDocumentNo = docObj.DocNo;

                                    // Case for internal project
                                    if (projectObj.ProjectTypeId == 1)
                                    {
                                        for (int i = 0; i < userCount; i++)
                                        {
                                            var userId = Convert.ToInt32(groupDataTable.Rows[2]["Column" + (1 + i)].ToString());
                                            var userFullName = groupDataTable.Rows[0]["Column" + (1 + i)].ToString();
                                            var docRoleMatrixObj = DMList0.FirstOrDefault(t => t.DocId == docId && t.UserId == userId);

                                            if (docRoleMatrixObj == null
                                                && !string.IsNullOrEmpty(dataRow["Column" + (7 + i)].ToString()))
                                            {
                                                var docRoleMatrix = new DistributionMatrixDetailByUser()
                                                {
                                                    DMId = 0,
                                                    DMName = "Auto Distribution Matrix Of Project",
                                                    DocId = docObj.ID,
                                                    DocNo = docObj.DocNo,
                                                    DocTitle = docObj.DocTitle,
                                                    RevisionId = docObj.RevisionId,
                                                    RevisionName = docObj.RevisionName,
                                                    UserId = userId,
                                                    UserName = userFullName,
                                                    ProjectId = docObj.ProjectId,
                                                    ProjectName = docObj.ProjectName,
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    CreatedByName = UserSession.Current.User.FullName,
                                                    CreatedDate = DateTime.Now,
                                                    Deadline = docObj.PlanedDate,
                                                    TypeId = docObj.TypeId,
                                                    TypeName = docObj.TypeId == 1 ? "EMDR" : "Shop Drawing",
                                                    IsDistributed = false,
                                                };
                                                addDocRoleMatrixList.Add(docRoleMatrix);
                                            }
                                            else if (docRoleMatrixObj != null &&
                                                     string.IsNullOrEmpty(dataRow["Column" + (7 + i)].ToString()))
                                            {
                                                removeDocRoleMatrixList.Add(docRoleMatrixObj);
                                            }
                                        }
                                    }
                                    // ----------------------------------------------------------------------------------
                                    // Case for service project
                                    else
                                    {
                                        for (int i = 0; i < userCount; i++)
                                        {
                                            var userId = Convert.ToInt32(groupDataTable.Rows[2]["Column" + (1 + i)].ToString());
                                            var userFullName = groupDataTable.Rows[0]["Column" + (1 + i)].ToString();
                                            var existDMByUserAndDoc = DMList0.Where(t=>t.DocId == docId && t.UserId == userId).ToList();

                                            if (!string.IsNullOrEmpty(dataRow["Column" + (7 + i)].ToString()))
                                            {
                                                var statusNameList = dataRow["Column" + (7 + i)].ToString().Split(',').Where(t => !string.IsNullOrEmpty(t)).ToList();
                                                foreach (var statusName in statusNameList)
                                                {
                                                    if (existDMByUserAndDoc.All(t => t.DocStatusName != statusName))
                                                    {
                                                        var statusObj = this.statusService.GetByName(statusName, projectObj.ID);
                                                        if (statusObj != null)
                                                        {
                                                            var docRoleMatrix = new DistributionMatrixDetailByUser()
                                                            {
                                                                DMId = 0,
                                                                DMName = "Auto Distribution Matrix Of Project",
                                                                DocId = docObj.ID,
                                                                DocNo = docObj.DocNo,
                                                                DocTitle = docObj.DocTitle,
                                                                RevisionId = docObj.RevisionId,
                                                                RevisionName = docObj.RevisionName,
                                                                UserId = userId,
                                                                UserName = userFullName,
                                                                ProjectId = docObj.ProjectId,
                                                                ProjectName = docObj.ProjectName,
                                                                CreatedBy = UserSession.Current.User.Id,
                                                                CreatedByName = UserSession.Current.User.FullName,
                                                                CreatedDate = DateTime.Now,
                                                                Deadline = docObj.PlanedDate,
                                                                TypeId = docObj.TypeId,
                                                                TypeName = docObj.TypeId == 1 ? "EMDR" : "Shop Drawing",
                                                                IsDistributed = false,
                                                            };

                                                            docRoleMatrix.DocStatusId = statusObj.ID;
                                                            docRoleMatrix.DocStatusName = statusObj.Name;

                                                            addDocRoleMatrixList.Add(docRoleMatrix);
                                                        }
                                                    }
                                                }

                                                removeDocRoleMatrixList.AddRange(existDMByUserAndDoc.Where(t => !statusNameList.Contains(t.DocStatusName)));
                                            }
                                            else
                                            {
                                                removeDocRoleMatrixList.AddRange(existDMByUserAndDoc);
                                            }
                                        }
                                    }
                                    // -----------------------------------------------------------------------------
                                }
                            }
                        }

                        if (this.cbCheckValidFile.Checked)
                        {
                            foreach (var docRoleMatrix in addDocRoleMatrixList)
                            {
                                this.dmdeDetailByUserService.Insert(docRoleMatrix);
                            }

                            foreach (var docRoleMatrix in removeDocRoleMatrixList)
                            {
                                this.dmdeDetailByUserService.Delete(docRoleMatrix);
                            }


                            this.blockError.Visible = true;
                            this.lblError.Text =
                                "Data of document distribution matrix detail file is valid. System import successfull!";
                        }
                        else
                        {
                            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                "CloseAndRebind();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo +
                                     "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}