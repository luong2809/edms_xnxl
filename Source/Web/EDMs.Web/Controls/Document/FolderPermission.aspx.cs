﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Library;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class FolderPermission : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The user data permission.
        /// </summary>
        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The Service WorkGroup 
        /// </summary>
        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;
        private bool ProjectRoot;
        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        } 


        /// <summary>
        /// Initializes a new instance of the <see cref="FolderPermission"/> class.
        /// </summary>
        public FolderPermission()
        {
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.userService = new UserService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.workGroupService=new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.ProjectRoot = true ;

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
                {
                    this.lblFolderDirName.Text = "<font style='text-decoration: underline;'>Folder</font>: ";
                    var folderId = Request.QueryString["folderId"];
                    var folderNeedToMove = this.folderService.GetById(Convert.ToInt32(folderId));
                    if (folderNeedToMove != null)
                    {
                        this.lblFolderDirName.Text += folderNeedToMove.DirName;
                        this.LoadComboData();
                        var listFolder = this.folderService.GetAll().Where(t => t.ParentID == 1276).Select(t=>t.ID).ToList();
                       // var ParrentFolder = this.folderService.GetAll().Where(t => t.Name == "ThTC1" || t.Name == "MSP1" || t.Name == "MSP8").Select(t => t.ID).ToList();
                        var ChildFolder = this.folderService.GetAll().Where(t => listFolder.Contains(Convert.ToInt32(t.ParentID))).Select(t => t.ID).ToList();
                        if (!listFolder.Contains(Convert.ToInt32(folderId)) && !ChildFolder.Contains(Convert.ToInt32(folderId)))
                        {
                            var IDProject = (HtmlGenericControl)this.divContent.FindControl("IDProject");
                            if (IDProject != null) {
                                IDProject.Visible = false;
                                this.ProjectRoot = false;
                            }
                        }

                        //this.LoadPermissionData(folderId.Trim());
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
            {
                var listFolderId = new List<int>();
                var selectedFolderId = Request.QueryString["folderId"];
                var selectedFolder = this.folderService.GetById(Convert.ToInt32(selectedFolderId));

                var selectedGroup = this.ddlGroup.SelectedValue;
                var selectedUser = this.ddlUser.SelectedValue;

                if (selectedFolder != null)
                {
                    var categoryId = selectedFolder.CategoryID;
                    var listParentFolderId = this.GetAllParentID(Convert.ToInt32(selectedFolderId), listFolderId);
                    listParentFolderId.Add(Convert.ToInt32(selectedFolderId));
                    ////var cookie = Request.Cookies["allchildfolder"];
                    var allChildFolderSession = Session["allChildFolder"];
                    var allChildFolderWithoutNativeFileFolderSession = Session["allChildFolderWithoutNativeFileFolder"];

                    if (selectedUser == "0")
                    {
                        //var groupDatapermission =
                        //    this.groupDataPermissionService.GetByRoleId(Convert.ToInt32(selectedGroup));

                        //var addingPermission =
                        //    listParentFolderId.Where(
                        //        t => !groupDatapermission.Select(x => x.FolderIdList).Contains(t.ToString())).Select(
                        //            t =>
                        //            new GroupDataPermission()
                        //                {
                        //                    CategoryIdList = categoryId.ToString(),
                        //                    RoleId = Convert.ToInt32(selectedGroup),
                        //                    FolderIdList = t.ToString(),
                        //                    IsFullPermission = this.rdbFullPermission.Checked,
                        //                    CreatedDate = DateTime.Now,
                        //                    CreatedBy = UserSession.Current.User.Id
                        //                }).ToList();

                        //if (this.cbApplyAll.Checked)
                        //{
                        //    if (allChildFolderSession != null)
                        //    {
                        //        var allChildNodes = this.rdbFullPermission.Checked 
                        //                            ? (List<string>)allChildFolderSession
                        //                            : (List<string>)allChildFolderWithoutNativeFileFolderSession;

                        //        addingPermission.AddRange(
                        //            allChildNodes.Select(
                        //                t =>
                        //                new GroupDataPermission()
                        //                    {
                        //                        CategoryIdList = categoryId.ToString(),
                        //                        RoleId = Convert.ToInt32(selectedGroup),
                        //                        FolderIdList = t,
                        //                        IsFullPermission = this.rdbFullPermission.Checked,
                        //                        CreatedDate = DateTime.Now,
                        //                        CreatedBy = UserSession.Current.User.Id
                        //                    }));

                        //        addingPermission = addingPermission.Distinct().ToList();
                        //    }
                        //}

                        //this.groupDataPermissionService.AddGroupDataPermissions(addingPermission.ToList());
                    }
                    else if (selectedUser != "")
                    {
                        var userDatapermission =
                            this.userDataPermissionService.GetByUserId(Convert.ToInt32(selectedUser));
                        var addingPermission =
                            listParentFolderId.Where(
                                t => !userDatapermission.Select(x => x.FolderId.ToString()).Contains(t.ToString()))
                                .Select(
                                    t =>
                                        new UserDataPermission()
                                        {
                                            CategoryId = categoryId,
                                            RoleId = Convert.ToInt32(selectedGroup),
                                            FolderId = t,
                                            UserId = Convert.ToInt32(selectedUser),
                                            IsFullPermission = this.rdbFullPermission.Checked,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        }).ToList();

                        if (this.cbApplyAll.Checked)
                        {
                            if (allChildFolderSession != null)
                            {
                                var allChildNodes = this.rdbFullPermission.Checked
                                    ? (List<int>) allChildFolderSession
                                    : (List<int>) allChildFolderWithoutNativeFileFolderSession;

                                addingPermission.AddRange(
                                    allChildNodes.Select(
                                        t =>
                                            new UserDataPermission()
                                            {
                                                CategoryId = categoryId,
                                                RoleId = Convert.ToInt32(selectedGroup),
                                                FolderId = t,
                                                UserId = Convert.ToInt32(selectedUser),
                                                IsFullPermission = this.rdbFullPermission.Checked,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = UserSession.Current.User.Id
                                            }));
                            }
                        }

                        this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
                    }

                    // Reload combobox User
                    var usersInPermission =
                        this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"]))
                            .Select(t => t.UserId)
                            .Distinct()
                            .ToList();
                    var listUser =
                        this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue))
                            .Where(t => !usersInPermission.Contains(t.Id))
                            .ToList();

                    //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

                    this.ddlUser.DataSource = listUser;
                    this.ddlUser.DataTextField = "FullName";
                    this.ddlUser.DataValueField = "Id";
                    this.ddlUser.DataBind();

                    this.grdPermission.Rebind();

                    
                }
            }
        }

        private List<int> GetAllParentID(int folderId, List<int> listFolderId)
        {
            var folder = this.folderService.GetById(folderId);
            if (folder.ParentID != null)
            {
                listFolderId.Add(folder.ParentID.Value);
                this.GetAllParentID(folder.ParentID.Value, listFolderId);
            }

            return listFolderId;
        }

        private void LoadComboData()
        {
            //var groupsInPermission = this.groupDataPermissionService.GetAllByFolder(Request.QueryString["folderId"]).Select(t => t.RoleId).Distinct().ToList();
            var listGroup = this.roleService.GetAll(false).Where(t => !this.AdminGroup.Contains(t.Id));
            this.ddlGroup.DataSource = listGroup;
            this.ddlGroup.DataTextField = "FullName";
            this.ddlGroup.DataValueField = "Id";
            this.ddlGroup.DataBind();

            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

            //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();

            ////var listParent = this.scopeProjectService.GetAll().Where(t => !string.IsNullOrEmpty(t.ParentId.ToString()) && t.ParentId != 0).Select(t=>t.ParentId).ToList();
            ////var projectInPermission = UserSession.Current.User.Id == 1 ?
            ////    this.scopeProjectService.GetAll().OrderBy(t => t.Name)
            ////   : this.scopeProjectService.GetAllInPermission(UserSession.Current.User.Id).OrderBy(t => t.Name);
            ////var project = projectInPermission.Where(t=>!listParent.Contains(t.ID)).ToList();
            ////project.Insert(0, new ScopeProject { Name = null });
            ////this.ddlProject.DataSource = project;
            ////this.ddlProject.DataTextField = "Name";
            ////this.ddlProject.DataValueField = "ID";
            ////this.ddlProject.DataBind();


        }

        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
            {
                
                var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"]));

                var listDataPermission = usersInPermission
                                        .Select(t => new Data.Entities.FolderPermissionItem
                                                { 
                                                    ID = t.ID, 
                                                    IsFullPermission = t.IsFullPermission.GetValueOrDefault(), 
                                                    Name = t.UserFullName, 
                                                    IsGroup = false 
                                                }).ToList();

                this.grdPermission.DataSource = listDataPermission;
            }
            else
            {
                this.grdPermission.DataSource = new List<DataPermission>();    
            }
        }

        protected void ddlGroup_SelectedIndexChange(object sender, EventArgs e)
        {
            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

            //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var isGroup = Convert.ToBoolean(item["IsGroup"].Text);
            var selectedFolderId = Request.QueryString["folderId"];
            var allChildFolderSession = Session["allChildFolder"];

            ////var cookie = Request.Cookies["allchildfolder"];

            if (isGroup)
            {
                var groupPermission = this.groupDataPermissionService.GetById(permissionId);
                if (groupPermission != null)
                {
                    var groupDatapermission = this.groupDataPermissionService.GetByRoleId(groupPermission.RoleId.GetValueOrDefault());
                    if (allChildFolderSession != null)
                    {
                        var allChildNodes = (List<string>)allChildFolderSession;
                        ////allChildNodes.Add(selectedFolderId);

                        var deletePermission = groupDatapermission.Where(t => allChildNodes.Contains(t.FolderIdList) || t.FolderIdList == selectedFolderId);
                        this.groupDataPermissionService.DeleteGroupDataPermission(deletePermission.ToList());
                    }
                }
            }
            else
            {
                var userPermission = this.userDataPermissionService.GetById(permissionId);
                if (userPermission != null)
                {
                    var userDatapermission = this.userDataPermissionService.GetByUserId(userPermission.UserId.GetValueOrDefault());
                    if (allChildFolderSession != null)
                    {
                        var allChildNodes = (List<int>)allChildFolderSession;
                        ////allChildNodes.Add(selectedFolderId);

                        var deletePermission = userDatapermission.Where(t => allChildNodes.Contains(t.FolderId.GetValueOrDefault()) || t.FolderId.ToString() == selectedFolderId);
                        this.userDataPermissionService.DeleteUserDataPermission(deletePermission.ToList());
                    }
                }
            }

            // Reload combobox User
            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

            //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

      
    }
}