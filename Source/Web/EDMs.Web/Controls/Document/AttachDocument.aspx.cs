﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.Net.Mail;
using System.Configuration;
using System.Text;
using System.Net;
using EDMs.Business.Services.Security;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AttachDocument : Page
    {

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        private readonly AttachFilesPackageService attachFileService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly DistributionMatrixDetailByUserService dmDetailByUserService;

        private readonly DisciplineService disciplineService;

        private readonly VendorPackageService vendorpackageService;

        private readonly AreaService areaService;

        private readonly DocumentPackage_TKService documentPackage_TKService;

        private readonly AttachFilesPackage_TKService attachFilesPackage_TKService;

        private readonly FolderService folderService;

        private readonly DocumentService documentService;

        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AttachDocument()
        {
            this.documentPackageService = new DocumentPackageService();
            this.attachFileService = new AttachFilesPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.dmDetailByUserService = new DistributionMatrixDetailByUserService();
            this.disciplineService = new DisciplineService();
            this.vendorpackageService = new VendorPackageService();
            this.areaService = new AreaService();
            this.documentPackage_TKService = new DocumentPackage_TKService();
            this.attachFilesPackage_TKService = new AttachFilesPackage_TKService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.userService = new UserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (this.Request.QueryString["isFullPermission"] == "false")
                {
                    this.btnSave.Visible = false;
                    this.UploadControl.Visible = false;
                    this.rbtnForConstruction.Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                    this.grdDocument.Height = 400;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    //var docObj = this.documentPackageService.GetById(docId);
                    //var dmDetail = this.dmDetailByUserService.GetDMDetailByUser(UserSession.Current.UserId, docObj.ID);
                    //if (dmDetail != null && !dmDetail.IsOpened.GetValueOrDefault())
                    //{
                    //    dmDetail.IsOpened = true;
                    //    this.dmDetailByUserService.Update(dmDetail);
                    //}


                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Session.Remove("IsFillData");
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var docObj = this.documentPackageService.GetById(docId);

                    var projectObj = this.scopeProjectService.GetById(docObj.ProjectId.GetValueOrDefault());

                    var disciplineObj = this.disciplineService.GetById(docObj.DisciplineId.GetValueOrDefault());

                    var flag = false;
                    var targetFolder = string.Empty;
                    if (docObj.TypeId == 1)
                    {
                        targetFolder = "../.." + disciplineObj.DisciplinePath;
                        if (this.rbtnForConstruction.Checked)
                        {
                            targetFolder = "../.." + disciplineObj.DisciplinePath.Replace("01.All Revision", "06.For Construction");
                        }
                    }
                    else if (docObj.TypeId == 2)
                    {
                        targetFolder = "../.." + disciplineObj.DisciplinePath.Replace("01.All Revision", "03.Shop Drawing/01.All Revision");
                    }

                    var packagevendor = this.vendorpackageService.GetById(docObj.VendorPackagesId.GetValueOrDefault());

                    if (docObj.TypeId.GetValueOrDefault() == 3)
                    {
                        targetFolder = "../.." + packagevendor.VendorPath;
                    }

                    if (docObj.TypeId.GetValueOrDefault() == 4)
                    {
                        var areaobj = this.areaService.GetById(docObj.AreaId.GetValueOrDefault());
                        targetFolder = "../.." + areaobj.AreaPath;
                    }

                    var listUpload = docuploader.UploadedFiles;

                    var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };

                    if (listUpload.Count > 0)
                    {
                        foreach (UploadedFile docFile in listUpload)
                        {
                            var docFileName = docFile.FileName.Replace("&", "-").Replace("+", " ");

                            //var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;
                            var serverDocFileName = docFileName;

                            if (docObj.TypeId.GetValueOrDefault() != 3 && docObj.TypeId.GetValueOrDefault() != 4)
                            {


                                // Path file to save on server disc
                                var saveAllFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                                var saveLatestFilePath = Path.Combine(Server.MapPath(targetFolder.Replace("01.All Revision", "02.Latest Revision")), serverDocFileName);

                                // Path file to download from server
                                var serverFilePath = docObj.DocPath + "/" + serverDocFileName;
                                if (this.rbtnForConstruction.Checked && docObj.TypeId.GetValueOrDefault() == 1)
                                {
                                    serverFilePath = docObj.DocPath.Replace("01.All Revision",
                                                           "06.For Construction") + "/" + serverDocFileName;
                                }

                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                // Copy file to server
                                docFile.SaveAs(saveAllFilePath, true);
                                if (docObj.IsLeaf.GetValueOrDefault() && !this.rbtnForConstruction.Checked)
                                {
                                    File.Copy(saveAllFilePath, saveLatestFilePath, true);
                                }
                                // ----------------------------------------------------------

                                // Create attach info
                                var attachFile = new AttachFilesPackage()
                                {
                                    DocumentPackageID = docId,
                                    FileName = docFileName,
                                    Extension = fileExt,
                                    FilePath = serverFilePath,
                                    ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                    FileSize = (double)docFile.ContentLength / 1024,

                                    AttachType = 1,
                                    AttachTypeName = "Document file",
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now
                                };

                                this.attachFileService.Insert(attachFile);
                                // --------------------------------------------------------------------

                                // Update Distribution status
                                //var dmDetailByUserListOfDoc = this.dmDetailByUserService.GetAllByDoc(docId, 0);
                                //foreach (var dmDetailByUser in dmDetailByUserListOfDoc)
                                //{
                                //    dmDetailByUser.IsDistributed = true;
                                //    this.dmDetailByUserService.Update(dmDetailByUser);
                                //}
                                // --------------------------------------------------------------------

                                // Update has attach file status for document
                                docObj.HasAttachFile = this.attachFileService.GetAllDocId(docId).Any();
                                docObj.Complete = 100;
                                docObj.UpdatedAttachFile = DateTime.Now;
                                this.documentPackageService.Update(docObj);
                                // --------------------------------------------------------------------

                                // Delete old docRev on Latest folder
                                if (docObj.ParentId != null && docObj.IsLeaf.GetValueOrDefault() && !this.rbtnForConstruction.Checked)
                                {
                                    var docRelatedList = this.documentPackageService.GetAllRevDoc(docObj.ParentId.Value).Where(t => t.ID != docObj.ID).Select(t => t.ID).ToList();
                                    var attachDocList = this.attachFileService.GetAllDocId(docRelatedList);
                                    foreach (var attachDoc in attachDocList)
                                    {
                                        var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                                        if (File.Exists(attachFilePath))
                                        {
                                            File.Delete(attachFilePath);
                                        }
                                    }
                                }
                                // ----------------------------------------------------------------------------------
                            }
                            else //if (docObj.TypeId.GetValueOrDefault() == 3)
                            {


                                // Path file to save on server disc
                                var saveAllFilePath = Path.Combine(Server.MapPath(targetFolder), Utility.RemoveSpecialCharacterFileName(serverDocFileName));


                                // Path file to download from server
                                var serverFilePath = docObj.DocPath + "/" + Utility.RemoveSpecialCharacterFileName(serverDocFileName);
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);


                                docFile.SaveAs(saveAllFilePath, true);


                                var attachFile = new AttachFilesPackage()
                                {
                                    DocumentPackageID = docId,
                                    FileName = docFileName,
                                    Extension = fileExt,
                                    FilePath = serverFilePath,
                                    ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                    FileSize = (double)docFile.ContentLength / 1024,

                                    AttachType = 1,
                                    AttachTypeName = "Document file",
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now
                                };

                                this.attachFileService.Insert(attachFile);


                                docObj.HasAttachFile = this.attachFileService.GetAllDocId(docId).Any();
                                docObj.Complete = 100;
                                docObj.UpdatedAttachFile = DateTime.Now;
                                this.documentPackageService.Update(docObj);
                            }
                        }
                    }
                }
                //module them cho phong thiet ke - c Quynh
                else if (!string.IsNullOrEmpty(Request.QueryString["docTKId"]))
                {

                    var docTKId = Convert.ToInt32(this.Request.QueryString["docTKId"]);
                    var docObj = this.documentPackage_TKService.GetById(docTKId);

                    //var projectObj = this.scopeProjectService.GetById(docObj.ProjectId.GetValueOrDefault());

                    //var disciplineObj = this.disciplineService.GetById(docObj.DisciplineId.GetValueOrDefault());

                    var flag = false;
                    var targetFolder = string.Empty;
                    targetFolder = docObj.DocPath;

                    //if (docObj.TypeId == 1)
                    //{
                    //    targetFolder = "../.." + disciplineObj.DisciplinePath;
                    //    if (this.rbtnForConstruction.Checked)
                    //    {
                    //        targetFolder = "../.." + disciplineObj.DisciplinePath.Replace("01.All Revision", "06.For Construction");
                    //    }
                    //}
                    //else if (docObj.TypeId == 2)
                    //{
                    //    targetFolder = "../.." + disciplineObj.DisciplinePath.Replace("01.All Revision", "03.Shop Drawing/01.All Revision");
                    //}
                    //var packagevendor = this.vendorpackageService.GetById(docObj.VendorPackagesId.GetValueOrDefault());
                    //if (docObj.TypeId.GetValueOrDefault() == 3)
                    //{
                    //    targetFolder = "../.." + packagevendor.VendorPath;
                    //}
                    //if (docObj.TypeId.GetValueOrDefault() == 4)
                    //{
                    //    var areaobj = this.areaService.GetById(docObj.AreaId.GetValueOrDefault());
                    //    targetFolder = "../.." + areaobj.AreaPath;
                    //}

                    var listUpload = docuploader.UploadedFiles;

                    var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };

                    if (listUpload.Count > 0)
                    {
                        List<string> fileNameList = new List<string>();
                        foreach (UploadedFile docFile in listUpload)
                        {
                            var docFileName = docFile.FileName.Replace("&", "-").Replace("+", " ");

                            //var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;
                            var serverDocFileName = docFileName;

                            //if (docObj.TypeId.GetValueOrDefault() != 3 && docObj.TypeId.GetValueOrDefault() != 4)
                            //{

                            // Path file to save on server disc
                            var saveAllFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                            //var saveLatestFilePath = Path.Combine(Server.MapPath(targetFolder.Replace("01.All Revision", "02.Latest Revision")), serverDocFileName);

                            // Path file to download from server
                            var serverFilePath = docObj.DocPath + "/" + serverDocFileName;
                            //if (this.rbtnForConstruction.Checked && docObj.TypeId.GetValueOrDefault() == 1)
                            //{
                            //    serverFilePath = docObj.DocPath.Replace("01.All Revision",
                            //                           "06.For Construction") + "/" + serverDocFileName;
                            //}

                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                            bool folderExists = Directory.Exists(Server.MapPath(targetFolder));
                            if (!folderExists)
                            {
                                Directory.CreateDirectory(Server.MapPath(targetFolder));
                            }

                            // Copy file to server
                            docFile.SaveAs(saveAllFilePath, true);
                            //if (docObj.IsLeaf.GetValueOrDefault() && !this.rbtnForConstruction.Checked)
                            //{
                            //    File.Copy(saveAllFilePath, saveLatestFilePath, true);
                            //}
                            // ----------------------------------------------------------

                            // Create attach info
                            var attachFileObj = new AttachFilesPackage_TK()
                            {
                                DocumentPackageID = docTKId,
                                FileName = docFileName,
                                Extension = fileExt,
                                FilePath = serverFilePath,
                                ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                FileSize = (double)docFile.ContentLength / 1024,

                                AttachType = 1,
                                AttachTypeName = "Document file",
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now
                            };

                            this.attachFilesPackage_TKService.Insert(attachFileObj);
                            // --------------------------------------------------------------------

                            // Update Distribution status
                            //var dmDetailByUserListOfDoc = this.dmDetailByUserService.GetAllByDoc(docId, 0);
                            //foreach (var dmDetailByUser in dmDetailByUserListOfDoc)
                            //{
                            //    dmDetailByUser.IsDistributed = true;
                            //    this.dmDetailByUserService.Update(dmDetailByUser);
                            //}
                            // --------------------------------------------------------------------

                            // Update has attach file status for document
                            docObj.HasAttachFile = this.attachFilesPackage_TKService.GetAllDocId(docTKId).Any();
                            //docObj.Complete = 100;
                            docObj.UpdatedAttachFile = DateTime.Now;
                            this.documentPackage_TKService.Update(docObj);
                            //them tai lieu vao doc library
                            var mainTransFolder = new Folder();
                            if (docObj.DocumentTypeId == 1)
                            {
                                mainTransFolder = this.folderService.GetById(7323);
                            }
                            else if (docObj.DocumentTypeId == 2)
                            {
                                mainTransFolder = this.folderService.GetById(7321);
                            }
                            else if (docObj.DocumentTypeId == 3)
                            {
                                mainTransFolder = this.folderService.GetById(7324);
                            }
                            else if (docObj.DocumentTypeId == 4)
                            {
                                mainTransFolder = this.folderService.GetById(7322);
                            }


                            var listAttachFilePackage = this.attachFilesPackage_TKService.GetAllDocumentFileByDocId(docTKId);

                            
                            foreach (var attachFile in listAttachFilePackage)
                            {
                                if (mainTransFolder != null && !string.IsNullOrEmpty(mainTransFolder.DirName))
                                {
                                    // Path file to save on server disc
                                    var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                    // Path file to download from server
                                    serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                    fileExt = attachFile.Extension;

                                    if (!File.Exists(saveFilePath))
                                    {
                                        var document = new Data.Entities.Document()
                                        {
                                            Name = attachFile.FileName,
                                            FileExtension = fileExt,
                                            FileExtensionIcon = attachFile.ExtensionIcon,
                                            FilePath = serverFilePath,
                                            FolderID = mainTransFolder.ID,
                                            IsLeaf = true,
                                            IsDelete = false,
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedDate = DateTime.Now
                                        };
                                        this.documentService.Insert(document);
                                        fileNameList.Add(document.Name);
                                    }
                                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                    {
                                        File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                    }
                                }
                            }
                            

                            // --------------------------------------------------------------------
                            // Delete old docRev on Latest folder
                            //if (docObj.ParentId != null && docObj.IsLeaf.GetValueOrDefault() && !this.rbtnForConstruction.Checked)
                            //{
                            //    var docRelatedList = this.documentPackageService.GetAllRevDoc(docObj.ParentId.Value).Where(t => t.ID != docObj.ID).Select(t => t.ID).ToList();
                            //    var attachDocList = this.attachFileService.GetAllDocId(docRelatedList);
                            //    foreach (var attachDoc in attachDocList)
                            //    {
                            //        var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                            //        if (File.Exists(attachFilePath))
                            //        {
                            //            File.Delete(attachFilePath);
                            //        }
                            //    }
                            //}
                            // ----------------------------------------------------------------------------------
                            //}
                            //else //if (docObj.TypeId.GetValueOrDefault() == 3)
                            //{
                            //    // Path file to save on server disc
                            //    var saveAllFilePath = Path.Combine(Server.MapPath(targetFolder), Utility.RemoveSpecialCharacterFileName(serverDocFileName));
                            //    // Path file to download from server
                            //    var serverFilePath = docObj.DocPath + "/" + Utility.RemoveSpecialCharacterFileName(serverDocFileName);
                            //    var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            //    docFile.SaveAs(saveAllFilePath, true);
                            //    var attachFile = new AttachFilesPackage()
                            //    {
                            //        DocumentPackageID = docId,
                            //        FileName = docFileName,
                            //        Extension = fileExt,
                            //        FilePath = serverFilePath,
                            //        ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                            //        FileSize = (double)docFile.ContentLength / 1024,
                            //        AttachType = 1,
                            //        AttachTypeName = "Document file",
                            //        CreatedBy = UserSession.Current.User.Id,
                            //        CreatedDate = DateTime.Now
                            //    };
                            //    this.attachFileService.Insert(attachFile);
                            //    docObj.HasAttachFile = this.attachFileService.GetAllDocId(docId).Any();
                            //    docObj.Complete = 100;
                            //    docObj.UpdatedAttachFile = DateTime.Now;
                            //    this.documentPackageService.Update(docObj);
                            //}
                        }
                        if (ConfigurationManager.AppSettings["EnableSendNotification"] != "false")
                        {
                            //NotificationAddNewFile(docObj, fileNameList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            this.docuploader.UploadedFiles.Clear();

            this.grdDocument.Rebind();
        }

        private void NotificationAddNewFile(DocumentPackages_TK docObj, List<string> fileNameList)
        {
            if (docObj != null)
            {
                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                };
                var CreatedUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());
                var subject = "Document #DocumentNumber# has been uploaded file on EDMS System by #CreatedUser#".Replace("#DocumentNumber#", docObj.DocNo).Replace("#CreatedUser#", CreatedUser != null ? CreatedUser.FullName : string.Empty);
                var body = @"Document #DocumentNumber# has been uploaded file on EDMS System.<br>
                                 <u>Document information:</u><br>
                                   Document number: #DocumentNumber#<br>
                                   Document Title: #DocumentTitle#<br>
                                   Document Type: #DocumentType#<br>
                                   Deadline: #Deadline#<br>
                                   File: #FileName#<br>
                                Please access <a href="+ "http://spf-edms.vietsov.com.vn:8010/"+">http://spf-edms.vietsov.com.vn:8010/</a> for more information.";
                string nameList = "";
                foreach (var item in fileNameList)
                {
                    nameList += item + "; ";
                }
                var message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                message.Subject = subject;
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;
                message.Body = body
                    .Replace("#DocumentNumber#", docObj.DocNo != null ? docObj.DocNo : string.Empty)
                    .Replace("#DocumentTitle#", docObj.DocTitle != null ? docObj.DocTitle : string.Empty)
                    .Replace("#DocumentType#", docObj.DocumentTypeName != null ? docObj.DocumentTypeName.ToString() : string.Empty)
                    .Replace("#Deadline#", docObj.PlanedDate != null ? docObj.PlanedDate.Value.ToString("dd/MM/yyyy") : string.Empty)
                    .Replace("#FileName#", nameList != null ? nameList : string.Empty);
                //message.To.Add(new MailAddress("edms.cd@vietsov.com.vn"));
                //phan quyen cho emdr phong thuong mai
                List<int> userTM = new List<int>() {1, 581, 735, 586, 484, 451, 465, 637, 734, 460, 609, 578, 551, 495, 597, 654,
                653, 733, 732, 592, 731, 591, 730, 729, 461, 631, 463, 554, 704, 555, 557, 576, 575, 526, 573, 523, 478,
                667, 565, 736, 569, 527, 656, 564, 563, 652, 660, 668, 500, 452, 544, 502, 503, 512, 510, 508, 506, 504,
                536, 479, 538, 501, 584, 496, 610, 728, 723, 727, 677 };

                var userAll = this.userService.GetAll().ToList();
                var userTMList = userAll.Where(t => userTM.Contains(t.Id)).ToList();
                foreach (var item in userTMList)
                {
                    message.CC.Add(new MailAddress(item.Email));
                }

                //var userdc = this.userService.GetByID(projectObj.DocumentControlId.GetValueOrDefault());
                //if (userdc != null && !string.IsNullOrEmpty(userdc.Email))
                //{
                //    message.CC.Add(new MailAddress(userdc.Email));
                //}
                //else
                //{
                //    var userdclist = this.userService.GetAll(false).Where(t => t.IsDC.GetValueOrDefault() && !string.IsNullOrEmpty(t.Email)).ToList();
                //    foreach (var item in userdclist)
                //    {
                //        message.CC.Add(new MailAddress(item.Email));
                //    }

                //}
                smtpClient.Send(message);
            }
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var attachDocId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var attachDoc = this.attachFileService.GetById(attachDocId);

                // Delete Physical file
                var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                var attachFileAllRevPath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath));

                if (File.Exists(attachFilePath))
                {
                    File.Delete(attachFilePath);
                }

                if (File.Exists(attachFileAllRevPath))
                {
                    File.Delete(attachFileAllRevPath);
                }
                // ----------------------------------------

                // Delete Attach Info
                this.attachFileService.Delete(attachDocId);
                // ----------------------------------------

                // Update has attach info for document
                var attachList = this.attachFileService.GetAllDocId(attachDoc.DocumentPackageID.GetValueOrDefault());
                if (!attachList.Any())
                {
                    var docObj = this.documentPackageService.GetById(attachDoc.DocumentPackageID.GetValueOrDefault());
                    docObj.HasAttachFile = false;
                    docObj.UpdatedAttachFile = null;
                    this.documentPackageService.Update(docObj);
                }
                // ----------------------------------------

                this.grdDocument.Rebind();
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["docTKId"]))
            {
                var attachDoc = this.attachFilesPackage_TKService.GetById(attachDocId);

                // Delete Physical file
                //var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                var attachFileAllRevPath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath));

                //if (File.Exists(attachFilePath))
                //{
                //    File.Delete(attachFilePath);
                //}

                if (File.Exists(attachFileAllRevPath))
                {
                    File.Delete(attachFileAllRevPath);
                }
                // ----------------------------------------

                // Delete Attach Info
                this.attachFilesPackage_TKService.Delete(attachDocId);
                // ----------------------------------------

                // Update has attach info for document
                var attachList = this.attachFilesPackage_TKService.GetAllDocId(attachDoc.DocumentPackageID.GetValueOrDefault());
                if (!attachList.Any())
                {
                    var docObj = this.documentPackage_TKService.GetById(attachDoc.DocumentPackageID.GetValueOrDefault());
                    docObj.HasAttachFile = false;
                    docObj.UpdatedAttachFile = null;
                    this.documentPackage_TKService.Update(docObj);
                }
                // ----------------------------------------

                this.grdDocument.Rebind();
            }
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["docId"]);
                var document = this.documentPackageService.GetById(docId);
                var attachList = this.attachFileService.GetAllDocumentFileByDocId(docId);

                this.grdDocument.DataSource = attachList;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["docTKId"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["docTKId"]);
                var document = this.documentPackage_TKService.GetById(docId);
                var attachList = this.attachFilesPackage_TKService.GetAllDocumentFileByDocId(docId);

                this.grdDocument.DataSource = attachList;
            }
            else
            {
                this.grdDocument.DataSource = new List<AttachFilesPackage>();
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void setGrdRadioButtonOnClick()
        {
            int i;
            RadioButton radioButton;
            for (i = 0; i < grdDocument.Items.Count; i++)
            {

                radioButton = (RadioButton)grdDocument.Items[i].FindControl("rdSelect");

                radioButton.Attributes.Add("OnClick", "SelectMeOnly(" + radioButton.ClientID + ", " + "'grdDocument'" + ")");
            }
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            //((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;

            //var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
            //var attachFileId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            //var attachFileObj = this.attachFileService.GetById(attachFileId);
            //if (attachFileObj != null)
            //{
            //    var attachFiles = this.attachFileService.GetAllByDocId(attachFileObj.DocumentId.GetValueOrDefault());
            //    foreach (var attachFile in attachFiles)
            //    {
            //        attachFile.IsDefault = attachFile.ID == attachFileId;
            //        this.attachFileService.Update(attachFile);
            //    }
            //}
        }
    }
}