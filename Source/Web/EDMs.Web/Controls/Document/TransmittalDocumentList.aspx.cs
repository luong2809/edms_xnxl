﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using Aspose.Cells;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Security;
using EDMs.Web.Utilities;
using OfficeHelper.Utilities.Data;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Web.UI;

    using EDMs.Business.Services.Document;
    using EDMs.Data.Entities;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalDocumentList : Page
    {
        /// <summary>
        /// The transmittal service.
        /// </summary>
        private readonly IncomingTransmittalService transmittalService;

        private readonly DocumentNewService documentNewService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly DocumentPackageService documentPackageService;

        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly AttachFilesCommentService attachFilesCommentService;

        private readonly TemplateManagementService templateManagementService;

        private readonly UserService userService;


        /// <summary>
        /// Initializes a new instance of the <see cref="RevisionHistory"/> class.
        /// </summary>
        public TransmittalDocumentList()
        {
            this.documentNewService = new DocumentNewService();
            this.transmittalService = new IncomingTransmittalService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.documentPackageService = new DocumentPackageService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.attachFilesCommentService = new AttachFilesCommentService();
            this.templateManagementService = new TemplateManagementService();
            this.userService = new UserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (Request.QueryString["tranId"] != null)
            {
                var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
                var listDocument = new List<DocumentPackage>();

                var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                foreach (var item in attachDocList)
                {
                    var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                    if (docObj != null)
                    {
                        listDocument.Add(docObj);
                    }
                }

                this.grdDocument.DataSource = listDocument;
            }
        }

        protected void grdDocument_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "DocDetail":
                    {
                        var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
                        var transObj = this.transmittalService.GetById(tranId);
                        if (transObj != null)
                        {
                            if (transObj.ProjectId == 51 || transObj.ProjectId == 54)
                            {
                                var docId = Convert.ToInt32(dataItem.GetDataKeyValue("ID").ToString());
                                e.DetailTableView.DataSource = this.attachFilesCommentService.GetAllByDocId(docId);
                            }
                            else
                            {
                                var docId = Convert.ToInt32(dataItem.GetDataKeyValue("ID").ToString());
                                e.DetailTableView.DataSource = this.attachFilesPackageService.GetAllDocumentFileByDocId(docId);
                            }

                        }
                        break;
                    }
            }
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            if (Request.QueryString["tranId"] != null)
            {
                var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
                //  var transObj = this.transmittalService.GetById(tranId);
                // if (transObj != null)
                // {

                var temp = this.attachDocToTransmittalService.GetByDoc(tranId, docId);
                if (temp != null)
                {
                    this.attachDocToTransmittalService.Delete(temp);
                }
                #region
                /*
                var templateManagement = this.templateManagementService.GetSpecial(4,
                        transObj.ProjectId.GetValueOrDefault());
                if (templateManagement != null)
                {

                    if (transObj.ProjectId == 3)
                    {
                        var count = 1;
                        var filePath = Server.MapPath("../../Transmittals") + @"\";
                        var workbook = new Workbook();


                        workbook.Open(filePath + templateManagement.FilePath);
                        var sheet1 = workbook.Worksheets[0];

                        var fromUser = this.userService.GetByID(transObj.FromId.GetValueOrDefault());
                        var toUser = this.userService.GetByID(transObj.ToId.GetValueOrDefault());

                        sheet1.Cells["Q5"].PutValue(transObj.TransmittalNumber);
                        sheet1.Cells["G7"].PutValue(fromUser != null ? fromUser.FullName : string.Empty);
                        sheet1.Cells["G8"].PutValue(fromUser != null ? fromUser.Position : string.Empty);
                        sheet1.Cells["G9"].PutValue(fromUser != null ? toUser.FullName : string.Empty);
                        sheet1.Cells["G10"].PutValue(fromUser != null ? toUser.Position : string.Empty);
                        sheet1.Cells["G11"].PutValue(transObj.ReasonForIssue);

                        var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                        sheet1.Cells.InsertRows(19, attachDocList.Count > 0 ? attachDocList.Count - 1 : 0);
                        foreach (var attachDoc in attachDocList)
                        {
                            var docObj =
                                this.documentPackageService.GetById(attachDoc.DocumentId.GetValueOrDefault());
                            if (docObj != null)
                            {
                                sheet1.Cells.Merge(17 + count, 2, 1, 6);
                                sheet1.Cells.Merge(17 + count, 8, 1, 4);

                                sheet1.Cells[17 + count, 1].PutValue(count);
                                sheet1.Cells[17 + count, 2].PutValue(docObj.DocNo);
                                sheet1.Cells[17 + count, 8].PutValue(docObj.DocTitle);
                                sheet1.Cells["M" + (18 + count).ToString()].PutValue(docObj.RevisionName);
                                count += 1;

                                docObj.OutgoingTransNo = transObj.TransmittalNumber;
                                docObj.OutgoingTransDate = transObj.IssuseDate;

                                this.documentPackageService.Update(docObj);
                            }
                        }

                        var filename = transObj.Name + "_" + DateTime.Now.ToBinary() + ".xls";
                        workbook.Save(filePath + @"Generated\" + filename);

                        var serverPath = "../../Transmittals/Generated/";
                        transObj.GeneratePath = serverPath + filename;
                        transObj.IsGenerate = true;

                        this.transmittalService.Update(transObj);

                    }
                    else// if (transObj.ProjectId == 8 || transObj.ProjectId == 9 || transObj.ProjectId == 10)
                    {
                        var listTran = new List<Transmittal> {transObj};
                        var transInfo = Utility.ConvertToDataTable(listTran);
                        var docDt = new DataTable();
                        var ds = new DataSet();
                        var count = 0;

                        var listColumn = new DataColumn[]
                        {
                            new DataColumn("Index", Type.GetType("System.String")),
                            new DataColumn("DocumentNumber", Type.GetType("System.String")),
                            new DataColumn("RevisionName", Type.GetType("System.String")),
                            new DataColumn("Description", Type.GetType("System.String")),
                            new DataColumn("Remark", Type.GetType("System.String"))
                        };
                        docDt.Columns.AddRange(listColumn);

                        var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                        foreach (var attachDoc in attachDocList)
                        {
                            var docObj =
                                this.documentPackageService.GetById(attachDoc.DocumentId.GetValueOrDefault());
                            if (docObj != null)
                            {
                                var dataItem = docDt.NewRow();
                                count += 1;
                                dataItem["Index"] = count;
                                dataItem["DocumentNumber"] = docObj.DocNo;
                                dataItem["RevisionName"] = docObj.RevisionName;
                                dataItem["Description"] = docObj.DocTitle;

                                dataItem["Remark"] = "1 Bộ";

                                docDt.Rows.Add(dataItem);

                                docObj.OutgoingTransNo = transObj.TransmittalNumber;
                                docObj.OutgoingTransDate = transObj.IssuseDate;

                                this.documentPackageService.Update(docObj);
                            }
                        }

                        ds.Tables.Add(docDt);
                        ds.Tables[0].TableName = "Table";

                        var rootPath = Server.MapPath("../../Transmittals/");
                        const string WordPath = @"Template\";
                        const string WordPathExport = @"Generated\";
                        var StrTemplateFileName = templateManagement.FilePath;
                        var strOutputFileName = transObj.Name + "_" + DateTime.Now.ToBinary() + ".doc";
                        var isSuccess = OfficeCommon.ExportToWordWithRegion(
                            rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName,
                            transInfo, ds);

                        if (isSuccess)
                        {
                            var serverPath = "../../Transmittals/Generated/";
                            transObj.GeneratePath = serverPath + strOutputFileName;
                            transObj.IsGenerate = true;

                            this.transmittalService.Update(transObj);
                        }
                    }
                }*/
                #endregion
                // }

            }

            this.grdDocument.Rebind();
        }
    }
}