﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocMasterList : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;


        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly DocumentStatusProcessService docStatusProcessService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocMasterList()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.docStatusProcessService = new DocumentStatusProcessService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var docStatusProcessList = new List<DocumentStatusProcess>();
            var existDocumentPackageList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var error="";
                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;

                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectObj = this.scopeProjectService.GetById(projectID);
                            if (projectObj != null)
                            {
                                // Get doc status list if project type is Service
                                var statusList = new List<Status>();
                                if (projectObj.ProjectTypeId == 2)
                                {
                                    statusList = this.statusService.GetAllByProject(projectObj.ID).OrderBy(t => t.PercentCompleteDefault).ToList();
                                }
                                // ------------------------------------------------------------------

                                var disciplineId = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                                    ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                                    : 0;
                                var disciplineObj = this.disciplineService.GetById(disciplineId);
                                if (disciplineObj != null)
                                {
                                    var dataTable = datasheet.Cells.ExportDataTable(4, 2, datasheet.Cells.MaxRow - 1,
                                        7 + statusList.Count);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                        {
                                            currentDocumentNo = dataRow["Column1"].ToString();
                                           
                                            var existDocObj = this.documentPackageService.GetOneByDocNo(currentDocumentNo, projectObj.ID);
                                            

                                            var revObj = this.revisionService.GetByName(dataRow["Column4"].ToString());
                                            var docTypeObj = this.documentTypeService.GetByName(dataRow["Column6"].ToString(), projectObj.ID);
                                            var departmentObj = this.roleService.GetByName(dataRow["Column5"].ToString());

                                            var existdocrev = this.documentPackageService.GetOneByDocNo(currentDocumentNo, revObj.Name, projectObj.ID);
                                            if (existdocrev == null)
                                            {

                                                var docObj = new DocumentPackage();
                                                docObj.ProjectId = projectObj.ID;
                                                docObj.ProjectName = projectObj.Name;

                                                docObj.DocNo = dataRow["Column1"].ToString();
                                                docObj.AnotherDocNo = dataRow["Column2"].ToString();
                                                docObj.DocTitle = dataRow["Column3"].ToString();
                                                docObj.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                docObj.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                docObj.StatusID = 0;
                                                docObj.StatusName = string.Empty;

                                                docObj.ReferenceFromID = 0;
                                                docObj.ReferenceFromName = string.Empty;

                                                docObj.DisciplineId = disciplineObj.ID;
                                                docObj.DisciplineName = disciplineObj.FullName;

                                                docObj.DeparmentId = departmentObj != null ? departmentObj.Id : 0;
                                                docObj.DeparmentName = departmentObj != null ? departmentObj.FullName : string.Empty;

                                                docObj.Weight = 0.0;

                                                docObj.IsCriticalDoc = false;
                                                docObj.IsPriorityDoc = false;
                                                docObj.IsVendorDoc = false;

                                                docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                                docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                docObj.TypeId = 1;
                                                var strRevPlanedDate = dataRow["Column7"].ToString();
                                                var revPlanedDate = new DateTime();
                                                if (Utility.ConvertStringToDateTime(strRevPlanedDate, ref revPlanedDate))
                                                {
                                                    docObj.PlanedDate = revPlanedDate;
                                                }

                                                docObj.DocPath = disciplineObj.DisciplinePath;

                                                docObj.CreatedBy = UserSession.Current.User.Id;
                                                docObj.CreatedDate = DateTime.Now;
                                                docObj.IsLeaf = true;
                                                docObj.IsDelete = false;
                                                docObj.IsEMDR = true;
                                                docObj.IsDistributed = false;

                                                // Init doc status for document
                                                if (statusList.Count > 0)
                                                {
                                                    docObj.StatusID = statusList[0].ID;
                                                    docObj.StatusName = statusList[0].FullNameWithWeight;
                                                }
                                                // ------------------------------------------------------

                                                if (existDocObj != null)
                                                {
                                                    docObj.ParentId = existDocObj.ParentId ?? existDocObj.ID;
                                                    existDocObj.IsLeaf = false;
                                                    existDocObj.UpdatedBy = UserSession.Current.User.Id;
                                                    existDocObj.UpdatedDate = DateTime.Now;
                                                    existDocumentPackageList.Add(existDocObj);
                                                }

                                                documentPackageList.Add(docObj);

                                                for (int j = 0; j < statusList.Count; j++)
                                                {
                                                    var docStatusProcess = new DocumentStatusProcess()
                                                    {
                                                        DocNumber = docObj.DocNo,
                                                        StatusId = statusList[j].ID,
                                                        StatusName = statusList[j].Name
                                                    };

                                                    if (!string.IsNullOrEmpty(dataRow["Column" + (8 + j)].ToString()))
                                                    {
                                                        var planedDate = dataRow["Column" + (8 + j)];
                                                        docStatusProcess.PlantDate = (DateTime?)planedDate;
                                                    }

                                                    docStatusProcessList.Add(docStatusProcess);
                                                }
                                            }
                                            else
                                            {
                                                error += currentDocumentNo + "</br>";
                                            }

                                        }
                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        foreach (var docObj in documentPackageList)
                                        {
                                            //var validDocNoFolder = Utility.RemoveSpecialCharacterForFolder(docObj.DocNo);
                                            //var targetFolder = "../../DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name);
                                            //var physicalDocNoFolder = Path.Combine(Server.MapPath(targetFolder), validDocNoFolder);
                                            //var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/" + validDocNoFolder;

                                            //docObj.DocPath = projectObj.ProjectPath + "/01.All Revision";

                                            var docId = this.documentPackageService.Insert(docObj);
                                            foreach (var docStatusProcessItem in docStatusProcessList.Where(t => t.DocNumber == docObj.DocNo))
                                            {
                                                docStatusProcessItem.DocId = docId;
                                                this.docStatusProcessService.Insert(docStatusProcessItem);
                                            }


                                            //if (docId != null)
                                            //{

                                            //    if (!Directory.Exists(physicalDocNoFolder))
                                            //    {
                                            //        Directory.CreateDirectory(physicalDocNoFolder);
                                            //    }
                                            //}
                                        }

                                        foreach (var docObj in existDocumentPackageList)
                                        {
                                            this.documentPackageService.Update(docObj);
                                        }
                                        if (string.IsNullOrEmpty(error))
                                        {
                                            this.blockError.Visible = true;
                                            this.lblError.Text =
                                                "Data of document master list file is valid. System import successfull!";
                                        }
                                        else
                                        {
                                            this.blockError.Visible = true;
                                            this.lblError.Text = "Have Error: Documents < " + error + " > really existed!";
                                        }
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                }
                                else
                                {
                                    this.blockError.Visible = true;
                                    this.lblError.Text =
                                        "Have Error: Project '" + projectObj.Name + "' don't have Document Type '" +
                                        currentSheetName + "'. Please remove this sheet out of master file.";
                                }
                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }
                    }
                }
           }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}