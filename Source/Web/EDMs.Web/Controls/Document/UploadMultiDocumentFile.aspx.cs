﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Security.Cryptography;
using EDMs.Business.Services.Library;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UploadMultiDocumentFile : Page
    {

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly AttachFilesPackageService attachFileService;

        private readonly DocumentPackageService documentPackageService;

        private readonly FolderService folderService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public UploadMultiDocumentFile()
        {
            this.documentService = new DocumentService();
            this.attachFileService = new AttachFilesPackageService();
            this.documentPackageService = new DocumentPackageService();
            this.folderService = new FolderService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    if (!UserSession.Current.IsAdmin &&
                        !UserSession.Current.IsDC)
                    {
                        this.btnSave.Visible = false;
                        this.UploadControl.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Controls/System/Login.aspx");
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                const string TargetFolder = "../../DocumentLibrary";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/"
                    ? string.Empty
                    : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary";
                var listUpload = docuploader.UploadedFiles;
                var splitAttachFileNameCharacter =
                                    ConfigurationManager.AppSettings.Get("SplitAttachFileNameCharacter");

                var listExistFile = new List<string>();

                var fileIcon = new Dictionary<string, string>()
                {
                    {"doc", "~/images/wordfile.png"},
                    {"docx", "~/images/wordfile.png"},
                    {"dotx", "~/images/wordfile.png"},
                    {"xls", "~/images/excelfile.png"},
                    {"xlsx", "~/images/excelfile.png"},
                    {"pdf", "~/images/pdffile.png"},
                    {"7z", "~/images/7z.png"},
                    {"dwg", "~/images/dwg.png"},
                    {"dxf", "~/images/dxf.png"},
                    {"rar", "~/images/rar.png"},
                    {"zip", "~/images/zip.png"},
                    {"txt", "~/images/txt.png"},
                    {"xml", "~/images/xml.png"},
                    {"xlsm", "~/images/excelfile.png"},
                    {"bmp", "~/images/bmp.png"},
                };

                if (listUpload.Count > 0)
                {
                    var uncompleteUploadFileList = new List<string>();
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileName = docFile.FileName;
                        var serverDocFileName = docFileName;


                        var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                        var folder = this.folderService.GetById(folderId);
                        if (folder != null)
                        {
                            // Path file to save on server disc
                            var saveFilePath = Path.Combine(Server.MapPath(folder.DirName), serverDocFileName);
                            // Path file to download from server
                            var serverFilePath = folder.DirName + "/" + serverDocFileName;
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                docFileName.Length - docFileName.LastIndexOf(".") - 1);

                            if (!File.Exists(saveFilePath))
                            {
                                docFile.SaveAs(saveFilePath, true);

                                var document = new Document()
                                {
                                    Name = docFileName,
                                    FileExtension = fileExt,
                                    FileExtensionIcon =
                                        fileIcon.ContainsKey(fileExt.ToLower())
                                            ? fileIcon[fileExt.ToLower()]
                                            : "~/images/otherfile.png",
                                    FilePath = serverFilePath,
                                    FolderID = folderId,
                                    IsLeaf = true,
                                    IsDelete = false,
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now
                                };

                                this.documentService.Insert(document);

                                if (this.cbAutoUpdateEMDR.Checked)
                                {

                                    var docInfo = docFile.FileName.Split(splitAttachFileNameCharacter.ToCharArray());
                                    var docNo = docInfo[0].Replace(" ", string.Empty);
                                    var docRev = docInfo.Any() ? docInfo[1] : string.Empty;
                                    var docObj = this.documentPackageService.GetOneByDocNo(docNo, docRev,Convert.ToInt32(folder.ProjectId));
                                    if (docObj != null)
                                    {
                                        serverDocFileName = DateTime.Now.ToBinary() + serverDocFileName;

                                        // Path file to save on server disc
                                        var newsaveFilePath = Path.Combine(Server.MapPath(TargetFolder),
                                            serverDocFileName);

                                        // Path file to download from server
                                        serverFilePath = serverFolder + "/" + serverDocFileName;
                                        fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                            docFileName.Length - docFileName.LastIndexOf(".") - 1);

                                        if (File.Exists(saveFilePath))
                                        {
                                            File.Copy(saveFilePath, newsaveFilePath, true);
                                        }

                                        var attachFile = new AttachFilesPackage()
                                        {
                                            DocumentPackageID = docObj.ID,
                                            FileName = docFileName,
                                            Extension = fileExt,
                                            FilePath = serverFilePath,
                                            ExtensionIcon =
                                                fileIcon.ContainsKey(fileExt.ToLower())
                                                    ? fileIcon[fileExt.ToLower()]
                                                    : "~/images/otherfile.png",
                                            FileSize = (double) docFile.ContentLength/1024,

                                            AttachType = 1,
                                            AttachTypeName = "Document file",
                                            CreatedBy = UserSession.Current.User.Id,
                                            CreatedDate = DateTime.Now
                                        };

                                        this.attachFileService.Insert(attachFile);
                                    }
                                    else
                                    {
                                        uncompleteUploadFileList.Add(docFile.FileName);
                                    }
                                }
                            }
                            else
                            {
                                listExistFile.Add(docFileName);
                            }
                        }
                    }

                    this.docuploader.UploadedFiles.Clear();

                    if (uncompleteUploadFileList.Count > 0)
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = "Can't find some document with Document number: <br/>";
                        foreach (var item in uncompleteUploadFileList)
                        {
                            this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" + item.Split(splitAttachFileNameCharacter.ToCharArray())[0] + "'</span> to attach file <span style='color: orange; font-weight: bold'>'" + item + "'</span> <br/>";
                        }
                    }
                    else if (listExistFile.Count > 0)
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text += "This folder is have exist files: <br/>";
                        foreach (var item in listExistFile)
                        {
                            this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" + item + "'</span> <br/>";
                        }
                    }
                    else
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = "All document files upload complete.";
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error when upload document file: <br/>'" + ex.Message + "'";
            }
        }
    }
}