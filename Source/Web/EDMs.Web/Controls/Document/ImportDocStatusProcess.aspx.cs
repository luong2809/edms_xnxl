﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocStatusProcess : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;


        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly DocumentStatusProcessService docStatusProcessService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocStatusProcess()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.docStatusProcessService = new DocumentStatusProcessService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension== ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var error = "";
                        for (var i = 0; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;
                            var docCount = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectId = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                                : 0;
                            var statusList = this.statusService.GetAllByProject(projectId).OrderBy(t => t.PercentCompleteDefault);



                            var docTable = datasheet.Cells.ExportDataTable(4, 1, docCount, 10);
                            foreach (DataRow docItem in docTable.Rows)
                            {
                                if (!string.IsNullOrEmpty(docItem["Column1"].ToString()))
                                {
                                    var docId = Convert.ToInt32(docItem["Column1"].ToString());
                                    var docObj = this.documentPackageService.GetById(docId);

                                    // Update actual date for Document Status Process
                                    if (!string.IsNullOrEmpty(docItem["Column8"].ToString()))
                                    {
                                        var currentDocStatusProcess = this.docStatusProcessService.GetOneByDocAndStatus(docObj.ID, docObj.StatusID.GetValueOrDefault());

                                        if (currentDocStatusProcess != null)
                                        {
                                            currentDocStatusProcess.ActualDate = (DateTime?)docItem["Column8"];
                                            this.docStatusProcessService.Update(currentDocStatusProcess);
                                        }
                                    }
                                    // ---------------------------------------------------------------------------------

                                    // Get info for update document
                                    if (!string.IsNullOrEmpty(docItem["Column9"].ToString()))
                                    {
                                        docObj.CommentCode = docItem["Column9"].ToString();
                                    }

                                    if (!string.IsNullOrEmpty(docItem["Column10"].ToString()))
                                    {
                                        var nextStatusItem = statusList.FirstOrDefault(t => t.FullNameWithWeight == docItem["Column10"].ToString());
                                        if (nextStatusItem != null)
                                        {
                                            docObj.StatusID = nextStatusItem.ID;
                                            docObj.StatusName = nextStatusItem.FullNameWithWeight;
                                            docObj.StatusEffectDate = DateTime.Now;
                                        }
                                    }
                                    // -----------------------------------------------------------------------------------

                                    this.documentPackageService.Update(docObj);
                                    
                                }
                            }
                        }

                        this.blockError.Visible = true;
                        this.lblError.Text = "Update success full.";
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo +
                                     "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}