﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.Drawing;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class EMDR_TM : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly DocumentPackage_TKService documentPackage_TKService = new DocumentPackage_TKService();

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.Page.IsPostBack)
            {
                if (UserSession.Current.UserId == 732 || UserSession.Current.UserId == 1)
                {
                    this.RadPane3.Visible = true;
                    this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = true;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = true;
                }
                LoadObjectTree();
            }
        }

        private void LoadObjectTree()
        {
            RadTreeNode node1 = new RadTreeNode("Offical ITB", "1");
            RadTreeNode node2 = new RadTreeNode("Bid Circulation Advance", "2");
            RadTreeNode node3 = new RadTreeNode("PQPOC Enquiry Response", "3");
            RadTreeNode node4 = new RadTreeNode("Native File-Format Form", "4");

            this.rtvDoctype.Nodes.Add(node1);
            this.rtvDoctype.Nodes.Add(node2);
            this.rtvDoctype.Nodes.Add(node3);
            this.rtvDoctype.Nodes.Add(node4);
            for (int i = 0; i < 3; i++)
            {
                this.rtvDoctype.Nodes[i].ImageUrl = @"Images/discipline.png";
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.rtvDoctype.SelectedNode != null)
            {
                var projectList = this.documentPackage_TKService.GetAll().Where(t => t.DocumentTypeId == Convert.ToInt32(rtvDoctype.SelectedValue));
                this.grdDocument.DataSource = projectList;
            }
            else
            {
                var projectList = this.documentPackage_TKService.GetAll();
                this.grdDocument.DataSource = projectList;
            }

        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (System.Web.UI.WebControls.Image)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format(
                    "return ShowEditForm('{0}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"]);
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.documentPackage_TKService.Delete(disId);

            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            //AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item["No."].BackColor = Color.Aqua;// ColorTranslator.FromHtml("#00608f");// Color.Aqua;
                    item["No."].BorderColor = Color.Aqua;// ColorTranslator.FromHtml("#00608f"); //
                }
            }
        }

        protected void rtvDoctype_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        protected void rtvDoctype_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/discipline.png";
        }
    }
}

