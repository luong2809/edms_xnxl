﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Security.Cryptography;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AttachDocumentPackage : Page
    {

        private readonly AttachFilesPackageService attachFileService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AttachDocumentPackage()
        {
            this.attachFileService = new AttachFilesPackageService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {

                    if (this.Request.QueryString["isFullPermission"] == "false")
                    {
                        this.btnSave.Visible = false;
                        this.UploadControl.Visible = false;
                        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
                        this.grdDocument.Height = 400;
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Controls/System/Login.aspx");
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Session.Remove("IsFillData");
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                var docObj = this.documentPackageService.GetById(docId);

                var projectObj = this.scopeProjectService.GetById(docObj.ProjectId.GetValueOrDefault());

                var flag = false;
                var targetFolder = string.Empty;
                if (docObj.TypeId == 1)
                {
                    targetFolder = "../../DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name);
                }
                else
                {
                    targetFolder = "../../DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/03.Shop Drawing";
                }

                var listUpload = docuploader.UploadedFiles;

                var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };

                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileName = docFile.FileName;

                        //var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName;
                        var serverDocFileName = docFileName;

                        // Path file to save on server disc
                        var saveAllFilePath = Path.Combine(Server.MapPath(targetFolder + "/01.All Revision"), serverDocFileName);
                        var saveLatestFilePath = Path.Combine(Server.MapPath(targetFolder + "/02.Latest Revision"), serverDocFileName);

                        // Path file to download from server
                        var serverFilePath = docObj.DocPath + "/01.All Revision/" + serverDocFileName;
                        var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                        docFile.SaveAs(saveAllFilePath, true);
                        docFile.SaveAs(saveLatestFilePath, true);


                        var attachFile = new AttachFilesPackage()
                        {
                            DocumentPackageID = docId,
                            FileName = docFileName,
                            Extension = fileExt,
                            FilePath = serverFilePath,
                            ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                            FileSize = (double)docFile.ContentLength / 1024,

                            AttachType = 1,
                            AttachTypeName = "Document file",
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now
                        };

                        this.attachFileService.Insert(attachFile);
                    }

                    docObj.HasAttachFile = this.attachFileService.GetAllDocId(docId).Any();
                    docObj.UpdatedAttachFile = DateTime.Now;
                    this.documentPackageService.Update(docObj);

                    // Delete old docRev on Latest folder
                    if (docObj.ParentId != null)
                    {
                        var docRelatedList = this.documentPackageService.GetAllRelatedDocument(docObj.ID).Where(t => t.ID != docObj.ID).Select(t => t.ID).ToList();
                        var attachDocList = this.attachFileService.GetAllDocId(docRelatedList);
                        foreach (var attachDoc in attachDocList)
                        {
                            var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                            if (File.Exists(attachFilePath))
                            {
                                File.Delete(attachFilePath);
                            }
                        }
                    }
                }
            }

            this.docuploader.UploadedFiles.Clear();

            this.grdDocument.Rebind();
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var attachDocId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var attachDoc = this.attachFileService.GetById(attachDocId);
            var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
            if (File.Exists(attachFilePath))
            {
                File.Delete(attachFilePath);
            }

            this.attachFileService.Delete(attachDocId);
            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["docId"]);
                var document = this.documentPackageService.GetById(docId);
                var attachList = this.attachFileService.GetAllDocumentFileByDocId(docId);

                this.grdDocument.DataSource = attachList;
            }
            else
            {
                this.grdDocument.DataSource = new List<AttachFilesPackage>();
            }
        }
    }
}