﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Web.UI;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UpdateDocStatusProgress : Page
    {

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        private readonly StatusService statusService;

        private DocumentStatusProcessService docStatusProcessService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public UpdateDocStatusProgress()
        {
            this.documentPackageService = new DocumentPackageService();
            this.statusService = new StatusService();
            this.docStatusProcessService = new DocumentStatusProcessService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        var currentDocStatusProcess = this.docStatusProcessService.GetOneByDocAndStatus(docObj.ID,
                            docObj.StatusID.GetValueOrDefault());

                        this.txtCurrentDocStatus.Text = docObj.StatusName;
                        this.txtCurrentPlan.SelectedDate = currentDocStatusProcess != null
                            ? currentDocStatusProcess.PlantDate
                            : null;

                        this.LoadComboData(docObj);
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                var docObj = this.documentPackageService.GetById(docId);
                if (docObj != null)
                {
                    var currentDocStatusProcess = this.docStatusProcessService.GetOneByDocAndStatus(docObj.ID,
                            docObj.StatusID.GetValueOrDefault());
                    currentDocStatusProcess.ActualDate = this.txtCurrentActual.SelectedDate;
                    currentDocStatusProcess.CommentCode = this.txtCommentCode.Text;

                    if (this.ddlStatus.SelectedIndex != 0)
                    {
                        docObj.StatusID = Convert.ToInt32(this.ddlStatus.SelectedValue);
                        docObj.StatusName = this.ddlStatus.SelectedItem.Text;
                    }

                    this.docStatusProcessService.Update(currentDocStatusProcess);
                    // Can send notification in here if necessary
                    //------------------------------------------------------------

                    this.documentPackageService.Update(docObj);
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseWindow", "CloseAndRebind();", true);
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData(DocumentPackage docObj)
        {
            var statusList = this.statusService.GetAllByProject(docObj.ProjectId.GetValueOrDefault()).Where(t => t.ID != docObj.StatusID).OrderBy(t => t.PercentCompleteDefault).ToList();
            statusList.Insert(0, new Status() {ID = 0});
            this.ddlStatus.DataSource = statusList;
            this.ddlStatus.DataTextField = "FullNameWithWeight";
            this.ddlStatus.DataValueField = "ID";
            this.ddlStatus.DataBind();
        }
    }
}