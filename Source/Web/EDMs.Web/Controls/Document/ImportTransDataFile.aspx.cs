﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.Hosting;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Web.UI;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using EDMs.Business.Services.Scope;
    using EDMs.Web.Utilities;
    using Telerik.Web.UI;
    using Aspose.Cells;
    using System.Web.Configuration;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportTransDataFile : Page
    {
        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly ToListService toListService;

        private readonly IncomingTransmittalService incomingTransmittalService;
        private readonly AutoDistributionMatriceService autoDistributionMatriceService;
        private readonly UserService userService;
        private readonly AttachDocToTransmittalService attachDocToTransmittalService;
        private readonly DocumentPackageService documentPackageService;
        private readonly ScopeProjectService scopeProjectService;
        private readonly UserDataPermissionService userDataPermissionService;
        private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportTransDataFile()
        {
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.toListService = new ToListService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.userService = new UserService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.scopeProjectService = new ScopeProjectService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.documentService = new DocumentService();
            this.autoDistributionMatriceService = new AutoDistributionMatriceService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();
                var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
                if (!string.IsNullOrEmpty(projectObj.organizationComment1)) { this.rbtnreview1.Visible = true; this.rbtnreview1.Text = "From " + projectObj.organizationComment1; }
                if (!string.IsNullOrEmpty(projectObj.organizationComment2)) { this.rbtnreview1.Visible = true; this.rbtnreview1.Text = "From " + projectObj.organizationComment2; }
                if (!string.IsNullOrEmpty(projectObj.organizationComment3)) { this.rbtnreview1.Visible = true; this.rbtnreview1.Text = "From " + projectObj.organizationComment3; }
            }
        }

        private void LoadComboData()
        {
            var tolist = this.toListService.GetAll();
            tolist.Insert(0, new ToList() { ID = 0, Name = string.Empty });
            this.ddlFromList.DataSource = tolist;
            this.ddlFromList.DataTextField = "Name";
            this.ddlFromList.DataValueField = "Id";
            this.ddlFromList.DataBind();

            var projectList = this.scopeProjectService.GetAll();
            this.ddlProject.DataSource = projectList;
            this.ddlProject.DataTextField = "FullName";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //try
            //{
            var DirName_ = "";
            var docNonExistList = new List<string>();
            foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
            {
                var extension = docFile.GetExtension();
                if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                {
                    var transFileName = docFile.FileName;
                    var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                    docFile.SaveAs(importPath);

                    var owner = this.toListService.GetAll().FirstOrDefault(t => t.Name == "XNXL");

                    var workbook = new Workbook();
                    workbook.Open(importPath);
                    var tranSheet = workbook.Worksheets[0];
                    var projectID = this.ddlProject.SelectedItem != null
                            ? Convert.ToInt32(this.ddlProject.SelectedValue)
                            : 0;
                    var projectObj = this.scopeProjectService.GetById(projectID);
                    var listSelectedDocId = new List<int>();
                    var listAttachFilePackage = new List<AttachFilesPackage>();
                    var doclist = new List<DocumentPackage>();
                    if (projectObj != null)
                    {
                        File.Copy(importPath, Server.MapPath("../../Transmittals/Import/" + transFileName), true);
                        var newServerPath = "/Transmittals/Import/" + transFileName;
                        // Create incoming Trans Info
                        var outgoingTransObj = new IncomingTransmittal();

                        outgoingTransObj.TransNumber = tranSheet.Cells["F6"].Value != null ? tranSheet.Cells["F6"].Value.ToString() : string.Empty;
                        outgoingTransObj.ProjectName = projectObj.FullName;
                        outgoingTransObj.ProjectId = projectObj.ID;
                        outgoingTransObj.FromId = Convert.ToInt32(this.ddlFromList.SelectedValue);
                        outgoingTransObj.FromName = this.ddlFromList.SelectedItem.Text;
                        outgoingTransObj.ToId = owner.ID;
                        outgoingTransObj.ToName = owner.Name;
                        outgoingTransObj.AttachFilePath = newServerPath;
                        outgoingTransObj.AttachFileName = transFileName;
                        outgoingTransObj.TypeInTrans = Convert.ToInt32(this.ddlType.SelectedValue);
                        var date = new DateTime();
                        if (Utility.ConvertStringToDateTime(tranSheet.Cells["F5"].Value.ToString(), ref date))
                        {
                            outgoingTransObj.ReceivedDate = date;
                        }

                        outgoingTransObj.CreatedBy = UserSession.Current.User.Id;
                        outgoingTransObj.CreatedDate = DateTime.Now;
                        outgoingTransObj.TypeId = 1;

                        var transId = this.incomingTransmittalService.Insert(outgoingTransObj);
                        // Create attach doc to trans info
                        if (outgoingTransObj != null)
                        {
                            var dataTable = tranSheet.Cells.ExportDataTable(11, 1, tranSheet.Cells.MaxDataRow - 37, 5);

                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!string.IsNullOrEmpty(dataRow["Column1"].ToString().Trim()) && dataRow["Column1"].ToString() != "(*) STATUS according to Coordination Procedure." && dataRow["Column1"].ToString().Length > 5)
                                {
                                    var revName = dataRow["Column2"].ToString().Replace(" ", string.Empty);
                                    var docNo = dataRow["Column1"].ToString();
                                    var status = dataRow["Column4"].ToString();
                                    var deadline = dataRow["Column5"].ToString();
                                    var docObj = this.documentPackageService.GetOneByDocNo_(docNo, revName, projectObj.ID);

                                    if (docObj != null)
                                    {
                                        listSelectedDocId.Add(docObj.ID);
                                        doclist.Add(docObj);
                                        var attachDoc = new AttachDocToTransmittal()
                                        {
                                            TransmittalId = transId,
                                            DocumentId = docObj.ID
                                        };
                                        if (!this.attachDocToTransmittalService.IsExist(transId.GetValueOrDefault(), docObj.ID))
                                        {
                                            this.attachDocToTransmittalService.Insert(attachDoc);
                                            if (outgoingTransObj.TypeInTrans.GetValueOrDefault() == 1)
                                            {
                                                docObj.IncomingTransNo = outgoingTransObj.TransNumber;
                                                docObj.IncomingTransDate = outgoingTransObj.ReceivedDate;
                                                docObj.Notes = status;
                                                var deadlineComment = new DateTime();
                                                if (Utility.ConvertStringToDateTime(deadline, ref deadlineComment))
                                                {
                                                    docObj.PlanedDate = deadlineComment;
                                                }
                                                else if (string.IsNullOrEmpty(deadline))
                                                {
                                                    docObj.PlanedDate = docObj.DeadlineReview3;
                                                }
                                                this.documentPackageService.Update(docObj);
                                            }
                                            else if (outgoingTransObj.TypeInTrans.GetValueOrDefault() == 2)
                                            {
                                                docObj.IncomingHardCopyTransNo = outgoingTransObj.TransNumber;
                                                docObj.IncomingHardCopyTransDate = outgoingTransObj.ReceivedDate;
                                                docObj.Notes = status;
                                                this.documentPackageService.Update(docObj);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        docNonExistList.Add(docNo + "_" + revName);
                                    }
                                }
                            }
                            //var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(outgoingTransObj.ID);
                            //if (outgoingTransObj.TypeInTrans.GetValueOrDefault() == 1)
                            //{
                            //    foreach (var item in attachDocList)
                            //    {
                            //        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                            //        if (docObj != null)
                            //        {


                            //            docObj.IncomingTransNo = outgoingTransObj.TransNumber;
                            //            docObj.IncomingTransDate = outgoingTransObj.ReceivedDate;

                            //            this.documentPackageService.Update(docObj);
                            //        }
                            //    }
                            //}
                            //else if (outgoingTransObj.TypeInTrans.GetValueOrDefault() == 2)
                            //{
                            //    foreach (var item in attachDocList)
                            //    {
                            //        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                            //        if (docObj != null)
                            //        {


                            //            docObj.IncomingHardCopyTransNo = outgoingTransObj.TransNumber;
                            //            docObj.IncomingHardCopyTransDate = outgoingTransObj.ReceivedDate;

                            //            this.documentPackageService.Update(docObj);
                            //        }
                            //    }
                            //}
                            //}
                            // Create Trans Folder on document Library
                            var dirName = "../../DocumentLibrary/SharedDoc/" +
                                          Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/" + RemoveAllSpecialCharacter("02. Transmittals In");
                            if (this.rbtnVendor.Checked)
                            {
                                dirName = "../../DocumentLibrary/SharedDoc/" +
                                          Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/Vendor/" + RemoveAllSpecialCharacter("02. Transmittals In");
                            }
                            else if (this.rbtnreview1.Checked || this.rbtnreview2.Checked || this.rbtnreview3.Checked)
                            {
                                dirName = "";
                            }
                            var mainTransFolder = this.folderService.GetByDirName(dirName);
                            if (mainTransFolder != null)
                            {
                                var transFolder = new Folder()
                                {
                                    Name = Utility.RemoveSpecialCharacterForFolder(outgoingTransObj.TransNumber),
                                    Description = Utility.RemoveSpecialCharacterForFolder(outgoingTransObj.TransNumber),
                                    ParentID = mainTransFolder.ID,
                                    DirName = mainTransFolder.DirName + "/" + Utility.RemoveSpecialCharacterForFolder(outgoingTransObj.TransNumber),
                                    CreatedBy = UserSession.Current.UserId,
                                    CreatedDate = DateTime.Now,
                                    ProjectId = mainTransFolder.ProjectId,
                                    ProjectName = mainTransFolder.ProjectName
                                };

                                Directory.CreateDirectory(Server.MapPath(transFolder.DirName));
                                var transFolderId = this.folderService.Insert(transFolder);

                                DirName_ = transFolder.DirName;
                                var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(mainTransFolder.ID);
                                foreach (var parentPermission in usersInPermissionOfParent)
                                {
                                    var childPermission = new UserDataPermission()
                                    {
                                        CategoryId = parentPermission.CategoryId,
                                        RoleId = parentPermission.RoleId,
                                        FolderId = transFolderId,
                                        UserId = parentPermission.UserId,
                                        IsFullPermission = parentPermission.IsFullPermission,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    };

                                    this.userDataPermissionService.Insert(childPermission);
                                }

                                // Update trans folder id for outgoing transmittal
                                outgoingTransObj.FolderId = transFolderId;
                                this.incomingTransmittalService.Update(outgoingTransObj);

                                foreach (var docId in listSelectedDocId)
                                {
                                    listAttachFilePackage.AddRange(
                                        this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                                }

                                foreach (var attachFile in listAttachFilePackage)
                                {
                                    if (transFolder != null && !string.IsNullOrEmpty(transFolder.DirName))
                                    {
                                        // Path file to save on server disc
                                        var saveFilePath = Path.Combine(Server.MapPath(transFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                        // Path file to download from server
                                        var serverFilePath = transFolder.DirName + "/" + attachFile.FileName;
                                        var fileExt = attachFile.Extension;

                                        if (!File.Exists(saveFilePath))
                                        {
                                            var document = new Data.Entities.Document()
                                            {
                                                Name = attachFile.FileName,
                                                FileExtension = fileExt,
                                                FileExtensionIcon = attachFile.ExtensionIcon,
                                                FilePath = serverFilePath,
                                                FolderID = transFolder.ID,
                                                IsLeaf = true,
                                                IsDelete = false,
                                                CreatedBy = UserSession.Current.User.Id,
                                                CreatedDate = DateTime.Now
                                            };
                                            this.documentService.Insert(document);
                                        }

                                        if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                        {
                                            File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                        }
                                    }
                                }
                            }
                        }

                        this.NotifiListDocument(doclist, projectObj.ID, outgoingTransObj, DirName_);
                    }
                }
            }

            if (docNonExistList.Count > 0)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Transmittal object created Successfull.</br>" +
                                     "But can't find documents:</br>";
                foreach (var docNo in docNonExistList)
                {
                    this.lblError.Text += docNo + "</br>";
                }

                this.lblError.Text += "to attach to Transmittal. Please create Document first and Attach to transmittal again.";
            }
            else
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Import transmittal file Successfull.";
            }
            //}
            //catch (Exception ex)
            //{
            //    this.blockError.Visible = true;
            //    this.lblError.Text = "Have error at Transmittal Form: '" + ex.Message + "'";
            //}
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }


        private string RemoveAllSpecialCharacter(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z]+", string.Empty);
        }
        private void NotifiListDocument(List<DocumentPackage> Listdoc, int projectid, IncomingTransmittal transmittal, string dername)
        {
            //try
            //{
            if (transmittal != null)
            {
                var userListid = this.autoDistributionMatriceService.GetAllByProject(projectid).Select(t => t.UserId.GetValueOrDefault()).Distinct().ToList();

                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    //Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount_Back"], ConfigurationManager.AppSettings["EmailPass_Back"])
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], string.Empty)
                };
                int count = 0;
                var containtable = string.Empty;
                string discipline = Listdoc.Any() ? Listdoc[0].DisciplineName : "";
                var subject = "New Transmittal (#Trans#) has been created on SFP System _ Discipline (" + discipline + ")";

                var message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                message.Subject = subject.Replace("#Trans#", transmittal.TransNumber);
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;

                var bodyContent = @"<div style=‘text-align: center;’> 
                                    <span class=‘Apple-tab-span’>Dear All,&nbsp;</span><br />
                                   
                                    <p style=‘text-align: center;’><strong><span style=‘font-size: 18px;’>please be informed that the following document of transmittal(#Trans#)</span></strong></p><br/><br/>
                                       <table border='1' cellspacing='0'>
                                       <tr>
                                       <th style='text-align:center; width:40px'>No.</th>
                                       <th style='text-align:center; width:330px'>Document Number</th>
                                       <th style='text-align:center; width:60px'>Revision</th>
                                       <th style='text-align:center; width:330px'>Document Title</th>
                                       <th style='text-align:center; width:330px'>Status (*)</th>
                                       <th style='text-align:center; width:330px'>Deadline Comment</th>
                                       </tr>";
                foreach (var document in Listdoc)
                {
                    var deadline = string.Empty;
                    count += 1;
                    deadline = document.PlanedDate != null ? document.PlanedDate.Value.ToString("dd/MM/yyyy") : "";
                    bodyContent += @"<tr>
                               <td>" + count + @"</td>
                               <td>" + document.DocNo + @"</td>
                               <td>"
                                   + document.RevisionName + @"</td>
                               <td>"
                                   + document.DocTitle + @"</td>
                               <td>"
                                   + document.Notes + @"</td>
                               <td>"
                                   + deadline + @"</td>";
                }
                var st = @"\\spf-edms.vietsov.com.vn:8010\XNXL_Transmittal" + dername.Replace("../../DocumentLibrary/SharedDoc", string.Empty);
                bodyContent += @"</table>
                                       <br/>
                                       <span><br />
                                    &nbsp;This link to access&nbsp;:&nbsp; <a href='" + st + "'>" + st + "</a>" +
                              @" <br/> Thanks and regards, <br/> System Admin.";
                message.Body = bodyContent.Replace("#Trans#", transmittal.TransNumber);

                if (WebConfigurationManager.AppSettings["NotificationAdmin"] == "true")
                {
                    message.To.Add(new MailAddress("trinhxuanluong@truetech.com.vn"));
                    //message.To.Add(new MailAddress("edms.cd@vietsov.com.vn"));
                }
                if (WebConfigurationManager.AppSettings["NotificationEng"] == "true")
                {
                    var Userlist = this.userService.GetSpecialUser(userListid).Where(t => !string.IsNullOrEmpty(t.Email)).Distinct().ToList();
                    foreach (var user in Userlist)
                    {
                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            message.To.Add(new MailAddress(user.Email));
                        }
                    }
                }
                smtpClient.Send(message);

                //message.Body = message.Body + "  <br/>user creat  :" + UserSession.Current.User.Username + "Time action :" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                //message.To.Clear();
                //message.To.Add(new MailAddress("edms.cd@vietsov.com.vn"));
                //smtpClient.Send(message);
            }
            //}
            //catch (Exception ex) { }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.rbtnreview1.Visible = false;
            this.rbtnreview3.Visible = false;
            this.rbtnreview2.Visible = false;

            var projectObj = this.scopeProjectService.GetById(Convert.ToInt32(this.ddlProject.SelectedValue));
            if (!string.IsNullOrEmpty(projectObj.organizationComment1)) { this.rbtnreview1.Visible = true; this.rbtnreview1.Text = "From " + projectObj.organizationComment1; }
            if (!string.IsNullOrEmpty(projectObj.organizationComment2)) { this.rbtnreview1.Visible = true; this.rbtnreview1.Text = "From " + projectObj.organizationComment2; }
            if (!string.IsNullOrEmpty(projectObj.organizationComment3)) { this.rbtnreview1.Visible = true; this.rbtnreview1.Text = "From " + projectObj.organizationComment3; }
        }
    }
}