﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using OfficeHelper.Utilities.Data;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalAttachDocument : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly IncomingTransmittalService transmittalService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The group data permission service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly DocumentPackageService documentPackageService;

        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly FolderService folderService;

        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly TemplateManagementService templateManagementService;

        private readonly DocRoleMatrixService docRoleMatrixService;
        private readonly DistributionMatrixDetailByUserService dmDetailByUserService;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly int TransmittalFolderId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TransFolderId"));

        /// <summary>
        /// Initializes a new instance of the <see cref="TransmittalAttachDocument"/> class.
        /// </summary>
        public TransmittalAttachDocument()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.receivedFromService = new ReceivedFromService();
            this.transmittalService = new IncomingTransmittalService();
            this.categoryService = new CategoryService();
            this.userService = new UserService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.workGroupService = new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.folderService = new FolderService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.templateManagementService = new TemplateManagementService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.docRoleMatrixService = new DocRoleMatrixService();
            this.dmDetailByUserService = new DistributionMatrixDetailByUserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.Page.IsPostBack)
            {
                this.LoadComboData();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
            var workgroupId = 0;
            var docNo = this.txtDocNo.Text.Trim();
            var docTitle = this.txtDocTitle.Text.Trim();

            var isGetAllRevision = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("GetAllRevisionInTrans"));

            var pageSize = this.grdDocument.PageSize;
            var currentPage = this.grdDocument.CurrentPageIndex;
            var startingRecordNumber = currentPage*pageSize;

            var listDoc = this.documentPackageService.SearchDocument(
                projectId,
                workgroupId,
                docNo,
                docTitle,
                string.Empty,
                isGetAllRevision);
            this.grdDocument.VirtualItemCount = listDoc.Count;
            this.grdDocument.DataSource =
                listDoc.OrderByDescending(t => t.ID).Skip(startingRecordNumber).Take(pageSize);
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectList = new List<ScopeProject>();

            if (UserSession.Current.IsAdmin)
            {
                projectList = this.scopeProjectService.GetAll();
            }
            else if (UserSession.Current.IsManager)
            {
                var projectIdsInPermission = this.docRoleMatrixService.GetProjectIdsInPermissionByRole(UserSession.Current.RoleId);
                projectList = this.scopeProjectService.GetAll().Where(t => projectIdsInPermission.Contains(t.ID)).ToList();
            }
            else if (UserSession.Current.IsEngineer || UserSession.Current.IsLead)
            {
                var projectIdsInPermission = this.dmDetailByUserService.GetProjectIdListInPermission(UserSession.Current.User.Id);
                projectList = this.scopeProjectService.GetAll().Where(t => projectIdsInPermission.Contains(t.ID)).ToList();
            }
            else
            {
                projectList = this.scopeProjectService.GetAllByUser(UserSession.Current.User.Id);
            }

            if (projectList.Any())
            {
                this.ddlProject.DataSource = projectList;
                this.ddlProject.DataTextField = "FullName";
                this.ddlProject.DataValueField = "ID";
                this.ddlProject.DataBind();
            }
        }

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var objTran = this.transmittalService.GetById(tranId);
                var listSelectedDocId = new List<int>();
                var listAttachFilePackage = new List<AttachFilesPackage>();

                if (objTran != null)
                {
                    var count = 1;
                    var filePath = Server.MapPath("../../Transmittals") + @"\";
                    var workbook = new Workbook();

                    workbook.Open(filePath + @"Template\TransmittalTemplate.xls");
                    var sheet1 = workbook.Worksheets[0];

                    var fromUser = this.userService.GetByID(objTran.FromId.GetValueOrDefault());
                    var toUser = this.userService.GetByID(objTran.ToId.GetValueOrDefault());

                    sheet1.Cells["Q5"].PutValue(objTran.TransNumber);
                    sheet1.Cells["G7"].PutValue(fromUser != null ? fromUser.FullName : string.Empty);
                    sheet1.Cells["G8"].PutValue(fromUser != null ? fromUser.Position : string.Empty);
                    sheet1.Cells["G9"].PutValue(fromUser != null ? toUser.FullName : string.Empty);
                    sheet1.Cells["G10"].PutValue(fromUser != null ? toUser.Position : string.Empty);
                    sheet1.Cells["G11"].PutValue("");

                    foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                    {
                        var cboxSelected =
                            (CheckBox)
                                item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                        if (cboxSelected.Checked)
                        {
                            var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                            listSelectedDocId.Add(docId);

                            var attachDoc = new AttachDocToTransmittal()
                            {
                                TransmittalId = tranId,
                                DocumentId = docId
                            };
                            if (!this.attachDocToTransmittalService.IsExist(tranId, docId))
                            {
                                this.attachDocToTransmittalService.Insert(attachDoc);
                            }

                            cboxSelected.Checked = false;
                        }
                    }

                    var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                    sheet1.Cells.InsertRows(19, attachDocList.Count > 0 ? attachDocList.Count - 1 : 0);
                    foreach (var item in attachDocList)
                    {
                        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                        if (docObj != null)
                        {
                            sheet1.Cells.Merge(17 + count, 2, 1, 6);
                            sheet1.Cells.Merge(17 + count, 8, 1, 4);

                            sheet1.Cells[17 + count, 1].PutValue(count);
                            sheet1.Cells[17 + count, 2].PutValue(docObj.DocNo);
                            sheet1.Cells[17 + count, 8].PutValue(docObj.DocTitle);
                            sheet1.Cells["M" + (18 + count).ToString()].PutValue(docObj.RevisionName);
                            count += 1;

                            docObj.OutgoingTransNo = objTran.TransNumber;
                            docObj.OutgoingTransDate = objTran.ReceivedDate;

                            this.documentPackageService.Update(docObj);
                        }
                    }

                    var filename = Utility.RemoveSpecialCharacter(objTran.TransNumber) + ".xls";

                    workbook.Save(filePath + @"Generated\" + filename);

                    var serverPath = "../../Transmittals/Generated/";

                    //if (File.Exists(Server.MapPath(objTran.AttachFilePath)))
                    //{
                    //    File.Delete(Server.MapPath(objTran.AttachFilePath));
                    //}

                    objTran.AttachFilePath = serverPath + filename;
                    objTran.AttachFileName = filename;

                    this.transmittalService.Update(objTran);
                }

                // Auto create transmittal folder on Document library and share document files
                var mainTransFolder = this.folderService.GetById(objTran.FolderId.GetValueOrDefault());

                try
                {
                    foreach (var docId in listSelectedDocId)
                    {
                        listAttachFilePackage.AddRange(
                            this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                    }

                    foreach (var attachFile in listAttachFilePackage)
                    {
                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                        // Path file to download from server
                        var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                        var fileExt = attachFile.Extension;

                        if (!File.Exists(saveFilePath))
                        {
                            var document = new Data.Entities.Document()
                            {
                                Name = attachFile.FileName,
                                FileExtension = fileExt,
                                FileExtensionIcon = attachFile.ExtensionIcon,
                                FilePath = serverFilePath,
                                FolderID = mainTransFolder.ID,
                                IsLeaf = true,
                                IsDelete = false,
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now
                            };
                            this.documentService.Insert(document);
                        }

                        if (File.Exists(Server.MapPath(attachFile.FilePath)))
                        {
                            File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        protected void ddlProject_SelectedIndexChange(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item.BackColor = Color.Aqua;
                    item.BorderColor = Color.Aqua;
                }
            }
        }
    }
}