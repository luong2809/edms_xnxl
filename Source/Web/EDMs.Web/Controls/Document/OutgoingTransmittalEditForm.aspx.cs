﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Text.RegularExpressions;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class OutgoingTransmittalEditForm : Page
    {
        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly ToListService toListService;

        private readonly IncomingTransmittalService outgoingTransmittalService;

        private readonly UserService userService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly FolderService folderService;

        private readonly UserDataPermissionService userDataPermissionService;


        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public OutgoingTransmittalEditForm()
        {
            this.toListService = new ToListService();
            this.outgoingTransmittalService = new IncomingTransmittalService();
            this.userService = new UserService();
            this.scopeProjectService = new ScopeProjectService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.txtReceivedDate.SelectedDate = DateTime.Now;
                this.LoadComboData();
                this.UploadControl.Visible = true;

                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    this.UploadControl.Visible = false;
                    this.CreatedInfo.Visible = true;
                    var incomingTransObj =
                    this.outgoingTransmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    if (incomingTransObj != null)
                    {
                        this.txtName.Text = incomingTransObj.Name;
                        this.txtTransmittalNumber.Text = incomingTransObj.TransNumber;
                        this.ddlFrom.SelectedValue = incomingTransObj.FromId.GetValueOrDefault().ToString();
                        this.ddlTo.SelectedValue = incomingTransObj.ToId.GetValueOrDefault().ToString();
                        this.txtAttention.Text = incomingTransObj.AttentionName;
                        this.txtReceivedDate.SelectedDate = incomingTransObj.ReceivedDate;

                        var createdUser = this.userService.GetByID(incomingTransObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + incomingTransObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (incomingTransObj.LastUpdatedBy != null && incomingTransObj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(incomingTransObj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + incomingTransObj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var outGoingTransObj = this.outgoingTransmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                if (outGoingTransObj != null)
                {
                    outGoingTransObj.Name = this.txtName.Text.Trim();
                    outGoingTransObj.TransNumber = this.txtTransmittalNumber.Text.Trim();
                    outGoingTransObj.ProjectName = this.ddlProject.SelectedItem != null
                        ? this.ddlProject.SelectedItem.Text
                        : string.Empty;
                    outGoingTransObj.ProjectId = this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0;
                    outGoingTransObj.FromId = Convert.ToInt32(this.ddlFrom.SelectedValue);
                    outGoingTransObj.FromName = this.ddlFrom.SelectedItem.Text;
                    outGoingTransObj.ToId = Convert.ToInt32(this.ddlTo.SelectedValue);
                    outGoingTransObj.ToName = this.ddlTo.SelectedItem.Text;
                    outGoingTransObj.AttentionName = this.txtAttention.Text.Trim();
                    outGoingTransObj.ReceivedDate = this.txtReceivedDate.SelectedDate;
                    outGoingTransObj.LastUpdatedBy = UserSession.Current.User.Id;
                    outGoingTransObj.LastUpdatedDate = DateTime.Now;
                    outGoingTransObj.TypeId = 2;
                    this.outgoingTransmittalService.Update(outGoingTransObj);

                    // Update Trans Folder when trans name be changed
                    var transFolder = this.folderService.GetById(outGoingTransObj.FolderId.GetValueOrDefault());
                    if (transFolder != null
                        && transFolder.Name != Utility.RemoveSpecialCharacterForFolder(outGoingTransObj.TransNumber))
                    {
                        var oldPath = Server.MapPath(transFolder.DirName);
                        var newPath = Server.MapPath(transFolder.DirName.Replace(transFolder.Name,
                            Utility.RemoveSpecialCharacterForFolder(outGoingTransObj.TransNumber)));
                        if (Directory.Exists(oldPath))
                        {
                            Directory.Move(oldPath, newPath);

                            transFolder.DirName = transFolder.DirName.Replace(transFolder.Name,
                            Utility.RemoveSpecialCharacterForFolder(outGoingTransObj.TransNumber));
                            transFolder.Name = Utility.RemoveSpecialCharacterForFolder(outGoingTransObj.TransNumber);
                            this.folderService.Update(transFolder);
                        }
                    }

                    // ------------------------------------------------------------------------------------------
                }
            }
            else
            {
                var outGoingTransObj = new IncomingTransmittal()
                    {
                        Name = this.txtName.Text.Trim(),
                        TransNumber = this.txtTransmittalNumber.Text.Trim(),
                        ProjectName = this.ddlProject.SelectedItem != null
                        ? this.ddlProject.SelectedItem.Text
                        : string.Empty,
                        ProjectId = this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0,
                        FromId = Convert.ToInt32(this.ddlFrom.SelectedValue),
                        FromName = this.ddlFrom.SelectedItem.Text,
                        ToId = Convert.ToInt32(this.ddlTo.SelectedValue),
                        ToName = this.ddlTo.SelectedItem.Text,
                        AttentionName = this.txtAttention.Text.Trim(),
                        ReceivedDate = this.txtReceivedDate.SelectedDate,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        TypeId = 2
                    };

                foreach (UploadedFile transFile in this.docuploader.UploadedFiles)
                {
                    var filePath = Server.MapPath("../../Transmittals/Import/");
                    var fileName = DateTime.Now.ToString("ddMMyyhhmmss") + transFile.FileName;
                    transFile.SaveAs(filePath + fileName, true);

                    outGoingTransObj.AttachFileName = transFile.FileName;
                    outGoingTransObj.AttachFilePath = "/Transmittals/Import/" + fileName;
                }

                if (this.docuploader.UploadedFiles.Count == 0)
                {
                    outGoingTransObj.AttachFileName = string.Empty;
                    outGoingTransObj.AttachFilePath = string.Empty;
                }

                this.outgoingTransmittalService.Insert(outGoingTransObj);

                // Create Trans Folder on document Library
                var projectObj = this.scopeProjectService.GetById(outGoingTransObj.ProjectId.GetValueOrDefault());
                var dirName = "../../DocumentLibrary/SharedDoc/" +
                              Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/"+RemoveAllSpecialCharacter("01. Transmittals Out");

                if (this.rbtnVendor.Checked)
                {
                    dirName = "../../DocumentLibrary/SharedDoc/" +
                              Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/Vendor/" + RemoveAllSpecialCharacter("01. Transmittals Out");
                }
                else if(this.rbtnOrganization.Checked)
                {
                    dirName = "";
                }
                var mainTransFolder = this.folderService.GetByDirName(dirName);
                if (mainTransFolder != null)
                {
                    var transFolder = new Folder()
                    {
                        Name = Utility.RemoveSpecialCharacterForFolder(outGoingTransObj.TransNumber),
                        Description = Utility.RemoveSpecialCharacterForFolder(outGoingTransObj.TransNumber),
                        ParentID = mainTransFolder.ID,
                        DirName = mainTransFolder.DirName + "/" + Utility.RemoveSpecialCharacterForFolder(outGoingTransObj.TransNumber),
                        CreatedBy = UserSession.Current.UserId,
                        CreatedDate = DateTime.Now,
                        ProjectId = mainTransFolder.ProjectId,
                        ProjectName = mainTransFolder.ProjectName
                    };

                    Directory.CreateDirectory(Server.MapPath(transFolder.DirName));
                    var transFolderId = this.folderService.Insert(transFolder);
                    var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(mainTransFolder.ID);
                    foreach (var parentPermission in usersInPermissionOfParent)
                    {
                        var childPermission = new UserDataPermission()
                        {
                            CategoryId = parentPermission.CategoryId,
                            RoleId = parentPermission.RoleId,
                            FolderId = transFolderId,
                            UserId = parentPermission.UserId,
                            IsFullPermission = parentPermission.IsFullPermission,
                            CreatedDate = DateTime.Now,
                            CreatedBy = UserSession.Current.User.Id
                        };

                        this.userDataPermissionService.Insert(childPermission);
                    }
                    // ------------------------------------------------------------------------------------------

                    // Update trans folder id for outgoing transmittal
                    outGoingTransObj.FolderId = transFolderId;
                    this.outgoingTransmittalService.Update(outGoingTransObj);
                    // ------------------------------------------------------------------------------------------
                }
            }

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }
        private string RemoveAllSpecialCharacter(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z]+", string.Empty);
        }
        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectList = this.scopeProjectService.GetAll();
            this.ddlProject.DataSource = projectList;
            this.ddlProject.DataTextField = "FullName";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            var tolist = this.toListService.GetAll();
            tolist.Insert(0, new ToList() { ID = 0, Name = string.Empty });

            this.ddlTo.DataSource = tolist;
            this.ddlTo.DataTextField = "Name";
            this.ddlTo.DataValueField = "ID";
            this.ddlTo.DataBind();
            this.ddlTo.SelectedIndex = 0;

            this.ddlFrom.DataSource = tolist;
            this.ddlFrom.DataTextField = "Name";
            this.ddlFrom.DataValueField = "ID";
            this.ddlFrom.DataBind();
            this.ddlFrom.SelectedIndex = 0;
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc, bool isUpdateOldRev)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            var revisionPath = "../../DocumentLibrary/RevisionHistory/";
            var serverRevisionFolder = HostingEnvironment.ApplicationVirtualPath + "/DocumentLibrary/RevisionHistory/";
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {

                    ////if (isUpdateOldRev)
                    ////{
                    ////    docFile.SaveAs(saveFileRevisionPath, true);
                    ////}
                    ////else
                    ////{
                    ////    docFile.SaveAs(saveFilePath, true);
                    ////    var fileinfo = new FileInfo(saveFilePath);
                    ////    fileinfo.CopyTo(saveFileRevisionPath, true);
                    ////}

                    ////if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                    ////{
                    ////    watcherService.ExecuteCommand(129);
                    ////}
                }
            }
        }
    }
}