﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Web.Hosting;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportUpdateVendor : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DocumentNewService documentNewService;

        private readonly CategoryService categoryService;

        private readonly OptionalTypeDetailService optionalTypeDetailService;

        private readonly OriginatorService originatorService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly DepartmentService departmentService;
        private readonly PlatformService platformService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly UserDataPermissionService userDataPermissionService;
        private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;
        private readonly VendorPackageService vendorpackageService;
        private readonly AttachFilesCommentService attachfileComment;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportUpdateVendor()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            //this.languageService = new LanguageService();
            this.documentService = new DocumentService();
            this.documentNewService = new DocumentNewService();
            this.categoryService = new CategoryService();
            this.optionalTypeDetailService = new OptionalTypeDetailService();
            this.originatorService = new OriginatorService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.vendorpackageService = new VendorPackageService();
            this.attachfileComment = new AttachFilesCommentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var newDocumentPackageList = new List<DocumentPackage>();
           
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        
                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;

                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectObj = this.scopeProjectService.GetById(projectID);
                            if (projectObj != null)
                            {
                                var vendorpackageid = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                                    ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                                    : 0;
                                var vendorpackage = this.vendorpackageService.GetById(vendorpackageid);
                                if (vendorpackage != null)
                                {
                                    var dataTable = datasheet.Cells.ExportDataTable(5, 0, datasheet.Cells.MaxRow - 1,33);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            var docId = Convert.ToInt32(dataRow["Column2"].ToString());
                                            currentDocumentNo = dataRow["Column3"].ToString();
                                            var docObj = this.documentPackageService.GetById(docId);
                                            if (docObj != null)
                                            {
                                                var revObj = this.revisionService.GetByName(dataRow["Column5"].ToString());
                                                var docTypeObj = this.documentTypeService.GetByFullName(dataRow["Column7"].ToString(), projectObj.ID);
                                                var discipline = this.disciplineService.GetByName(dataRow["Column6"].ToString(), projectObj.ID);
                                                // Add new document revision
                                                if (docObj.RevisionId != revObj.ID)
                                                {
                                                    var newRevDoc = new DocumentPackage();
                                                    newRevDoc.ProjectId = projectObj.ID;
                                                    newRevDoc.ProjectName = projectObj.Name;

                                                    newRevDoc.DocNo = dataRow["Column3"].ToString();
                                                    newRevDoc.DocTitle = dataRow["Column4"].ToString();
                                                    newRevDoc.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                    newRevDoc.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                    newRevDoc.StatusID = 0;
                                                    newRevDoc.StatusName = string.Empty;

                                                    newRevDoc.ReferenceFromID = 0;
                                                    newRevDoc.ReferenceFromName = string.Empty;

                                                    newRevDoc.DisciplineId = discipline != null ?discipline.ID:0;
                                                    newRevDoc.DisciplineName =discipline!= null?  discipline.FullName: string.Empty;

                                                    newRevDoc.DeparmentId =  0;
                                                    newRevDoc.DeparmentName =  string.Empty;

                                                    newRevDoc.Weight = 0.0;
                                                    newRevDoc.Complete = 0.0;

                                                    newRevDoc.IsCriticalDoc = false;
                                                    newRevDoc.IsPriorityDoc = false;
                                                    newRevDoc.IsVendorDoc = true;
                                                    newRevDoc.VendorPackagesId = vendorpackage.ID;
                                                    newRevDoc.VendorPackagesName = vendorpackage.Name;

                                                    newRevDoc.RevisionId = revObj != null ? revObj.ID : 0;
                                                    newRevDoc.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    newRevDoc.TypeId = docObj.TypeId;
                                                    newRevDoc.CurrenCodeName = dataRow["Column8"].ToString();
                                                    newRevDoc.StatusName = dataRow["Column9"].ToString();

                                                    var strPlanedDate = dataRow["Column10"].ToString();
                                                    var planedDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strPlanedDate, ref planedDate))
                                                    {
                                                        newRevDoc.IssuePlanDate = planedDate;
                                                    }

                                                  
                                                    newRevDoc.CreatedBy = UserSession.Current.User.Id;
                                                    newRevDoc.CreatedDate = DateTime.Now;
                                                    newRevDoc.IsLeaf = true;
                                                    newRevDoc.IsDelete = false;
                                                    newRevDoc.IsEMDR = true;
                                                    newRevDoc.ParentId = docObj.ParentId ?? docObj.ID;

                                                    newDocumentPackageList.Add(newRevDoc);

                                                    // Update isleaf for old doc revision
                                                    docObj.IsLeaf = false;
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    documentPackageList.Add(docObj);
                                                }
                                                // Update exist Document revision
                                                else
                                                {
                                                    docObj.ProjectId = projectObj.ID;
                                                    docObj.ProjectName = projectObj.Name;

                                                    docObj.DocNo = dataRow["Column3"].ToString();
                                                    docObj.DocTitle = dataRow["Column4"].ToString();
                                                    docObj.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                    docObj.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                    docObj.StatusID = 0;
                                                    docObj.StatusName = string.Empty;

                                                    docObj.ReferenceFromID = 0;
                                                    docObj.ReferenceFromName = string.Empty;

                                                    docObj.DisciplineId = discipline!= null? discipline.ID:0;
                                                    docObj.DisciplineName = discipline != null ?discipline.FullName:string.Empty;

                                                    docObj.DeparmentId = 0;
                                                    docObj.DeparmentName =  string.Empty;

                                                    docObj.Weight = 0.0;

                                                    docObj.IsCriticalDoc = false;
                                                    docObj.IsPriorityDoc = false;
                                                    docObj.IsVendorDoc = true;

                                                    docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                                    docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    //docObj.TypeId = 1;

                                                    docObj.CurrenCodeName = dataRow["Column8"].ToString();
                                                    docObj.StatusName = dataRow["Column9"].ToString();

                                                    var strPlanedDate = dataRow["Column10"].ToString();
                                                    var planedDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strPlanedDate, ref planedDate))
                                                    {
                                                        docObj.IssuePlanDate = planedDate;
                                                    }

                                                    docObj.TransInFromVendorNo = dataRow["Column11"].ToString();
                                                    var TransInFromVendor = dataRow["Column12"].ToString();
                                                    var TransInFromVendorDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(TransInFromVendor, ref TransInFromVendorDate))
                                                    {
                                                        docObj.TransInFromVendorDate = TransInFromVendorDate;
                                                    }

                                                    var endorResponse = dataRow["Column13"].ToString();
                                                    var endorResponseDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(endorResponse, ref endorResponseDate))
                                                    {
                                                        docObj.VendorResponseDate = endorResponseDate;
                                                    }



                                                    docObj.OutgoingTransNo = dataRow["Column14"].ToString();
                                                    var OutgoingTrans = dataRow["Column15"].ToString();
                                                    var OutgoingTransDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(OutgoingTrans, ref OutgoingTransDate))
                                                    {
                                                        docObj.OutgoingTransDate = OutgoingTransDate;
                                                    }

                                                    docObj.TransOutToOrganizationNo = dataRow["Column16"].ToString();
                                                    var TransOutToOrganization = dataRow["Column17"].ToString();
                                                    var TransOutToOrganizationDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(TransOutToOrganization, ref TransOutToOrganizationDate))
                                                    {
                                                        docObj.TransOutToOrganizationDate = TransOutToOrganizationDate;
                                                    }
                                                  docObj.Organization = dataRow["Column18"].ToString();

                                                  var deadline1 = dataRow["Column19"].ToString();
                                                  var deadline1Date = new DateTime();
                                                  if (Utility.ConvertStringToDateTime(deadline1, ref deadline1Date))
                                                  {
                                                      docObj.DeadlineReview1 = deadline1Date;
                                                  }
                                                    docObj.TransInFromReviewNo1 = dataRow["Column20"].ToString().Trim();
                                                    var TransInFromReview1 = dataRow["Column21"].ToString();
                                                    var TransInFromReview1Date = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(TransInFromReview1, ref TransInFromReview1Date))
                                                    {
                                                        docObj.TransInFromReviewDate1 = TransInFromReview1Date;
                                                    }
                                                    docObj.StatusReview1 = dataRow["Column22"].ToString().Trim();
                                                    docObj.CodeReview1 = dataRow["Column23"].ToString().Trim();


                                                    var deadline2 = dataRow["Column24"].ToString();
                                                    var deadline2Date = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(deadline2, ref deadline2Date))
                                                    {
                                                        docObj.DeadlineReview2 = deadline2Date;
                                                    }
                                                    docObj.TransInFromReviewNo2 = dataRow["Column25"].ToString().Trim();
                                                    var TransInFromReview2 = dataRow["Column26"].ToString();
                                                    var TransInFromReview2Date = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(TransInFromReview2, ref TransInFromReview2Date))
                                                    {
                                                        docObj.TransInFromReviewDate2 = TransInFromReview2Date;
                                                    }
                                                    docObj.StatusReview2 = dataRow["Column27"].ToString().Trim();
                                                    docObj.CodeReview2 = dataRow["Column28"].ToString().Trim();

                                                    var deadline3 = dataRow["Column29"].ToString();
                                                    var deadline3Date = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(deadline3, ref deadline3Date))
                                                    {
                                                        docObj.DeadlineReview3 = deadline3Date;
                                                    }
                                                    docObj.TransInFromReviewNo3 = dataRow["Column30"].ToString().Trim();
                                                    var TransInFromReview3 = dataRow["Column31"].ToString();
                                                    var TransInFromReview3Date = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(TransInFromReview3, ref TransInFromReview3Date))
                                                    {
                                                        docObj.TransInFromReviewDate3 = TransInFromReview3Date;
                                                    }
                                                    docObj.StatusReview3 = dataRow["Column32"].ToString().Trim();
                                                    docObj.CodeReview3 = dataRow["Column33"].ToString().Trim();
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    docObj.IsLeaf = true;
                                                    docObj.IsDelete = false;
                                                    docObj.IsEMDR = true;
                                                    documentPackageList.Add(docObj);
                                                }
                                            }
                                        }
                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        foreach (var docObj in documentPackageList)
                                        {
                                           

                                            this.documentPackageService.Update(docObj);

                                            if (!string.IsNullOrEmpty(docObj.OutgoingTransNo))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.OutgoingTransNo);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };

                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                        try
                                                        {
                                                            // Auto create transmittal folder on Document library and share document files
                                                            var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                            foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                            {
                                                                // Path file to save on server disc
                                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                // Path file to download from server
                                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                var fileExt = attachFile.Extension;

                                                                if (!File.Exists(saveFilePath))
                                                                {
                                                                    var document = new Data.Entities.Document()
                                                                    {
                                                                        Name = attachFile.FileName,
                                                                        FileExtension = fileExt,
                                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                                        FilePath = serverFilePath,
                                                                        FolderID = mainTransFolder.ID,
                                                                        IsLeaf = true,
                                                                        IsDelete = false,
                                                                        CreatedBy = UserSession.Current.User.Id,
                                                                        CreatedDate = DateTime.Now
                                                                    };
                                                                    this.documentService.Insert(document);
                                                                }

                                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                {
                                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                }
                                                            }



                                                            foreach (var attachFile in this.attachfileComment.GetAllByDocId(docObj.ID))
                                                            {
                                                                // Path file to save on server disc
                                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                // Path file to download from server
                                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                var fileExt = attachFile.Extension;

                                                                if (!File.Exists(saveFilePath))
                                                                {
                                                                    var document = new Data.Entities.Document()
                                                                    {
                                                                        Name = attachFile.FileName,
                                                                        FileExtension = fileExt,
                                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                                        FilePath = serverFilePath,
                                                                        FolderID = mainTransFolder.ID,
                                                                        IsLeaf = true,
                                                                        IsDelete = false,
                                                                        CreatedBy = UserSession.Current.User.Id,
                                                                        CreatedDate = DateTime.Now
                                                                    };
                                                                    this.documentService.Insert(document);
                                                                }

                                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                {
                                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                }
                                                            }
                                                        }
                                                        catch { }
                                                    }
                                                }
                                            }


                                            if (!string.IsNullOrEmpty(docObj.TransOutToOrganizationNo))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransOutToOrganizationNo);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };
                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                    }
                                                }
                                            }



                                            if (!string.IsNullOrEmpty(docObj.TransInFromVendorNo))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromVendorNo);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };
                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                        try
                                                        {
                                                            // Auto create transmittal folder on Document library and share document files
                                                            var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                            foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                            {
                                                                // Path file to save on server disc
                                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                // Path file to download from server
                                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                var fileExt = attachFile.Extension;

                                                                if (!File.Exists(saveFilePath))
                                                                {
                                                                    var document = new Data.Entities.Document()
                                                                    {
                                                                        Name = attachFile.FileName,
                                                                        FileExtension = fileExt,
                                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                                        FilePath = serverFilePath,
                                                                        FolderID = mainTransFolder.ID,
                                                                        IsLeaf = true,
                                                                        IsDelete = false,
                                                                        CreatedBy = UserSession.Current.User.Id,
                                                                        CreatedDate = DateTime.Now
                                                                    };
                                                                    this.documentService.Insert(document);
                                                                }

                                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                {
                                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                }
                                                            }

                                                        }
                                                        catch { }
                                                    }
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo1))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo1);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };
                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                    }
                                                }
                                            }


                                            if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo2))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo2);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };
                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                    }
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo3))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo3);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };
                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                    }
                                                }
                                            }
                                        }

                                        foreach (var docObj in newDocumentPackageList)
                                        {
                                            var docId = this.documentPackageService.Insert(docObj);
                                            if (docId != null)
                                            {

                                                if (!string.IsNullOrEmpty(docObj.OutgoingTransNo))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.OutgoingTransNo);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                            try
                                                            {
                                                                // Auto create transmittal folder on Document library and share document files
                                                                var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                                foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                                {
                                                                    // Path file to save on server disc
                                                                    var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                    // Path file to download from server
                                                                    var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                    var fileExt = attachFile.Extension;

                                                                    if (!File.Exists(saveFilePath))
                                                                    {
                                                                        var document = new Data.Entities.Document()
                                                                        {
                                                                            Name = attachFile.FileName,
                                                                            FileExtension = fileExt,
                                                                            FileExtensionIcon = attachFile.ExtensionIcon,
                                                                            FilePath = serverFilePath,
                                                                            FolderID = mainTransFolder.ID,
                                                                            IsLeaf = true,
                                                                            IsDelete = false,
                                                                            CreatedBy = UserSession.Current.User.Id,
                                                                            CreatedDate = DateTime.Now
                                                                        };
                                                                        this.documentService.Insert(document);
                                                                    }

                                                                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                    {
                                                                        File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                    }
                                                                }

                                                                foreach (var attachFile in this.attachfileComment.GetAllByDocId(docObj.ID))
                                                                {
                                                                    // Path file to save on server disc
                                                                    var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                    // Path file to download from server
                                                                    var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                    var fileExt = attachFile.Extension;

                                                                    if (!File.Exists(saveFilePath))
                                                                    {
                                                                        var document = new Data.Entities.Document()
                                                                        {
                                                                            Name = attachFile.FileName,
                                                                            FileExtension = fileExt,
                                                                            FileExtensionIcon = attachFile.ExtensionIcon,
                                                                            FilePath = serverFilePath,
                                                                            FolderID = mainTransFolder.ID,
                                                                            IsLeaf = true,
                                                                            IsDelete = false,
                                                                            CreatedBy = UserSession.Current.User.Id,
                                                                            CreatedDate = DateTime.Now
                                                                        };
                                                                        this.documentService.Insert(document);
                                                                    }

                                                                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                    {
                                                                        File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                    }
                                                                }
                                                            }
                                                            catch { }
                                                        }
                                                    }
                                                }


                                                if (!string.IsNullOrEmpty(docObj.TransOutToOrganizationNo))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransOutToOrganizationNo);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                        }
                                                    }
                                                }
                                                
                                                
                                                
                                                if (!string.IsNullOrEmpty(docObj.TransInFromVendorNo))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromVendorNo);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                            try
                                                            {
                                                                // Auto create transmittal folder on Document library and share document files
                                                                var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                                foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                                {
                                                                    // Path file to save on server disc
                                                                    var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                    // Path file to download from server
                                                                    var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                    var fileExt = attachFile.Extension;

                                                                    if (!File.Exists(saveFilePath))
                                                                    {
                                                                        var document = new Data.Entities.Document()
                                                                        {
                                                                            Name = attachFile.FileName,
                                                                            FileExtension = fileExt,
                                                                            FileExtensionIcon = attachFile.ExtensionIcon,
                                                                            FilePath = serverFilePath,
                                                                            FolderID = mainTransFolder.ID,
                                                                            IsLeaf = true,
                                                                            IsDelete = false,
                                                                            CreatedBy = UserSession.Current.User.Id,
                                                                            CreatedDate = DateTime.Now
                                                                        };
                                                                        this.documentService.Insert(document);
                                                                    }

                                                                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                    {
                                                                        File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                    }
                                                                }
                                                            }
                                                            catch { }
                                                        }
                                                    }
                                                }

                                                if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo1))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo1);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                        } 
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo2))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo2);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                        }
                                                    }
                                                }

                                                if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo3))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo3);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                        } 
                                                    }
                                                }
                                            }
                                        }

                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document master list file is valid. System import successfull!";
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                }
                                else
                                {
                                    this.blockError.Visible = true;
                                    this.lblError.Text =
                                        "Have Error: Project '" + projectObj.Name + "' don't have Document Type '" +
                                        currentSheetName + "'. Please remove this sheet out of master file.";
                                }
                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }
                    }
                }
           }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}