﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ExportDistributionMatrix : Page
    {
        private readonly RoleService roleService;
        private readonly ScopeProjectService scopeProjectService;
        private readonly DistributionMatrixDetailByUserService dmDetailByUserService;
        private readonly UserService userService;
        private readonly AutoDistributionMatriceService autoDMService;
        private readonly DocumentPackageService docPackageService;
        private readonly StatusService docStatusService;

        public ExportDistributionMatrix()
        {
            this.roleService = new RoleService();
            this.scopeProjectService = new ScopeProjectService();
            this.dmDetailByUserService = new DistributionMatrixDetailByUserService();
            this.userService = new UserService();
            this.autoDMService = new AutoDistributionMatriceService();
            this.docPackageService = new DocumentPackageService();
            this.docStatusService = new StatusService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["projectId"]))
                {
                    var projectId = Convert.ToInt32(this.Request.QueryString["projectId"]);
                    var projectObj = this.scopeProjectService.GetById(projectId);
                    if (projectObj != null)
                    {
                        this.txtProjectName.Text = projectObj.FullName;

                        var roleInProject = this.autoDMService.GetAllByProject(projectId).Select(t => t.RoleId.GetValueOrDefault());

                        var roleList = this.roleService.GetAll(false).Where(t => roleInProject.Contains(t.Id));
                        this.ddlRoles.DataValueField = "Id";
                        this.ddlRoles.DataTextField = "FullName";
                        this.ddlRoles.DataSource = roleList;
                        this.ddlRoles.DataBind();
                    }
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var roleId = Convert.ToInt32(this.ddlRoles.SelectedValue);
            var roleObj = this.roleService.GetByID(roleId);
            var projectId = Convert.ToInt32(this.Request.QueryString["projectId"]);
            var projectObj = this.scopeProjectService.GetById(projectId);
            if (roleObj != null && projectObj != null)
            {
                if (projectObj.ProjectTypeId == 1)
                {
                    this.ExportDMForInternalProject(projectObj, roleObj);
                }
                else
                {
                    this.ExportDMForServiceProject(projectObj, roleObj);
                }
            }
        }

        private void ExportDMForInternalProject(ScopeProject projectObj, Role roleObj)
        {
            var userIdInProjectList = this.autoDMService.GetAllByProject(projectObj.ID).Select(t => t.UserId.GetValueOrDefault());
            var masterDoclist = this.docPackageService.GetAllDocMaster(projectObj.ID);

            var userInProject = this.userService.GetAllByRoleId(roleObj.Id).Where(t => userIdInProjectList.Contains(t.Id)).OrderBy(t => t.DisciplineName).ThenBy(t => t.UserNameWithFullName).ToList();

            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\AutoDocDistributionmatrixByUser_InternalProject_Template.xlsm");
            var dataSheet = workbook.Worksheets[0];
            var tempdataSheet = workbook.Worksheets[1];

            var dtFull = new DataTable();
            dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocID", typeof (String)),
                        new DataColumn("NoIndex", typeof (String)),
                        new DataColumn("DocNo", typeof (String)),
                        new DataColumn("DocTitle", typeof (String)),
                        new DataColumn("DocRev", typeof (String)),
                    });

            var userCount = 0;
            var userGroupByDiscipline = userInProject.GroupBy(t => t.DisciplineName);
            foreach (var userInDiscipline in userGroupByDiscipline)
            {
                // Put user's discipline infor
                dataSheet.Cells[1, 6 + userCount].PutValue(string.IsNullOrEmpty(userInDiscipline.Key) ? "None Discipline" : userInDiscipline.Key);
                if (userInDiscipline.ToList().Count > 1)
                {
                    dataSheet.Cells.Merge(1, 6 + userCount, 1, userInDiscipline.ToList().Count);
                }
                // -----------------------------------------------------------------------------------

                // Put user info
                foreach (var userItem in userInDiscipline)
                {
                    dataSheet.Cells[2, 6 + userCount].PutValue(userItem.FullName);
                    dataSheet.Cells[3, 6 + userCount].PutValue(userCount + 1);
                    dataSheet.Cells[4, 6 + userCount].PutValue(userItem.Id);

                    // Add dynamic matrix column
                    var colGroup = new DataColumn("Col_" + userItem.Id, typeof(String));
                    dtFull.Columns.Add(colGroup);
                    userCount += 1;
                }
                // -----------------------------------------------------------------------------------
            }

            // Put project name and department name
            dataSheet.Cells["E1"].PutValue(dataSheet.Cells["E1"].Value.ToString()
                .Replace("<ProjectName>", projectObj.FullName));
            dataSheet.Cells["E2"].PutValue(dataSheet.Cells["E2"].Value.ToString()
                .Replace("<Department>", "Dept: " + roleObj.FullName));
            // ----------------------------------------------------------------------------------------

            // Repair data for documents binding
            var docCount = 1;
            var docGroupByDiscipline = masterDoclist.GroupBy(t => t.DisciplineName);
            var DMList = this.dmDetailByUserService.GetAll();
            foreach (var docListOfDiscipline in docGroupByDiscipline)
            {
                var dataRow = dtFull.NewRow();
                dataRow["DocID"] = 0;
                dataRow["DocNo"] = docListOfDiscipline.Key;
                dtFull.Rows.Add(dataRow);
                //count += 1;

                var docList = docListOfDiscipline.OrderBy(t => t.DocNo).ToList();
                foreach (var document in docList)
                {
                    dataRow = dtFull.NewRow();
                    dataRow["DocID"] = document.ID;
                    dataRow["NoIndex"] = docCount;
                    dataRow["DocNo"] = document.DocNo;
                    dataRow["DocTitle"] = document.DocTitle;
                    dataRow["DocRev"] = document.RevisionName;

                    var dmDetailList = DMList.Where(t => t.DocId == document.ID).ToList();
                    foreach (var dmDetailItem in dmDetailList)
                    {
                        if (userInProject.Exists(t => t.Id == dmDetailItem.UserId))
                        {
                            dataRow["Col_" + dmDetailItem.UserId] = "X";
                        }
                    }

                    dtFull.Rows.Add(dataRow);

                    docCount += 1;
                }
            }
            // ---------------------------------------------------------------------------------------

            // Binding document list
            dataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
            tempdataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
            // -------------------------------------------------------------------------------------------

            dataSheet.Cells["A1"].PutValue("AutoDMProject");
            dataSheet.Cells["A2"].PutValue(dtFull.Rows.Count);
            dataSheet.Cells["A3"].PutValue(userInProject.Count);
            dataSheet.Cells["A4"].PutValue(projectObj.ID);

            tempdataSheet.IsVisible = false;

            var filename = projectObj.Name + "_DocDistributionMatrix_" + roleObj.Name + ".xlsm";
            workbook.Save(filePath + filename);
            this.Download_File(filePath + filename);
            //this.DownloadByWriteByte(filePath + filename, filename, true);
        }

        private void ExportDMForServiceProject(ScopeProject projectObj, Role roleObj)
        {
            var docStatusList = this.docStatusService.GetAllByProject(projectObj.ID).OrderBy(t => t.PercentCompleteDefault).ToList();
            var userIdInProjectList = this.autoDMService.GetAllByProject(projectObj.ID).Select(t => t.UserId.GetValueOrDefault());
            var masterDoclist = this.docPackageService.GetAllDocMaster(projectObj.ID);

            var userInProject = this.userService.GetAllByRoleId(roleObj.Id).Where(t => userIdInProjectList.Contains(t.Id)).OrderBy(t => t.DisciplineName).ThenBy(t => t.UserNameWithFullName).ToList();

            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\AutoDocDistributionmatrixByUser_ServiceProject_Template.xlsm");
            var dataSheet = workbook.Worksheets[1];
            var tempdataSheet = workbook.Worksheets[2];
            var rangeSheet = workbook.Worksheets[0];

            // Create range of Doc Status
            var rangedocStatusList = rangeSheet.Cells.CreateRange("B3",
                    "C" + (docStatusList.Count == 0 ? 3 : docStatusList.Count + 2));
            rangedocStatusList.Name = "DocStatus";
            for (int j = 0; j < docStatusList.Count; j++)
            {
                rangedocStatusList[j, 0].PutValue(docStatusList[j].Name);
                rangedocStatusList[j, 1].PutValue(docStatusList[j].PercentCompleteDefault + " %");
            }
            // ---------------------------------------------------------------

            var dtFull = new DataTable();
            dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocID", typeof (String)),
                        new DataColumn("NoIndex", typeof (String)),
                        new DataColumn("DocNo", typeof (String)),
                        new DataColumn("DocTitle", typeof (String)),
                        new DataColumn("DocRev", typeof (String)),
                    });

            var userCount = 0;
            var userGroupByDiscipline = userInProject.GroupBy(t => t.DisciplineName);
            foreach (var userInDiscipline in userGroupByDiscipline)
            {
                // Put user's discipline infor
                dataSheet.Cells[1, 6 + userCount].PutValue(string.IsNullOrEmpty(userInDiscipline.Key) ? "None Discipline" : userInDiscipline.Key);
                if (userInDiscipline.ToList().Count > 1)
                {
                    dataSheet.Cells.Merge(1, 6 + userCount, 1, userInDiscipline.ToList().Count);
                }
                // -----------------------------------------------------------------------------------

                // Put user info
                foreach (var userItem in userInDiscipline)
                {
                    dataSheet.Cells[2, 6 + userCount].PutValue(userItem.FullName);
                    dataSheet.Cells[3, 6 + userCount].PutValue(userCount + 1);
                    dataSheet.Cells[4, 6 + userCount].PutValue(userItem.Id);

                    // Add dynamic matrix column
                    var colGroup = new DataColumn("Col_" + userItem.Id, typeof(String));
                    dtFull.Columns.Add(colGroup);
                    userCount += 1;
                }
                // -----------------------------------------------------------------------------------
            }

            // Put project name and department name
            dataSheet.Cells["E1"].PutValue(dataSheet.Cells["E1"].Value.ToString()
                .Replace("<ProjectName>", projectObj.FullName));
            dataSheet.Cells["E2"].PutValue(dataSheet.Cells["E2"].Value.ToString()
                .Replace("<Department>", "Dept: " + roleObj.FullName));
            // ----------------------------------------------------------------------------------------

            // Repair data for documents binding
            var docCount = 1;
            var docGroupByDiscipline = masterDoclist.GroupBy(t => t.DisciplineName);
            var DMList = this.dmDetailByUserService.GetAll();
            foreach (var docListOfDiscipline in docGroupByDiscipline)
            {
                var dataRow = dtFull.NewRow();
                dataRow["DocID"] = 0;
                dataRow["DocNo"] = docListOfDiscipline.Key;
                dtFull.Rows.Add(dataRow);
                //count += 1;

                var docList = docListOfDiscipline.OrderBy(t => t.DocNo).ToList();
                foreach (var document in docList)
                {
                    dataRow = dtFull.NewRow();
                    dataRow["DocID"] = document.ID;
                    dataRow["NoIndex"] = docCount;
                    dataRow["DocNo"] = document.DocNo;
                    dataRow["DocTitle"] = document.DocTitle;
                    dataRow["DocRev"] = document.RevisionName;

                    var dmDetailList = DMList.Where(t => t.DocId == document.ID).ToList();
                    foreach (var dmDetailItem in dmDetailList)
                    {
                        if (userInProject.Exists(t => t.Id == dmDetailItem.UserId))
                        {
                            dataRow["Col_" + dmDetailItem.UserId] += dmDetailItem.DocStatusName + ",";
                        }
                    }

                    dtFull.Rows.Add(dataRow);

                    docCount += 1;
                }
            }
            // ---------------------------------------------------------------------------------------

            // Binding document list
            dataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
            tempdataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
            // -------------------------------------------------------------------------------------------

            // Create Validation
            //var validations = dataSheet.Validations;
            //this.CreateValidation(rangedocStatusList.Name, validations, 5, dtFull.Rows.Count + 4, 6, 5 + userCount);
            // ----------------------------------------------------------------

            dataSheet.Cells["A1"].PutValue("AutoDMProject");
            dataSheet.Cells["A2"].PutValue(dtFull.Rows.Count);
            dataSheet.Cells["A3"].PutValue(userInProject.Count);
            dataSheet.Cells["A4"].PutValue(projectObj.ID);

            // Hide temp data sheet for compare
            tempdataSheet.IsVisible = false;
            // -----------------------------------------------
            var filename = projectObj.Name + "_DocDistributionMatrix_" + roleObj.Name + ".xlsm";
            workbook.Save(filePath + filename);
            this.Download_File(filePath + filename);
        }


        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                this.Page.Response.ClearHeaders();
                this.Page.Response.ClearContent();
                this.Page.Response.ContentType = "Application/octet-stream";
                this.Page.Response.AddHeader("Content-Length", data.Length.ToString());
                this.Page.Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);


                this.Page.Response.BinaryWrite(data);


                this.Page.Response.Flush();

                this.Page.Response.End();


                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }


            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }
    }
}