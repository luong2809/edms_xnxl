﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Web.Hosting;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocShopDrawingMasterList : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DocumentNewService documentNewService;

        private readonly CategoryService categoryService;

        private readonly OptionalTypeDetailService optionalTypeDetailService;

        private readonly OriginatorService originatorService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly DepartmentService departmentService;
        private readonly PlatformService platformService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocShopDrawingMasterList()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.languageService = new LanguageService();
            this.documentService = new DocumentService();
            this.documentNewService = new DocumentNewService();
            this.categoryService = new CategoryService();
            this.optionalTypeDetailService = new OptionalTypeDetailService();
            this.originatorService = new OriginatorService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var existDocumentPackageList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);

                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;

                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectObj = this.scopeProjectService.GetById(projectID);
                            if (projectObj != null)
                            {
                                var dataTable = datasheet.Cells.ExportDataTable(4, 2, datasheet.Cells.MaxRow - 1,
                                    6);
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                    {
                                        currentDocumentNo = dataRow["Column1"].ToString();
                                        var existDocObj = this.documentPackageService.GetOneByDocNo(currentDocumentNo, projectObj.ID);

                                        var revObj = this.revisionService.GetByName(dataRow["Column5"].ToString());
                                        
                                        var refDocObj = this.documentPackageService.GetOneByDocNo(dataRow["Column2"].ToString(), projectObj.ID);
                                        if (refDocObj != null)
                                        {
                                            var docObj = new DocumentPackage();
                                            docObj.ProjectId = projectObj.ID;
                                            docObj.ProjectName = projectObj.Name;

                                            docObj.DocNo = dataRow["Column1"].ToString();
                                            docObj.AnotherDocNo = dataRow["Column3"].ToString();
                                            docObj.DocTitle = dataRow["Column4"].ToString();
                                            docObj.DocumentTypeId = refDocObj.DocumentTypeId;
                                            docObj.DocumentTypeName = refDocObj.DocumentTypeName;
                                            docObj.StatusID = 0;
                                            docObj.StatusName = string.Empty;

                                            docObj.ReferenceFromID = 0;
                                            docObj.ReferenceFromName = string.Empty;

                                            docObj.DisciplineId = refDocObj.DisciplineId;
                                            docObj.DisciplineName = refDocObj.DisciplineName;

                                            docObj.DeparmentId = refDocObj.DeparmentId;
                                            docObj.DeparmentName = refDocObj.DeparmentName;

                                            docObj.Weight = 0.0;

                                            docObj.IsCriticalDoc = false;
                                            docObj.IsPriorityDoc = false;
                                            docObj.IsVendorDoc = false;

                                            docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                            docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                            docObj.TypeId = 2;
                                            //docObj.RevisionPlanedDate = Convert.ToDateTime(dataRow["Column6"]);
                                            var PlanedDate = dataRow["Column6"].ToString();
                                            var planedDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(PlanedDate, ref planedDate))
                                            {
                                                docObj.PlanedDate = planedDate;
                                            }

                                            docObj.ReferenceDocId = refDocObj.ID;
                                            docObj.ReferenceDocNo = refDocObj.DocNo;
                                            docObj.ReferenceDocTitle = refDocObj.DocTitle;
                                            docObj.CreatedBy = UserSession.Current.User.Id;
                                            docObj.CreatedDate = DateTime.Now;
                                            docObj.IsLeaf = true;
                                            docObj.IsDelete = false;
                                            docObj.IsEMDR = true;

                                            var disciplineObj = this.disciplineService.GetById(refDocObj.DisciplineId.GetValueOrDefault());
                                            if (disciplineObj != null)
                                            {
                                                docObj.DocPath = disciplineObj.DisciplinePath.Replace("01.All Revision", "03.Shop Drawing/01.All Revision");
                                            }
                                            

                                            if (existDocObj != null)
                                            {
                                                docObj.ParentId = existDocObj.ParentId ?? existDocObj.ID;
                                                existDocObj.IsLeaf = false;
                                                existDocObj.UpdatedBy = UserSession.Current.User.Id;
                                                existDocObj.UpdatedDate = DateTime.Now;
                                                existDocumentPackageList.Add(existDocObj);
                                            }

                                            documentPackageList.Add(docObj);
                                        }
                                    }
                                }

                                if (this.cbCheckValidFile.Checked)
                                {
                                    foreach (var docObj in documentPackageList)
                                    {
                                        //var validDocNoFolder = Utility.RemoveSpecialCharacterForFolder(docObj.DocNo);
                                        //var targetFolder = "../../DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/00.ShopDrawing";
                                        //var physicalDocNoFolder = Path.Combine(Server.MapPath(targetFolder), validDocNoFolder);
                                        //var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/00.ShopDrawing/" + validDocNoFolder;

                                        //docObj.DocPath = serverFolder;

                                        var docId = this.documentPackageService.Insert(docObj);
                                        //if (docId != null)
                                        //{

                                        //    if (!Directory.Exists(physicalDocNoFolder))
                                        //    {
                                        //        Directory.CreateDirectory(physicalDocNoFolder);
                                        //    }
                                        //}
                                    }

                                    foreach (var docObj in existDocumentPackageList)
                                    {
                                        this.documentPackageService.Update(docObj);
                                    }

                                    this.blockError.Visible = true;
                                    this.lblError.Text =
                                        "Data of document master list file is valid. System import successfull!";
                                }
                                else
                                {
                                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                        "CloseAndRebind();", true);
                                }

                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo +
                                     "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}