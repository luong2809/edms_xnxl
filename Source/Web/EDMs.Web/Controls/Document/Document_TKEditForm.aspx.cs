﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using System.Net.Mail;
using System.Configuration;
using System.Text;
using System.Net;
using System.Collections.Generic;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Document_TKEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DocumentPackage_TKService documentPackage_TKService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly FolderService folderService;

        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public Document_TKEditForm()
        {
            this.userService = new UserService();
            this.documentPackage_TKService = new DocumentPackage_TKService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {

                    var objDocumentPackage_TKService = this.documentPackage_TKService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    this.CreatedInfo.Visible = objDocumentPackage_TKService != null;

                    if (objDocumentPackage_TKService != null)
                    {

                        this.txtDocNo.Text = objDocumentPackage_TKService.DocNo;
                        this.txtDocTitle.Text = objDocumentPackage_TKService.DocTitle;
                        this.ddlDocType.SelectedValue = objDocumentPackage_TKService.DocumentTypeId.ToString();
                        this.txtDeadline.SelectedDate = objDocumentPackage_TKService.PlanedDate;

                        var createdUser = this.userService.GetByID(objDocumentPackage_TKService.CreatedBy.GetValueOrDefault());
                        this.lblCreated.Text = "Created at " + objDocumentPackage_TKService.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objDocumentPackage_TKService.UpdatedBy != null && objDocumentPackage_TKService.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objDocumentPackage_TKService.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objDocumentPackage_TKService.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var documentId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var documentObj = this.documentPackage_TKService.GetById(documentId);
                    if (documentObj != null)
                    {
                        documentObj.DocNo = this.txtDocNo.Text.Trim();
                        documentObj.DocTitle = this.txtDocTitle.Text.Trim();
                        documentObj.DocumentTypeId = Convert.ToInt32(this.ddlDocType.SelectedValue.Trim());
                        documentObj.DocumentTypeName = this.ddlDocType.SelectedItem.Text.Trim();
                        documentObj.PlanedDate = this.txtDeadline.SelectedDate.GetValueOrDefault();
                        documentObj.UpdatedBy = UserSession.Current.User.Id;
                        documentObj.UpdatedDate = DateTime.Now;
                        this.documentPackage_TKService.Update(documentObj);
                    }
                }
                else
                {
                    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibraryTK/Projects/PHUQUOC/" + ddlDocType.SelectedItem.Text;
                    var documentObj = new DocumentPackages_TK()
                    {
                        DocNo = this.txtDocNo.Text.Trim(),
                        DocTitle = this.txtDocTitle.Text.Trim(),
                        DocumentTypeId = Convert.ToInt32(this.ddlDocType.SelectedValue.Trim()),
                        DocumentTypeName = this.ddlDocType.SelectedItem.Text.Trim(),
                        PlanedDate = this.txtDeadline.SelectedDate.GetValueOrDefault(),
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        DocPath = serverFolder
                    };

                    this.documentPackage_TKService.Insert(documentObj);
                    //this.NotificationAddNewWp(documentObj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        private string RemoveAllSpecialCharacter(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z]+", string.Empty);
        }

        private void NotificationAddNewWp(DocumentPackages_TK docObj)
        {
            if (docObj != null)
            {
                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                };

                var CreatedUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());
                var subject = "Document #DocumentNumber# has been created on EDMS System by #CreatedUser#".Replace("#DocumentNumber#", docObj.DocNo).Replace("#CreatedUser#", CreatedUser != null ? CreatedUser.FullName : string.Empty);
                var body = @"Document #DocumentNumber# has been created on EDMS System.
                                 Document information:
                                   Document number: #DocumentNumber#
                                   Document Title: #DocumentTitle#
                                   Document Type: #DocumentType#
                                   Deadline: #Deadline#
                                Please access http://spf-edms.vietsov.com.vn:8010/ for more information.";

                var message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                message.Subject = subject;
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;
                message.Body = body
                    .Replace("#DocumentNumber#", docObj.DocNo != null ? docObj.DocNo : string.Empty)
                    .Replace("#DocumentTitle#", docObj.DocTitle != null ? docObj.DocTitle : string.Empty)
                    .Replace("#DocumentType#", docObj.DocumentTypeName != null ? docObj.DocumentTypeName.ToString() : string.Empty)
                    .Replace("#Deadline#", docObj.PlanedDate != null ? docObj.PlanedDate.Value.ToString("dd/MM/yyyy") : string.Empty);

                message.To.Add(new MailAddress("edms.cd@vietsov.com.vn"));
                //phan quyen cho emdr phong thuong mai
                List<int> userTM = new List<int>() {1, 581, 735, 586, 484, 451, 465, 637, 734, 460, 609, 578, 551, 495, 597, 654,
                653, 733, 732, 592, 731, 591, 730, 729, 461, 631, 463, 554, 704, 555, 557, 576, 575, 526, 573, 523, 478,
                667, 565, 736, 569, 527, 656, 564, 563, 652, 660, 668, 500, 452, 544, 502, 503, 512, 510, 508, 506, 504,
                536, 479, 538, 501, 584, 496, 610, 728, 723, 727, 677 };

                var userAll = this.userService.GetAll().ToList();
                var userTMList = userAll.Where(t => userTM.Contains(t.Id)).ToList();
                foreach (var item in userTMList)
                {
                    //message.CC.Add(new MailAddress(item.Email));
                }

                //var userdc = this.userService.GetByID(projectObj.DocumentControlId.GetValueOrDefault());
                //if (userdc != null && !string.IsNullOrEmpty(userdc.Email))
                //{
                //    message.CC.Add(new MailAddress(userdc.Email));
                //}
                //else
                //{
                //    var userdclist = this.userService.GetAll(false).Where(t => t.IsDC.GetValueOrDefault() && !string.IsNullOrEmpty(t.Email)).ToList();
                //    foreach (var item in userdclist)
                //    {
                //        message.CC.Add(new MailAddress(item.Email));
                //    }

                //}
                smtpClient.Send(message);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtDocNo.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            //var userList = this.userService.GetAll();
            //var dcList = userList.Where(t => t.IsDC.GetValueOrDefault()).ToList();
            //var pmList = userList.Where(t => t.IsManager.GetValueOrDefault() || t.IsLeader.GetValueOrDefault()).ToList();

            //dcList.Insert(0, new User(){Id = 0});
            //pmList.Insert(0, new User() { Id = 0 });

            //this.ddlDC.DataSource = dcList;
            //this.ddlDC.DataValueField = "Id";
            //this.ddlDC.DataTextField = "FullName";
            //this.ddlDC.DataBind();

            //this.ddlPM.DataSource = pmList;
            //this.ddlPM.DataValueField = "Id";
            //this.ddlPM.DataTextField = "FullName";
            //this.ddlPM.DataBind();
        }
    }
}