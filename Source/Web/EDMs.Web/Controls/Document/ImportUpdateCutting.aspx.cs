﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Web.Hosting;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportUpdateCutting : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DocumentNewService documentNewService;

        private readonly CategoryService categoryService;

        private readonly OptionalTypeDetailService optionalTypeDetailService;

        private readonly OriginatorService originatorService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly DepartmentService departmentService;
        private readonly PlatformService platformService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly UserDataPermissionService userDataPermissionService;
        private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly AttachFilesCommentService attachfileComment;
        private readonly AreaService areaService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportUpdateCutting()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.areaService = new AreaService();
            this.documentService = new DocumentService();
            this.documentNewService = new DocumentNewService();
            this.categoryService = new CategoryService();
            this.optionalTypeDetailService = new OptionalTypeDetailService();
            this.originatorService = new OriginatorService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.attachFilesPackageService = new AttachFilesPackageService();
          
            this.attachfileComment = new AttachFilesCommentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var newDocumentPackageList = new List<DocumentPackage>();
           
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        
                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;

                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectObj = this.scopeProjectService.GetById(projectID);
                            if (projectObj != null)
                            {
                                var vendorpackageid = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                                    ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                                    : 0;
                                var AreaObj = this.areaService.GetById(vendorpackageid);
                                if (AreaObj != null)
                                {
                                    var dataTable = datasheet.Cells.ExportDataTable(5, 0, datasheet.Cells.MaxRow - 1,13);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            var docId = Convert.ToInt32(dataRow["Column2"].ToString());
                                            currentDocumentNo = dataRow["Column3"].ToString();
                                            var docObj = this.documentPackageService.GetById(docId);
                                            if (docObj != null)
                                            {
                                                var revObj = this.revisionService.GetByName(dataRow["Column5"].ToString());
                                                var existdocrev = this.documentPackageService.GetOneByDocNo(currentDocumentNo, revObj.Name, projectObj.ID);
                                                // Add new document revision
                                                if (docObj.RevisionId != revObj.ID && existdocrev ==null)
                                                {
                                                    var newRevDoc = new DocumentPackage();
                                                    newRevDoc.ProjectId = projectObj.ID;
                                                    newRevDoc.ProjectName = projectObj.Name;

                                                    newRevDoc.DocNo = dataRow["Column3"].ToString();
                                                    newRevDoc.DocTitle = dataRow["Column4"].ToString();
                                                  
                                                    newRevDoc.StatusID = 0;
                                                    newRevDoc.StatusName = string.Empty;
                                                    newRevDoc.ReferenceFromID = 0;
                                                    newRevDoc.ReferenceFromName = string.Empty;
                                                    newRevDoc.DeparmentId =  0;
                                                    newRevDoc.DeparmentName =  string.Empty;
                                                    newRevDoc.Weight = 0.0;

                                                    newRevDoc.IsCriticalDoc = false;
                                                    newRevDoc.IsPriorityDoc = false;
                                                    newRevDoc.IsVendorDoc = false;

                                                    newRevDoc.RevisionId = revObj != null ? revObj.ID : 0;
                                                    newRevDoc.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    newRevDoc.TypeId = docObj.TypeId;


                                                    var startdate = dataRow["Column6"].ToString();
                                                    var startDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(startdate, ref startDate))
                                                    {
                                                        newRevDoc.StartDate = startDate;
                                                    }

                                                    var deadline = dataRow["Column7"].ToString();
                                                    var deadLine = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(deadline, ref deadLine))
                                                    {
                                                        newRevDoc.PlanedDate = deadLine;
                                                    }
                                                    newRevDoc.Complete = !string.IsNullOrEmpty(dataRow["Column8"].ToString())
                                                  ? Convert.ToDouble(dataRow["Column8"].ToString()) * 100
                                                  : 0;
                                                    newRevDoc.VendorNameOfCutting = dataRow["Column9"].ToString();

                                                    newRevDoc.IncomingTransNo = dataRow["Column10"].ToString();
                                                    var TransInFromVendor = dataRow["Column11"].ToString();
                                                    var TransInFromVendorDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(TransInFromVendor, ref TransInFromVendorDate))
                                                    {
                                                        newRevDoc.IncomingTransDate = TransInFromVendorDate;
                                                    }

                                                    newRevDoc.OutgoingTransNo = dataRow["Column12"].ToString();
                                                    var OutgoingTrans = dataRow["Column13"].ToString();
                                                    var OutgoingTransDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(OutgoingTrans, ref OutgoingTransDate))
                                                    {
                                                        newRevDoc.OutgoingTransDate = OutgoingTransDate;
                                                    }
                                                    newRevDoc.AreaId = docObj.AreaId;
                                                    newRevDoc.AreaName = docObj.AreaName;
                                                  
                                                    newRevDoc.CreatedBy = UserSession.Current.User.Id;
                                                    newRevDoc.CreatedDate = DateTime.Now;
                                                    newRevDoc.IsLeaf = true;
                                                    newRevDoc.IsDelete = false;
                                                    newRevDoc.IsEMDR = true;
                                                    newRevDoc.ParentId = docObj.ParentId ?? docObj.ID;

                                                    newDocumentPackageList.Add(newRevDoc);

                                                    // Update isleaf for old doc revision
                                                    docObj.IsLeaf = false;
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    documentPackageList.Add(docObj);
                                                }
                                                // Update exist Document revision
                                                else
                                                {
                                                    docObj.ProjectId = projectObj.ID;
                                                    docObj.ProjectName = projectObj.Name;

                                                    docObj.DocNo = dataRow["Column3"].ToString();
                                                    docObj.DocTitle = dataRow["Column4"].ToString();

                                                    docObj.StatusID = 0;
                                                    docObj.StatusName = string.Empty;

                                                    docObj.ReferenceFromID = 0;
                                                    docObj.ReferenceFromName = string.Empty;

                                              
                                                    docObj.DeparmentId = 0;
                                                    docObj.DeparmentName =  string.Empty;

                                                    docObj.Weight = 0.0;

                                                    docObj.IsCriticalDoc = false;
                                                    docObj.IsPriorityDoc = false;
                                                    docObj.IsVendorDoc = false;

                                                    docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                                    docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    docObj.TypeId = 4;
                                                    var startdate = dataRow["Column6"].ToString();
                                                    var startDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(startdate, ref startDate))
                                                    {
                                                        docObj.StartDate = startDate;
                                                    }

                                                    var deadline = dataRow["Column7"].ToString();
                                                    var deadLine = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(deadline, ref deadLine))
                                                    {
                                                        docObj.PlanedDate = deadLine;
                                                    }
                                                    docObj.Complete = !string.IsNullOrEmpty(dataRow["Column8"].ToString())
                                                  ? Convert.ToDouble(dataRow["Column8"].ToString()) * 100
                                                  : 0;
                                                    docObj.VendorNameOfCutting = dataRow["Column9"].ToString();

                                                    docObj.IncomingTransNo = dataRow["Column10"].ToString();
                                                    var TransInFromVendor = dataRow["Column11"].ToString();
                                                    var TransInFromVendorDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(TransInFromVendor, ref TransInFromVendorDate))
                                                    {
                                                        docObj.IncomingTransDate = TransInFromVendorDate;
                                                    }

                                                    docObj.OutgoingTransNo = dataRow["Column12"].ToString();
                                                    var OutgoingTrans = dataRow["Column13"].ToString();
                                                    var OutgoingTransDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(OutgoingTrans, ref OutgoingTransDate))
                                                    {
                                                        docObj.OutgoingTransDate = OutgoingTransDate;
                                                    }
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    docObj.IsLeaf = true;
                                                    docObj.IsDelete = false;
                                                    docObj.IsEMDR = true;
                                                    documentPackageList.Add(docObj);
                                                }
                                            }
                                        }
                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        foreach (var docObj in documentPackageList)
                                        {
                                           

                                            this.documentPackageService.Update(docObj);

                                            if (!string.IsNullOrEmpty(docObj.OutgoingTransNo))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.OutgoingTransNo);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };

                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                        try
                                                        {
                                                            // Auto create transmittal folder on Document library and share document files
                                                            var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                            foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                            {
                                                                // Path file to save on server disc
                                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                // Path file to download from server
                                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                var fileExt = attachFile.Extension;

                                                                if (!File.Exists(saveFilePath))
                                                                {
                                                                    var document = new Data.Entities.Document()
                                                                    {
                                                                        Name = attachFile.FileName,
                                                                        FileExtension = fileExt,
                                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                                        FilePath = serverFilePath,
                                                                        FolderID = mainTransFolder.ID,
                                                                        IsLeaf = true,
                                                                        IsDelete = false,
                                                                        CreatedBy = UserSession.Current.User.Id,
                                                                        CreatedDate = DateTime.Now
                                                                    };
                                                                    this.documentService.Insert(document);
                                                                }

                                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                {
                                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                }
                                                            }
                                                            
                                                        }
                                                        catch { }
                                                    }
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };
                                                    if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                    {
                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                        try
                                                        {
                                                            // Auto create transmittal folder on Document library and share document files
                                                            var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                            foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                            {
                                                                // Path file to save on server disc
                                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                // Path file to download from server
                                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                var fileExt = attachFile.Extension;

                                                                if (!File.Exists(saveFilePath))
                                                                {
                                                                    var document = new Data.Entities.Document()
                                                                    {
                                                                        Name = attachFile.FileName,
                                                                        FileExtension = fileExt,
                                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                                        FilePath = serverFilePath,
                                                                        FolderID = mainTransFolder.ID,
                                                                        IsLeaf = true,
                                                                        IsDelete = false,
                                                                        CreatedBy = UserSession.Current.User.Id,
                                                                        CreatedDate = DateTime.Now
                                                                    };
                                                                    this.documentService.Insert(document);
                                                                }

                                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                {
                                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                }
                                                            }

                                                        }
                                                        catch { }
                                                    }
                                                }
                                            }

                                          
                                        }

                                        foreach (var docObj in newDocumentPackageList)
                                        {
                                            var docId = this.documentPackageService.Insert(docObj);
                                            if (docId != null)
                                            {

                                                if (!string.IsNullOrEmpty(docObj.OutgoingTransNo))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.OutgoingTransNo);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                            try
                                                            {
                                                                // Auto create transmittal folder on Document library and share document files
                                                                var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                                foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                                {
                                                                    // Path file to save on server disc
                                                                    var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                    // Path file to download from server
                                                                    var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                    var fileExt = attachFile.Extension;

                                                                    if (!File.Exists(saveFilePath))
                                                                    {
                                                                        var document = new Data.Entities.Document()
                                                                        {
                                                                            Name = attachFile.FileName,
                                                                            FileExtension = fileExt,
                                                                            FileExtensionIcon = attachFile.ExtensionIcon,
                                                                            FilePath = serverFilePath,
                                                                            FolderID = mainTransFolder.ID,
                                                                            IsLeaf = true,
                                                                            IsDelete = false,
                                                                            CreatedBy = UserSession.Current.User.Id,
                                                                            CreatedDate = DateTime.Now
                                                                        };
                                                                        this.documentService.Insert(document);
                                                                    }

                                                                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                    {
                                                                        File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                    }
                                                                }

                                                               
                                                            }
                                                            catch { }
                                                        }
                                                    }
                                                }


                                            
                                                
                                                
                                                if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };
                                                        if (!this.attachDocToTransmittalService.IsExist(transObj.ID, transObj.ID))
                                                        {
                                                            this.attachDocToTransmittalService.Insert(docTrans);
                                                            try
                                                            {
                                                                // Auto create transmittal folder on Document library and share document files
                                                                var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                                foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                                {
                                                                    // Path file to save on server disc
                                                                    var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                                                    // Path file to download from server
                                                                    var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                    var fileExt = attachFile.Extension;

                                                                    if (!File.Exists(saveFilePath))
                                                                    {
                                                                        var document = new Data.Entities.Document()
                                                                        {
                                                                            Name = attachFile.FileName,
                                                                            FileExtension = fileExt,
                                                                            FileExtensionIcon = attachFile.ExtensionIcon,
                                                                            FilePath = serverFilePath,
                                                                            FolderID = mainTransFolder.ID,
                                                                            IsLeaf = true,
                                                                            IsDelete = false,
                                                                            CreatedBy = UserSession.Current.User.Id,
                                                                            CreatedDate = DateTime.Now
                                                                        };
                                                                        this.documentService.Insert(document);
                                                                    }

                                                                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                    {
                                                                        File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                    }
                                                                }
                                                            }
                                                            catch { }
                                                        }
                                                    }
                                                }

                                            }
                                        }

                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document master list file is valid. System import successfull!";
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                }
                                else
                                {
                                    this.blockError.Visible = true;
                                    this.lblError.Text =
                                        "Have Error: Project '" + projectObj.Name + "' don't have Document Type '" +
                                        currentSheetName + "'. Please remove this sheet out of master file.";
                                }
                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }
                    }
                }
           }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}