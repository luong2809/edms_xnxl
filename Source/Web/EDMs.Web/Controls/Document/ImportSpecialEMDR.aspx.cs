﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportSpecialEMDR : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly DocumentStatusProcessService docStatusProcessService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportSpecialEMDR()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.folderService = new FolderService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.docStatusProcessService = new DocumentStatusProcessService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var docStatusProcessList = new List<DocumentStatusProcess>();
            bool changesof = false;
            bool changehard = false;
            var newDocumentPackageList = new List<DocumentPackage>();
            var newdocStatusProcessList = new List<DocumentStatusProcess>();
            //try
            //{
            foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
            {
                var extension = docFile.GetExtension();
                if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                {
                    var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                    docFile.SaveAs(importPath);

                    var workbook = new Workbook();
                    workbook.Open(importPath);

                    var datasheet = workbook.Worksheets[0];

                    currentSheetName = datasheet.Name;
                    currentDocumentNo = string.Empty;

                    var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                        ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                        : 0;
                    var projectObj = this.scopeProjectService.GetById(projectID);
                    if (projectObj != null)
                    {
                        // Get doc status list if project type is Service
                        var statusList = new List<Status>();
                        if (projectObj.ProjectTypeId == 2)
                        {
                            statusList = this.statusService.GetAllByProject(projectObj.ID).OrderBy(t => t.PercentCompleteDefault).ToList();
                        }
                        // ------------------------------------------------------------------

                        var disciplineId = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                            ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                            : 0;
                        var disciplineObj = this.disciplineService.GetById(disciplineId);
                        if (disciplineObj != null)
                        {
                            var dataTable = datasheet.Cells.ExportDataTable(2, 1, datasheet.Cells.MaxRow, 4);
                            var docProject = this.documentPackageService.GetAllOfProject(projectID, 1);
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                {
                                    currentDocumentNo = dataRow["Column1"].ToString();
                                    string[] list = currentDocumentNo.Split('-');
                                    var docTypeName = "";
                                    if (projectObj.ID == 51)
                                    {
                                        docTypeName = list[5];
                                    }
                                    else
                                    {
                                        docTypeName = list[4];
                                    }
                                    var docTypeObj = this.documentTypeService.GetByName(docTypeName, projectObj.ID);
                                    var revObj = this.revisionService.GetByName(dataRow["Column2"].ToString());
                                    var docObj = docProject.FirstOrDefault(t => t.DocNo.Trim().Replace(" ", string.Empty) == currentDocumentNo.Trim().Replace(" ", string.Empty));
                                    if (docObj != null)
                                    {
                                        //var departmentObj = this.roleService.GetByFullName(dataRow["Column7"].ToString());

                                        // Add new document revision
                                        if (docObj.RevisionId != revObj.ID)
                                        {
                                            var newRevDoc = new DocumentPackage();
                                            newRevDoc.ProjectId = projectObj.ID;
                                            newRevDoc.ProjectName = projectObj.Name;

                                            newRevDoc.DocNo = dataRow["Column1"].ToString();
                                            newRevDoc.DocTitle = dataRow["Column3"].ToString();
                                            newRevDoc.DocumentTypeId = docObj.DocumentTypeId;
                                            newRevDoc.DocumentTypeName = docObj.DocumentTypeName;
                                            //newRevDoc.StatusID = 0;
                                            //newRevDoc.StatusName = string.Empty;

                                            //newRevDoc.ReferenceFromID = 0;
                                            //newRevDoc.ReferenceFromName = string.Empty;

                                            newRevDoc.DisciplineId = docObj.DisciplineId;
                                            newRevDoc.DisciplineName = docObj.DisciplineName;

                                            //newRevDoc.DeparmentId = docObj.DeparmentId;
                                            //newRevDoc.DeparmentName = docObj.DeparmentName;

                                            //newRevDoc.Weight = docObj.Weight;

                                            //newRevDoc.IsCriticalDoc = false;
                                            //newRevDoc.IsPriorityDoc = false;
                                            //newRevDoc.IsVendorDoc = false;

                                            newRevDoc.RevisionId = revObj != null ? revObj.ID : 0;
                                            newRevDoc.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                            newRevDoc.TypeId = docObj.TypeId;

                                            var strPlanedDate = dataRow["Column4"].ToString();
                                            var planedDate = new DateTime();
                                            if (Utility.ConvertStringToDateTimeDateMonth(strPlanedDate, ref planedDate))
                                            {
                                                newRevDoc.PlanedDate = planedDate;
                                            }

                                            newRevDoc.CreatedBy = UserSession.Current.User.Id;
                                            newRevDoc.CreatedDate = DateTime.Now;
                                            newRevDoc.IsLeaf = true;
                                            newRevDoc.IsDelete = false;
                                            newRevDoc.IsEMDR = true;
                                            newRevDoc.ParentId = docObj.ParentId ?? docObj.ID;

                                            newRevDoc.StatusID = docObj.StatusID;
                                            newRevDoc.StatusName = docObj.StatusName;
                                            newRevDoc.DocPath = docObj.DocPath;

                                            //keep info incoming hard copy
                                            newRevDoc.IncomingHardCopyTransDate = docObj.IncomingHardCopyTransDate;
                                            newRevDoc.IncomingHardCopyTransNo = docObj.IncomingHardCopyTransNo;

                                            newDocumentPackageList.Add(newRevDoc);

                                            // Update isleaf for old doc revision
                                            docObj.IsLeaf = false;
                                            docObj.UpdatedBy = UserSession.Current.User.Id;
                                            docObj.UpdatedDate = DateTime.Now;
                                            documentPackageList.Add(docObj);

                                        }
                                        // ------------------------------------------------------------------------------------------------------
                                        // Update exist Document revision
                                        else
                                        {
                                            docObj.ProjectId = projectObj.ID;
                                            docObj.ProjectName = projectObj.Name;

                                            docObj.DocNo = dataRow["Column1"].ToString();
                                            docObj.DocTitle = dataRow["Column3"].ToString();
                                            //docObj.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                            //docObj.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                            //docObj.StatusID = 0;
                                            //docObj.StatusName = string.Empty;

                                            //docObj.ReferenceFromID = 0;
                                            //docObj.ReferenceFromName = string.Empty;

                                            //docObj.DisciplineId = disciplineObj.ID;
                                            //docObj.DisciplineName = disciplineObj.FullName;

                                            //docObj.DeparmentId = departmentObj != null ? departmentObj.Id : 0;
                                            //docObj.DeparmentName = departmentObj != null ? departmentObj.FullName : string.Empty;

                                            //docObj.Weight = 0.0;

                                            //docObj.IsCriticalDoc = false;
                                            //docObj.IsPriorityDoc = false;
                                            //docObj.IsVendorDoc = false;

                                            //docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                            //docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;


                                            var strPlanedDate = dataRow["Column4"].ToString();
                                            var planedDate = new DateTime();
                                            if (Utility.ConvertStringToDateTimeDateMonth(strPlanedDate, ref planedDate))
                                            {
                                                docObj.PlanedDate = planedDate;
                                            }

                                            //docObj.IncomingTransNo = dataRow["Column9"].ToString();
                                            //var strIncomingTransDate = dataRow["Column10"].ToString();
                                            //var incomingTransDate = new DateTime();
                                            //if (Utility.ConvertStringToDateTime(strIncomingTransDate, ref incomingTransDate))
                                            //{
                                            //    docObj.IncomingTransDate = incomingTransDate;
                                            //}

                                            //docObj.IncomingHardCopyTransNo = dataRow["Column11"].ToString();
                                            //var strIncomingHardCopyTransDate = dataRow["Column12"].ToString();
                                            //var incomingHardCopyTransDate = new DateTime();
                                            //if (Utility.ConvertStringToDateTime(strIncomingHardCopyTransDate, ref incomingHardCopyTransDate))
                                            //{
                                            //    docObj.IncomingHardCopyTransDate = incomingHardCopyTransDate;
                                            //}

                                            //docObj.Complete = !string.IsNullOrEmpty(dataRow["Column13"].ToString())
                                            //    ? Convert.ToDouble(dataRow["Column13"].ToString()) * 100
                                            //    : 0;
                                            //docObj.Weight = !string.IsNullOrEmpty(dataRow["Column14"].ToString())
                                            //    ? Convert.ToDouble(dataRow["Column14"].ToString()) * 100
                                            //    : 0;
                                            //docObj.CommentCode = dataRow["Column15"].ToString().Trim();
                                            //var dateissue = dataRow["Column16"].ToString();
                                            //var Dateissue = new DateTime();
                                            //if (Utility.ConvertStringToDateTime(dateissue, ref Dateissue))
                                            //{
                                            //    docObj.DeadlineIssue = Dateissue;
                                            //}
                                            docObj.UpdatedBy = UserSession.Current.User.Id;
                                            docObj.UpdatedDate = DateTime.Now;
                                            docObj.IsLeaf = true;
                                            docObj.IsDelete = false;
                                            docObj.IsEMDR = true;
                                            documentPackageList.Add(docObj);

                                            // Get update Plan date info for doc status process
                                            //var existDocStatusProcessList = this.docStatusProcessService.GetAllByDoc(docObj.ID);
                                            //for (int j = 0; j < statusList.Count; j++)
                                            //{
                                            //    var existDocStatusProcess = existDocStatusProcessList.FirstOrDefault(t => t.StatusName == statusList[j].Name);
                                            //    if (existDocStatusProcess != null)
                                            //    {
                                            //        var docStatusplanedDate = dataRow["Column" + (16 + j)];
                                            //        existDocStatusProcess.PlantDate = (DateTime?)docStatusplanedDate;

                                            //        docStatusProcessList.Add(existDocStatusProcess);
                                            //    }
                                            //}
                                            // ----------------------------------------------------------------------------------------------------------------

                                        }
                                    }
                                    else
                                    {

                                        var newRevDoc = new DocumentPackage();
                                        newRevDoc.ProjectId = projectObj.ID;
                                        newRevDoc.ProjectName = projectObj.Name;

                                        newRevDoc.DocNo = dataRow["Column1"].ToString();
                                        newRevDoc.DocTitle = dataRow["Column3"].ToString();

                                        newRevDoc.DisciplineId = disciplineObj.ID;
                                        newRevDoc.DisciplineName = disciplineObj.FullName;

                                        newRevDoc.DocumentTypeId = docTypeObj.ID;
                                        newRevDoc.DocumentTypeName = docTypeObj.FullName;

                                        newRevDoc.RevisionId = revObj != null ? revObj.ID : 0;
                                        newRevDoc.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                        newRevDoc.TypeId = 1;

                                        var strPlanedDate = dataRow["Column4"].ToString();
                                        var planedDate = new DateTime();
                                        if (Utility.ConvertStringToDateTimeDateMonth(strPlanedDate, ref planedDate))
                                        {
                                            newRevDoc.PlanedDate = planedDate;
                                        }

                                        newRevDoc.CreatedBy = UserSession.Current.User.Id;
                                        newRevDoc.CreatedDate = DateTime.Now;
                                        newRevDoc.IsLeaf = true;
                                        newRevDoc.IsDelete = false;
                                        newRevDoc.IsEMDR = true;

                                        if (disciplineObj != null)
                                        {
                                            newRevDoc.DocPath = newRevDoc.TypeId == 1
                                                            ? disciplineObj.DisciplinePath
                                                            : disciplineObj.DisciplinePath.Replace("01.All Revision", "03.Shop Drawing/01.All Revision");
                                        }


                                        newDocumentPackageList.Add(newRevDoc);
                                    }
                                }
                            }

                            if (this.cbCheckValidFile.Checked)
                            {
                                // Update Docinfo
                                foreach (var docObj in documentPackageList)
                                {

                                    this.documentPackageService.Update(docObj);

                                }
                                // ------------------------------------------------------------------------------------------------

                                // Update Doc Status Process
                                //foreach (var docStatusProcessItem in docStatusProcessList)
                                //{
                                //    this.docStatusProcessService.Update(docStatusProcessItem);
                                //}
                                // ------------------------------------------------------------------------------------------------

                                // Create new document revision
                                foreach (var docObj in newDocumentPackageList)
                                {
                                    this.documentPackageService.Insert(docObj);
                                    //if (docId != null)
                                    //{
                                    //    Create doc status process info
                                    //   var docStatusProcessOfDoc = newdocStatusProcessList.Where(t => t.DocNumber == docObj.DocNo);
                                    //    foreach (var docStatusProcess in docStatusProcessOfDoc)
                                    //    {
                                    //        docStatusProcess.DocId = docId;
                                    //        this.docStatusProcessService.Insert(docStatusProcess);
                                    //    }
                                    //    ---------------------------------------

                                    //}
                                }
                                // --------------------------------------------------------------------------------------------------------

                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Data of document master list file is valid. System import successfull!";
                            }
                            else
                            {
                                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                    "CloseAndRebind();", true);
                            }
                        }
                        else
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text =
                                "Have Error: Project '" + projectObj.Name + "' don't have Document Type '" +
                                currentSheetName + "'. Please remove this sheet out of master file.";
                        }
                    }
                    else
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text =
                            "Master list file is invalid. Can't find the project to import!";

                    }
                }
            }

            //}
            //catch (Exception ex)
            //{
            //    this.blockError.Visible = true;
            //    this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            //}

        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}