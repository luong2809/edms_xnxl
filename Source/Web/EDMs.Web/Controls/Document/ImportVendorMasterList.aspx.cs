﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.IO;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportVendorMasterList : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DocumentNewService documentNewService;

        private readonly CategoryService categoryService;

        private readonly OptionalTypeDetailService optionalTypeDetailService;

        private readonly OriginatorService originatorService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly DepartmentService departmentService;
        private readonly PlatformService platformService;
        private readonly VendorPackageService vendorpackageService;
        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly UserDataPermissionService userDataPermissionService;
        private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportVendorMasterList()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.languageService = new LanguageService();
            this.documentService = new DocumentService();
            this.documentNewService = new DocumentNewService();
            this.categoryService = new CategoryService();
            this.optionalTypeDetailService = new OptionalTypeDetailService();
            this.originatorService = new OriginatorService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
            this.vendorpackageService = new VendorPackageService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.attachFilesPackageService = new AttachFilesPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var existDocumentPackageList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var error="";
                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var worksheet = workbook.Worksheets[i];

                            var projectId = worksheet.Cells["A2"].Value.ToString();
                            var projectObj = this.scopeProjectService.GetById(!string.IsNullOrEmpty(projectId)
                                            ? Convert.ToInt32(projectId)
                                            : 0);
                          
                            var workgroupId = worksheet.Cells["A1"].Value;

                           
                            currentSheetName = worksheet.Name;
                            currentDocumentNo = string.Empty;
                            if (projectObj != null)
                            {
                                var vendorpackage = this.vendorpackageService.GetById(Convert.ToInt32(workgroupId));
                              //  var disciplineObj = this.disciplineService.GetById(disciplineId);
                                if (vendorpackage != null)
                                {
                                    var dataTable = worksheet.Cells.ExportDataTable(3, 1, worksheet.Cells.MaxRow - 1, 8);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                        {
                                            currentDocumentNo = dataRow["Column1"].ToString();
                                           
                                            var existDocObj = this.documentPackageService.GetOneByDocNo(currentDocumentNo, projectObj.ID);
                                            

                                            var revObj = this.revisionService.GetByName(dataRow["Column3"].ToString());
                                            var docTypeObj = this.documentTypeService.GetByName(dataRow["Column5"].ToString(), projectObj.ID);
                                            var discipline = this.disciplineService.GetByName(dataRow["Column4"].ToString(),projectObj.ID);

                                            var existdocrev = this.documentPackageService.GetOneByDocNo(currentDocumentNo, revObj.Name, projectObj.ID);
                                            if (existdocrev == null)
                                            {

                                                var docObj = new DocumentPackage();
                                                docObj.ProjectId = projectObj.ID;
                                                docObj.ProjectName = projectObj.Name;

                                                docObj.DocNo = dataRow["Column1"].ToString();
                                                docObj.DocTitle = dataRow["Column2"].ToString();
                                                docObj.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                docObj.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                docObj.CurrenCodeName = dataRow["Column6"].ToString();
                                                docObj.CurrentCodeID = 0;

                                                docObj.StatusID = 0;
                                                docObj.StatusName = dataRow["Column7"].ToString();

                                                docObj.ReferenceFromID = 0;
                                                docObj.ReferenceFromName = string.Empty;

                                                docObj.DisciplineId = discipline != null ?discipline.ID:0;
                                                docObj.DisciplineName =discipline!= null? discipline.Name: string.Empty;

                                                docObj.DeparmentId =  0;
                                                docObj.DeparmentName =  string.Empty;
                                             

                                                docObj.Weight = 0.0;

                                                docObj.IsCriticalDoc = false;
                                                docObj.IsPriorityDoc = false;
                                                docObj.IsVendorDoc = true;

                                                docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                                docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                docObj.TypeId = 3;

                                                var issue = dataRow["Column8"].ToString();
                                                DateTime issueDate = new DateTime();
                                                if (Utility.ConvertStringToDateTime(issue, ref issueDate))
                                                {
                                                    docObj.IssuePlanDate = issueDate;
                                                }
                                               
                                                //docObj.TransInFromVendorNo = dataRow["Column9"].ToString();
                                                //var fvno = dataRow["Column10"].ToString();
                                                //DateTime fvnodate = new DateTime();
                                                //if (Utility.ConvertStringToDateTime(fvno, ref fvnodate))
                                                //{
                                                //    docObj.TransInFromVendorDate = fvnodate;
                                                //}
                                                //var vdrespone = dataRow["Column11"].ToString();
                                                //DateTime vdresponsedate = new DateTime();
                                                //if (Utility.ConvertStringToDateTime(vdrespone, ref vdresponsedate))
                                                //{
                                                //    docObj.VendorResponseDate = vdresponsedate;
                                                //}


                                                docObj.DocPath = vendorpackage.VendorPath;
                                                docObj.CreatedBy = UserSession.Current.User.Id;
                                                docObj.CreatedDate = DateTime.Now;
                                                docObj.IsLeaf = true;
                                                docObj.IsDelete = false;
                                                docObj.VendorPackagesId = vendorpackage.ID;
                                                docObj.VendorPackagesName = vendorpackage.Name;
                                                docObj.IsEMDR = true;
                                                docObj.IsDistributed = false;

                                                if (existDocObj != null)
                                                {
                                                    docObj.ParentId = existDocObj.ParentId ?? existDocObj.ID;
                                                    existDocObj.IsLeaf = false;
                                                    existDocObj.UpdatedBy = UserSession.Current.User.Id;
                                                    existDocObj.UpdatedDate = DateTime.Now;
                                                    existDocumentPackageList.Add(existDocObj);
                                                }

                                                documentPackageList.Add(docObj);
                                            }
                                            else
                                            {
                                                error += currentDocumentNo + "</br>";
                                            }

                                        }
                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        foreach (var docObj in documentPackageList)
                                        {
                                            

                                            var docId = this.documentPackageService.Insert(docObj);

                                            //if (!string.IsNullOrEmpty(docObj.TransInFromVendorNo))
                                            //{
                                            //    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromVendorNo);
                                            //    if (transObj != null)
                                            //    {
                                            //        var docTrans = new AttachDocToTransmittal()
                                            //        {
                                            //            DocumentId = docObj.ID,
                                            //            TransmittalId = transObj.ID,
                                            //        };

                                            //        this.attachDocToTransmittalService.Insert(docTrans);
                                            //        try
                                            //        {
                                            //            // Auto create transmittal folder on Document library and share document files
                                            //            var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                            //            foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                            //            {
                                            //                // Path file to save on server disc
                                            //                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                            //                // Path file to download from server
                                            //                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                            //                var fileExt = attachFile.Extension;

                                            //                if (!File.Exists(saveFilePath))
                                            //                {
                                            //                    var document = new Data.Entities.Document()
                                            //                    {
                                            //                        Name = attachFile.FileName,
                                            //                        FileExtension = fileExt,
                                            //                        FileExtensionIcon = attachFile.ExtensionIcon,
                                            //                        FilePath = serverFilePath,
                                            //                        FolderID = mainTransFolder.ID,
                                            //                        IsLeaf = true,
                                            //                        IsDelete = false,
                                            //                        CreatedBy = UserSession.Current.User.Id,
                                            //                        CreatedDate = DateTime.Now
                                            //                    };
                                            //                    this.documentService.Insert(document);
                                            //                }

                                            //                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                            //                {
                                            //                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                            //                }
                                            //            }
                                            //        }
                                            //        catch { }
                                            //    }
                                            //}
                                         
                                        }

                                        foreach (var docObj in existDocumentPackageList)
                                        {
                                            this.documentPackageService.Update(docObj);
                                        }

                                        if (string.IsNullOrEmpty(error))
                                        {
                                            this.blockError.Visible = true;
                                            this.lblError.Text =
                                                "Data of document master list file is valid. System import successfull!";
                                        }
                                        else
                                        {
                                            this.blockError.Visible = true;
                                            this.lblError.Text = "Have Error: Documents < " + error + " > really existed!";
                                        }
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                }
                                else
                                {
                                    this.blockError.Visible = true;
                                    this.lblError.Text =
                                        "Have Error: Project '" + projectObj.Name + "' don't have Document Type '" +
                                        currentSheetName + "'. Please remove this sheet out of master file.";
                                }
                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }
                    }
                }
           }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}