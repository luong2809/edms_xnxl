﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Web.UI;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.Web.UI.WebControls;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AutoDistributionMatrixConfigForm : Page
    {
        private readonly RoleService roleService;

        private readonly UserService userService;

        private readonly ProjectDepartmentService projectDepartmentService;

        private readonly AutoDistributionMatriceService autoDistributionMatriceService;

        private readonly ScopeProjectService projectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AutoDistributionMatrixConfigForm()
        {
            this.roleService = new RoleService();
            this.projectDepartmentService = new ProjectDepartmentService();
            this.userService = new UserService();
            this.autoDistributionMatriceService = new AutoDistributionMatriceService();
            this.projectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.LoadComboData();
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
            var projectObj = this.projectService.GetById(projectId);
            var departmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue);
            foreach (var userItem in this.lbUser.CheckedItems)
            {
                var item = new AutoDistributionMatrix()
                {
                    ProjectId = projectId,
                    ProjectName = projectObj.FullName,
                    RoleId = departmentId,
                    RoleName = this.ddlDepartment.SelectedItem.Text,
                    UserId = Convert.ToInt32(userItem.Value),
                    UserFullName = userItem.Text,
                    CreatedBy = UserSession.Current.UserId,
                    CreatedDate = DateTime.Now
                };

                this.autoDistributionMatriceService.Insert(item);
            }
            this.LoadUser(projectObj, departmentId);
            this.grdPermission.Rebind();
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        private void LoadComboData()
        {
            var departmentList = this.roleService.GetAll(true);
            var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
            var projectObj = this.projectService.GetById(projectId);
            //var departmentExistList = this.projectDepartmentService.GetAllByProject(projectId).Select(t => t.DepartmentId.GetValueOrDefault()).ToList();
            this.ddlDepartment.DataSource = departmentList;//.Where(t => !departmentExistList.Contains(t.Id));
            this.ddlDepartment.DataValueField = "Id";
            this.ddlDepartment.DataTextField = "FullName";
            this.ddlDepartment.DataBind();

            if (this.ddlDepartment.SelectedItem != null)
            {
                var departmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue);
                this.LoadUser(projectObj, departmentId);
            }
        }

        private void LoadUser(ScopeProject projectObj, int departmentId)
        {
            var userList = departmentId == 1 ? this.userService.GetAll() : this.userService.GetAllByRoleId(departmentId);
            var userExistList = this.autoDistributionMatriceService.GetAllByProject(projectObj.ID).Select(t => t.UserId.GetValueOrDefault()).ToList();

            this.lbUser.DataSource = userList.Where(t => !userExistList.Contains(t.Id)).OrderBy(t => t.FullNameWithRoleDepartment);
            this.lbUser.DataValueField = "Id";
            this.lbUser.DataTextField = "FullNameWithRoleDepartment";
            this.lbUser.DataBind();
        }

        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var porjectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var userList = this.autoDistributionMatriceService.GetAllByProject(porjectId);
                this.grdPermission.DataSource = userList;
            }
        }

        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var itemId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.autoDistributionMatriceService.Delete(itemId);

            this.LoadComboData();
        }

        protected void ddlDepartment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlDepartment.SelectedItem != null && !string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var projectObj = this.projectService.GetById(projectId);
                var departmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue);

                this.LoadUser(projectObj, departmentId);
            }
        }
    }
}