﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using OfficeHelper.Utilities.Data;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportSpecialTranInAttachDoc : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly IncomingTransmittalService transmittalService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The group data permission service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly DocumentPackageService documentPackageService;

        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly FolderService folderService;

        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly TemplateManagementService templateManagementService;

        private readonly DocRoleMatrixService docRoleMatrixService;

        private readonly DistributionMatrixDetailByUserService dmDetailByUserService;

        private readonly AttachFilesCommentService attachFilesCommentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportSpecialTranInAttachDoc()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.receivedFromService = new ReceivedFromService();
            this.transmittalService = new IncomingTransmittalService();
            this.categoryService = new CategoryService();
            this.userService = new UserService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.workGroupService = new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.folderService = new FolderService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.templateManagementService = new TemplateManagementService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.docRoleMatrixService = new DocRoleMatrixService();
            this.dmDetailByUserService = new DistributionMatrixDetailByUserService();
            this.attachFilesCommentService = new AttachFilesCommentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);
                        var workbook = new Workbook();
                        workbook.Open(importPath);

                        var datasheet = workbook.Worksheets[0];
                        if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                        {
                            var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                            var objTran = this.transmittalService.GetById(tranId);
                            var listSelectedDocId = new List<int>();
                            var listAttachFilePackage = new List<AttachFilesPackage>();
                            var listAttachFileComment = new List<AttachFilesComment>();
                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                                    ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                                    : 0;
                            if (objTran != null)
                            {
                                var fromUser = this.userService.GetByID(objTran.FromId.GetValueOrDefault());
                                var toUser = this.userService.GetByID(objTran.ToId.GetValueOrDefault());
                                var dataTable = datasheet.Cells.ExportDataTable(2, 1, datasheet.Cells.MaxRow,
                                    4);
                                var docInProject = this.documentPackageService.GetAllOfProject(projectID, 1);
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()))
                                    {
                                        var docObj = docInProject.FirstOrDefault(t => t.DocNo == dataRow["Column1"].ToString() && t.RevisionName == dataRow["Column2"].ToString());
                                        if (docObj != null)
                                        {
                                            //var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                                            listSelectedDocId.Add(docObj.ID);

                                            var attachDoc = new AttachDocToTransmittal()
                                            {
                                                TransmittalId = tranId,
                                                DocumentId = docObj.ID
                                            };
                                            if (!this.attachDocToTransmittalService.IsExist(tranId, docObj.ID))
                                            {
                                                this.attachDocToTransmittalService.Insert(attachDoc);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        this.blockError.Visible = true;
                                        this.lblError.Text = "Docno trống";
                                    }
                                }

                                var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                                if (objTran.TypeInTrans.GetValueOrDefault() == 1)
                                {
                                    foreach (var item in attachDocList)
                                    {
                                        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                        if (docObj != null)
                                        {
                                            docObj.IncomingTransNo = objTran.TransNumber;
                                            docObj.IncomingTransDate = objTran.ReceivedDate;
                                            this.documentPackageService.Update(docObj);
                                        }
                                    }
                                }
                                else if (objTran.TypeInTrans.GetValueOrDefault() == 2)
                                {
                                    foreach (var item in attachDocList)
                                    {
                                        var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                        if (docObj != null)
                                        {
                                            docObj.IncomingHardCopyTransNo = objTran.TransNumber;
                                            docObj.IncomingHardCopyTransDate = objTran.ReceivedDate;
                                            this.documentPackageService.Update(docObj);
                                        }
                                    }
                                }
                                if (objTran.TypeId == 1)
                                {
                                    // Auto create transmittal folder on Document library and share document files
                                    var mainTransFolder = this.folderService.GetById(objTran.FolderId.GetValueOrDefault());

                                    if (objTran.ProjectId == 51 || objTran.ProjectId == 54)
                                    {
                                        foreach (var docId in listSelectedDocId)
                                        {
                                            listAttachFileComment.AddRange(
                                                this.attachFilesCommentService.GetAllByDocId(docId));
                                        }

                                        foreach (var attachFile in listAttachFileComment)
                                        {
                                            if (mainTransFolder != null && !string.IsNullOrEmpty(mainTransFolder.DirName))
                                            {
                                                // Path file to save on server disc
                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                                // Path file to download from server
                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                var fileExt = attachFile.Extension;

                                                if (!File.Exists(saveFilePath))
                                                {
                                                    var document = new Data.Entities.Document()
                                                    {
                                                        Name = attachFile.FileName,
                                                        FileExtension = fileExt,
                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                        FilePath = serverFilePath,
                                                        FolderID = mainTransFolder.ID,
                                                        IsLeaf = true,
                                                        IsDelete = false,
                                                        CreatedBy = UserSession.Current.User.Id,
                                                        CreatedDate = DateTime.Now
                                                    };
                                                    this.documentService.Insert(document);
                                                }

                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                {
                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var docId in listSelectedDocId)
                                        {
                                            listAttachFilePackage.AddRange(
                                                this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                                        }

                                        foreach (var attachFile in listAttachFilePackage)
                                        {
                                            if (mainTransFolder != null && !string.IsNullOrEmpty(mainTransFolder.DirName))
                                            {
                                                // Path file to save on server disc
                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                                // Path file to download from server
                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                var fileExt = attachFile.Extension;

                                                if (!File.Exists(saveFilePath))
                                                {
                                                    var document = new Data.Entities.Document()
                                                    {
                                                        Name = attachFile.FileName,
                                                        FileExtension = fileExt,
                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                        FilePath = serverFilePath,
                                                        FolderID = mainTransFolder.ID,
                                                        IsLeaf = true,
                                                        IsDelete = false,
                                                        CreatedBy = UserSession.Current.User.Id,
                                                        CreatedDate = DateTime.Now
                                                    };
                                                    this.documentService.Insert(document);
                                                }

                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                {
                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (objTran.TypeId == 2)
                                {
                                    if (projectID == 51)
                                    {
                                        foreach (var item in attachDocList)
                                        {
                                            var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                            if (docObj != null)
                                            {
                                                docObj.OutgoingTransNo = objTran.TransNumber;
                                                docObj.OutgoingTransDate = objTran.ReceivedDate;
                                                this.documentPackageService.Update(docObj);
                                            }
                                        }
                                        // Auto create transmittal folder on Document library and share document files
                                        var mainTransFolder = this.folderService.GetById(objTran.FolderId.GetValueOrDefault());

                                        foreach (var docId in listSelectedDocId)
                                        {
                                            listAttachFilePackage.AddRange(
                                                this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                                        }

                                        foreach (var attachFile in listAttachFilePackage)
                                        {
                                            if (mainTransFolder != null && !string.IsNullOrEmpty(mainTransFolder.DirName))
                                            {
                                                // Path file to save on server disc
                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                                // Path file to download from server
                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                var fileExt = attachFile.Extension;

                                                if (!File.Exists(saveFilePath))
                                                {
                                                    var document = new Data.Entities.Document()
                                                    {
                                                        Name = attachFile.FileName,
                                                        FileExtension = fileExt,
                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                        FilePath = serverFilePath,
                                                        FolderID = mainTransFolder.ID,
                                                        IsLeaf = true,
                                                        IsDelete = false,
                                                        CreatedBy = UserSession.Current.User.Id,
                                                        CreatedDate = DateTime.Now
                                                    };
                                                    this.documentService.Insert(document);
                                                }

                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                {
                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}