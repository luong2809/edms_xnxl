﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Business.Services.Library;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Text.RegularExpressions;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using System.Xml.Linq;
    using EDMs.Web.Utilities;
    using System.Drawing;
    using Telerik.Web.Zip;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AttachMarkupFile : Page
    {

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        private readonly MarkupFileService markupFileService;

        private readonly AttachFilesPackageService attachFileService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AttachMarkupFile()
        {
            this.documentPackageService = new DocumentPackageService();
            this.markupFileService = new MarkupFileService();
            this.attachFileService = new AttachFilesPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //this.btnConsolidate.Visible = Convert.ToBoolean(this.Request.QueryString["canconsolidate"]);
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Session.Remove("IsFillData");
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                var docObj = this.documentPackageService.GetById(docId);
                var flag = false;
                const string TargetFolder = "../../DocumentLibrary/MarkupFiles";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/MarkupFiles";
                var listUpload = docuploader.UploadedFiles;

                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        
                        var docFileName = docFile.GetNameWithoutExtension();

                        var serverDocFileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFileName + "_" + UserSession.Current.User.Username + docFile.GetExtension();

                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(TargetFolder), serverDocFileName);

                        // Path file to download from server
                        var serverFilePath = serverFolder + "/" + serverDocFileName;

                        docFile.SaveAs(saveFilePath, true);
                        var contentFile = File.ReadAllText(saveFilePath);
                        contentFile = contentFile.Replace("xmlns=\"http://www.infograph.com\"", "");
                        contentFile = Regex.Replace(contentFile, "Author name[^\\w\\*]\"\\w+\"", "Author name=\"" + UserSession.Current.User.FullName + "\"");
                        File.WriteAllText(saveFilePath, contentFile);

                        var docAssignUserId = Convert.ToInt32(this.Request.QueryString["docAssignUserId"]);
                        var attachFile = new MarkupFile()
                            {
                                DocId = docId,
                                FileName = docFileName + "_" + UserSession.Current.User.Username,
                                FilePath = serverFilePath,
                                ExtentionIcon = "~/images/comment.png",
                                FileSize = (double)docFile.ContentLength / 1024,

                                MarkupBy = UserSession.Current.User.Id,
                                MarkupDate = DateTime.Now,
                                MarkupByName = UserSession.Current.User.UserNameWithFullName,
                                IsConsolidateFile = false,
                                OriginalFileName = docObj.DocNo,
                                DocAssignUserId = docAssignUserId,
                            };

                        this.markupFileService.Insert(attachFile);
                    }
                }
            }

            this.docuploader.UploadedFiles.Clear();

            this.grdDocument.Rebind();
        }

        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            this.markupFileService.Delete(docId);
            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["docId"]);
                var document = this.documentPackageService.GetById(docId);
                //var attachList = Convert.ToBoolean(this.Request.QueryString["canconsolidate"])
                //? this.markupFileService.GetAllByDocId(docId)
                //: this.markupFileService.GetAllByDocIdAndUser(docId, UserSession.Current.User.Id);
                var attachList = this.markupFileService.GetAllByDocId(docId);
                foreach (var markupFile in attachList)
                {
                    markupFile.CanDelete = markupFile.MarkupBy == UserSession.Current.UserId;
                }
                this.grdDocument.DataSource = attachList;
            }
            else
            {
                this.grdDocument.DataSource = new List<MarkupFile>();
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("Delete"))
            {
                var dmId = Convert.ToInt32(e.Argument.Split('_')[1]);
                this.markupFileService.Delete(dmId);

                this.grdDocument.Rebind();

            }
        }

        public void setGrdRadioButtonOnClick()
        {
            int i;
            RadioButton radioButton;
            for (i = 0; i < grdDocument.Items.Count; i++)
            {

                radioButton = (RadioButton)grdDocument.Items[i].FindControl("rdSelect");

                radioButton.Attributes.Add("OnClick", "SelectMeOnly(" + radioButton.ClientID + ", " + "'grdDocument'" + ")");
            }
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            //((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;

            //var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
            //var attachFileId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            //var attachFileObj = this.attachFileService.GetById(attachFileId);
            //if (attachFileObj != null)
            //{
            //    var attachFiles = this.attachFileService.GetAllByDocId(attachFileObj.DocumentId.GetValueOrDefault());
            //    foreach (var attachFile in attachFiles)
            //    {
            //        attachFile.IsDefault = attachFile.ID == attachFileId;
            //        this.attachFileService.Update(attachFile);
            //    }
            //}
        }

        protected void btnConsolidate_Click(object sender, EventArgs e)
        {
            var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
            this.ConsolidateDocument(docId);
        }

        private void ConsolidateDocument(int docId)
        {
            var docObj = this.documentPackageService.GetById(docId);
            var markupFileList = this.markupFileService.GetAllByDocId(docId);

            if (markupFileList.Count > 0)
            {
                var fileList = markupFileList.Select(t => new FileInfo(Server.MapPath(t.FilePath))).ToList();
                var desFile = XDocument.Load(fileList[0].FullName);

                for (int i = 1; i < fileList.Count(); i++)
                {
                    var sourceFile = XDocument.Load(fileList[i].FullName);
                    Utility.XmlMultiMerge(ref desFile, sourceFile);
                }

                const string TargetFolder = "../../DocumentLibrary/MarkupFiles";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/MarkupFiles";

                var docFileName = markupFileList[0].OriginalFileName;
                var serverDocFileName = DateTime.Now.ToBinary() + "_" + docFileName + "_Consolidated_" + UserSession.Current.User.Username + ".xrl";

                // Path file to save on server disc
                var saveFilePath = Path.Combine(Server.MapPath(TargetFolder), serverDocFileName);

                // Path file to download from server
                var serverFilePath = serverFolder + "/" + serverDocFileName;

                desFile.Save(saveFilePath);
                var temp = new FileInfo(saveFilePath);

                var attachFile = new MarkupFile()
                {
                    DocId = docId,
                    FileName = docFileName + "_Consolidated_" + UserSession.Current.User.Username,
                    FilePath = serverFilePath,
                    ExtentionIcon = "~/images/comment.png",
                    FileSize = (double)temp.Length / 1024,

                    MarkupBy = UserSession.Current.User.Id,
                    MarkupDate = DateTime.Now,
                    MarkupByName = UserSession.Current.User.UserNameWithFullName,
                    IsConsolidateFile = true,
                    OriginalFileName = docObj.DocNo
                };

                this.markupFileService.Insert(attachFile);
            }

        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["IsConsolidateFile"].Text == "True")
                {
                    item.BackColor = Color.Green;
                    item.BorderColor = Color.Green;
                }

                if (item["IsRejectFile"].Text == "True")
                {
                    item.BackColor = Color.IndianRed;
                    item.BorderColor = Color.IndianRed;
                }
            }
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
            var docObj = this.documentPackageService.GetById(docId);
            var attachFileList = this.attachFileService.GetAllDocId(docId);

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.zip");
            var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);

            var markupFileList = this.markupFileService.GetAllByDocId(docId);

            foreach (var markupFile in markupFileList)
            {
                if (File.Exists(Server.MapPath(markupFile.FilePath)))
                {
                    docPack.Add(Server.MapPath(markupFile.FilePath));
                }
            }

            foreach (var attachFile in attachFileList)
            {
                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                {
                    docPack.Add(Server.MapPath(attachFile.FilePath));
                }
            }

            this.DownloadByWriteByte(serverTotalDocPackPath, docObj.DocNo + ".zip", true);
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.WriteFile(strFileName);

                //if (DeleteOriginalFile)
                //{
                //    File.SetAttributes(strFileName, FileAttributes.Normal);
                //    File.Delete(strFileName);
                //}
                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}