﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageInfoEditFormCutting : Page
    {

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;



        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService;

        private readonly FolderService folderService;

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly RoleService roleService;

        private readonly AttachFilesPackageService attachFilePackageService;

        private readonly StatusService statusService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;




        private readonly AreaService areaService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentPackageInfoEditFormCutting()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
            this.documentPackageService = new DocumentPackageService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.userService = new UserService();
            this.roleService = new RoleService();
            this.statusService = new StatusService();
            this.folderService = new FolderService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.attachFilePackageService = new AttachFilesPackageService();

            this.areaService = new AreaService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////this.LoadComboData();
                if (!UserSession.Current.User.Role.IsAdmin.GetValueOrDefault() && !UserSession.Current.User.IsDC.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["projId"]))
                {
                    var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                    var projObj = this.scopeProjectService.GetById(projectId);
                    this.txtProjectName.Text = projObj.Name;
                    this.LoadComboData(projectId);
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.CreatedInfo.Visible = true;
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        this.LoadComboData(docObj.ProjectId.GetValueOrDefault());
                        this.txtProjectName.Text = docObj.ProjectName;
                        this.LoadDocInfo(docObj);

                        this.txtDocNumber.ReadOnly = true;

                        var createdUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + docObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (docObj.UpdatedBy != null && docObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(docObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + docObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (this.Page.IsValid && !string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var projObj = this.scopeProjectService.GetById(projectId);
                DocumentPackage docObj;
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        var oldRevision = docObj.RevisionId;
                        var newRevision = Convert.ToInt32(this.ddlRevision.SelectedValue);
                        if (newRevision > oldRevision)
                        {
                            var docObjNew = new DocumentPackage();
                            this.CollectData(ref docObjNew, projObj);

                            // Insert new doc
                            docObjNew.CreatedBy = UserSession.Current.User.Id;
                            docObjNew.CreatedDate = DateTime.Now;
                            docObjNew.IsLeaf = true;
                            docObjNew.IsDelete = false;
                            docObjNew.IsEMDR = true;
                            docObjNew.ParentId = docObj.ParentId ?? docObj.ID;
                            docObjNew.TypeId = docObj.TypeId;
                            docObjNew.DocPath = docObj.DocPath;

                            this.documentPackageService.Insert(docObjNew);
                            //}
                            // Upate old doc
                            docObj.IsLeaf = false;

                            // Delete attach file of old Document
                            // var attachDocList = this.attachFilePackageService.GetAllDocId(docObj.ID);
                            //foreach (var attachDoc in attachDocList)
                            //{
                            //    var latestAttachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));

                            //    if (File.Exists(latestAttachFilePath))
                            //    {
                            //        File.Delete(latestAttachFilePath);
                            //    }
                            //}
                        }
                        else
                        {
                            this.CollectData(ref docObj, projObj);
                        }
                        
                        docObj.UpdatedBy = UserSession.Current.User.Id;
                        docObj.UpdatedDate = DateTime.Now;
                        this.documentPackageService.Update(docObj);

                        if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                if (!this.attachDocToTransmittalService.IsExist(transObj.ID, docId))
                                {
                                    this.attachDocToTransmittalService.Insert(docTrans);

                                    try
                                    {
                                        // Auto create transmittal folder on Document library and share document files
                                        var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                        foreach (var attachFile in this.attachFilePackageService.GetAllDocumentFileByDocId(docObj.ID))
                                        {
                                            // Path file to save on server disc
                                            var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                            // Path file to download from server
                                            var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                            var fileExt = attachFile.Extension;

                                            if (!File.Exists(saveFilePath))
                                            {
                                                var document = new Data.Entities.Document()
                                                {
                                                    Name = attachFile.FileName,
                                                    FileExtension = fileExt,
                                                    FileExtensionIcon = attachFile.ExtensionIcon,
                                                    FilePath = serverFilePath,
                                                    FolderID = mainTransFolder.ID,
                                                    IsLeaf = true,
                                                    IsDelete = false,
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    CreatedDate = DateTime.Now
                                                };
                                                this.documentService.Insert(document);
                                            }

                                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                            {
                                                File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                            }
                                        }

                                    }
                                    catch { }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(docObj.OutgoingTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.OutgoingTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                if (!this.attachDocToTransmittalService.IsExist(transObj.ID, docId))
                                {
                                    this.attachDocToTransmittalService.Insert(docTrans);
                                    try
                                    {
                                        // Auto create transmittal folder on Document library and share document files
                                        var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                        foreach (var attachFile in this.attachFilePackageService.GetAllDocumentFileByDocId(docObj.ID))
                                        {
                                            // Path file to save on server disc
                                            var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                            // Path file to download from server
                                            var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                            var fileExt = attachFile.Extension;

                                            if (!File.Exists(saveFilePath))
                                            {
                                                var document = new Data.Entities.Document()
                                                {
                                                    Name = attachFile.FileName,
                                                    FileExtension = fileExt,
                                                    FileExtensionIcon = attachFile.ExtensionIcon,
                                                    FilePath = serverFilePath,
                                                    FolderID = mainTransFolder.ID,
                                                    IsLeaf = true,
                                                    IsDelete = false,
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    CreatedDate = DateTime.Now
                                                };
                                                this.documentService.Insert(document);
                                            }

                                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                            {
                                                File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                            }
                                        }

                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                }
                else
                {
                    docObj = new DocumentPackage()
                    {
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        IsLeaf = true,
                        IsDelete = false,
                        IsDistributed = false,
                        IsEMDR = true,
                        //TypeId=3
                    };

                    this.CollectData(ref docObj, projObj);

                    docObj.TypeId = 4;

                    var docId = this.documentPackageService.Insert(docObj);
                    if (docId != null)
                    {


                        if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                if (!this.attachDocToTransmittalService.IsExist(transObj.ID, docObj.ID))
                                {
                                    this.attachDocToTransmittalService.Insert(docTrans);
                                    try
                                    {
                                        // Auto create transmittal folder on Document library and share document files
                                        var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                        foreach (var attachFile in this.attachFilePackageService.GetAllDocumentFileByDocId(docObj.ID))
                                        {
                                            // Path file to save on server disc
                                            var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                            // Path file to download from server
                                            var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                            var fileExt = attachFile.Extension;

                                            if (!File.Exists(saveFilePath))
                                            {
                                                var document = new Data.Entities.Document()
                                                {
                                                    Name = attachFile.FileName,
                                                    FileExtension = fileExt,
                                                    FileExtensionIcon = attachFile.ExtensionIcon,
                                                    FilePath = serverFilePath,
                                                    FolderID = mainTransFolder.ID,
                                                    IsLeaf = true,
                                                    IsDelete = false,
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    CreatedDate = DateTime.Now
                                                };
                                                this.documentService.Insert(document);
                                            }

                                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                            {
                                                File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                            }
                                        }

                                    }
                                    catch { }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(docObj.OutgoingTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.OutgoingTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                if (!this.attachDocToTransmittalService.IsExist(transObj.ID, docObj.ID))
                                {
                                    this.attachDocToTransmittalService.Insert(docTrans);
                                    try
                                    {
                                        // Auto create transmittal folder on Document library and share document files
                                        var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                        foreach (var attachFile in this.attachFilePackageService.GetAllDocumentFileByDocId(docObj.ID))
                                        {
                                            // Path file to save on server disc
                                            var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName);
                                            // Path file to download from server
                                            var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                            var fileExt = attachFile.Extension;

                                            if (!File.Exists(saveFilePath))
                                            {
                                                var document = new Data.Entities.Document()
                                                {
                                                    Name = attachFile.FileName,
                                                    FileExtension = fileExt,
                                                    FileExtensionIcon = attachFile.ExtensionIcon,
                                                    FilePath = serverFilePath,
                                                    FolderID = mainTransFolder.ID,
                                                    IsLeaf = true,
                                                    IsDelete = false,
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    CreatedDate = DateTime.Now
                                                };
                                                this.documentService.Insert(document);
                                            }

                                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                            {
                                                File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                            }
                                        }

                                    }
                                    catch { }
                                }
                            }
                        }
                    }


                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseWindow", "CloseAndRebind();", true);
            }
        }


        //  protected bool CheckDocNo()

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            ////if(this.txtName.Text.Trim().Length == 0)
            ////{
            ////    this.fileNameValidator.ErrorMessage = "Please enter file name.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = false;
            ////}
            ////else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            ////{
            ////    var docId = Convert.ToInt32(Request.QueryString["docId"]);
            ////    this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = true; ////!this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            ////}
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);

                if (this.documentService.IsDocumentExist(folderId, fileName))
                {
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData(int projectId)
        {

            var vendorpackage = this.areaService.GetAllProject(projectId);
            this.ddlAreas.DataSource = vendorpackage.OrderBy(t => t.Name);
            this.ddlAreas.DataTextField = "Name";
            this.ddlAreas.DataValueField = "ID";
            this.ddlAreas.DataBind();

            var listRevision = this.revisionService.GetAll();
            this.ddlRevision.DataSource = listRevision;
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataBind();




        }

        private void CollectData(ref DocumentPackage docObj, ScopeProject projObj)
        {
            docObj.AreaId = this.ddlAreas.SelectedItem != null ? Convert.ToInt32(this.ddlAreas.SelectedValue) : 0;
            docObj.AreaName = this.ddlAreas.SelectedItem != null ? this.ddlAreas.SelectedItem.Text : string.Empty;
            var AreasPath = this.areaService.GetById(docObj.AreaId.GetValueOrDefault());
            if (AreasPath != null)
            {
                docObj.DocPath = AreasPath.AreaPath;
            }
            docObj.ProjectId = projObj.ID;
            docObj.ProjectName = projObj.Name;

            docObj.DocNo = this.txtDocNumber.Text.Trim();
            docObj.DocTitle = this.txtDocumentTitle.Text.Trim();

            docObj.ReferenceFromID = 0;
            docObj.ReferenceFromName = string.Empty;


            docObj.DeparmentId = 0;
            docObj.DeparmentName = string.Empty;

            docObj.Complete = this.txtComplete.Value.GetValueOrDefault();
            docObj.Weight = 0.0;
            docObj.Notes = string.Empty;
            docObj.CommentCode = string.Empty;
            docObj.VendorNameOfCutting = this.txtVendorName.Text;
            docObj.IsCriticalDoc = false;
            docObj.IsPriorityDoc = false;
            docObj.IsVendorDoc = false;
            docObj.AnotherDocNo = string.Empty;
            docObj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            docObj.RevisionName = this.ddlRevision.SelectedItem.Text;

            docObj.OutgoingTransNo = this.txtouttransno.Text;
            docObj.OutgoingTransDate = this.txtouttransdate.SelectedDate;
            docObj.IncomingTransNo = this.txtIncomingNo.Text;
            docObj.IncomingTransDate = this.txtIncomingdate.SelectedDate;
        }

        private void LoadDocInfo(DocumentPackage docObj)
        {
            this.txtDocNumber.Text = docObj.DocNo;

            this.txtDocumentTitle.Text = docObj.DocTitle;

            this.ddlRevision.SelectedValue = docObj.RevisionId.GetValueOrDefault().ToString();
            this.ddlAreas.SelectedValue = docObj.AreaId.GetValueOrDefault().ToString();
            this.txtComplete.Value = docObj.Complete;
            this.txtVendorName.Text = docObj.VendorNameOfCutting;
            this.txtStartDate.SelectedDate = docObj.StartDate;
            this.txtDeadline.SelectedDate = docObj.PlanedDate;

            this.txtIncomingNo.Text = docObj.IncomingTransNo;
            this.txtIncomingdate.SelectedDate = docObj.IncomingTransDate;

            this.txtouttransno.Text = docObj.OutgoingTransNo;
            this.txtouttransdate.SelectedDate = docObj.OutgoingTransDate;
        }

        //protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var statusObj = this.statusService.GetById(Convert.ToInt32(this.ddlStatus.SelectedValue));
        //    if (statusObj != null)
        //    {
        //        this.txtComplete.Value = Math.Round((double) (statusObj.PercentCompleteDefault*this.txtWeight.Value.GetValueOrDefault()/1000), 2);
        //    }
        //}

        protected void ddlRevision_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //this.txtFinalCode.Text = string.Empty;
        }
    }
}