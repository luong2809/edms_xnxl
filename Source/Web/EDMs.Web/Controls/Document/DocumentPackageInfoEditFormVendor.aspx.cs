﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageInfoEditFormVendor : Page
    {

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService;

        

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        
        private readonly RoleService roleService;
        
        private readonly AttachFilesPackageService attachFilePackageService;

        private readonly StatusService statusService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly VendorPackageService vendorpackageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentPackageInfoEditFormVendor()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
            this.documentPackageService = new DocumentPackageService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.userService = new UserService();
            this.roleService = new RoleService();
            this.statusService = new StatusService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.attachFilePackageService = new AttachFilesPackageService();
            this.vendorpackageService = new VendorPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////this.LoadComboData();
                if (!UserSession.Current.User.Role.IsAdmin.GetValueOrDefault() && !UserSession.Current.User.IsDC.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["projId"]))
                {
                    var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                    var projObj = this.scopeProjectService.GetById(projectId);
                    this.txtProjectName.Text = projObj.Name;
                    this.LoadComboData(projectId);


                    if (!string.IsNullOrEmpty(projObj.organizationComment1))
                    {
                        this.Review1.Visible = true;
                        this.lablereview1.Text = projObj.organizationComment1;
                    }
                    if (!string.IsNullOrEmpty(projObj.organizationComment2))
                    {
                        this.review2.Visible = true;
                        this.lablereview2.Text = projObj.organizationComment2;
                    }
                    if (!string.IsNullOrEmpty(projObj.organizationComment3))
                    {
                        this.review3.Visible = true;
                        this.lablereview3.Text = projObj.organizationComment3;
                    }

                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.CreatedInfo.Visible = true;
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        this.LoadComboData(docObj.ProjectId.GetValueOrDefault());
                        this.txtProjectName.Text = docObj.ProjectName;
                        this.LoadDocInfo(docObj);

                        this.txtDocNumber.ReadOnly = true;

                        var createdUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + docObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (docObj.UpdatedBy != null && docObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(docObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + docObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            
            if (this.Page.IsValid && !string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var projObj = this.scopeProjectService.GetById(projectId);
                DocumentPackage docObj;
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        var oldRevision = docObj.RevisionId;
                        var newRevision = Convert.ToInt32(this.ddlRevision.SelectedValue);
                        if (newRevision > oldRevision)
                        {
                            var docObjNew = new DocumentPackage();
                            this.CollectData(ref docObjNew, projObj);

                            // Insert new doc
                            docObjNew.CreatedBy = UserSession.Current.User.Id;
                            docObjNew.CreatedDate = DateTime.Now;
                            docObjNew.IsLeaf = true;
                            docObjNew.IsDelete = false;
                            docObjNew.IsEMDR = true;
                            docObjNew.ParentId = docObj.ParentId ?? docObj.ID;
                            docObjNew.TypeId = docObj.TypeId;
                            docObjNew.DocPath = docObj.DocPath;
                           
                          
                                 this.documentPackageService.Insert(docObjNew);
//}
                            // Upate old doc
                            docObj.IsLeaf = false;

                            // Delete attach file of old Document
                           // var attachDocList = this.attachFilePackageService.GetAllDocId(docObj.ID);
                            //foreach (var attachDoc in attachDocList)
                            //{
                            //    var latestAttachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));

                            //    if (File.Exists(latestAttachFilePath))
                            //    {
                            //        File.Delete(latestAttachFilePath);
                            //    }
                            //}
                        }
                        else
                        {
                            this.CollectData(ref docObj, projObj);
                        }

                        docObj.UpdatedBy = UserSession.Current.User.Id;
                        docObj.UpdatedDate = DateTime.Now;
                        this.documentPackageService.Update(docObj);

                        if (!string.IsNullOrEmpty(docObj.IncomingHardCopyTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingHardCopyTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                this.attachDocToTransmittalService.Insert(docTrans);
                            }
                        }

                        if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                this.attachDocToTransmittalService.Insert(docTrans);
                            }
                        }
                    }
                }
                else
                {
                    docObj = new DocumentPackage()
                    {
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        IsLeaf = true,
                        IsDelete = false,
                        IsDistributed = false,
                        IsEMDR=true,
                        //TypeId=3
                    };

                    this.CollectData(ref docObj, projObj);

                    docObj.TypeId = 3;

                    var vemdorpath = this.vendorpackageService.GetById(docObj.VendorPackagesId.GetValueOrDefault());
                    if (vemdorpath != null)
                    {
                        docObj.DocPath = vemdorpath.VendorPath;
                    }


                   
                   // var docNOcheck = this.documentPackageService.GetOneByDocNo(docObj.DocNo, docObj.RevisionName, docObj.ProjectId.GetValueOrDefault());
                   
                   //if (docNOcheck == null)
                   //{
                        var docId = this.documentPackageService.Insert(docObj);
                        if (docId != null)
                        {
                            //var validDocNoFolder = Utility.RemoveSpecialCharacterForFolder(docObj.DocNo);
                            //var targetFolder = "../../DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projObj.Name);
                            //var physicalDocNoFolder = Path.Combine(Server.MapPath(targetFolder), validDocNoFolder);

                            //if (!Directory.Exists(physicalDocNoFolder))
                            //{
                            //    Directory.CreateDirectory(physicalDocNoFolder);
                            //}

                            if (!string.IsNullOrEmpty(docObj.TransInFromVendorNo))
                            {
                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromVendorNo);
                                if (transObj != null)
                                {
                                    var docTrans = new AttachDocToTransmittal()
                                    {
                                        DocumentId = docObj.ID,
                                        TransmittalId = transObj.ID,
                                    };

                                    this.attachDocToTransmittalService.Insert(docTrans);
                                }
                            }

                            if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo1))
                            {
                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo1);
                                if (transObj != null)
                                {
                                    var docTrans = new AttachDocToTransmittal()
                                    {
                                        DocumentId = docObj.ID,
                                        TransmittalId = transObj.ID,
                                    };

                                    this.attachDocToTransmittalService.Insert(docTrans);
                                }
                            }
                            if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo2))
                            {
                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo2);
                                if (transObj != null)
                                {
                                    var docTrans = new AttachDocToTransmittal()
                                    {
                                        DocumentId = docObj.ID,
                                        TransmittalId = transObj.ID,
                                    };

                                    this.attachDocToTransmittalService.Insert(docTrans);
                                }
                            }
                            if (!string.IsNullOrEmpty(docObj.TransInFromReviewNo3))
                            {
                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.TransInFromReviewNo3);
                                if (transObj != null)
                                {
                                    var docTrans = new AttachDocToTransmittal()
                                    {
                                        DocumentId = docObj.ID,
                                        TransmittalId = transObj.ID,
                                    };

                                    this.attachDocToTransmittalService.Insert(docTrans);
                                }
                            }
                        }
                    //}
                    //else
                    //{

                    //}
                  
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseWindow", "CloseAndRebind();", true);
            }
        }


      //  protected bool CheckDocNo()

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            ////if(this.txtName.Text.Trim().Length == 0)
            ////{
            ////    this.fileNameValidator.ErrorMessage = "Please enter file name.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = false;
            ////}
            ////else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            ////{
            ////    var docId = Convert.ToInt32(Request.QueryString["docId"]);
            ////    this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = true; ////!this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            ////}
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                
                if(this.documentService.IsDocumentExist(folderId, fileName))
                {
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData(int projectId)
        {
            var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId)
                    .OrderBy(t => t.ID)
                    .ToList();
            disciplineList.Insert(0, new Discipline() {ID = 0});
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();

            var vendorpackage = this.vendorpackageService.GetAllVendorPackageOfProject(projectId);
            this.ddlpackagevendor.DataSource = vendorpackage.OrderBy(t=>t.Name);
            this.ddlpackagevendor.DataTextField = "Name";
            this.ddlpackagevendor.DataValueField = "ID";
            this.ddlpackagevendor.DataBind();

            var listRevision = this.revisionService.GetAll();
            this.ddlRevision.DataSource = listRevision;
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataBind();

            var docTypeList = this.documentTypeService.GetAllByProject(projectId);
            docTypeList.Insert(0, new DocumentType() {ID = 0});
            this.ddlDocType.DataSource = docTypeList;
            this.ddlDocType.DataTextField = "FullName";
            this.ddlDocType.DataValueField = "ID";
            this.ddlDocType.DataBind();

            //var statusList = this.statusService.GetAllByProject(projectId).OrderBy(t => t.PercentCompleteDefault).ToList();
            //statusList.Insert(0, new Status { ID = 0 });
            //this.ddlStatus.DataSource = statusList;
            //this.ddlStatus.DataTextField = "FullNameWithWeight";
            //this.ddlStatus.DataValueField = "ID";
            //this.ddlStatus.DataBind();
        }

        private void CollectData(ref DocumentPackage docObj, ScopeProject projObj)
        {
            docObj.ProjectId = projObj.ID;
            docObj.ProjectName = projObj.Name;

            docObj.DocNo = this.txtDocNumber.Text.Trim();
            docObj.DocTitle = this.txtDocumentTitle.Text.Trim();
            docObj.DocumentTypeId = this.ddlDocType.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDocType.SelectedValue)
                                        : 0;
            docObj.DocumentTypeName = this.ddlDocType.SelectedItem != null
                                          ? this.ddlDocType.SelectedItem.Text
                                          : string.Empty;
            docObj.VendorPackagesId = this.ddlpackagevendor.SelectedItem != null ? Convert.ToInt32(this.ddlpackagevendor.SelectedValue) : 0;
            docObj.VendorPackagesName = this.ddlpackagevendor.SelectedItem != null ? this.ddlpackagevendor.SelectedItem.Text : string.Empty;
            docObj.StatusID =0; //this.ddlStatus.SelectedItem != null
            //                            ? Convert.ToInt32(this.ddlDocType.SelectedValue)
            //                            : 0;
            docObj.StatusName = this.txtStatus.Text;
            docObj.CurrenCodeName = this.txtCurrentCode.Text;
            docObj.IssuePlanDate = this.txtPlanIssue.SelectedDate;


            docObj.ReferenceFromID =  0;
            docObj.ReferenceFromName =  string.Empty;

            docObj.DisciplineId = this.ddlDiscipline.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlDiscipline.SelectedValue)
                                      : 0;
            docObj.DisciplineName = this.ddlDiscipline.SelectedItem != null
                                        ? this.ddlDiscipline.SelectedItem.Text
                                        : string.Empty;
            docObj.DeparmentId = 0;
            docObj.DeparmentName =  string.Empty;

            docObj.Complete = 0.0;
            docObj.Weight = 0.0;
            docObj.Notes = string.Empty;
            docObj.CommentCode = string.Empty ;
            
            docObj.IsCriticalDoc = false;
            docObj.IsPriorityDoc = false;
            docObj.IsVendorDoc = true;
            docObj.AnotherDocNo = string.Empty;
            docObj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            docObj.RevisionName = this.ddlRevision.SelectedItem.Text;


           

                docObj.TransInFromVendorNo = this.txttransinfromvendorno.Text;
                docObj.TransInFromVendorDate = this.txtTransinfromvendordate.SelectedDate;
                docObj.VendorResponseDate = this.txtVendorresponse.SelectedDate;
                docObj.OutgoingTransNo = this.txtouttransno.Text;
                docObj.OutgoingTransDate = this.txtouttransdate.SelectedDate;
                docObj.TransOutToOrganizationNo = this.txttransouttoorganizationno.Text;
                docObj.TransOutToOrganizationDate = this.txttransouttoorganizationdate.SelectedDate;
                docObj.Organization = this.txtorganization.Text;
                docObj.DeadlineReview1 = this.txtdeadlinereview1.SelectedDate;
                docObj.TransInFromReviewNo1 = this.txttransinfromreviewno1.Text;
                docObj.TransInFromReviewDate1 = this.txttransinfromreviewdate1.SelectedDate;
                docObj.StatusReview1 = this.txtstatusreview1.Text;
                docObj.CodeReview1 = this.txtcodereview1.Text;
                docObj.DeadlineReview2 = this.txtdeadlinereview2.SelectedDate;
                docObj.TransInFromReviewNo2 = this.txttransinfromreviewno2.Text;
                docObj.TransInFromReviewDate2 = this.txttransinfromreviewdate2.SelectedDate;
                docObj.StatusReview2 = this.txtstatusreview2.Text;
                docObj.CodeReview2 = this.txtcodereview2.Text;
                docObj.DeadlineReview3 = this.txtdeadlinereview3.SelectedDate;
                docObj.TransInFromReviewNo3 = this.txttransinfromreviewno3.Text;
                docObj.TransInFromReviewDate3 = this.txttransinfromreviewdate3.SelectedDate;
                docObj.StatusReview3 = this.txtstatusreview3.Text;
                docObj.CodeReview3 = this.txtcodereview3.Text;
            
           
           
        }

        private void LoadDocInfo(DocumentPackage docObj)
        {
            this.txtDocNumber.Text = docObj.DocNo;
            this.txtVendorDocNumber.Text = docObj.AnotherDocNo;
            this.txtDocumentTitle.Text = docObj.DocTitle;
            this.ddlDocType.SelectedValue = docObj.DocumentTypeId.GetValueOrDefault().ToString();
            this.ddlDiscipline.SelectedValue = docObj.DisciplineId.GetValueOrDefault().ToString();
            this.ddlRevision.SelectedValue = docObj.RevisionId.GetValueOrDefault().ToString();
            this.ddlpackagevendor.SelectedValue = docObj.VendorPackagesId.GetValueOrDefault().ToString();

            this.txtStatus.Text = docObj.StatusName;
            this.txtCurrentCode.Text = docObj.CurrenCodeName;
            this.txtPlanIssue.SelectedDate = docObj.IssuePlanDate;

            this.txttransinfromvendorno.Text = docObj.TransInFromVendorNo;
            this.txtTransinfromvendordate.SelectedDate = docObj.TransInFromVendorDate;
            this.txtVendorresponse.SelectedDate = docObj.VendorResponseDate;

            this.txtouttransno.Text = docObj.OutgoingTransNo;
            this.txtouttransdate.SelectedDate = docObj.OutgoingTransDate;

            this.txttransouttoorganizationno.Text = docObj.TransOutToOrganizationNo;
            this.txttransouttoorganizationdate.SelectedDate = docObj.TransOutToOrganizationDate;
            this.txtorganization.Text = docObj.Organization;

            this.txtdeadlinereview1.SelectedDate = docObj.DeadlineReview1;
            this.txttransinfromreviewno1.Text = docObj.TransInFromReviewNo1;
            this.txttransinfromreviewdate1.SelectedDate = docObj.TransInFromReviewDate1;
            this.txtstatusreview1.Text = docObj.StatusReview1;
            this.txtcodereview1.Text = docObj.CodeReview1;

            this.txtdeadlinereview2.SelectedDate = docObj.DeadlineReview2;
            this.txttransinfromreviewno2.Text = docObj.TransInFromReviewNo2;
            this.txttransinfromreviewdate2.SelectedDate = docObj.TransInFromReviewDate2;
            this.txtstatusreview2.Text = docObj.StatusReview2;
            this.txtcodereview2.Text = docObj.CodeReview2;

            this.txtdeadlinereview3.SelectedDate = docObj.DeadlineReview3;
            this.txttransinfromreviewno3.Text = docObj.TransInFromReviewNo1;
            this.txttransinfromreviewdate3.SelectedDate = docObj.TransInFromReviewDate3;
            this.txtstatusreview3.Text = docObj.StatusReview3;
            this.txtcodereview3.Text = docObj.CodeReview3;
        }

        //protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var statusObj = this.statusService.GetById(Convert.ToInt32(this.ddlStatus.SelectedValue));
        //    if (statusObj != null)
        //    {
        //        this.txtComplete.Value = Math.Round((double) (statusObj.PercentCompleteDefault*this.txtWeight.Value.GetValueOrDefault()/1000), 2);
        //    }
        //}

        protected void ddlRevision_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //this.txtFinalCode.Text = string.Empty;
        }
    }
}