﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.UI;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UploadMultiEMDRDocumentFile : Page
    {

        private readonly AttachFilesPackageService attachFileService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly DisciplineService disciplineService;
        private readonly VendorPackageService vendorpackageService;

        private readonly DistributionMatrixDetailByUserService dmDetailByUserService;

        private readonly AreaService areaService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public UploadMultiEMDRDocumentFile()
        {
            this.attachFileService = new AttachFilesPackageService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.disciplineService = new DisciplineService();
            this.vendorpackageService = new VendorPackageService();
            this.dmDetailByUserService = new DistributionMatrixDetailByUserService();
            this.areaService = new AreaService();

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    if (!this.IsPostBack)
            //    {
            //        if (!UserSession.Current.IsAdmin &&
            //            !UserSession.Current.IsDC)
            //        {
            //            this.btnSave.Visible = false;
            //            this.UploadControl.Visible = false;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Response.Redirect("~/Controls/System/Login.aspx");
            //}
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //try
            //{
                var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var projectObj = this.scopeProjectService.GetById(projectId);
                var docOfProject = new List<DocumentPackage>();
                if (!string.IsNullOrEmpty(this.Request.QueryString["type"]))
                {
                    var typeId = Convert.ToInt32(this.Request.QueryString["type"]);
                    docOfProject = this.documentPackageService.GetAllOfProject(projectId, typeId);
                }
                else
                {
                    docOfProject = this.documentPackageService.GetAllOfProject(projectId, 1);
                }

                var listUpload = docuploader.UploadedFiles;
                var splitAttachFileNameCharacter = ConfigurationManager.AppSettings.Get("SplitAttachFileNameCharacter");

                var listExistFile = new List<string>();

                var fileIcon = new Dictionary<string, string>()
                {
                    {"doc", "~/images/wordfile.png"},
                    {"docx", "~/images/wordfile.png"},
                    {"dotx", "~/images/wordfile.png"},
                    {"xls", "~/images/excelfile.png"},
                    {"xlsx", "~/images/excelfile.png"},
                    {"pdf", "~/images/pdffile.png"},
                    {"7z", "~/images/7z.png"},
                    {"dwg", "~/images/dwg.png"},
                    {"dxf", "~/images/dxf.png"},
                    {"rar", "~/images/rar.png"},
                    {"zip", "~/images/zip.png"},
                    {"txt", "~/images/txt.png"},
                    {"xml", "~/images/xml.png"},
                    {"xlsm", "~/images/excelfile.png"},
                    {"bmp", "~/images/bmp.png"},
                };
           
            if (listUpload.Count > 0)
                {
                    var uncompleteUploadFileList = new List<string>();

                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileName = docFile.FileName.Replace("&", "-").Replace("+", " ");
                        var serverDocFileName = docFileName;

                        var targetFolder = string.Empty;
                        var docInfo = docFile.FileName.Split(splitAttachFileNameCharacter.ToCharArray());
                        var docNo = "";
                        var docRev = "";
                        if (projectObj.ID == 51 || projectObj.ID == 54)
                        {
                            docNo = docInfo[0].Trim() + "_" + docInfo[1].Trim();
                            char[] listChar = new char[] { ' ' };
                            var revInfo = docInfo[2].Split(listChar, 3);
                            docRev = revInfo[0] + " " + revInfo[1];
                        }
                        else
                        {
                            docNo = docInfo[0].Trim();
                            docRev = docInfo.Any() ? docInfo[1] : string.Empty;
                        }
                        //var docNo = docInfo[0].Replace(" ", string.Empty);
                        //var docRev = docInfo.Any() ? docInfo[1] : string.Empty;
                        var docObj = docOfProject.FirstOrDefault(t => t.DocNo == docNo && t.RevisionName == docRev);

                        if (docObj != null)
                        {
                            var disciplineObj = this.disciplineService.GetById(docObj.DisciplineId.GetValueOrDefault());
                            if (docObj.TypeId.GetValueOrDefault() != 3 && docObj.TypeId.GetValueOrDefault() != 4)
                            {
                                if (docObj.TypeId == 1)
                                {
                                    targetFolder = "../.." + disciplineObj.DisciplinePath;
                                    if (this.rbtnForConstruction.Checked)
                                    {
                                        targetFolder = "../.." +
                                                       disciplineObj.DisciplinePath.Replace("01.All Revision",
                                                           "06.For Construction");
                                    }
                                }
                                else
                                {
                                    targetFolder = "../.." +
                                                   disciplineObj.DisciplinePath.Replace("01.All Revision",
                                                       "03.Shop Drawing/01.All Revision");
                                }

                                var newsaveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                                var saveLatestFilePath =
                                    Path.Combine(
                                        Server.MapPath(targetFolder.Replace("01.All Revision", "02.Latest Revision")),
                                        serverDocFileName);

                                // Path file to download from server
                                var serverFilePath = docObj.DocPath + "/" + serverDocFileName;
                                if (this.rbtnForConstruction.Checked && docObj.TypeId.GetValueOrDefault() == 1)
                                {
                                    serverFilePath = docObj.DocPath.Replace("01.All Revision",
                                                           "06.For Construction") + "/" + serverDocFileName;
                                }

                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                    docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                if (Directory.Exists(Server.MapPath(targetFolder)))
                                {
                                    // Copy file to server
                                    docFile.SaveAs(newsaveFilePath, true);
                                    if (!this.rbtnForConstruction.Checked)
                                    {
                                        File.Copy(newsaveFilePath, saveLatestFilePath, true);
                                        // ----------------------------------------------------------
                                    }
                                    // Create attach info
                                    var attachFile = new AttachFilesPackage()
                                    {
                                        DocumentPackageID = docObj.ID,
                                        FileName = docFileName,
                                        Extension = fileExt,
                                        FilePath = serverFilePath,
                                        ExtensionIcon =
                                            fileIcon.ContainsKey(fileExt.ToLower())
                                                ? fileIcon[fileExt.ToLower()]
                                                : "~/images/otherfile.png",
                                        FileSize = (double)docFile.ContentLength / 1024,

                                        AttachType = 1,
                                        AttachTypeName = "Document file",
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };

                                    this.attachFileService.Insert(attachFile);
                                    // --------------------------------------------------------------------

                                    // Update Distribution status
                                    //var dmDetailByUserListOfDoc = this.dmDetailByUserService.GetAllByDoc(docObj.ID,
                                    //    0);
                                    //foreach (var dmDetailByUser in dmDetailByUserListOfDoc)
                                    //{
                                    //    dmDetailByUser.IsDistributed = true;
                                    //    this.dmDetailByUserService.Update(dmDetailByUser);
                                    //}
                                    // --------------------------------------------------------------------
                                    docObj.HasAttachFile = true;
                                    docObj.Complete = 100;
                                    docObj.UpdatedAttachFile = DateTime.Now;
                                    this.documentPackageService.Update(docObj);

                                    // Delete old docRev on Latest folder
                                    if (docObj.ParentId != null && !this.rbtnForConstruction.Checked)
                                    {
                                        var docRelatedList =
                                            this.documentPackageService.GetAllRevDoc(docObj.ParentId.Value)
                                                .Where(t => t.ID != docObj.ID)
                                                .Select(t => t.ID)
                                                .ToList();
                                        var attachDocList = this.attachFileService.GetAllDocId(docRelatedList);
                                        foreach (var attachDoc in attachDocList)
                                        {
                                            var attachFilePath =
                                                Path.Combine(
                                                    Server.MapPath("../.." +
                                                                   attachDoc.FilePath.Replace("01.All Revision",
                                                                       "02.Latest Revision")));
                                            if (File.Exists(attachFilePath))
                                            {
                                                File.Delete(attachFilePath);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    uncompleteUploadFileList.Add(docFile.FileName);
                                }
                            }
                            else
                            {
                                if (docObj.TypeId.GetValueOrDefault() == 3)
                                {
                                    var vendor = this.vendorpackageService.GetById(docObj.VendorPackagesId.GetValueOrDefault());
                                    targetFolder = "../.." + vendor.VendorPath;
                                }

                                if (docObj.TypeId.GetValueOrDefault() == 4)
                                {
                                    var areaobj = this.areaService.GetById(docObj.AreaId.GetValueOrDefault());
                                    targetFolder = "../.." + areaobj.AreaPath;
                                }

                                var newsaveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                                var saveLatestFilePath = Path.Combine(Server.MapPath(targetFolder),
                                    serverDocFileName);

                                // Path file to download from server
                                var serverFilePath = docObj.DocPath + "/" + serverDocFileName;
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1,
                                    docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                if (Directory.Exists(Server.MapPath(targetFolder)))
                                {
                                    docFile.SaveAs(newsaveFilePath, true);

                                    var attachFile = new AttachFilesPackage()
                                    {
                                        DocumentPackageID = docObj.ID,
                                        FileName = docFileName,
                                        Extension = fileExt,
                                        FilePath = serverFilePath,
                                        ExtensionIcon =
                                            fileIcon.ContainsKey(fileExt.ToLower())
                                                ? fileIcon[fileExt.ToLower()]
                                                : "~/images/otherfile.png",
                                        FileSize = (double)docFile.ContentLength / 1024,

                                        AttachType = 1,
                                        AttachTypeName = "Document file",
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };

                                    this.attachFileService.Insert(attachFile);
                                    docObj.HasAttachFile = true;
                                    docObj.Complete = 100;
                                    docObj.UpdatedAttachFile = DateTime.Now;
                                    this.documentPackageService.Update(docObj);
                                }
                                else
                                {
                                    uncompleteUploadFileList.Add(docFile.FileName);
                                }
                            }
                        }
                        else
                        {
                            uncompleteUploadFileList.Add(docFile.FileName);
                        }
                    }
                    this.docuploader.UploadedFiles.Clear();

                    if (uncompleteUploadFileList.Count > 0)
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = "Can't find some document with Document number: <br/>";
                        foreach (var item in uncompleteUploadFileList)
                        {
                            var docInfo = item.Split(splitAttachFileNameCharacter.ToCharArray());
                            var docNo = "";
                            var docRev = "";
                            if (projectObj.ID == 51)
                            {
                                docNo = docInfo[0].Trim() + "_" + docInfo[1].Trim();
                                char[] listChar = new char[] { ' ' };
                                var revInfo = docInfo[2].Split(listChar, 3);
                                docRev = revInfo[0] + " " + revInfo[1];
                            }
                            else
                            {
                                docNo = docInfo[0].Trim();
                                docRev = docInfo.Any() ? docInfo[1] : string.Empty;
                            }
                            this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" +
                                                  docNo + "' Rev " + docRev +
                                                  "</span> to attach file <span style='color: orange; font-weight: bold'>'" +
                                                  item + "'</span> <br/>";
                        }
                    }
                    else if (listExistFile.Count > 0)
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text += "This folder is have exist files: <br/>";
                        foreach (var item in listExistFile)
                        {
                            this.lblError.Text += "<span style='color: blue; font-weight: bold'>'" + item +
                                                  "'</span> <br/>";
                        }
                    }
                    else
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = "All document files upload complete.";
                    }
                }
                // this.docuploader.UploadedFiles.Clear();


            //}
            //catch (Exception ex)
            //{
            //    this.blockError.Visible = true;
            //    this.lblError.Text = "Have error when upload document file: <br/>'" + ex.Message + "'";
            //}
        }
    }
}