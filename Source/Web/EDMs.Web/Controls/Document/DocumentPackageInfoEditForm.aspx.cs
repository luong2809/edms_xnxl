﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageInfoEditForm : Page
    {

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;



        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService;



        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly RoleService roleService;

        private readonly AttachFilesPackageService attachFilePackageService;

        private readonly StatusService statusService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        // private DistributionMatrixDetailByUserService dmDetailByUserService;

        //    private DocumentStatusProcessService docStatusProcessService;

        private readonly CodeService documentCodeservice;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentPackageInfoEditForm()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.scopeProjectService = new ScopeProjectService();
            this.documentPackageService = new DocumentPackageService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.userService = new UserService();
            this.roleService = new RoleService();
            this.statusService = new StatusService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.attachFilePackageService = new AttachFilesPackageService();
            this.documentCodeservice = new CodeService();
          //  this.dmDetailByUserService = new DistributionMatrixDetailByUserService();
           // this.docStatusProcessService = new DocumentStatusProcessService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////this.LoadComboData();
                if (!UserSession.Current.User.Role.IsAdmin.GetValueOrDefault() && !UserSession.Current.User.Role.IsUpdate.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }
                if (Convert.ToInt32(this.Request.QueryString["type"]) == 2)
                {
                    this.LiReferRenceDoc.Visible = true;
                }
                if (!string.IsNullOrEmpty(this.Request.QueryString["projId"]))
                {
                    var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                    var projObj = this.scopeProjectService.GetById(projectId);
                    this.txtProjectName.Text = projObj.Name;
                    this.LoadComboData(projectId);
                  //  this.DocStatusControl.Visible = projObj.ProjectTypeId == 2;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.CreatedInfo.Visible = true;
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        this.LoadComboData(docObj.ProjectId.GetValueOrDefault());
                        this.txtProjectName.Text = docObj.ProjectName;
                        this.LoadDocInfo(docObj);

                        this.txtDocNumber.ReadOnly = true;

                        var createdUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + docObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (docObj.UpdatedBy != null && docObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(docObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + docObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid && !string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var projObj = this.scopeProjectService.GetById(projectId);
                DocumentPackage docObj;
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        var oldRevision = docObj.RevisionId;
                        var newRevision = Convert.ToInt32(this.ddlRevision.SelectedValue);

                        // Case for increase revision for document
                        if (newRevision != oldRevision)
                        {
                            var docObjNew = new DocumentPackage();
                            this.CollectData(ref docObjNew, projObj);

                            // Insert new doc
                            docObjNew.CreatedBy = UserSession.Current.User.Id;
                            docObjNew.CreatedDate = DateTime.Now;
                            docObjNew.IsLeaf = true;
                            docObjNew.IsDelete = false;
                            docObjNew.IsEMDR = true;
                            docObjNew.ParentId = docObj.ParentId ?? docObj.ID;
                            docObjNew.PlanedDate = docObjNew.DeadlineReview3;
                            docObjNew.TypeId = docObj.TypeId;
                            docObjNew.DocPath = docObj.DocPath;

                            //clear info incoming hard copy
                            docObjNew.IncomingHardCopyTransDate = null;
                            docObjNew.IncomingHardCopyTransNo = string.Empty;

                            if (docObj.TypeId.GetValueOrDefault() == 2)
                            {
                                docObjNew.ReferenceDocId = docObj.ReferenceDocId;
                                docObjNew.ReferenceDocNo = docObj.ReferenceDocNo;
                                docObjNew.ReferenceDocTitle = docObj.ReferenceDocTitle;
                            }
                            var docNOcheck = this.documentPackageService.GetOneByDocNo(docObjNew.DocNo, docObjNew.RevisionName, docObjNew.ProjectId.GetValueOrDefault());
                            if (docNOcheck == null || docNOcheck.TypeId.GetValueOrDefault() == 2)
                            {
                                var newRevDocId = this.documentPackageService.Insert(docObjNew);

                                // Upate old doc
                                docObj.IsLeaf = false;
                                docObj.UpdatedBy = UserSession.Current.User.Id;
                                docObj.UpdatedDate = DateTime.Now;
                                this.documentPackageService.Update(docObj);

                                // Inherit DM Detail by user from old doc revision
                                //var oldDMDetailByUser = this.dmDetailByUserService.GetAllByDoc(docObj.ID);
                                //foreach (var dmDetailByUser in oldDMDetailByUser)
                                //{
                                //    var newDMDetailByUser = new DistributionMatrixDetailByUser()
                                //    {
                                //        DMId = 0,
                                //        DMName = "Auto Distribution Matrix Of Project",

                                //        DocId = newRevDocId,
                                //        DocNo = docObjNew.DocNo,
                                //        DocTitle = docObjNew.DocTitle,
                                //        RevisionId = docObjNew.RevisionId,
                                //        RevisionName = docObjNew.RevisionName,

                                //        UserId = dmDetailByUser.UserId,
                                //        UserName = dmDetailByUser.UserName,
                                //        ProjectId = dmDetailByUser.ProjectId,
                                //        ProjectName = dmDetailByUser.ProjectName,
                                //        CreatedBy = UserSession.Current.User.Id,
                                //        CreatedByName = UserSession.Current.User.FullName,
                                //        CreatedDate = DateTime.Now,
                                //        Deadline = docObjNew.PlanedDate,
                                //        TypeId = docObjNew.TypeId,
                                //        TypeName = docObjNew.TypeId == 1 ? "EMDR" : "Shop Drawing",
                                //        IsDistributed = false,

                                //        DocStatusId = dmDetailByUser.DocStatusId,
                                //        DocStatusName = dmDetailByUser.DocStatusName
                                //    };

                                //    this.dmDetailByUserService.Insert(newDMDetailByUser);
                                //}

                                // Create new doc status process
                                //if (projObj.ProjectTypeId == 2)
                                //{
                                //    var docStatusProcessList = this.docStatusProcessService.GetAllByDoc(docObj.ID);
                                //    foreach (var docStatusProcess in docStatusProcessList)
                                //    {
                                //        var newdocStatusProcess = new DocumentStatusProcess()
                                //        {
                                //            DocId = docObjNew.ID,
                                //            DocNumber = docObjNew.DocNo,
                                //            StatusId = docStatusProcess.StatusId,
                                //            StatusName = docStatusProcess.StatusName,
                                //            PlantDate = docStatusProcess.PlantDate,
                                //            ActualDate = docStatusProcess.ActualDate
                                //        };

                                //        this.docStatusProcessService.Insert(newdocStatusProcess);
                                //    }
                                //}
                            }
                            // Delete attach file of old Document
                            //var attachDocList = this.attachFilePackageService.GetAllDocId(docObj.ID);
                            //foreach (var attachDoc in attachDocList)
                            //{
                            //    var latestAttachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                            //    if (File.Exists(latestAttachFilePath))
                            //    {
                            //        File.Delete(latestAttachFilePath);
                            //    }
                            //}
                        }
                        else
                        {
                            this.CollectData(ref docObj, projObj);
                            docObj.UpdatedBy = UserSession.Current.User.Id;
                            docObj.UpdatedDate = DateTime.Now;
                            this.documentPackageService.Update(docObj);
                        }



                        if (!string.IsNullOrEmpty(docObj.IncomingHardCopyTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingHardCopyTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                this.attachDocToTransmittalService.Insert(docTrans);
                            }
                        }

                        if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                        {
                            var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                            if (transObj != null)
                            {
                                var docTrans = new AttachDocToTransmittal()
                                {
                                    DocumentId = docObj.ID,
                                    TransmittalId = transObj.ID,
                                };

                                this.attachDocToTransmittalService.Insert(docTrans);
                            }
                        }
                    }
                }
                else
                {
                    docObj = new DocumentPackage()
                    {
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        IsLeaf = true,
                        IsDelete = false,
                        IsDistributed = false,
                        IsEMDR = true
                    };

                    this.CollectData(ref docObj, projObj);

                    docObj.TypeId = Convert.ToInt32(this.Request.QueryString["type"]);

                    var disciplineObj = this.disciplineService.GetById(docObj.DisciplineId.GetValueOrDefault());
                    if (disciplineObj != null)
                    {
                        docObj.DocPath = docObj.TypeId == 1
                                        ? disciplineObj.DisciplinePath
                                        : disciplineObj.DisciplinePath.Replace("01.All Revision", "03.Shop Drawing/01.All Revision");
                    }


                    if (!string.IsNullOrEmpty(this.Request.QueryString["Reference"]))
                    {
                        var ReferencedocId = Convert.ToInt32(this.Request.QueryString["Reference"]);
                        var ReferencedocObj = this.documentPackageService.GetById(ReferencedocId);
                        docObj.ReferenceDocId = ReferencedocObj.ID;
                        docObj.ReferenceDocNo = ReferencedocObj.DocNo;
                        docObj.ReferenceDocTitle = ReferencedocObj.ReferenceDocTitle;

                    }
                    var docNOcheck = this.documentPackageService.GetOneByDocNo(docObj.DocNo, docObj.RevisionName, docObj.ProjectId.GetValueOrDefault());

                    if (docNOcheck == null || docNOcheck.TypeId.GetValueOrDefault() == 2)
                    {
                        var docId = this.documentPackageService.Insert(docObj);
                        if (docId != null)
                        {
                            //var validDocNoFolder = Utility.RemoveSpecialCharacterForFolder(docObj.DocNo);
                            //var targetFolder = "../../DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projObj.Name);
                            //var physicalDocNoFolder = Path.Combine(Server.MapPath(targetFolder), validDocNoFolder);

                            //if (!Directory.Exists(physicalDocNoFolder))
                            //{
                            //    Directory.CreateDirectory(physicalDocNoFolder);
                            //}

                            if (!string.IsNullOrEmpty(docObj.IncomingHardCopyTransNo))
                            {
                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingHardCopyTransNo);
                                if (transObj != null)
                                {
                                    var docTrans = new AttachDocToTransmittal()
                                    {
                                        DocumentId = docObj.ID,
                                        TransmittalId = transObj.ID,
                                    };

                                    this.attachDocToTransmittalService.Insert(docTrans);
                                }
                            }

                            if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                            {
                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                                if (transObj != null)
                                {
                                    var docTrans = new AttachDocToTransmittal()
                                    {
                                        DocumentId = docObj.ID,
                                        TransmittalId = transObj.ID,
                                    };

                                    this.attachDocToTransmittalService.Insert(docTrans);
                                }
                            }
                        }
                    }
                    //else
                    //{

                    //}

                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseWindow", "CloseAndRebind();", true);
            }
        }


        //  protected bool CheckDocNo()

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            ////if(this.txtName.Text.Trim().Length == 0)
            ////{
            ////    this.fileNameValidator.ErrorMessage = "Please enter file name.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = false;
            ////}
            ////else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            ////{
            ////    var docId = Convert.ToInt32(Request.QueryString["docId"]);
            ////    this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = true; ////!this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            ////}
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);

                if (this.documentService.IsDocumentExist(folderId, fileName))
                {
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData(int projectId)
        {
            var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId)
                    .OrderBy(t => t.ID)
                    .ToList();
            disciplineList.Insert(0, new Discipline() { ID = 0 });
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();

            var departmentList = this.roleService.GetAll(false);
            departmentList.Insert(0, new Role() { Id = 0 });
            this.ddlDepartment.DataSource = departmentList;
            this.ddlDepartment.DataTextField = "FullName";
            this.ddlDepartment.DataValueField = "Id";
            this.ddlDepartment.DataBind();

            var listRevision = this.revisionService.GetAll();
            this.ddlRevision.DataSource = listRevision;
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataBind();

            var docTypeList = this.documentTypeService.GetAllByProject(projectId);
            docTypeList.Insert(0, new DocumentType() { ID = 0 });
            this.ddlDocType.DataSource = docTypeList;
            this.ddlDocType.DataTextField = "FullName";
            this.ddlDocType.DataValueField = "ID";
            this.ddlDocType.DataBind();

            var statusList = this.statusService.GetAllByProject(projectId).ToList();
            statusList.Insert(0, new Status { ID = 0 });
            this.ddlStatus.DataSource = statusList;
            this.ddlStatus.DataTextField = "FullName";
            this.ddlStatus.DataValueField = "ID";
            this.ddlStatus.DataBind();

            var listcode = this.documentCodeservice.GetAllByProject(projectId).ToList();
            listcode.Insert(0, new Code() { ID = 0, Name = string.Empty });
            this.ddlCodes.DataSource = listcode;
            this.ddlCodes.DataTextField = "Name";
            this.ddlCodes.DataValueField = "ID";
            this.ddlCodes.DataBind();
        }

        private void CollectData(ref DocumentPackage docObj, ScopeProject projObj)
        {
            docObj.ProjectId = projObj.ID;
            docObj.ProjectName = projObj.Name;

            docObj.DocNo = this.txtDocNumber.Text.Trim();
            docObj.DocTitle = this.txtDocumentTitle.Text.Trim();
            docObj.DocumentTypeId = this.ddlDocType.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDocType.SelectedValue)
                                        : 0;
            docObj.DocumentTypeName = this.ddlDocType.SelectedItem != null
                                          ? this.ddlDocType.SelectedItem.Text
                                          : string.Empty;

            docObj.StatusID = this.ddlStatus.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlStatus.SelectedValue)
                                        : 0;
            docObj.StatusName = this.ddlStatus.SelectedItem != null
                                          ? this.ddlStatus.SelectedItem.Text
                                          : string.Empty;

            docObj.ReferenceFromID = this.ddlReferenceFrom.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlReferenceFrom.SelectedValue)
                                        : 0;
            docObj.ReferenceFromName = this.ddlReferenceFrom.SelectedItem != null
                                          ? this.ddlReferenceFrom.SelectedItem.Text
                                          : string.Empty;

            docObj.DisciplineId = this.ddlDiscipline.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlDiscipline.SelectedValue)
                                      : 0;
            docObj.DisciplineName = this.ddlDiscipline.SelectedItem != null
                                        ? this.ddlDiscipline.SelectedItem.Text
                                        : string.Empty;
            docObj.DeparmentId = this.ddlDepartment.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlDepartment.SelectedValue)
                                      : 0;
            docObj.DeparmentName = this.ddlDepartment.SelectedItem != null
                                        ? this.ddlDepartment.SelectedItem.Text
                                        : string.Empty;
            docObj.Complete = this.txtComplete.Value.GetValueOrDefault();
            docObj.Weight = this.txtWeight.Value.GetValueOrDefault();
            docObj.Notes = this.txtNotes.Text.Trim();
            docObj.CommentCode = this.ddlCodes.SelectedItem.Text;// this.txtCommentCode.Text.Trim();

            docObj.StatusName = this.ddlStatus.SelectedItem.Text;
            docObj.StatusID = Convert.ToInt32(this.ddlStatus.SelectedValue);
            docObj.CommentCodeId = Convert.ToInt32(ddlCodes.SelectedValue);
            docObj.IsCriticalDoc = this.cbIsCritical.Checked;
            docObj.IsPriorityDoc = this.cbIsPriority.Checked;
            docObj.IsVendorDoc = this.cbIsVendor.Checked;

            docObj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            docObj.RevisionName = this.ddlRevision.SelectedItem.Text;

            docObj.IncomingTransDate = this.txtIncomingTransDate.SelectedDate;
            docObj.IncomingTransNo = this.txtIncomingTransNo.Text.Trim();

            docObj.IncomingHardCopyTransDate = this.txtIncomingHardCopyTransDate.SelectedDate;
            docObj.IncomingHardCopyTransNo = this.txtIncomingHardCopyTransNo.Text.Trim();

            docObj.OutgoingTransDate = this.txtOutgoingTransDate.SelectedDate;
            docObj.OutgoingTransNo = this.txtOutgoingTransNo.Text.Trim();

            docObj.PlanedDate = this.txtDeadline.SelectedDate;
            //docObj.AnotherDocNo = this.txtVendorDocNumber.Text.Trim();
            docObj.DeadlineComment = this.txtDeadlineComment.SelectedDate;
            docObj.DeadlineResponse = this.txtDeadlineResponse.SelectedDate;

        }

        private void LoadDocInfo(DocumentPackage docObj)
        {
            this.txtDocNumber.Text = docObj.DocNo;
            this.txtReferDocNumber.Text = docObj.ReferenceDocNo;
            this.txtDocumentTitle.Text = docObj.DocTitle;
            this.ddlDocType.SelectedValue = docObj.DocumentTypeId.GetValueOrDefault().ToString();
            this.ddlDiscipline.SelectedValue = docObj.DisciplineId.GetValueOrDefault().ToString();
            this.ddlStatus.SelectedValue = docObj.StatusID.ToString();
            this.ddlReferenceFrom.SelectedValue = docObj.ReferenceFromID.ToString();
            this.txtComplete.Value = docObj.Complete;
            this.txtWeight.Value = docObj.Weight;
            this.txtNotes.Text = docObj.Notes;
            //this.txtCommentCode.Text = docObj.CommentCode;
            this.ddlStatus.SelectedValue = docObj.StatusID.GetValueOrDefault().ToString();
            this.ddlCodes.SelectedValue = docObj.CommentCodeId.GetValueOrDefault().ToString();
            this.cbIsCritical.Checked = docObj.IsCriticalDoc.GetValueOrDefault();
            this.cbIsPriority.Checked = docObj.IsPriorityDoc.GetValueOrDefault();
            this.cbIsVendor.Checked = docObj.IsVendorDoc.GetValueOrDefault();

            this.ddlRevision.SelectedValue = docObj.RevisionId.GetValueOrDefault().ToString();
            this.ddlDepartment.SelectedValue = docObj.DeparmentId.GetValueOrDefault().ToString();
            this.txtIncomingHardCopyTransDate.SelectedDate = docObj.IncomingHardCopyTransDate;
            this.txtIncomingHardCopyTransNo.Text = docObj.IncomingHardCopyTransNo;
            this.txtIncomingTransDate.SelectedDate = docObj.IncomingTransDate;
            this.txtIncomingTransNo.Text = docObj.IncomingTransNo;
            this.txtOutgoingTransDate.SelectedDate = docObj.OutgoingTransDate;
            this.txtOutgoingTransNo.Text = docObj.OutgoingTransNo;
            this.txtDeadline.SelectedDate = docObj.PlanedDate;
            this.txtDeadlineComment.SelectedDate = docObj.DeadlineComment;
            this.txtDeadlineResponse.SelectedDate = docObj.DeadlineResponse;

        }

        protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var statusObj = this.statusService.GetById(Convert.ToInt32(this.ddlStatus.SelectedValue));
            if (statusObj != null)
            {
                this.txtComplete.Value = Math.Round((double)(statusObj.PercentCompleteDefault * this.txtWeight.Value.GetValueOrDefault() / 1000), 2);
            }
        }

        protected void ddlRevision_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //this.txtFinalCode.Text = string.Empty;
        }
    }
}