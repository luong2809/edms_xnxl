﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoDistributionMatrixConfigForm.aspx.cs" Inherits="EDMs.Web.Controls.Document.AutoDistributionMatrixConfigForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }
        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart {
            display: none;
        }

        .RadComboBox table td.rcbInputCell {
            padding: 0!important;
        }
    </style>
    <telerik:RadScriptBlock runat="server">
        <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

        <script type="text/javascript">
            function CloseAndRebind(args) {
                GetRadWindow().BrowserWindow.refreshGrid(args);
                GetRadWindow().close();
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                return oWindow;
            }

            function CancelEdit() {
                GetRadWindow().close();
            }
        </script>
    </telerik:RadScriptBlock>

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />

        <div style="width: 100%">
            <ul style="list-style-type: none">
                <div class="qlcbFormItem">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>

        <div style="width: 100%" runat="server" id="divContent">
            <ul style="list-style-type: none">
                <li style="width: 550px;">
                    <div>
                        <label style="width: 80px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Department
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="min25Percent" Width="400px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_OnSelectedIndexChanged" />
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 550px;">
                    <div>
                        <label style="width: 80px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">User
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadComboBox ID="lbUser" runat="server" Filter="Contains" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" Width="400"
                                Height="150px" Style="max-width: 500px !important">
                            </telerik:RadComboBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

            </ul>
        </div>

        <div style="width: 100%">
            <ul style="list-style-type: none">
                <li style="width: 500px; padding-top: 5px; padding-bottom: 3px; text-align: center">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Add User" OnClick="btnSave_Click" Width="130px" Style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/addNew.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                </li>
            </ul>
        </div>

        <telerik:RadGrid ID="grdPermission" runat="server" AllowPaging="True"
            AutoGenerateColumns="False" CellSpacing="0" CellPadding="0"
            PageSize="100" Height="300" GridLines="None"
            Skin="Windows7" AllowFilteringByColumn="True"
            OnNeedDataSource="grdPermission_OnNeedDataSource"
            OnDeleteCommand="grdPermission_OnDeteleCommand">
            <GroupingSettings CaseSensitive="False"></GroupingSettings>
            <MasterTableView DataKeyNames="ID" ClientDataKeyNames="ID" Width="100%">
                <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; records." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                <Columns>
                    <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete"
                        ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                        <HeaderStyle Width="5%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridButtonColumn>

                    <telerik:GridBoundColumn DataField="UserFullName" HeaderText="Department - User Name" UniqueName="UserFullName"
                        ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                        <HeaderStyle Width="90%" HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </telerik:GridBoundColumn>

                </Columns>
                <CommandItemStyle Height="25px"></CommandItemStyle>
            </MasterTableView>
            <ClientSettings>
                <Selecting AllowRowSelect="true"></Selecting>
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
            </ClientSettings>
        </telerik:RadGrid>
        <asp:HiddenField runat="server" ID="docUploadedIsExist" />
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf" />

        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlDepartment" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="lbUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdPermission">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlDepartment" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="lbUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlDepartment">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lbUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }

            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
