﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportUpdateEMDR : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        private readonly CodeService codeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;

       // private readonly DocumentStatusProcessService docStatusProcessService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportUpdateEMDR()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.folderService = new FolderService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.codeService = new CodeService();
           // this.docStatusProcessService = new DocumentStatusProcessService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var docStatusProcessList = new List<DocumentStatusProcess>();
            bool changesof = false;
            bool changehard = false;
            var newDocumentPackageList = new List<DocumentPackage>();
            var newdocStatusProcessList = new List<DocumentStatusProcess>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        
                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;

                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectObj = this.scopeProjectService.GetById(projectID);
                            if (projectObj != null)
                            {
                                // Get doc status list if project type is Service
                                var statusList = new List<Status>();
                                if (projectObj.ProjectTypeId == 2)
                                {
                                    statusList = this.statusService.GetAllByProject(projectObj.ID).OrderBy(t => t.PercentCompleteDefault).ToList();
                                }
                                // ------------------------------------------------------------------

                                var disciplineId = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                                    ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                                    : 0;
                                var disciplineObj = this.disciplineService.GetById(disciplineId);
                                if (disciplineObj != null)
                                {
                                    var dataTable = datasheet.Cells.ExportDataTable(5, 0, datasheet.Cells.MaxRow - 1,
                                        16 + statusList.Count);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            var docId = Convert.ToInt32(dataRow["Column2"].ToString());
                                            currentDocumentNo = dataRow["Column3"].ToString();
                                            var docObj = this.documentPackageService.GetById(docId);
                                            if (docObj != null)
                                            {
                                                var revObj = this.revisionService.GetByName(dataRow["Column5"].ToString());
                                                var docTypeObj = this.documentTypeService.GetByFullName(dataRow["Column8"].ToString(), projectObj.ID);
                                                var departmentObj = this.roleService.GetByFullName(dataRow["Column7"].ToString());
                                                
                                                // Add new document revision
                                                if (docObj.RevisionId != revObj.ID)
                                                {
                                                    var newRevDoc = new DocumentPackage();
                                                    newRevDoc.ProjectId = projectObj.ID;
                                                    newRevDoc.ProjectName = projectObj.Name;

                                                    newRevDoc.DocNo = dataRow["Column3"].ToString();
                                                    newRevDoc.DocTitle = dataRow["Column4"].ToString();
                                                    newRevDoc.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                    newRevDoc.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                    newRevDoc.StatusID = 0;
                                                    newRevDoc.StatusName = string.Empty;

                                                    newRevDoc.ReferenceFromID = 0;
                                                    newRevDoc.ReferenceFromName = string.Empty;

                                                    newRevDoc.DisciplineId = disciplineObj.ID;
                                                    newRevDoc.DisciplineName = disciplineObj.FullName;

                                                    newRevDoc.DeparmentId = departmentObj != null ? departmentObj.Id : 0;
                                                    newRevDoc.DeparmentName = departmentObj != null ? departmentObj.FullName : string.Empty;

                                                    newRevDoc.Weight = docObj.Weight;

                                                    newRevDoc.IsCriticalDoc = false;
                                                    newRevDoc.IsPriorityDoc = false;
                                                    newRevDoc.IsVendorDoc = false;

                                                    newRevDoc.RevisionId = revObj != null ? revObj.ID : 0;
                                                    newRevDoc.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    newRevDoc.TypeId = docObj.TypeId;

                                                    var strPlanedDate = dataRow["Column6"].ToString();
                                                    var planedDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(strPlanedDate, ref planedDate))
                                                    {
                                                        newRevDoc.PlanedDate = planedDate;
                                                    }

                                                    newRevDoc.IncomingTransNo = dataRow["Column9"].ToString();
                                                    var strIncomingTransDate = dataRow["Column10"].ToString();
                                                    var incomingTransDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(strIncomingTransDate, ref incomingTransDate))
                                                    {
                                                        newRevDoc.IncomingTransDate = incomingTransDate;
                                                    }
                                                    newRevDoc.IncomingHardCopyTransDate = null;
                                                    newRevDoc.IncomingHardCopyTransNo = string.Empty;
                                                    //newRevDoc.IncomingHardCopyTransNo = dataRow["Column11"].ToString();
                                                    //var strIncomingHardCopyTransDate = dataRow["Column12"].ToString();
                                                    //var incomingHardCopyTransDate = new DateTime();
                                                    //if (Utility.ConvertStringToDateTime(strIncomingHardCopyTransDate, ref incomingHardCopyTransDate))
                                                    //{
                                                    //    newRevDoc.IncomingHardCopyTransDate = incomingHardCopyTransDate;
                                                    //}

                                                    var status = this.statusService.GetAll().FirstOrDefault(t => t.FullName == dataRow["Column13"].ToString());
                                                    if (status != null)
                                                    {
                                                        docObj.StatusID = status.ID;
                                                        docObj.StatusName = status.FullName;
                                                    }
                                                    var code = this.codeService.GetByName(dataRow["Column14"].ToString());
                                                    if (code != null)
                                                    {
                                                        docObj.CommentCode = code.Name;
                                                        docObj.CommentCodeId = code.ID;
                                                    }

                                                    var dateissue = dataRow["Column15"].ToString();
                                                    var Dateissue = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(dateissue, ref Dateissue))
                                                    {
                                                        docObj.DeadlineComment = Dateissue;
                                                    }
                                                    dateissue = dataRow["Column16"].ToString();
                                                    Dateissue = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(dateissue, ref Dateissue))
                                                    {
                                                        docObj.DeadlineResponse = Dateissue;
                                                    }
                                                    newRevDoc.CreatedBy = UserSession.Current.User.Id;
                                                    newRevDoc.CreatedDate = DateTime.Now;
                                                    newRevDoc.IsLeaf = true;
                                                    newRevDoc.IsDelete = false;
                                                    newRevDoc.IsEMDR = true;
                                                    newRevDoc.ParentId = docObj.ParentId ?? docObj.ID;

                                                    newRevDoc.StatusID = docObj.StatusID;
                                                    newRevDoc.StatusName = docObj.StatusName;
                                                    newRevDoc.DocPath = docObj.DocPath;

                                                    newDocumentPackageList.Add(newRevDoc);

                                                    // Update isleaf for old doc revision
                                                    docObj.IsLeaf = false;
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    documentPackageList.Add(docObj);

                                                    // Inherit docstatus process of old doc
                                                    for (int j = 0; j < statusList.Count; j++)
                                                    {
                                                        var docStatusProcess = new DocumentStatusProcess()
                                                        {
                                                            DocNumber = docObj.DocNo,
                                                            StatusId = statusList[j].ID,
                                                            StatusName = statusList[j].Name
                                                        };

                                                        if (!string.IsNullOrEmpty(dataRow["Column" + (16 + j)].ToString()))
                                                        {
                                                            var docStatusplanedDate = dataRow["Column" + (16 + j)];
                                                            docStatusProcess.PlantDate = (DateTime?)docStatusplanedDate;
                                                        }

                                                        newdocStatusProcessList.Add(docStatusProcess);
                                                    }
                                                    // ---------------------------------------------------------------------------
                                                }
                                                // ------------------------------------------------------------------------------------------------------
                                                // Update exist Document revision
                                                else
                                                {
                                                    docObj.ProjectId = projectObj.ID;
                                                    docObj.ProjectName = projectObj.Name;

                                                    docObj.DocNo = dataRow["Column3"].ToString();
                                                    docObj.DocTitle = dataRow["Column4"].ToString();
                                                    docObj.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                    docObj.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                    docObj.StatusID = 0;
                                                    docObj.StatusName = string.Empty;

                                                    docObj.ReferenceFromID = 0;
                                                    docObj.ReferenceFromName = string.Empty;

                                                    docObj.DisciplineId = disciplineObj.ID;
                                                    docObj.DisciplineName = disciplineObj.FullName;

                                                    docObj.DeparmentId = departmentObj != null ? departmentObj.Id : 0;
                                                    docObj.DeparmentName = departmentObj != null ? departmentObj.FullName : string.Empty;

                                                    docObj.Weight = 0.0;

                                                    docObj.IsCriticalDoc = false;
                                                    docObj.IsPriorityDoc = false;
                                                    docObj.IsVendorDoc = false;

                                                    docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                                    docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    //docObj.TypeId = 1;

                                                    var strPlanedDate = dataRow["Column6"].ToString();
                                                    var planedDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(strPlanedDate, ref planedDate))
                                                    {
                                                        docObj.PlanedDate = planedDate;
                                                    }

                                                    docObj.IncomingTransNo = dataRow["Column9"].ToString();
                                                    var strIncomingTransDate = dataRow["Column10"].ToString();
                                                    var incomingTransDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(strIncomingTransDate, ref incomingTransDate))
                                                    {
                                                        docObj.IncomingTransDate = incomingTransDate;
                                                    }

                                                    docObj.IncomingHardCopyTransNo = dataRow["Column11"].ToString();
                                                    var strIncomingHardCopyTransDate = dataRow["Column12"].ToString();
                                                    var incomingHardCopyTransDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(strIncomingHardCopyTransDate, ref incomingHardCopyTransDate))
                                                    {
                                                        docObj.IncomingHardCopyTransDate = incomingHardCopyTransDate;
                                                    }

                                                    //docObj.Complete = !string.IsNullOrEmpty(dataRow["Column13"].ToString())
                                                    //    ? Convert.ToDouble(dataRow["Column13"].ToString()) * 100
                                                    //    : 0;
                                                    //docObj.Weight = !string.IsNullOrEmpty(dataRow["Column14"].ToString())
                                                    //    ? Convert.ToDouble(dataRow["Column14"].ToString()) * 100
                                                    //    : 0;
                                                    var status = this.statusService.GetAll().FirstOrDefault(t => t.FullName == dataRow["Column13"].ToString());
                                                    if(status != null)
                                                    {
                                                        docObj.StatusID = status.ID;
                                                        docObj.StatusName = status.FullName;
                                                    }
                                                    var code = this.codeService.GetByName(dataRow["Column14"].ToString());
                                                    if(code != null)
                                                    {
                                                        docObj.CommentCode =code.Name;
                                                        docObj.CommentCodeId = code.ID;
                                                    }
                                                   
                                                    var dateissue = dataRow["Column15"].ToString();
                                                    var Dateissue = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(dateissue, ref Dateissue))
                                                    {
                                                        docObj.DeadlineComment = Dateissue;
                                                    }
                                                    dateissue = dataRow["Column16"].ToString();
                                                     Dateissue = new DateTime();
                                                    if (Utility.ConvertStringToDateTimeDateMonth(dateissue, ref Dateissue))
                                                    {
                                                        docObj.DeadlineResponse = Dateissue;
                                                    }
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    docObj.IsLeaf = true;
                                                    docObj.IsDelete = false;
                                                    docObj.IsEMDR = true;
                                                    documentPackageList.Add(docObj);

                                                    // Get update Plan date info for doc status process
                                                    //var existDocStatusProcessList = this.docStatusProcessService.GetAllByDoc(docObj.ID);
                                                    //for (int j = 0; j < statusList.Count; j++)
                                                    //{
                                                    //    var existDocStatusProcess = existDocStatusProcessList.FirstOrDefault(t => t.StatusName == statusList[j].Name);
                                                    //    if (existDocStatusProcess != null)
                                                    //    {
                                                    //        var docStatusplanedDate = dataRow["Column" + (16 + j)];
                                                    //        existDocStatusProcess.PlantDate = (DateTime?)docStatusplanedDate;

                                                    //        docStatusProcessList.Add(existDocStatusProcess);
                                                    //    }
                                                    //}
                                                    // ----------------------------------------------------------------------------------------------------------------

                                                }
                                            }
                                        }
                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        // Update Docinfo
                                        foreach (var docObj in documentPackageList)
                                        {
                                            //var validDocNoFolder = Utility.RemoveSpecialCharacterForFolder(docObj.DocNo);
                                            //var targetFolder = "../../DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name);
                                            //var physicalDocNoFolder = Path.Combine(Server.MapPath(targetFolder), validDocNoFolder);
                                            //var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Projects/" + Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "/" + validDocNoFolder;
                                            
                                            //docObj.DocPath = serverFolder;

                                            //var docId=this.documentPackageService.Insert(docObj);
                                            //if (docId != null)
                                            //{
                                                
                                            //    if (!Directory.Exists(physicalDocNoFolder))
                                            //    {
                                            //        Directory.CreateDirectory(physicalDocNoFolder);
                                            //    }
                                            //}

                                            this.documentPackageService.Update(docObj);

                                          /*  if (!string.IsNullOrEmpty(docObj.IncomingHardCopyTransNo))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingHardCopyTransNo);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };

                                                    this.attachDocToTransmittalService.Insert(docTrans);
                                                    try
                                                    {
                                                        // Auto create transmittal folder on Document library and share document files
                                                        var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                        foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                        {
                                                            // Path file to save on server disc
                                                            var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                                            // Path file to download from server
                                                            var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                            var fileExt = attachFile.Extension;

                                                            if (!File.Exists(saveFilePath))
                                                            {
                                                                var document = new Data.Entities.Document()
                                                                {
                                                                    Name = attachFile.FileName,
                                                                    FileExtension = fileExt,
                                                                    FileExtensionIcon = attachFile.ExtensionIcon,
                                                                    FilePath = serverFilePath,
                                                                    FolderID = mainTransFolder.ID,
                                                                    IsLeaf = true,
                                                                    IsDelete = false,
                                                                    CreatedBy = UserSession.Current.User.Id,
                                                                    CreatedDate = DateTime.Now
                                                                };
                                                                this.documentService.Insert(document);
                                                            }

                                                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                            {
                                                                File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                            }
                                                        }
                                                    }
                                                    catch { }
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                                            {
                                                var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                                                if (transObj != null)
                                                {
                                                    var docTrans = new AttachDocToTransmittal()
                                                    {
                                                        DocumentId = docObj.ID,
                                                        TransmittalId = transObj.ID,
                                                    };

                                                    this.attachDocToTransmittalService.Insert(docTrans);
                                                    try
                                                    {
                                                        // Auto create transmittal folder on Document library and share document files
                                                        var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                        foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                        {
                                                            // Path file to save on server disc
                                                            var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                                            // Path file to download from server
                                                            var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                            var fileExt = attachFile.Extension;

                                                            if (!File.Exists(saveFilePath))
                                                            {
                                                                var document = new Data.Entities.Document()
                                                                {
                                                                    Name = attachFile.FileName,
                                                                    FileExtension = fileExt,
                                                                    FileExtensionIcon = attachFile.ExtensionIcon,
                                                                    FilePath = serverFilePath,
                                                                    FolderID = mainTransFolder.ID,
                                                                    IsLeaf = true,
                                                                    IsDelete = false,
                                                                    CreatedBy = UserSession.Current.User.Id,
                                                                    CreatedDate = DateTime.Now
                                                                };
                                                                this.documentService.Insert(document);
                                                            }

                                                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                            {
                                                                File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                            }
                                                        }
                                                    }
                                                    catch { }
                                                }
                                            }*/
                                        }
                                        // ------------------------------------------------------------------------------------------------

                                        //// Update Doc Status Process
                                        //foreach (var docStatusProcessItem in docStatusProcessList)
                                        //{
                                        //    this.docStatusProcessService.Update(docStatusProcessItem);
                                        //}
                                        // ------------------------------------------------------------------------------------------------
                                        
                                        // Create new document revision
                                        foreach (var docObj in newDocumentPackageList)
                                        {
                                            var docId = this.documentPackageService.Insert(docObj);
                                           // if (docId != null)
                                            //{
                                                // Create doc status process info
                                                //var docStatusProcessOfDoc = newdocStatusProcessList.Where(t => t.DocNumber == docObj.DocNo);
                                                //foreach (var docStatusProcess in docStatusProcessOfDoc)
                                                //{
                                                //    docStatusProcess.DocId = docId;
                                                //    this.docStatusProcessService.Insert(docStatusProcess);
                                                //}
                                                // ---------------------------------------

                                              /*  if (!string.IsNullOrEmpty(docObj.IncomingHardCopyTransNo))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingHardCopyTransNo);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };

                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                        try
                                                        {
                                                            // Auto create transmittal folder on Document library and share document files
                                                            var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                            foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                            {
                                                                // Path file to save on server disc
                                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                                                // Path file to download from server
                                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                var fileExt = attachFile.Extension;

                                                                if (!File.Exists(saveFilePath))
                                                                {
                                                                    var document = new Data.Entities.Document()
                                                                    {
                                                                        Name = attachFile.FileName,
                                                                        FileExtension = fileExt,
                                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                                        FilePath = serverFilePath,
                                                                        FolderID = mainTransFolder.ID,
                                                                        IsLeaf = true,
                                                                        IsDelete = false,
                                                                        CreatedBy = UserSession.Current.User.Id,
                                                                        CreatedDate = DateTime.Now
                                                                    };
                                                                    this.documentService.Insert(document);
                                                                }

                                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                {
                                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                }
                                                            }
                                                        }
                                                        catch { }
                                                    }
                                                }

                                                if (!string.IsNullOrEmpty(docObj.IncomingTransNo))
                                                {
                                                    var transObj = this.incomingTransmittalService.GetByTransNo(docObj.IncomingTransNo);
                                                    if (transObj != null)
                                                    {
                                                        var docTrans = new AttachDocToTransmittal()
                                                        {
                                                            DocumentId = docObj.ID,
                                                            TransmittalId = transObj.ID,
                                                        };

                                                        this.attachDocToTransmittalService.Insert(docTrans);
                                                        try
                                                        {
                                                            // Auto create transmittal folder on Document library and share document files
                                                            var mainTransFolder = this.folderService.GetById(transObj.FolderId.GetValueOrDefault());
                                                            foreach (var attachFile in this.attachFilesPackageService.GetAllDocumentFileByDocId(docObj.ID))
                                                            {
                                                                // Path file to save on server disc
                                                                var saveFilePath = Path.Combine(Server.MapPath(mainTransFolder.DirName), attachFile.FileName.Replace("&", "-"));
                                                                // Path file to download from server
                                                                var serverFilePath = mainTransFolder.DirName + "/" + attachFile.FileName;
                                                                var fileExt = attachFile.Extension;

                                                                if (!File.Exists(saveFilePath))
                                                                {
                                                                    var document = new Data.Entities.Document()
                                                                    {
                                                                        Name = attachFile.FileName,
                                                                        FileExtension = fileExt,
                                                                        FileExtensionIcon = attachFile.ExtensionIcon,
                                                                        FilePath = serverFilePath,
                                                                        FolderID = mainTransFolder.ID,
                                                                        IsLeaf = true,
                                                                        IsDelete = false,
                                                                        CreatedBy = UserSession.Current.User.Id,
                                                                        CreatedDate = DateTime.Now
                                                                    };
                                                                    this.documentService.Insert(document);
                                                                }

                                                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                                                {
                                                                    File.Copy(Server.MapPath(attachFile.FilePath), saveFilePath, true);
                                                                }
                                                            }
                                                        }
                                                        catch { }
                                                    }
                                                }*/
                                            //}
                                        }
                                        // --------------------------------------------------------------------------------------------------------
                                        documentPackageList = new List<DocumentPackage>();
                                        newDocumentPackageList = new List<DocumentPackage>();
                                        docStatusProcessList = new List<DocumentStatusProcess>();
                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document master list file is valid. System import successfull!";
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                }
                                else
                                {
                                    this.blockError.Visible = true;
                                    this.lblError.Text =
                                        "Have Error: Project '" + projectObj.Name + "' don't have Document Type '" +
                                        currentSheetName + "'. Please remove this sheet out of master file.";
                                }
                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }
                    }
                }
           }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}