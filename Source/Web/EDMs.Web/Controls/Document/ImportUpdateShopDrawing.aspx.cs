﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Web.Hosting;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportUpdateShopDrawing : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DocumentNewService documentNewService;

        private readonly CategoryService categoryService;

        private readonly OptionalTypeDetailService optionalTypeDetailService;

        private readonly OriginatorService originatorService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly DepartmentService departmentService;
        private readonly PlatformService platformService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly IncomingTransmittalService incomingTransmittalService;

        private readonly UserDataPermissionService userDataPermissionService;
        private readonly FolderService folderService;
        private readonly AttachFilesPackageService attachFilesPackageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportUpdateShopDrawing()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            //this.languageService = new LanguageService();
            this.documentService = new DocumentService();
            this.documentNewService = new DocumentNewService();
            this.categoryService = new CategoryService();
            this.optionalTypeDetailService = new OptionalTypeDetailService();
            this.originatorService = new OriginatorService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.incomingTransmittalService = new IncomingTransmittalService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.attachFilesPackageService = new AttachFilesPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var newDocumentPackageList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        
                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;

                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectObj = this.scopeProjectService.GetById(projectID);
                            if (projectObj != null)
                            {
                                var disciplineId = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                                    ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                                    : 0;
                                var disciplineObj = this.disciplineService.GetById(disciplineId);
                                if (disciplineObj != null)
                                {
                                    var dataTable = datasheet.Cells.ExportDataTable(5, 0, datasheet.Cells.MaxRow - 1,10);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            var docId = Convert.ToInt32(dataRow["Column2"].ToString());
                                            currentDocumentNo = dataRow["Column3"].ToString();
                                            var docObj = this.documentPackageService.GetById(docId);
                                            if (docObj != null)
                                            {
                                                var revObj = this.revisionService.GetByName(dataRow["Column5"].ToString());
                                                var docTypeObj = this.documentTypeService.GetByFullName(dataRow["Column8"].ToString(), projectObj.ID);
                                                var departmentObj = this.roleService.GetByFullName(dataRow["Column7"].ToString());
                                                // Add new document revision
                                                if (docObj.RevisionId != revObj.ID)
                                                {
                                                    var newRevDoc = new DocumentPackage();
                                                    newRevDoc.ProjectId = projectObj.ID;
                                                    newRevDoc.ProjectName = projectObj.Name;

                                                    newRevDoc.DocNo = dataRow["Column3"].ToString();
                                                    newRevDoc.DocTitle = dataRow["Column4"].ToString();
                                                    newRevDoc.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                    newRevDoc.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                    newRevDoc.StatusID = 0;
                                                    newRevDoc.StatusName = string.Empty;

                                                    newRevDoc.ReferenceFromID = 0;
                                                    newRevDoc.ReferenceFromName = string.Empty;

                                                    newRevDoc.DisciplineId = disciplineObj.ID;
                                                    newRevDoc.DisciplineName = disciplineObj.FullName;

                                                    newRevDoc.DeparmentId = departmentObj != null ? departmentObj.Id : 0;
                                                    newRevDoc.DeparmentName = departmentObj != null ? departmentObj.FullName : string.Empty;

                                                    newRevDoc.Weight = docObj.Weight;

                                                    newRevDoc.IsCriticalDoc = false;
                                                    newRevDoc.IsPriorityDoc = false;
                                                    newRevDoc.IsVendorDoc = false;

                                                    newRevDoc.RevisionId = revObj != null ? revObj.ID : 0;
                                                    newRevDoc.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    newRevDoc.TypeId = docObj.TypeId;

                                                    var strPlanedDate = dataRow["Column6"].ToString();
                                                    var planedDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strPlanedDate, ref planedDate))
                                                    {
                                                        newRevDoc.PlanedDate = planedDate;
                                                    }

                                                   
                                                   // newRevDoc.Complete = !string.IsNullOrEmpty(dataRow["Column9"].ToString())
                                                       // ? Convert.ToDouble(dataRow["Column9"].ToString()) * 100
                                                      //  : 0;
                                                    newRevDoc.Weight = !string.IsNullOrEmpty(dataRow["Column10"].ToString())
                                                        ? Convert.ToDouble(dataRow["Column10"].ToString()) * 100
                                                        : 0;

                                                   // newRevDoc.CommentCode = dataRow["Column15"].ToString().Trim();
                                                    newRevDoc.CreatedBy = UserSession.Current.User.Id;
                                                    newRevDoc.CreatedDate = DateTime.Now;
                                                    newRevDoc.IsLeaf = true;
                                                    newRevDoc.IsDelete = false;
                                                    newRevDoc.IsEMDR = true;
                                                    newRevDoc.ParentId = docObj.ParentId ?? docObj.ID;
                                                    newRevDoc.ReferenceDocId = docObj.ReferenceDocId;
                                                    newRevDoc.ReferenceDocNo = docObj.ReferenceDocNo;
                                                    newRevDoc.ReferenceDocTitle = docObj.ReferenceDocTitle;
                                                    newRevDoc.DocPath = docObj.DocPath;

                                                    newDocumentPackageList.Add(newRevDoc);

                                                    // Update isleaf for old doc revision
                                                    docObj.IsLeaf = false;
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    documentPackageList.Add(docObj);
                                                }
                                                // Update exist Document revision
                                                else
                                                {
                                                    docObj.ProjectId = projectObj.ID;
                                                    docObj.ProjectName = projectObj.Name;

                                                    docObj.DocNo = dataRow["Column3"].ToString();
                                                    docObj.DocTitle = dataRow["Column4"].ToString();
                                                    docObj.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                    docObj.DocumentTypeName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                    docObj.StatusID = 0;
                                                    docObj.StatusName = string.Empty;

                                                    docObj.ReferenceFromID = 0;
                                                    docObj.ReferenceFromName = string.Empty;

                                                    docObj.DisciplineId = disciplineObj.ID;
                                                    docObj.DisciplineName = disciplineObj.FullName;

                                                    docObj.DeparmentId = departmentObj != null ? departmentObj.Id : 0;
                                                    docObj.DeparmentName = departmentObj != null ? departmentObj.FullName : string.Empty;

                                                  

                                                    docObj.IsCriticalDoc = false;
                                                    docObj.IsPriorityDoc = false;
                                                    docObj.IsVendorDoc = false;

                                                    docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                                    docObj.RevisionName = revObj != null ? revObj.Name : string.Empty;
                                                    //docObj.TypeId = 1;

                                                    var strPlanedDate = dataRow["Column6"].ToString();
                                                    var planedDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strPlanedDate, ref planedDate))
                                                    {
                                                        docObj.PlanedDate = planedDate;
                                                    }

                                                
                                                    
                                                    docObj.Complete = !string.IsNullOrEmpty(dataRow["Column9"].ToString())
                                                        ? Convert.ToDouble(dataRow["Column9"].ToString()) * 100
                                                        : 0;
                                                    docObj.Weight = !string.IsNullOrEmpty(dataRow["Column10"].ToString())
                                                        ? Convert.ToDouble(dataRow["Column10"].ToString()) * 100
                                                        : 0;
                                                  //  docObj.CommentCode = dataRow["Column15"].ToString().Trim();
                                                    docObj.UpdatedBy = UserSession.Current.User.Id;
                                                    docObj.UpdatedDate = DateTime.Now;
                                                    docObj.IsLeaf = true;
                                                    docObj.IsDelete = false;
                                                    docObj.IsEMDR = true;
                                                    documentPackageList.Add(docObj);
                                                }
                                            }
                                        }
                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {  foreach (var docObj in newDocumentPackageList)
                                        {
                                            var docId = this.documentPackageService.Insert(docObj);
                                           
                                        }

                                        foreach (var docObj in documentPackageList)
                                        {

                                            this.documentPackageService.Update(docObj);

                                          
                                            }

                                        documentPackageList = new List<DocumentPackage>();
                                        newDocumentPackageList = new List<DocumentPackage>();

                                        this.blockError.Visible = true;
                                        this.lblError.Text =
                                            "Data of document master list file is valid. System import successfull!";
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey",
                                            "CloseAndRebind();", true);
                                    }
                                }
                                else
                                {
                                    this.blockError.Visible = true;
                                    this.lblError.Text =
                                        "Have Error: Project '" + projectObj.Name + "' don't have Document Type '" +
                                        currentSheetName + "'. Please remove this sheet out of master file.";
                                }
                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }
                    }
                }
           }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}