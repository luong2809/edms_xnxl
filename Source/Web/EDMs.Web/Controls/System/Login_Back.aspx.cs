﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web
{
    using System.Configuration;

    using EDMs.Business.Services.Security;

    public partial class Login_Back : Page
    {
        #region Fields
        private readonly UserService _userService;
        private readonly UserAccessSystemService useraccess;
        private readonly CountUserAccessService countuser;

        #endregion

        public Login_Back()
        {
            _userService = new UserService();
            useraccess = new UserAccessSystemService();
            countuser = new CountUserAccessService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            txtUsername.Focus();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = Utility.GetMd5Hash(txtPassword.Text);

            var user = _userService.GetUserByUsername(username);
            if (user != null && user.Password == password)
            {
                UserSession.CreateSession(user);

                var UserLogin = new UsersAccessSystem();
                var date = this.countuser.GetDate(DateTime.Now.Date);
                UserLogin.UserId = UserSession.Current.User.Id;
                UserLogin.UserName = UserSession.Current.User.Username;
                UserLogin.UserFullName = UserSession.Current.User.FullName;
                UserLogin.CoutID = date.Id;
                UserLogin.DateAccess = DateTime.Now.Date;
                UserLogin.TimeAccess = DateTime.Now.ToString("hh:mm:ss");
                this.useraccess.Insert(UserLogin);

                FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                if (Session["ReturnURL"] != null && Session["ReturnURL"].ToString() != "/")
                {
                    var returnUrl = Session["ReturnURL"].ToString();
                    Session.Remove("ReturnURL");
                    Response.Redirect(returnUrl);
                }
                else
                {
                    Response.Redirect("~/EMDR.aspx");
                }
            }
            else
            {
                Session.Clear();
                FormsAuthentication.SignOut();
                lblMessage.Text = "Username or password is incorrect.";
            }
        }
    }
}