﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Workflow;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class SharePermissionPage : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly FolderService folderService = new FolderService();

        private readonly RoleService roleService = new RoleService();

        private readonly UserService userService = new UserService();
        private readonly AutoDistributionMatriceService autoDMService=new AutoDistributionMatriceService();
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly DisciplineService DisciplineService = new DisciplineService();

        private readonly PermissionDisciplineService PermissionDisciplineService = new PermissionDisciplineService();
      //  private readonly DocRoleMatrixService docRoleMatrixService = new DocRoleMatrixService();

       // private readonly DistributionMatrixDetailByUserService dmDetailByUserService = new DistributionMatrixDetailByUserService();

        private readonly ProjectUserService projectUserService = new ProjectUserService();

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                this.LoadComboData();
                this.LoadListPanel();
                this.LoadSystemPanel();

                this.btnSave.Visible = UserSession.Current.IsDC || UserSession.Current.IsManager;
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
            }
        }

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.ddlProject.SelectedItem != null)
            {
                foreach (RadListBoxItem user in this.ddlUser.SelectedItems)
                {
                    var projectUser = new ProjectUser()
                    {
                        UserId = Convert.ToInt32(user.Value),
                        UserName = user.Text,
                        ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                        ProjectName = this.ddlProject.SelectedItem.Text,
                        IsDC = UserSession.Current.IsDC,
                        IsManager = UserSession.Current.IsManager
                    };

                    this.projectUserService.Insert(projectUser);
                }
                this.grdPermission.Rebind();
            }
        }

        /// <summary>
        /// The ddl project_ selected index change.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void ddlProject_SelectedIndexChange(object sender, EventArgs e)
        {
            this.ddlUser.Items.Clear();
            this.ReloadUserList();
            this.grdPermission.Rebind();
        }

        /// <summary>
        /// The rtv package_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void rtvDiscipline_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.LoadPermissionData(e.Node.Value);
            this.grdPermission.Rebind();
        }

        /// <summary>
        /// The rtv package_ on node data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void rtvDiscipline_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "~/Images/Discipline.png";
        }

        /// <summary>
        /// The grd permission_ on need data source.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (this.ddlProject.SelectedItem != null)
            {
                var dataList = this.projectUserService.GetAllByProject(Convert.ToInt32(this.ddlProject.SelectedValue));
                this.grdPermission.DataSource = dataList;
            }
            else
            {
                this.grdPermission.DataSource = new List<ProjectUser>();
            }
        }

        /// <summary>
        /// The grd permission_ on detele command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.projectUserService.Delete(permissionId);
            this.ReloadUserList();
            this.grdPermission.Rebind();
        }

        /// <summary>
        /// The load permission data.
        /// </summary>
        /// <param name="packageId">
        /// The package id.
        /// </param>
        private void LoadPermissionData(string packageId)
        {
            var usersInPermission = this.PermissionDisciplineService.GetUserIdListInPermission(Convert.ToInt32(packageId)).Distinct().ToList();
            var listUser = this.userService.GetAll().Where(t => !usersInPermission.Contains(t.Id)).OrderBy(t => t.Username);

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "UserNameWithFullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        /// <summary>
        /// The load combo data.
        /// </summary>
        private void LoadComboData()
        {
            var projectList = new List<ScopeProject>();

            if (UserSession.Current.IsAdmin)
            {
                projectList = this.scopeProjectService.GetAll();
            }
            //else if (UserSession.Current.IsManager)
            //{
            //    var projectIdsInPermission = this.docRoleMatrixService.GetProjectIdsInPermissionByRole(UserSession.Current.RoleId);
            //    projectList = this.scopeProjectService.GetAll().Where(t => projectIdsInPermission.Contains(t.ID)).ToList();
            //}
            //else if (UserSession.Current.IsEngineer || UserSession.Current.IsLead)
            //{
            //    var projectIdsInPermission = this.dmDetailByUserService.GetProjectIdListInPermission(UserSession.Current.User.Id);
            //    projectList = this.scopeProjectService.GetAll().Where(t => projectIdsInPermission.Contains(t.ID)).ToList();
            //}
            else
            {
                projectList = this.scopeProjectService.GetAllByUser(UserSession.Current.User.Id);
            }

            if (projectList.Any())
            {
                this.ddlProject.DataSource = projectList;
                this.ddlProject.DataTextField = "FullName";
                this.ddlProject.DataValueField = "ID";
                this.ddlProject.DataBind();
                this.ddlProject.SelectedIndex = 0;
            }

            this.ReloadUserList();
        }

        /// <summary>
        /// The reload user list.
        /// </summary>
        private void ReloadUserList()
        {
            if (this.ddlProject.SelectedItem != null)
            {
                var existUser = this.projectUserService.GetAllByProject(Convert.ToInt32(this.ddlProject.SelectedValue)).Select(t => t.UserId).ToList();
                var userIdInProjectList = this.autoDMService.GetAllByProject(Convert.ToInt32(this.ddlProject.SelectedValue)).Select(t => t.UserId.GetValueOrDefault());
                existUser.Add(UserSession.Current.UserId);

                ////var userList = this.userService.GetByRole(UserSession.Current.IsDC, UserSession.Current.IsManager).Where(t => !existUser.Contains(t.Id));
                var userList = this.userService.GetAll().Where(t => userIdInProjectList.Contains(t.Id));

                //if (UserSession.Current.IsDC)
                //{
                    userList = userList.Where(t => t.IsDC.GetValueOrDefault()).ToList();
                //}
                //else if (UserSession.Current.IsManager)
                //{
                //    userList = userList.Where(t => t.IsLeader.GetValueOrDefault()).ToList();
                //}

                this.ddlUser.DataSource = userList.OrderBy(t => t.FullNameWithRole).ThenBy(t => t.Username);
                this.ddlUser.DataTextField = "FullNameWithRole";
                this.ddlUser.DataValueField = "Id";
                this.ddlUser.DataBind();
            }
        }

        /// <summary>
        /// The load list panel.
        /// </summary>
        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"~/Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    if (item.Text == "Share Permission")
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        /// <summary>
        /// The load system panel.
        /// </summary>
        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    if (item.Text == "Share Permission")
                    {
                        item.Selected = true;
                    }
                }
            }
        }
    }
}

