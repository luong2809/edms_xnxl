﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Business.Services.Scope;

namespace EDMs.Web.Controls.Scope
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.IO;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class VendorPackageEditForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly VendorPackageService WorkGroupService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public VendorPackageEditForm()
        {
            this.userService = new UserService();
            this.WorkGroupService = new  VendorPackageService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.categoryService = new CategoryService();
            this.scopeProjectService = new ScopeProjectService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                var projectInPermission = this.scopeProjectService.GetAll();
                this.ddlProject.DataSource = projectInPermission;
                this.ddlProject.DataTextField = "Name";
                this.ddlProject.DataValueField = "ID";
                this.ddlProject.DataBind();

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var objWorkGroup = this.WorkGroupService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objWorkGroup != null)
                    {
                        this.txtName.Text = objWorkGroup.Name;
                        this.txtDescription.Text = objWorkGroup.Description;
                        this.txtComplete.Value = objWorkGroup.Complete;
                        this.txtWeight.Value = objWorkGroup.Weight;
                        this.ddlProject.SelectedValue = objWorkGroup.ProjectId.ToString();
                        this.txtvendorname.Text = objWorkGroup.VendorName;
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectObj = this.scopeProjectService.GetById(projectId);

                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var WorkGroupId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.WorkGroupService.GetById(WorkGroupId);
                    if (obj != null)
                    {
                        // Create document Folder
                        var validDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&','-');
                        var oldValidDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name);
                        var projectFolder = Server.MapPath("../.." + projectObj.ProjectPath);

                        if (Directory.Exists(projectFolder) && validDisciplineName != oldValidDisciplineName)
                        {
                            Directory.Move(projectFolder + "\\05.Vendor\\" + oldValidDisciplineName,
                                              projectFolder + "\\05.Vendor\\" + validDisciplineName);
                        }
                        else
                        {
                            Directory.CreateDirectory(projectFolder + "\\05.Vendor\\" + validDisciplineName);
                        }


                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.Complete = this.txtComplete.Value.GetValueOrDefault();
                        obj.Weight = this.txtWeight.Value.GetValueOrDefault();
                        obj.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                        obj.ProjectName = this.ddlProject.SelectedItem.Text;
                        obj.VendorName = this.txtvendorname.Text;
                        obj.VendorPath = projectObj.ProjectPath + "/05.Vendor/" +
                                                Utilities.Utility.RemoveSpecialCharacterForFolder(
                                                    this.txtName.Text.Trim()).Replace('&', '-');
                        this.WorkGroupService.Update(obj);
                    }
                }
                else
                {
                    var obj = new VendorPackage()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        Weight = this.txtWeight.Value.GetValueOrDefault(),
                        Complete = this.txtComplete.Value.GetValueOrDefault(),
                        ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue),
                        ProjectName = this.ddlProject.SelectedItem.Text,
                        VendorPath = projectObj.ProjectPath + "/05.Vendor/" + Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim()).Replace('&', '-'),
                        VendorName=this.txtvendorname.Text
                    };

                    this.WorkGroupService.Insert(obj);

                    var validDisciplineName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name).Replace('&', '-');
                    var projectFolder = Server.MapPath("../.." + projectObj.ProjectPath);

                    Directory.CreateDirectory(projectFolder + "\\05.Vendor\\" + validDisciplineName);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }
    }
}