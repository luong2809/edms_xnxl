﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ScopeProjectEditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly ScopeProjectService ScopeProjectService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly DistributionMatrixService dmService;

        private readonly FolderService folderService;

        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ScopeProjectEditForm()
        {
            this.userService = new UserService();
            this.ScopeProjectService = new ScopeProjectService();
            this.dmService = new DistributionMatrixService();
            this.folderService = new FolderService();
            this.userDataPermissionService = new UserDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    
                    var objScopeProject = this.ScopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    this.CreatedInfo.Visible = objScopeProject != null;

                    if (objScopeProject != null)
                    {
                        
                        this.txtName.Text = objScopeProject.Name;
                        this.txtDescription.Text = objScopeProject.Description;
                        this.txtStartDate.SelectedDate = objScopeProject.StartDate;
                        this.txtEndDate.SelectedDate = objScopeProject.EndDate;
                        this.txtFrequency.Value = objScopeProject.FrequencyForProgressChart.GetValueOrDefault();
                        this.cbIsAutoDistribution.Checked = objScopeProject.IsAutoDistribution.GetValueOrDefault();
                        this.ddlProjectType.SelectedValue = objScopeProject.ProjectTypeId.GetValueOrDefault().ToString();
                        var createdUser = this.userService.GetByID(objScopeProject.CreatedBy.GetValueOrDefault());
                        this.txtOrganization1.Text = objScopeProject.organizationComment1;
                        this.txtOrganization2.Text = objScopeProject.organizationComment2;
                        this.txtOrganization3.Text = objScopeProject.organizationComment3;
                        this.ddlDC.SelectedValue = objScopeProject.DCId.GetValueOrDefault().ToString();
                        this.ddlPM.SelectedValue = objScopeProject.PMId.GetValueOrDefault().ToString();

                        this.lblCreated.Text = "Created at " + objScopeProject.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objScopeProject.UpdatedBy != null && objScopeProject.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objScopeProject.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objScopeProject.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var ScopeProjectId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.ScopeProjectService.GetById(ScopeProjectId);
                    if (obj != null)
                    {
                        // Create Document folder
                        var validProjectName = Utilities.Utility.RemoveSpecialCharacterForFolder(this.txtName.Text.Trim());
                        var oldvalidProjectName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name);
                        var targetFolder = "../../DocumentLibrary/Projects";
                        var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Projects/" + validProjectName;

                        var oldphysicalProjectFolder = Path.Combine(Server.MapPath(targetFolder), oldvalidProjectName);
                        var newphysicalProjectFolder = Path.Combine(Server.MapPath(targetFolder), validProjectName);
                        if (Directory.Exists(oldphysicalProjectFolder) && oldphysicalProjectFolder != newphysicalProjectFolder)
                        {
                            Directory.Move(oldphysicalProjectFolder, newphysicalProjectFolder);

                            if (!Directory.Exists(newphysicalProjectFolder + @"\04.Comment"))
                            {
                                Directory.CreateDirectory(newphysicalProjectFolder + @"\04.Comment");
                            }

                            if (!Directory.Exists(newphysicalProjectFolder + @"\05.Vendor"))
                            {
                                Directory.CreateDirectory(newphysicalProjectFolder + @"\05.Vendor");
                            }

                            if (!Directory.Exists(newphysicalProjectFolder + @"\06.For Construction"))
                            {
                                Directory.CreateDirectory(newphysicalProjectFolder + @"\06.For Construction");
                            }

                            if (!Directory.Exists(newphysicalProjectFolder + @"\07.Cutting"))
                            {
                                Directory.CreateDirectory(newphysicalProjectFolder + @"\07.Cutting");
                            }

                        }
                        else
                        {
                            Directory.CreateDirectory(newphysicalProjectFolder);
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\01.All Revision");
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\02.Latest Revision");
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\03.Shop Drawing\01.All Revision");
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\03.Shop Drawing\02.Latest Revision");
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\04.Comment");
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\05.Vendor");
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\06.For Construction");
                            Directory.CreateDirectory(newphysicalProjectFolder + @"\07.Cutting");
                        }
                        // --------------------------------------------------

                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.StartDate = this.txtStartDate.SelectedDate;
                        obj.EndDate = this.txtEndDate.SelectedDate;
                        obj.FrequencyForProgressChart = (int?) this.txtFrequency.Value;
                        obj.DCId = this.ddlDC.SelectedItem != null
                            ? Convert.ToInt32(this.ddlDC.SelectedValue)
                            : 0;
                        obj.DCName = this.ddlDC.SelectedItem != null
                            ? this.ddlDC.SelectedItem.Text
                            : string.Empty;
                        obj.PMId = this.ddlPM.SelectedItem != null
                            ? Convert.ToInt32(this.ddlPM.SelectedValue)
                            : 0;
                        obj.PMName = this.ddlPM.SelectedItem != null
                            ? this.ddlPM.SelectedItem.Text
                            : string.Empty;

                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;
                        obj.ProjectTypeId = Convert.ToInt32(this.ddlProjectType.SelectedValue);
                        obj.ProjectTypeName = this.ddlProjectType.SelectedItem.Text;
                        obj.organizationComment1 = this.txtOrganization1.Text;
                        obj.organizationComment2 = this.txtOrganization2.Text;
                        obj.organizationComment3 = this.txtOrganization3.Text;
                        obj.ProjectPath = serverFolder;
                        //obj.IsAutoDistribution = this.cbIsAutoDistribution.Checked;
                        this.ScopeProjectService.Update(obj);

                        //if (!this.cbIsAutoDistribution.Checked)
                        //{
                            if (!this.dmService.GetByProject(obj.ID).Any())
                            {
                                var dmLeadObj = new DistributionMatrix()
                                {
                                    Name = obj.Name + "-" + "EMDR-DMatrix_Lead",

                                    Description = "EMDR Documents Distribution Matrix of project \"" + obj.Name + "\" for Leads",

                                    ProjectId = obj.ID,
                                    ProjectName = obj.Name,
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now,
                                    RoleId = 0,
                                    RoleName = "General",
                                    IsHaveDetail = false,
                                    TypeEngineerId = 1,
                                    TypeId = 1,
                                    TypeName = "EMDR",
                                    IsCanDelete = false
                                };

                                var dmEngineerObj = new DistributionMatrix()
                                {
                                    Name = obj.Name + "-" + "EMDR-DMatrix_Engineer",

                                    Description = "EMDR Documents Distribution Matrix of project \"" + obj.Name + "\" for Engineers",

                                    ProjectId = obj.ID,
                                    ProjectName = obj.Name,
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now,
                                    RoleId = 0,
                                    RoleName = "General",
                                    IsHaveDetail = false,
                                    TypeEngineerId = 2,
                                    TypeId = 1,
                                    TypeName = "EMDR",
                                    IsCanDelete = false
                                };

                                this.dmService.Insert(dmLeadObj);
                                this.dmService.Insert(dmEngineerObj);
                            }
                        //}

                        // Create/Update Out Trans folder
                        ////var outTransTargetFolder = "../../DocumentLibrary/SharedDoc";
                        ////var oldOutTransphysicalProjectFolder = Path.Combine(Server.MapPath(outTransTargetFolder), oldvalidProjectName);
                        ////var newOutTransphysicalProjectFolder = Path.Combine(Server.MapPath(outTransTargetFolder), validProjectName);
                        ////if (Directory.Exists(oldOutTransphysicalProjectFolder) && oldOutTransphysicalProjectFolder != newOutTransphysicalProjectFolder)
                        ////{
                        ////    Directory.Move(oldOutTransphysicalProjectFolder, newOutTransphysicalProjectFolder);
                        ////}
                        ////else
                        ////{
                        ////    Directory.CreateDirectory(newOutTransphysicalProjectFolder);
                        ////    Directory.CreateDirectory(newOutTransphysicalProjectFolder + @"\01. Transmittals");
                        ////}
                        // --------------------------------------------------

                        // Create Folder on Document Library
                        var rootFolder = this.folderService.GetById(2883);
                        var projectfolder = new Folder()
                        {
                            Name = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name),
                            Description = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name),
                            ParentID = rootFolder.ID,
                            DirName = rootFolder.DirName + "/" + Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name),
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ProjectId = obj.ID,
                            ProjectName = obj.Name
                        };

                        if (!Directory.Exists(Server.MapPath(projectfolder.DirName)))
                        {
                            Directory.CreateDirectory(Server.MapPath(projectfolder.DirName));
                            var projectFolderId = this.folderService.Insert(projectfolder);
                            var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(rootFolder.ID);
                            foreach (var parentPermission in usersInPermissionOfParent)
                            {
                                var childPermission = new UserDataPermission()
                                {
                                    CategoryId = parentPermission.CategoryId,
                                    RoleId = parentPermission.RoleId,
                                    FolderId = projectFolderId,
                                    UserId = parentPermission.UserId,
                                    IsFullPermission = parentPermission.IsFullPermission,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = UserSession.Current.User.Id
                                };

                                this.userDataPermissionService.Insert(childPermission);
                            }

                            var transfolderOut = new Folder()
                            {
                                Name = "01. Transmittals Out",
                                Description = "01. Transmittals Out",
                                ParentID = projectfolder.ID,
                                DirName = projectfolder.DirName + "/"+RemoveAllSpecialCharacter("01. Transmittals Out"),
                                CreatedBy = 1,
                                CreatedDate = DateTime.Now,
                                ProjectId = obj.ID,
                                ProjectName = obj.Name
                            };

                            Directory.CreateDirectory(Server.MapPath(transfolderOut.DirName));
                            var transFolderId = this.folderService.Insert(transfolderOut);
                            var usersInPermissionOfProjectFolder = this.userDataPermissionService.GetAllByFolder(projectfolder.ID);
                            foreach (var parentPermission in usersInPermissionOfProjectFolder)
                            {
                                var childPermission = new UserDataPermission()
                                {
                                    CategoryId = parentPermission.CategoryId,
                                    RoleId = parentPermission.RoleId,
                                    FolderId = transFolderId,
                                    UserId = parentPermission.UserId,
                                    IsFullPermission = parentPermission.IsFullPermission,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = UserSession.Current.User.Id
                                };

                                this.userDataPermissionService.Insert(childPermission);
                            }



                            var transfolderIn = new Folder()
                            {
                                Name = "02. Transmittals In",
                                Description = "02. Transmittals In",
                                ParentID = projectfolder.ID,
                                DirName = projectfolder.DirName + "/"+ RemoveAllSpecialCharacter("02. Transmittals In"),
                                CreatedBy = 1,
                                CreatedDate = DateTime.Now,
                                ProjectId = obj.ID,
                                ProjectName = obj.Name
                            };

                            Directory.CreateDirectory(Server.MapPath(transfolderIn.DirName));
                            var transFolderIdin = this.folderService.Insert(transfolderIn);
                            var usersInPermissionOfProjectFolderIn = this.userDataPermissionService.GetAllByFolder(projectfolder.ID);
                            foreach (var parentPermission in usersInPermissionOfProjectFolderIn)
                            {
                                var childPermission = new UserDataPermission()
                                {
                                    CategoryId = parentPermission.CategoryId,
                                    RoleId = parentPermission.RoleId,
                                    FolderId = transFolderIdin,
                                    UserId = parentPermission.UserId,
                                    IsFullPermission = parentPermission.IsFullPermission,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = UserSession.Current.User.Id
                                };

                                this.userDataPermissionService.Insert(childPermission);
                            }





                            /////////venfor trans
                            var vendorfolder = new Folder()
                            {
                                Name = "Vendor",
                                Description = "Vendor",
                                ParentID = projectfolder.ID,
                                DirName = projectfolder.DirName + "/" + RemoveAllSpecialCharacter("Vendor"),
                                CreatedBy = 1,
                                CreatedDate = DateTime.Now,
                                ProjectId = obj.ID,
                                ProjectName = obj.Name
                            };

                            Directory.CreateDirectory(Server.MapPath(vendorfolder.DirName));
                            var vendorfolderId = this.folderService.Insert(vendorfolder);
                            var usersInPermissionOfvendorfolder = this.userDataPermissionService.GetAllByFolder(projectfolder.ID);
                            foreach (var parentPermission in usersInPermissionOfvendorfolder)
                            {
                                var childPermission = new UserDataPermission()
                                {
                                    CategoryId = parentPermission.CategoryId,
                                    RoleId = parentPermission.RoleId,
                                    FolderId = vendorfolderId,
                                    UserId = parentPermission.UserId,
                                    IsFullPermission = parentPermission.IsFullPermission,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = UserSession.Current.User.Id
                                };

                                this.userDataPermissionService.Insert(childPermission);
                            }

                            var vendorfolderOut = new Folder()
                            {
                                Name = "01. Transmittals Out",
                                Description = "01. Transmittals Out",
                                ParentID = vendorfolder.ID,
                                DirName = vendorfolder.DirName + "/" + RemoveAllSpecialCharacter("01. Transmittals Out"),
                                CreatedBy = 1,
                                CreatedDate = DateTime.Now,
                                ProjectId = obj.ID,
                                ProjectName = obj.Name
                            };

                            Directory.CreateDirectory(Server.MapPath(vendorfolderOut.DirName));
                            var vendorfolderOutId = this.folderService.Insert(vendorfolderOut);
                            var usersInPermissionOfvendorfolderOut = this.userDataPermissionService.GetAllByFolder(vendorfolder.ID);
                            foreach (var parentPermission in usersInPermissionOfvendorfolderOut)
                            {
                                var childPermission = new UserDataPermission()
                                {
                                    CategoryId = parentPermission.CategoryId,
                                    RoleId = parentPermission.RoleId,
                                    FolderId = vendorfolderOutId,
                                    UserId = parentPermission.UserId,
                                    IsFullPermission = parentPermission.IsFullPermission,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = UserSession.Current.User.Id
                                };

                                this.userDataPermissionService.Insert(childPermission);
                            }



                            var vendorfolderIn = new Folder()
                            {
                                Name = "02. Transmittals In",
                                Description = "02. Transmittals In",
                                ParentID = vendorfolder.ID,
                                DirName = vendorfolder.DirName + "/" + RemoveAllSpecialCharacter("02. Transmittals In"),
                                CreatedBy = 1,
                                CreatedDate = DateTime.Now,
                                ProjectId = obj.ID,
                                ProjectName = obj.Name
                            };

                            Directory.CreateDirectory(Server.MapPath(transfolderIn.DirName));
                            var vendorfolderInId = this.folderService.Insert(transfolderIn);
                            var usersInPermissionOfvendorfolderIn = this.userDataPermissionService.GetAllByFolder(vendorfolder.ID);
                            foreach (var parentPermission in usersInPermissionOfvendorfolderIn)
                            {
                                var childPermission = new UserDataPermission()
                                {
                                    CategoryId = parentPermission.CategoryId,
                                    RoleId = parentPermission.RoleId,
                                    FolderId = vendorfolderInId,
                                    UserId = parentPermission.UserId,
                                    IsFullPermission = parentPermission.IsFullPermission,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = UserSession.Current.User.Id
                                };

                                this.userDataPermissionService.Insert(childPermission);
                            }
                        }
                        //---------------------------------------------------------------------------
                    }
                }
                else
                {
                    var obj = new ScopeProject()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        StartDate = this.txtStartDate.SelectedDate,
                        EndDate = this.txtEndDate.SelectedDate,
                        FrequencyForProgressChart = (int?) this.txtFrequency.Value,
                        DCId = this.ddlDC.SelectedItem != null
                            ? Convert.ToInt32(this.ddlDC.SelectedValue)
                            : 0,
                        DCName = this.ddlDC.SelectedItem != null
                            ? this.ddlDC.SelectedItem.Text
                            : string.Empty,
                        PMId = this.ddlPM.SelectedItem != null
                            ? Convert.ToInt32(this.ddlPM.SelectedValue)
                            : 0,
                        PMName = this.ddlPM.SelectedItem != null
                            ? this.ddlPM.SelectedItem.Text
                            : string.Empty,

                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        ProjectTypeId = Convert.ToInt32(this.ddlProjectType.SelectedValue),
                        ProjectTypeName = this.ddlProjectType.SelectedItem.Text,
                        //IsAutoDistribution = this.cbIsAutoDistribution.Checked,
                        organizationComment1=this.txtOrganization1.Text,
                        organizationComment2=this.txtOrganization2.Text,
                        organizationComment3=this.txtOrganization3.Text
                    };

                    var projectId = this.ScopeProjectService.Insert(obj);
                    if (projectId != null)
                    {
                        // Create document Folder
                        var validProjectName = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name);
                        var targetFolder = "../../DocumentLibrary/Projects";
                        var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/Projects/" + validProjectName;

                        var physicalProjectFolder = Path.Combine(Server.MapPath(targetFolder), validProjectName);

                        if (!Directory.Exists(physicalProjectFolder))
                        {
                            Directory.CreateDirectory(physicalProjectFolder);}
                            Directory.CreateDirectory(physicalProjectFolder + @"\01.All Revision");
                            Directory.CreateDirectory(physicalProjectFolder + @"\02.Latest Revision");
                            Directory.CreateDirectory(physicalProjectFolder + @"\03.Shop Drawing\01.All Revision");
                            Directory.CreateDirectory(physicalProjectFolder + @"\03.Shop Drawing\02.Latest Revision");
                            Directory.CreateDirectory(physicalProjectFolder + @"\04.Comment");
                            Directory.CreateDirectory(physicalProjectFolder + @"\05.Vendor");
                            Directory.CreateDirectory(physicalProjectFolder + @"\06.For Construction");
                            Directory.CreateDirectory(physicalProjectFolder + @"\07.Cutting");
                           
                        
                        obj.ProjectPath = serverFolder;

                        this.ScopeProjectService.Update(obj);
                        // --------------------------------------------------

                        // Create Out Trans folder
                        var outTransTargetFolder = "../../DocumentLibrary/SharedDoc";
                        var outTransPhysicalProjectFolder = Path.Combine(Server.MapPath(outTransTargetFolder), validProjectName);
                        if (!Directory.Exists(outTransPhysicalProjectFolder))
                        {
                            Directory.CreateDirectory(outTransPhysicalProjectFolder);
                            Directory.CreateDirectory(outTransPhysicalProjectFolder + @"\"+RemoveAllSpecialCharacter("01. Transmittals Out"));
                        }
                        // --------------------------------------------------

                        // Create Folder on Document Library
                        var rootFolder = this.folderService.GetById(2883);
                        var projectfolder = new Folder()
                        {
                            Name = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name),
                            Description = Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name),
                            ParentID = rootFolder.ID,
                            DirName = rootFolder.DirName + "/" + Utilities.Utility.RemoveSpecialCharacterForFolder(obj.Name),
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ProjectId = obj.ID,
                            ProjectName = obj.Name
                        };

                        //Directory.CreateDirectory(Server.MapPath(projectfolder.DirName));
                        var projectFolderId = this.folderService.Insert(projectfolder);
                        var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(rootFolder.ID);
                        foreach (var parentPermission in usersInPermissionOfParent)
                        {
                            var childPermission = new UserDataPermission()
                            {
                                CategoryId = parentPermission.CategoryId,
                                RoleId = parentPermission.RoleId,
                                FolderId = projectFolderId,
                                UserId = parentPermission.UserId,
                                IsFullPermission = parentPermission.IsFullPermission,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            };

                            this.userDataPermissionService.Insert(childPermission);
                        }

                        var transfolder = new Folder()
                        {
                            Name = "01. Transmittals Out",
                            Description = "01. Transmittals Out",
                            ParentID = projectfolder.ID,
                            DirName = projectfolder.DirName + "/" + RemoveAllSpecialCharacter("01. Transmittals Out"),
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ProjectId = obj.ID,
                            ProjectName = obj.Name
                        };

                        //Directory.CreateDirectory(Server.MapPath(transfolder.DirName));
                        var transFolderId = this.folderService.Insert(transfolder);
                        var usersInPermissionOfProjectFolder = this.userDataPermissionService.GetAllByFolder(projectfolder.ID);
                        foreach (var parentPermission in usersInPermissionOfProjectFolder)
                        {
                            var childPermission = new UserDataPermission()
                            {
                                CategoryId = parentPermission.CategoryId,
                                RoleId = parentPermission.RoleId,
                                FolderId = transFolderId,
                                UserId = parentPermission.UserId,
                                IsFullPermission = parentPermission.IsFullPermission,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            };

                            this.userDataPermissionService.Insert(childPermission);
                        }


                        var transfolderIn = new Folder()
                        {
                            Name = "02. Transmittals In",
                            Description = "02. Transmittals In",
                            ParentID = projectfolder.ID,
                            DirName = projectfolder.DirName + "/" + RemoveAllSpecialCharacter("02. Transmittals In"),
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ProjectId = obj.ID,
                            ProjectName = obj.Name
                        };

                        Directory.CreateDirectory(Server.MapPath(transfolderIn.DirName));
                        var transFolderIdin = this.folderService.Insert(transfolderIn);
                        var usersInPermissionOfProjectFolderIn = this.userDataPermissionService.GetAllByFolder(projectfolder.ID);
                        foreach (var parentPermission in usersInPermissionOfProjectFolderIn)
                        {
                            var childPermission = new UserDataPermission()
                            {
                                CategoryId = parentPermission.CategoryId,
                                RoleId = parentPermission.RoleId,
                                FolderId = transFolderIdin,
                                UserId = parentPermission.UserId,
                                IsFullPermission = parentPermission.IsFullPermission,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            };

                            this.userDataPermissionService.Insert(childPermission);
                        }

                        /////////venfor trans
                        var vendorfolder = new Folder()
                        {
                            Name = "Vendor",
                            Description = "Vendor",
                            ParentID = projectfolder.ID,
                            DirName = projectfolder.DirName + "/" + RemoveAllSpecialCharacter("Vendor"),
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ProjectId = obj.ID,
                            ProjectName = obj.Name
                        };

                        Directory.CreateDirectory(Server.MapPath(vendorfolder.DirName));
                        var vendorfolderId = this.folderService.Insert(vendorfolder);
                        var usersInPermissionOfvendorfolder = this.userDataPermissionService.GetAllByFolder(projectfolder.ID);
                        foreach (var parentPermission in usersInPermissionOfvendorfolder)
                        {
                            var childPermission = new UserDataPermission()
                            {
                                CategoryId = parentPermission.CategoryId,
                                RoleId = parentPermission.RoleId,
                                FolderId = vendorfolderId,
                                UserId = parentPermission.UserId,
                                IsFullPermission = parentPermission.IsFullPermission,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            };

                            this.userDataPermissionService.Insert(childPermission);
                        }

                        var vendorfolderOut = new Folder()
                        {
                            Name = "01. Transmittals Out",
                            Description = "01. Transmittals Out",
                            ParentID = vendorfolder.ID,
                            DirName = vendorfolder.DirName + "/" + RemoveAllSpecialCharacter("01. Transmittals Out"),
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ProjectId = obj.ID,
                            ProjectName = obj.Name
                        };

                        Directory.CreateDirectory(Server.MapPath(vendorfolderOut.DirName));
                        var vendorfolderOutId = this.folderService.Insert(vendorfolderOut);
                        var usersInPermissionOfvendorfolderOut = this.userDataPermissionService.GetAllByFolder(vendorfolder.ID);
                        foreach (var parentPermission in usersInPermissionOfvendorfolderOut)
                        {
                            var childPermission = new UserDataPermission()
                            {
                                CategoryId = parentPermission.CategoryId,
                                RoleId = parentPermission.RoleId,
                                FolderId = vendorfolderOutId,
                                UserId = parentPermission.UserId,
                                IsFullPermission = parentPermission.IsFullPermission,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            };

                            this.userDataPermissionService.Insert(childPermission);
                        }



                        var vendorfolderIn = new Folder()
                        {
                            Name = "02. Transmittals In",
                            Description = "02. Transmittals In",
                            ParentID = vendorfolder.ID,
                            DirName = vendorfolder.DirName + "/" + RemoveAllSpecialCharacter("02. Transmittals In"),
                            CreatedBy = 1,
                            CreatedDate = DateTime.Now,
                            ProjectId = obj.ID,
                            ProjectName = obj.Name
                        };

                        Directory.CreateDirectory(Server.MapPath(transfolderIn.DirName));
                        var vendorfolderInId = this.folderService.Insert(transfolderIn);
                        var usersInPermissionOfvendorfolderIn = this.userDataPermissionService.GetAllByFolder(vendorfolder.ID);
                        foreach (var parentPermission in usersInPermissionOfvendorfolderIn)
                        {
                            var childPermission = new UserDataPermission()
                            {
                                CategoryId = parentPermission.CategoryId,
                                RoleId = parentPermission.RoleId,
                                FolderId = vendorfolderInId,
                                UserId = parentPermission.UserId,
                                IsFullPermission = parentPermission.IsFullPermission,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            };

                            this.userDataPermissionService.Insert(childPermission);
                        }

                        //---------------------------------------------------------------------------

                        // Create default Distribution Matrix
                        //if (!this.cbIsAutoDistribution.Checked)
                        //{
                            var dmLeadObj = new DistributionMatrix()
                            {
                                Name = obj.Name + "-" + "EMDR - DMatrix_Lead",

                                Description = "EMDR Documents Distribution Matrix of project \"" + obj.Name + "\" for Leads",

                                ProjectId = obj.ID,
                                ProjectName = obj.Name,
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now,
                                RoleId = 0,
                                RoleName = "General",
                                IsHaveDetail = false,
                                TypeEngineerId = 1,
                                TypeId = 1,
                                TypeName = "EMDR",
                                IsCanDelete = false
                            };

                            var dmEngineerObj = new DistributionMatrix()
                            {
                                Name = obj.Name + "-" + "EMDR-DMatrix_Engineer",

                                Description = "EMDR Documents Distribution Matrix of project \"" + obj.Name + "\" for Engineers",

                                ProjectId = obj.ID,
                                ProjectName = obj.Name,
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now,
                                RoleId = 0,
                                RoleName = "General",
                                IsHaveDetail = false,
                                TypeEngineerId = 2,
                                TypeId = 1,
                                TypeName = "EMDR",
                                IsCanDelete = false
                            };

                            this.dmService.Insert(dmLeadObj);
                            this.dmService.Insert(dmEngineerObj);
                        //}
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        private string RemoveAllSpecialCharacter(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z]+", string.Empty);
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            var userList = this.userService.GetAll();
            var dcList = userList.Where(t => t.IsDC.GetValueOrDefault()).ToList();
            var pmList = userList.Where(t => t.IsManager.GetValueOrDefault() || t.IsLeader.GetValueOrDefault()).ToList();

            dcList.Insert(0, new User(){Id = 0});
            pmList.Insert(0, new User() { Id = 0 });

            this.ddlDC.DataSource = dcList;
            this.ddlDC.DataValueField = "Id";
            this.ddlDC.DataTextField = "FullName";
            this.ddlDC.DataBind();

            this.ddlPM.DataSource = pmList;
            this.ddlPM.DataValueField = "Id";
            this.ddlPM.DataTextField = "FullName";
            this.ddlPM.DataBind();
        }
    }
}