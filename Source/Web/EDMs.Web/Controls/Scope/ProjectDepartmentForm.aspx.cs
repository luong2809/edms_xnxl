﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Scope
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ProjectDepartmentForm : Page
    {
        private readonly RoleService roleService;

        private readonly ProjectDepartmentService projectDepartmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ProjectDepartmentForm()
        {
            this.roleService = new RoleService();
            this.projectDepartmentService = new ProjectDepartmentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            if (this.ddlDepartment.SelectedItem != null && !string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var item = new ProjectDepartment()
                {
                    ProjectId = Convert.ToInt32(this.Request.QueryString["projId"]),
                    DepartmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue),
                    DepartmentName = this.ddlDepartment.SelectedItem.Text,
                    CreatedBy = UserSession.Current.UserId,
                    CreatedDate = DateTime.Now
                };

                this.projectDepartmentService.Insert(item);

                this.LoadComboData();
                this.grdPermission.Rebind();
            }

        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        private void LoadComboData()
        {
            var departmentList = this.roleService.GetAll(false);

            var porjectId = Convert.ToInt32(this.Request.QueryString["projId"]);
            var departmentExistList = this.projectDepartmentService.GetAllByProject(porjectId).Select(t => t.DepartmentId.GetValueOrDefault()).ToList();

            this.ddlDepartment.DataSource = departmentList.Where(t => !departmentExistList.Contains(t.Id));
            this.ddlDepartment.DataValueField = "Id";
            this.ddlDepartment.DataTextField = "FullName";
            this.ddlDepartment.DataBind();
        }

        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var porjectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var departmentList = this.projectDepartmentService.GetAllByProject(porjectId);
                this.grdPermission.DataSource = departmentList;
            }
        }

        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var itemId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.projectDepartmentService.Delete(itemId);

            this.LoadComboData();
        }
    }
}