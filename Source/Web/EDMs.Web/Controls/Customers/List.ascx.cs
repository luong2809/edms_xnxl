﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="List.ascx.cs" company="">
//   
// </copyright>
// <summary>
//   The list.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Customers
{
    /// <summary>
    /// The list.
    /// </summary>
    public partial class List : UserControl
    {
        public string customerTitle = string.Empty;
        public string customerType = string.Empty;

        public string CustomerTitle
        {

            get { return customerTitle; }

            set { customerTitle = value; }

        }

        public string CustomerType
        {

            get { return customerType; }

            set { customerType = value; }

        }

        /// <summary>
        /// Gets ListPatientStatus.
        /// </summary>
        public List<PatientStatus> ListPatientStatus
        {
            get
            {
                var patientStatusService = new PatientStatusService();
                return patientStatusService.GetAll();
            }
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
                LoadCustomerByCustomerType();

            // call function load customers by Type
        }


        /// <summary>
        /// The load customer by customer type.
        /// </summary>
        protected void LoadCustomerByCustomerType()
        {
            var patientService = new PatientService();
            if (customerType == string.Empty || int.Parse(customerType) == 0)
            {
                grdKhachHang.DataSource = patientService.GetAll();
            }
            else
            {
                grdKhachHang.DataSource = patientService.GetByPatient(int.Parse(customerType));
            }
            grdKhachHang.DataBind();
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdKhachHang_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var patientStatusService = new PatientStatusService();

            grdKhachHang.DataSource = patientStatusService.GetAll();
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdKhachHang_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format("return ShowEditForm('{0}','{1}');",
                                                               e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex][
                                                                   "Id"], e.Item.ItemIndex);
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdKhachHang.MasterTableView.SortExpressions.Clear();
                grdKhachHang.MasterTableView.GroupByExpressions.Clear();
                grdKhachHang.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdKhachHang.MasterTableView.SortExpressions.Clear();
                grdKhachHang.MasterTableView.GroupByExpressions.Clear();
                grdKhachHang.MasterTableView.CurrentPageIndex = grdKhachHang.MasterTableView.PageCount - 1;
                grdKhachHang.Rebind();
            }
        }

        /// <summary>
        /// grdKhachHang Page Size Changed
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        //protected void grdKhachHang_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        //{
        //    grdKhachHang.PageSize = e.NewPageSize;
        //    var patientService = new PatientService();
        //    grdKhachHang.DataSource = patientService.GetAll();
        //    grdKhachHang.DataBind();
        //}

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdKhachHang_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var patientID = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
            var patientService = new PatientService();
            patientService.Delete(patientID);
        }
    }
}