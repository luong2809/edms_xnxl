﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="EDMs.Web.Controls.Customers.List" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    #ctl00_ContentPlaceHolder2_CustomerList_ctl00_ContentPlaceHolder2_CustomerListPanel,#ctl00_ContentPlaceHolder2_CustomerList_ctl00_ContentPlaceHolder2_CustomerList_grdKhachHangPanel
    {
        height:100% !important;
    }

</style>

<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdKhachHang"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdKhachHang">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdKhachHang"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="treeCustomerType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="CustomerList" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>
</telerik:RadCodeBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
    <Windows>
        <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Thông tin khách hàng" Height="500px" VisibleStatusbar="False"
            Width="630px" MaxHeight="500px" MaxWidth="630px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
            Modal="true">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
<telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
<telerik:RadGrid ID="grdKhachHang"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    CellSpacing="0"
    CellPadding="0" 
    PageSize="30" Height="100%"
    GridLines="None" 
    OnNeedDataSource="grdKhachHang_OnNeedDataSource"
    OnItemCreated="grdKhachHang_ItemCreated"
    OnDeleteCommand="grdKhachHang_DeleteCommand">

    <MasterTableView DataKeyNames="Id" ClientDataKeyNames="Id" CommandItemDisplay="Top" Width="100%" >
        <NoRecordsTemplate>Chưa có dữ liệu.</NoRecordsTemplate>

        <PagerStyle AlwaysVisible="True" FirstPageToolTip="Trang đầu" LastPageToolTip="Trang cuối" NextPagesToolTip="Trang sau"
            NextPageToolTip="Trang sau" PagerTextFormat="Đổi trang: {4} &amp;nbsp;Trang &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Tổng:  &lt;strong&gt;{5}&lt;/strong&gt; khách hàng."
            PrevPagesToolTip="Trang trước" PrevPageToolTip="Trang trước" PageSizeLabelText="Số dòng trên mỗi trang: " />
        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
        <Columns>
            <telerik:GridTemplateColumn>
                <HeaderStyle Width="50" />
                <ItemTemplate>
                    <asp:Image runat="server" Style="padding-left: 7px; cursor: pointer;" ID="EditLink" ImageUrl="../../Images/edit.png" />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridButtonColumn Text="Xóa" CommandName="Delete">
                <HeaderStyle Width="36px"></HeaderStyle>
            </telerik:GridButtonColumn>
            <telerik:GridTemplateColumn HeaderText="STT">
                <HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
                <ItemStyle Font-Bold="True" HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:Label align="center" ID="lblSTT" Enabled="True" runat="server" Text='<%# grdKhachHang.CurrentPageIndex*grdKhachHang.PageSize + grdKhachHang.Items.Count+1 %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn DataField="Id" HeaderText="Mã" UniqueName="Id" Visible="False" />
            <telerik:GridBoundColumn DataField="SSN" HeaderText="Mã" UniqueName="SSN" />

            <telerik:GridBoundColumn DataField="FullName" HeaderText="Tên KH" UniqueName="FullName" />

            <telerik:GridBoundColumn DataField="Sex" HeaderText="Giới tính" UniqueName="Gender" />

            <telerik:GridBoundColumn DataField="CellPhone" HeaderText="Phone" UniqueName="Phone" />

            <telerik:GridBoundColumn DataField="Address1" HeaderText="Địa chỉ" UniqueName="Address" />

            <telerik:GridBoundColumn DataField="Email" HeaderText="Email" UniqueName="Email" />

            <telerik:GridBoundColumn DataField="PatientStatus.Name" HeaderText="Tình trạng dữ liệu" UniqueName="PatientStatus" />

            <telerik:GridTemplateColumn HeaderText="Ngày sinh" UniqueName="Birthday" DataField="DateOfBirth">
                <ItemTemplate>
                    <asp:Label ID="lblFileDinhKem" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "DateOfBirth")).ToString("dd/MM/yyyy")  %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
        <CommandItemStyle Height="25px"></CommandItemStyle>
        <CommandItemTemplate>
            <a href="#" onclick="return ShowInsertForm();" style="padding-left: 7px">
                <img src="././Images/addNew.png" />
                Thêm mới khách hàng</a>
        </CommandItemTemplate>
    </MasterTableView>
    <ClientSettings>
        <Selecting AllowRowSelect="true"></Selecting>
        <ClientEvents OnRowDblClick="RowDblClick"></ClientEvents>
        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
    </ClientSettings>
</telerik:RadGrid>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function ShowEditForm(id, rowIndex) {
            var grid = $find("<%= grdKhachHang.ClientID %>");

            var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
            grid.get_masterTableView().selectItem(rowControl, true);

            window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
            return false;
        }
        function ShowInsertForm() {
            window.radopen("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");
            return false;
        }

        function refreshGrid(arg) {
            if (!arg) {
                $find("<%# ajaxCustomer.ClientID %>").ajaxRequest("Rebind");
            }
            else {
                $find("<%= ajaxCustomer.ClientID %>").ajaxRequest("RebindAndNavigate");
            }
        }
        function RowDblClick(sender, eventArgs) {
            window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
        }
    </script>
</telerik:RadCodeBlock>
