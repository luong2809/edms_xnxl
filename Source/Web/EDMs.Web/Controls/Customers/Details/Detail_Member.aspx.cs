﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Customers
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Detail_Member : Page
    {
        /// <summary>
        /// The patient status service
        /// </summary>
        private Patient_MemberCardService _patientMemberCardService;


        public Detail_Member()
        {
            _patientMemberCardService = new Patient_MemberCardService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
               
            }
        }

        protected void grdMemberCard_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                var patient = Request.QueryString["ID"];
                if (patient != null)
                {
                    int tempID;
                    if (int.TryParse(patient, out tempID))
                    {
                        var listCard = _patientMemberCardService.Find(x => x.CustomerID == tempID);
                        grdMemberCard.DataSource = listCard.OrderByDescending(t => t.UpdatedDate);
                    }
                }
            }
        }
        protected void grdMemberCard_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            int csID = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            _patientMemberCardService.Delete(csID);
        }
        protected void grdMemberCard_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format("return ShowEditForm('{0}','{1}');",
                                                               e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex][
                                                                   "Id"], e.Item.ItemIndex);
            }
        }
    }
}