﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Detail_Description.aspx.cs" Inherits="EDMs.Web.Controls.Customers.Detail_Description" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ghi chú</title>
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <script src="../../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        function RowDblClick(sender, eventArgs) {
            window.parent.radopen("Controls/Customers/Details/Detail_Description_Edit.aspx?ID=" + eventArgs.getDataKeyValue("ID"), "CustomerDialog");
        }
        $(document).ready(function () {
            $(".content").fadeIn("slow");
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content" style="display: none;">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <telerik:RadGrid ID="grdDescriptionHistory" runat="server"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                Width="100%" Skin="Windows7" ShowFooter="true"
                OnItemCreated="grdDescriptionHistory_ItemCreated"
                OnNeedDataSource="grdDescriptionHistory_NeedDataSource"
                OnDeleteCommand="grdDescriptionHistory_DeleteCommand">
                <MasterTableView ClientDataKeyNames="ID,CustomerID" DataKeyNames="ID">
                    <NoRecordsTemplate>
                        Chưa có dữ liệu.
                    </NoRecordsTemplate>
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="Trang đầu" LastPageToolTip="Trang cuối" NextPagesToolTip="Trang sau" NextPageToolTip="Trang sau" PagerTextFormat="Đổi trang: {4} &amp;nbsp;Trang &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Tổng:  &lt;strong&gt;{5}&lt;/strong&gt; dòng." PageSizeLabelText="Số dòng trên mỗi trang: " PrevPagesToolTip="Trang trước" PrevPageToolTip="Trang trước" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="STT">
                            <HeaderStyle HorizontalAlign="Center" Width="30" />
                            <ItemStyle Font-Bold="True" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblSTT" runat="server" align="center" Enabled="True" Text="<%# grdDescriptionHistory.CurrentPageIndex*grdDescriptionHistory.PageSize + grdDescriptionHistory.Items.Count+1 %>" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="ID" HeaderText="ID" UniqueName="ID" Visible="False" />
                        <telerik:GridBoundColumn DataField="Content" HeaderText="Nội dung" UniqueName="Content" Aggregate="Count" FooterText="Tổng số lần ghi chú: ">
                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UserFullName" HeaderText="Nhân viên ghi chú" UniqueName="UserFullName">
                            <HeaderStyle HorizontalAlign="Center" Width="120" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Ngày ghi chú" UniqueName="CreatedDate">
                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn HeaderText="Ngày ghi chú" UniqueName="CreatedDate">
                            <HeaderStyle Width="80" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedDate" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy h:mm tt}", DataBinder.Eval(Container.DataItem, "CreatedDate"))  %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Sửa">
                            <HeaderStyle Width="30" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;" AlternateText="Chỉnh sửa" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn HeaderText="Xóa" CommandName="Delete" Text="Xóa" ConfirmText="Bạn có muốn xóa ghi chú này?">
                            <HeaderStyle Width="30px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Selecting AllowRowSelect="true" />
                    <ClientEvents OnRowDblClick="RowDblClick" />
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="200" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
        </div>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                function RowDblClick(sender, eventArgs) {
                    var ID = eventArgs.getDataKeyValue("ID");
                    var cID = eventArgs.getDataKeyValue("CustomerID");
                    window.parent.radopen("Controls/Customers/Details/Detail_Description_Edit.aspx?ID=" + ID + "&cID=" + cID, "CustomerDialog");
                }

                function ShowEditForm(id, rowIndex) {
                    var grid = $find('<%= grdDescriptionHistory.ClientID %>');

                    var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
                    var ID = grid.get_masterTableView().get_dataItems()[rowIndex].getDataKeyValue("ID");
                    var cID = grid.get_masterTableView().get_dataItems()[rowIndex].getDataKeyValue("CustomerID");
                    grid.get_masterTableView().selectItem(rowControl, true);

                    window.parent.radopen("Controls/Customers/Details/Detail_Description_Edit.aspx?ID=" + ID + "&cID=" + cID, "CustomerDialog");
                    return false;
                }
            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
