﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Detail_UsedService_Edit.aspx.cs" Inherits="EDMs.Web.Controls.Customers.Detail_UsedService_Edit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tư vấn</title>
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../../Scripts/jquery-1.7.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".content").fadeIn("slow");
        });
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshTab(3);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <center><asp:Label ID="lblTenKhachHang" runat="server" Text="Tên Khách Hàng" Font-Bold="true" Font-Size="12" /></center>
        <div style="width: 100%">
            <ul style="list-style-type: none">
                <li style="width: 500px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <span style="color: #E44B25; font-size: 14px;">THÔNG TIN</span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Ngày giờ sử dụng
                            </span>
                        </label>
                        <div style="float: left;">
                            <telerik:RadDateTimePicker ID="txtDateTime" runat="server" Width="200" Calendar-CultureInfo="vi-VN">
                                <Calendar ID="Calendar2" runat="server" EnableKeyboardNavigation="true">
                                </Calendar>
                                <DateInput ToolTip="Date input" DateFormat="dd/MM/yyyy hh:mm tt" ></DateInput>
                            </telerik:RadDateTimePicker>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Nội dung
                            </span>
                        </label>
                        <div style="float: left;">
                            <asp:TextBox ID="txtContent" runat="server" Style="width: 350px;" TextMode="MultiLine" Rows="4"></asp:TextBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Bác sĩ
                            </span>
                        </label>
                        <div style="float: left;">
                            <telerik:RadComboBox ID="cboResource" runat="server" Width="350px">
                            </telerik:RadComboBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px;">
                    <div style="border-style: solid; border-color: #E44B25;"></div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                <li style="width: 500px;">
                    <span style="color: #E44B25; font-size: 14px;">DỊCH VỤ </span>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 3px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;"></span>
                        </label>
                        <div style="float: left;">
                            <telerik:RadComboBox ID="cboService" runat="server" Width="350px">
                            </telerik:RadComboBox>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>

                <li style="width: 500px; padding-top: 10px; padding-bottom: 3px; text-align: center">
                    <telerik:RadButton ID="btnCapNhat" runat="server" Text="Cập nhật" OnClick="btnCapNhat_Click">
                        <Icon PrimaryIconUrl="~/Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnCancel" runat="server" Text="Bỏ qua"
                        OnClick="btnCancel_Click">
                        <Icon PrimaryIconUrl="~/Images/Cancel.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                </li>
            </ul>
        </div>
       <%-- <asp:Button ID="Button1" runat="server" Text="Get Checked Items!"
OnClick="Button1_Click"></asp:Button>
            <br />
            <br />
            <div>
                <strong>Checked Items:</strong>
                <br />
                <br />
                <asp:Label ID="itemsClientSide" runat="server" BorderStyle="None" CssClass="text"></asp:Label>
            </div>--%>
    </form>
</body>
</html>
