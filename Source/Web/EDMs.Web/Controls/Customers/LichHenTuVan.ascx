﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LichHenTuVan.ascx.cs" Inherits="EDMs.Web.Controls.Customers.LichHenTuVan" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<style type="text/css">
    .auto-style2 {
        width: 120px;
    }
</style>

    <telerik:RadAjaxManager runat="Server" ID="RadAjaxManager1">
		<AjaxSettings>
			<telerik:AjaxSetting AjaxControlID="btnTimkiem">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1"/>
				</UpdatedControls>
			</telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />

<table width="100%" cellpadding="0" cellspacing="5">
    <tr>
        <td>
            <telerik:RadComboBox ID="ddlNam" runat="server" AppendDataBoundItems="True">
                <Items>
                    <telerik:RadComboBoxItem Value="-1" Text="---Tất cả---" />
                </Items>
            </telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadComboBox ID="ddlThang" runat="server" AppendDataBoundItems="true">
                <Items>
                    <telerik:RadComboBoxItem Value="-1" Text="---Tất cả---" />
                </Items>
            </telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadComboBox ID="ddlTenLoaiHopDong" runat="server" AppendDataBoundItems="true">
                 <Items>
                    <telerik:RadComboBoxItem Value="-1" Text="---Tất cả---" />
                </Items>
            </telerik:RadComboBox>
        </td>        
        <td>
            <telerik:RadComboBox ID="ddlTenPhongBan" runat="server" AppendDataBoundItems="true">
                <Items>
                    <telerik:RadComboBoxItem Value="-1" Text="---Tất cả---" />
                </Items>
            </telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadComboBox ID="ddlTenNhanVien" runat="server" AppendDataBoundItems="true">
                <Items>
                    <telerik:RadComboBoxItem Value="-1" Text="---Tất cả---" />
                </Items>
            </telerik:RadComboBox>
        </td>
        <td><telerik:RadButton ID="btnTimkiem" runat="server" Text="Tìm kiếm" 
                   ValidationGroup="111" /></td>
        <td>&nbsp;</td>
    </tr>
</table>

<telerik:RadGrid ID="RadGrid1" 
        runat="server" 
        AllowPaging="True" 
        AutoGenerateColumns="False" 
        CellSpacing="0"
        CellPadding="0"
        PageSize="30"
        GridLines="None"
        OnNeedDataSource="RadGrid1_OnNeedDataSource">

        <MasterTableView>
            <NoRecordsTemplate>Chưa có dữ liệu.</NoRecordsTemplate>
            <PagerStyle Mode="NextPrevAndNumeric" />

            <Columns>
                <telerik:GridBoundColumn DataField="STT" HeaderText="STT" UniqueName="STT"/>

                <telerik:GridBoundColumn DataField="MaKH" HeaderText="Mã" UniqueName="MaKH"/>

                <telerik:GridBoundColumn DataField="TenKH" HeaderText="Tên" UniqueName="TenKH"/>

                <telerik:GridBoundColumn DataField="NgayHen" HeaderText="Ngày hẹn" UniqueName="NgayHen"/>

                <telerik:GridBoundColumn DataField="GioHen" HeaderText="Giờ hẹn" UniqueName="GioHen"/>

                <telerik:GridBoundColumn DataField="Phone" HeaderText="Phone" UniqueName="Phone"/>

                <telerik:GridBoundColumn DataField="Address" HeaderText="Địa chỉ" UniqueName="Address"/>

                <telerik:GridBoundColumn DataField="Email" HeaderText="Email" UniqueName="Email" />

                <telerik:GridTemplateColumn HeaderText="Trình trạng">
                    <ItemTemplate>
                        <telerik:RadComboBox ID="ddlTrangThai" runat="server" 
                            AutoPostBack="true" 
                            DataTextField="" 
                            DataValueField=""
                            OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged"
                            SelectedValue='<%# Bind("CustomerStatusId") %>'/>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridDateTimeColumn DataField="Confirm" HeaderText="Confirm" UniqueName="Confirm" />
            </Columns>

        </MasterTableView>
    </telerik:RadGrid>

<table width="100%" cellpadding="0" cellspacing="5">
    <tr>
        <td colspan="2">Thông Tin Lịch Hẹn</td>
    </tr>
    <tr>
        <td class="auto-style2" >Mã nhân viên:</td>
        <td >
            <asp:Label ID="lblMaNhanVien" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Chức vụ:</td>
        <td>
            <asp:Label ID="lblChucVu" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Giới tính:</td>
        <td>
            <asp:Label ID="lblGioiTinh" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Ngày sinh:</td>
        <td>
            <asp:Label ID="lblNgaySinh" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Ngày vào làm:</td>
        <td>
            <asp:Label ID="lblNgayVaoLam" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">CMND:</td>
        <td>
            <asp:Label ID="lblCMND" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Ngày cấp:</td>
        <td>
            <asp:Label ID="lblNgayCap" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Nơi cấp:</td>
        <td>
            <asp:Label ID="lblNoiCap" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Cư ngụ:</td>
        <td>
            <asp:Label ID="lblCuNgu" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style1" colspan="2">
            <asp:Label ID="lblThongTinThem" runat="server"></asp:Label>
        </td>
    </tr>
</table>