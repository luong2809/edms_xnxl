﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.CostContract;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.CostContract
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class PRPermission : Page
    {

        private readonly PermissionService permissionService = new PermissionService();

        private readonly UserService userService = new UserService();

        private readonly CostContractProjectService projectService = new CostContractProjectService();

        private readonly ProcurementRequirementService prService = new ProcurementRequirementService();

        private readonly PermissionProcurementRequirementService permissionPRService = new PermissionProcurementRequirementService();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["prId"]))
                {
                    this.LoadPermissionData(Convert.ToInt32(this.Request.QueryString["prId"]));    
                }
                
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["prId"]))
            {
                var prId = Convert.ToInt32(this.Request.QueryString["prId"]);
                var prObj = this.prService.GetById(prId);
                if (prObj!= null)
                {
                    foreach (RadListBoxItem user in this.ddlUser.SelectedItems)
                    {
                        var permissionObj = new PermissionProcurementRequirement()
                        {
                            UserId = Convert.ToInt32(user.Value),
                            ProcurementRequirementID = prId,
                            ProjectId = prObj.ProjectID,
                            IsFullPermission = this.rbtnFull.Checked,
                            IsShared = this.rbbtnShareControl.Checked,
                            IsViewOnly = this.rbtnView.Checked,
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now
                        };
                        this.permissionPRService.Insert(permissionObj);
                    }

                    this.ReloadUserList();
                    this.grdPermission.Rebind();
                }
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }


        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["prId"]))
            {
                var packagePermissionList = this.permissionPRService.GetAllByPR(Convert.ToInt32(this.Request.QueryString["prId"]));
                this.grdPermission.DataSource = packagePermissionList;
            }
            
        }

        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
            this.permissionPRService.Delete(permissionId);

            this.ReloadUserList();
        }

        /// <summary>
        /// The load permission data.
        /// </summary>
        /// <param name="packageId">
        /// The package id.
        /// </param>
        private void LoadPermissionData(int prId)
        {
            var usersInPermission = this.permissionPRService.GetUserIdListInPermission(prId).Distinct().ToList();
            var listUser = this.userService.GetAll().Where(t => !usersInPermission.Contains(t.Id)).OrderBy(t => t.Username);

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "UserNameWithFullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        private void ReloadUserList()
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["prId"]))
            {
                var usersInPermission = this.permissionPRService.GetUserIdListInPermission(Convert.ToInt32(this.Request.QueryString["prId"])).Distinct().ToList();
                var listUser = this.userService.GetAll().Where(t => !usersInPermission.Contains(t.Id)).OrderBy(t => t.Username);

                this.ddlUser.DataSource = listUser;
                this.ddlUser.DataTextField = "UserNameWithFullName";
                this.ddlUser.DataValueField = "Id";
                this.ddlUser.DataBind();
            }
        }
    }
}