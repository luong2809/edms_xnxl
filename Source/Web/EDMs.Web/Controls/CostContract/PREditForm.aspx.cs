﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.CostContract;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.CostContract
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class PREditForm : Page
    {
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly ProcurementRequirementService prService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly ProcurementRequirementTypeService prTypeService;

        private readonly ScopeProjectService projectService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public PREditForm()
        {
            this.userService = new UserService();
            this.prService = new ProcurementRequirementService();
            this.projectService = new ScopeProjectService();
            this.prTypeService = new ProcurementRequirementTypeService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                
                if (!string.IsNullOrEmpty(this.Request.QueryString["prId"]))
                {
                    var objPR = this.prService.GetById(Convert.ToInt32(this.Request.QueryString["prId"]));
                    this.CreatedInfo.Visible = objPR != null;

                    if (objPR != null)
                    {
                        this.ddlProject.SelectedValue = objPR.ProjectID.GetValueOrDefault().ToString();
                        this.txtName.Text = objPR.Name;
                        this.txtBiddingPrice.Value = objPR.BiddingPrice;
                        this.txtBiddingFormality.Text = objPR.BiddingFormalityName;
                        this.txtBiddingMethod.Text = objPR.BiddingMethodName;
                        this.txtContractFormality.Text = objPR.ContractFormalityName;
                        this.txtContractProcessTime.SelectedDate = objPR.ContractProcessTime;
                        this.txtBiddingProcessTimePlanFrom.SelectedDate = objPR.BiddingProcessTimePlanFrom;
                        this.txtBiddingProcessTimePlanTo.SelectedDate = objPR.BiddingProcessTimePlanTo;
                        this.txtBiddingProcessTimeActualFrom.SelectedDate = objPR.BiddingProcessTimeActualFrom;
                        this.txtBiddingProcessTimeActualTo.SelectedDate = objPR.BiddingProcessTimeActualTo;
                        this.txtBidPrice.Value = objPR.BidPrice;
                        this.txtBidWinnerPrice.Value = objPR.BidWinnerPrice;
                        this.txtBidPriceDifference.Value = objPR.BidPriceDifference;
                        this.txtContractorBidWinner.Text = objPR.ContractorBidWinnerName;

                        var createdUser = this.userService.GetByID(objPR.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objPR.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objPR.UpdatedBy != null && objPR.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objPR.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objPR.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["prId"]))
                {
                    var contractorId = Convert.ToInt32(this.Request.QueryString["prId"]);
                    var obj = this.prService.GetById(contractorId);
                    if (obj != null)
                    {
                        this.CollectDate(ref obj);
                        obj.UpdatedBy = UserSession.Current.User.Id;
                        obj.UpdatedDate = DateTime.Now;
                        this.prService.Update(obj);
                    }
                }
                else
                {
                    var obj = new ProcurementRequirement();
                    this.CollectDate(ref obj);
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    this.prService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Vui lòng nhập Tên gói thầu.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        private void LoadComboData()
        {
            var projectList = this.projectService.GetAll();
            this.ddlProject.DataSource = projectList;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();
        }

        private void CollectDate(ref ProcurementRequirement objPR)
        {
            objPR.ProjectID = Convert.ToInt32(this.ddlProject.SelectedValue);
            objPR.ProjectName = this.ddlProject.SelectedItem.Text;

            objPR.Name = this.txtName.Text.Trim();
            objPR.BiddingPrice = this.txtBiddingPrice.Value;
            objPR.BiddingFormalityName = this.txtBiddingFormality.Text.Trim();
            objPR.BiddingMethodName = this.txtBiddingMethod.Text.Trim();
            objPR.ContractFormalityName = this.txtContractFormality.Text.Trim();
            objPR.ContractProcessTime = this.txtContractProcessTime.SelectedDate;
            objPR.BiddingProcessTimePlanFrom = this.txtBiddingProcessTimePlanFrom.SelectedDate;
            objPR.BiddingProcessTimePlanTo = this.txtBiddingProcessTimePlanTo.SelectedDate;
            objPR.BiddingProcessTimeActualFrom = this.txtBiddingProcessTimeActualFrom.SelectedDate;
            objPR.BiddingProcessTimeActualTo = this.txtBiddingProcessTimeActualTo.SelectedDate;
            objPR.BidPrice = this.txtBidPrice.Value;
            objPR.BidWinnerPrice = this.txtBidWinnerPrice.Value;
            objPR.BidPriceDifference = this.txtBidPriceDifference.Value;
            objPR.ContractorBidWinnerName = this.txtContractorBidWinner.Text.Trim();
        }
    }
}