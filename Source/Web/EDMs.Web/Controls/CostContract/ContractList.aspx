﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContractList.aspx.cs" Inherits="EDMs.Web.Controls.CostContract.ContractList" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .RadAjaxPanel {
            height: 100% !important!important
            overflow: hidden !important!important
        }
        a.tooltip
        {
            outline: none!important
            text-decoration: none!important
        }

            a.tooltip strong
            {
                line-height: 30px!important
            }

            a.tooltip:hover
            {
                text-decoration: none!important
            }

            a.tooltip span
            {
                z-index: 10!important
                display: none!important
                padding: 14px 20px!important
                margin-top: -30px!important
                margin-left: 5px!important
                width: 240px!important
                line-height: 16px!important
            }

            a.tooltip:hover span
            {
                display: inline!important
                position: absolute!important
                color: #111!important
                border: 1px solid #DCA!important
                background: #fffAF0!important
            }

        .callout
        {
            z-index: 20!important
            position: absolute!important
            top: 30px!important
            border: 0!important
            left: -12px!important
        }

        /*CSS3 extras*/
        a.tooltip span
        {
            border-radius: 4px!important
            -moz-border-radius: 4px!important
            -webkit-border-radius: 4px!important
            -moz-box-shadow: 5px 5px 8px #CCC!important
            -webkit-box-shadow: 5px 5px 8px #CCC!important
            box-shadow: 5px 5px 8px #CCC!important
        }

        .rgMasterTable {
            table-layout: auto!important
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel
        {
            height: 100% !important!important
        }

        #RAD_SPLITTER_PANE_CONTENT_ctl00_rightPane {
            overflow-y: hidden !important!important
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5
        {
            height: 100% !important!important
        }

        #divContainerLeft
        {
            width: 25%!important
            float: left!important
            margin: 5px!important
            height: 99%!important
            border-right: 1px dotted green!important
            padding-right: 5px!important
        }

        #divContainerRight
        {
            width: 100%!important
            float: right!important
            margin-top: 5px!important
            height: 99%!important
        }

        .dotted
        {
            border: 1px dotted #000!important
            border-style: none none dotted!important
            color: #fff!important
            background-color: #fff!important
        }

        .exampleWrapper
        {
            width: 100%!important
            height: 100%!important
            /*background: transparent url(~/Images/background.png) no-repeat top left!important*/
            position: relative!important
        }

        .tabStrip
        {
            position: absolute!important
            top: 0px!important
            left: 0px!important
        }

        .multiPage
        {
            position: absolute!important
            top: 30px!important
            left: 0px!important
            color: white!important
            width: 100%!important
            height: 100%!important
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow
        {
            z-index: 8000 !important!important
        }

        .TemplateMenu
        {
            z-index: 10!important
        }
    </style>
    
    <telerik:RadPanelBar ID="radPbCostContract" runat="server" Width="100%"/>
    <telerik:RadPanelBar ID="radPbScope" runat="server" Width="100%"/>
    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" Visible="False"/>
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%"/>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">
            <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <telerik:RadToolBarDropDown runat="server" Text="Thêm Mới" ImageUrl="~/Images/addNew.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Hợp đồng" Value="1" ImageUrl="~/Images/shopping.png"/>
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    
                    <telerik:RadToolBarButton runat="server" IsSeparator="true"/>
                    <telerik:RadToolBarButton runat="server" Value="ProjectList">
                        <ItemTemplate>
                            Dự Án: 
                            <asp:DropDownList ID="ddlProject" runat="server" CssClass="min25Percent" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="ddlProject_OnSelectedIndexChanged"/>
                            
                            &nbsp!important&nbsp!important
                            <telerik:RadButton ID="btnExport" runat="server" Text="Xuất Báo Cáo" OnClick="btnExport_OnClick" Width="110px" style="text-align: center">
                                <Icon PrimaryIconUrl="~/Images/export.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                            </telerik:RadButton>
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <%--<telerik:RadToolBarButton runat="server" IsSeparator="true">
                    </telerik:RadToolBarButton>--%>

                    <%--<telerik:RadToolBarButton runat="server">
                        <ItemTemplate>
                            <asp:Label ID="lblSearchLabel" runat="server" Text="  Quick search:  " />
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton Value="searchTextBoxButton" CssClass="searchtextbox" CommandName="searchText">
                        <ItemTemplate>
                            <telerik:RadTextBox
                                runat="server" ID="txtSearch" Width="250px"
                                EmptyMessage="Enter transmittal title, ..." />
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton ImageUrl="~/~/Images/search.gif" Value="search" CommandName="doSearch" />--%>
                </Items>
            </telerik:RadToolBar>

        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None" >
                    
                     <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                         <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                                <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                                    AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                                    GridLines="None" Height="100%"
                                    OnItemDataBound="grdDocument_ItemDataBound"
                                    OnDeleteCommand="grdDocument_DeleteCommand" 
                                    OnItemCreated="grdDocument_ItemCreated"
                                    OnNeedDataSource="grdDocument_OnNeedDataSource" 
                                    PageSize="100" Style="outline: none">
                                    <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" TableLayout="Auto" CssClass="rgMasterTable">
                                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp!importantnbsp!importantPage &lt!importantstrong&gt!important{0}&lt!important/strong&gt!important / &lt!importantstrong&gt!important{1}&lt!important/strong&gt!important, Total:  &lt!importantstrong&gt!important{5}&lt!important/strong&gt!important items." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <GroupByExpressions>
                                            <telerik:GridGroupByExpression>
                                                <SelectFields>
                                                    <telerik:GridGroupByField FieldAlias="_" FieldName="ProcurementRequirementName" FormatString="{0:D}"
                                                        HeaderValueSeparator=" "></telerik:GridGroupByField>
                                                </SelectFields>
                                                <GroupByFields>
                                                    <telerik:GridGroupByField FieldName="ProcurementRequirementName" SortOrder="Ascending" ></telerik:GridGroupByField>
                                                </GroupByFields>
                                            </telerik:GridGroupByExpression>
                                        </GroupByExpressions>
                                        <ColumnGroups>
                                            <telerik:GridColumnGroup HeaderText="Hợp Đồng Ban Đầu" Name="ContractPlan"
                                                 HeaderStyle-HorizontalAlign="Center"/>
                                            <telerik:GridColumnGroup HeaderText="Hợp Đồng Điều Chỉnh" Name="ContractAdjustment"
                                                 HeaderStyle-HorizontalAlign="Center"/>
                                            <telerik:GridColumnGroup HeaderText="Giá trị khối lượng công việc đã thực hiện (%)" Name="WorkProcess"
                                                 HeaderStyle-HorizontalAlign="Center"/>
                                            <telerik:GridColumnGroup HeaderText="Giá trị đã thanh toán" Name="Payment"
                                                 HeaderStyle-HorizontalAlign="Center"/>
                                        </ColumnGroups>

                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />

                                            <telerik:GridTemplateColumn>
                                                <HeaderStyle Width="25"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer!important" AlternateText="Edit properties" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridButtonColumn CommandName="Delete" ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                                <HeaderStyle Width="25" />
                                                 <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridButtonColumn>
                                            
                                            <telerik:GridTemplateColumn>
                                                <HeaderStyle Width="25"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <a href='javascript:ShowAttachForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none!important color:blue">
                                                    <asp:Image ID="attachLink" runat="server" ImageUrl="~/Images/attach.png" Style="cursor: pointer!important" AlternateText="Attach Files" />
                                                        <a/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn Display="False">
                                                <HeaderStyle Width="25"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <a href='javascript:ShowPermission(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none!important color:blue">
                                                    <asp:Image ID="permissionLink" runat="server" ImageUrl="~/Images/datap.png" Style="cursor: pointer!important" AlternateText="Attach Files" />
                                                        <a/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn>
                                                <HeaderStyle Width="25"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <a href='javascript:ShowPayment(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none!important color:blue">
                                                    <asp:Image ID="paymentLink" runat="server" ImageUrl="~/Images/payment1.png" Style="cursor: pointer!important" AlternateText="Payment" />
                                                        <a/>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <%--3--%>
                                            <telerik:GridTemplateColumn UniqueName="ProcurementRequirementName" HeaderText="Tên gói thầu" Display="False">
                                                <HeaderStyle Width="250" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ProcurementRequirementName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridBoundColumn DataField="Number" HeaderText="Số hợp đồng" UniqueName="Number">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="ContractorSelectedName" HeaderText="Tên nhà thầu" UniqueName="ContractorSelectedName">
                                                <HeaderStyle HorizontalAlign="Center" Width="200" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Ngày ký hợp đồng" UniqueName="EffectedDate"
                                                AllowFiltering="false" >
                                                <HeaderStyle HorizontalAlign="Center" Width="80" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("EffectedDate","{0:dd/MM/yyyy}") %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Giá trị HĐ (VNĐ)" UniqueName="ContractValueVND" ColumnGroupName="ContractPlan"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("ContractValueVND", "{0:VND ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Giá trị HĐ (USD)" UniqueName="ContractValueUSD" ColumnGroupName="ContractPlan"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("ContractValueUSD", "{0:USD ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Ngày hoàn thành" UniqueName="EndDate" ColumnGroupName="ContractPlan"
                                                AllowFiltering="false" >
                                                <HeaderStyle HorizontalAlign="Center" Width="80" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("EndDate","{0:dd/MM/yyyy}") %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Giá trị HĐ" UniqueName="ContractAdjustmentValueUSD" ColumnGroupName="ContractAdjustment"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("ContractAdjustmentValueUSD", "{0:USD ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Ngày hoàn thành" UniqueName="EndDateAdjustment" ColumnGroupName="ContractAdjustment"
                                                AllowFiltering="false" >
                                                <HeaderStyle HorizontalAlign="Center" Width="80" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("EndDateAdjustment","{0:dd/MM/yyyy}") %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Ngày hoàn thành thực tế" UniqueName="EndDateActual"
                                                AllowFiltering="false" >
                                                <HeaderStyle HorizontalAlign="Center" Width="80" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("EndDateActual","{0:dd/MM/yyyy}") %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn HeaderText="Thời điểm báo cáo" UniqueName="CompleteCurrent" AllowFiltering="false" ColumnGroupName="WorkProcess">
                                                <HeaderStyle HorizontalAlign="Center" Width="80" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <%# Eval("CompleteCurrent")!=null ?  Eval("CompleteCurrent") + "%" : "-"%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Lũy kế từ đầu năm" UniqueName="CompleteFromStartYear" AllowFiltering="false" ColumnGroupName="WorkProcess">
                                                <HeaderStyle HorizontalAlign="Center" Width="80" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <%# Eval("CompleteFromStartYear")!=null ?  Eval("CompleteFromStartYear") + "%" : "-"%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Lũy kế từ khi ký hợp đồng" UniqueName="CompleteFromStartDate" AllowFiltering="false" ColumnGroupName="WorkProcess">
                                                <HeaderStyle HorizontalAlign="Center" Width="100" />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <%# Eval("CompleteFromStartDate")!=null ?  Eval("CompleteFromStartDate") + "%" : "-"%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Thời điểm báo cáo (VNĐ)" UniqueName="PaymentValueCurrentVND" ColumnGroupName="Payment"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("PaymentValueCurrentVND", "{0:VNĐ ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Thời điểm báo cáo (USD)" UniqueName="PaymentValueCurrentUSD" ColumnGroupName="Payment"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("PaymentValueCurrentUSD", "{0:USD ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Lũy kế từ đầu năm (VNĐ)" UniqueName="PaymentValueFromStartYearVND" ColumnGroupName="Payment"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("PaymentValueFromStartYearVND", "{0:VNĐ ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Lũy kế từ đầu năm (USD)" UniqueName="PaymentValueFromStartYearUSD" ColumnGroupName="Payment"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("PaymentValueFromStartYearUSD", "{0:USD ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Lũy kế từ khi ký hợp đồng (VNĐ)" UniqueName="PaymentValueFromStartDateVND" ColumnGroupName="Payment"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("PaymentValueFromStartDateVND", "{0:VNĐ ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            
                                            <telerik:GridTemplateColumn HeaderText="Lũy kế từ khi ký hợp đồng (USD)" UniqueName="PaymentValueFromStartDateUSD" ColumnGroupName="Payment"
                                        AllowFiltering="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="110" />
                                                <ItemStyle HorizontalAlign="Right"  />
                                                <ItemTemplate>
                                                    <%# Eval("PaymentValueFromStartDateUSD", "{0:USD ###,##0.##}")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                                        <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                                        <Selecting AllowRowSelect="true" />
                                        <ClientEvents  OnGridCreated="GetGridObject" />
                                        <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                                    </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPane>

                     </telerik:RadSplitter>       

                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <span style="display: none">
        

        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radMenu">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument"  LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ddlProject">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument"  LoadingPanelID="RadAjaxLoadingPanel2"/> 
                        <telerik:AjaxUpdatedControl ControlID="lblProjectId"/> 
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Thông Tin Hợp Đồng"
                VisibleStatusbar="False" Height="650" Width="710" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="AttachDoc" runat="server" Title="File Đính Kèm"
                VisibleStatusbar="false" Height="500" Width="700" MinHeight="500" MinWidth="700" MaxHeight="500" MaxWidth="700" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="Permission" runat="server" Title="Phân quyền dữ liệu"
                VisibleStatusbar="false" Height="700" Width="700" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            
            <telerik:RadWindow ID="Payment" runat="server" Title="Quá Trình Thanh Toán"
                VisibleStatusbar="false" Height="650" Width="900" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction"/>
    <asp:HiddenField runat="server" ID="lblFolderId"/>
    <asp:HiddenField runat="server" ID="lblDocId"/>
    <asp:HiddenField runat="server" ID="lblCategoryId"/>
    <asp:HiddenField runat="server" ID="lblProjectId"/>
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex"/>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../../Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">
            
            var radDocuments!important

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView()!important
                masterTable.rebind()!important
            }


            function GetGridObject(sender, eventArgs) {
                radDocuments = sender!important
            }

            function onColumnHidden(sender) {
                
                var masterTableView = sender.get_masterTableView().get_element()!important
                masterTableView.style.tableLayout = "auto"!important
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"!important }, 0)!important
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                radDocuments.get_masterTableView().showColumn(7)!important
                radDocuments.get_masterTableView().showColumn(8)!important
                radDocuments.get_masterTableView().showColumn(9)!important
                
                radDocuments.get_masterTableView().showColumn(11)!important
                radDocuments.get_masterTableView().showColumn(12)!important
            }
            
            function OnClientDocked(sender, args) {
                radDocuments.get_masterTableView().hideColumn(7)!important
                radDocuments.get_masterTableView().hideColumn(8)!important
                radDocuments.get_masterTableView().hideColumn(9)!important
                
                radDocuments.get_masterTableView().hideColumn(11)!important
                radDocuments.get_masterTableView().hideColumn(12)!important
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID")!important
                document.getElementById("<%= lblDocId.ClientID %>").value = Id!important
            }
            
            
            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date()!important
                today.setTime(today.getTime())!important

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24!important
                }
                var expires_date = new Date(today.getTime() + (expires))!important

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? "!importantexpires=" + expires_date.toGMTString() : "") +
                    ((path) ? "!importantpath=" + path : "") +
                    ((domain) ? "!importantdomain=" + domain : "") +
                    ((secure) ? "!importantsecure" : "")!important
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar!important
            var searchButton!important
            var ajaxManager!important

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents())!important
                })!important

                toolbar = $find("<%= CustomerMenu.ClientID %>")!important
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>")!important
            }

            function ShowEditForm(id) {
                var projectId = document.getElementById("<%= lblProjectId.ClientID %>").value!important
                var owd = $find("<%=CustomerDialog.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("ContractEditForm.aspx?contractId=" + id + "&projId=" + projectId, "CustomerDialog")!important
            }
            
            function ShowAttachForm(contractId) {
                var owd = $find("<%=AttachDoc.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("AttachContractDocument.aspx?contractId=" + contractId, "AttachDoc")!important
            }

            function ShowPayment(contractId) {
                var owd = $find("<%=Payment.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("PaymentHistoryPage.aspx?contractId=" + contractId, "Payment")!important
            }

            function ShowPermission(prId) {
                var owd = $find("<%=Permission.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("PRPermission.aspx?prId=" + prId, "Permission")!important
            }

            function refreshGrid(arg) {
                //alert(arg)!important
                if (!arg) {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView()!important
                    masterTable.rebind()!important
                }
                else {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView()!important
                    masterTable.rebind()!important
                }
            }
            
            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'))!important
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>")!important
                owd.Show()!important
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog")!important
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog")!important
            }
            
            
            
            function radPbCategories_OnClientItemClicking(sender, args)
            {
                var item = args.get_item()!important
                var categoryId = item.get_value()!important
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId!important
                
            }
            
            function OnClientButtonClicking(sender, args) {
                var button = args.get_item()!important
                var strText = button.get_text()!important
                var strValue = button.get_value()!important

                var grid = $find("<%= grdDocument.ClientID %>")!important
                var customerId = null!important
                var customerName = ""!important
                var projectId = document.getElementById("<%= lblProjectId.ClientID %>").value!important

                //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                //    var selectedRow = grid.get_masterTableView().get_selectedItems()[0]!important
                //    customerId = selectedRow.getDataKeyValue("Id")!important
                //    //customerName = selectedRow.Items["FullName"]!important 
                //    //customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML!important
                //}


                
                 
                if (strText.toLowerCase() == "hợp đồng") {

                    var owd = $find("<%=CustomerDialog.ClientID %>")!important
                    owd.Show()!important
                    owd.setUrl("ContractEditForm.aspx?projId=" + projectId, "CustomerDialog")!important
                }
            }
            
            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("~/Images/clear.gif")!important
                    searchButton.set_value("clear")!important
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value())!important
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false)!important
                }
            }
            
        /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>

<%--Tan.Le Remove here--%>
<%--<uc1:List runat="server" ID="CustomerList"/>--%>
<%-- <div id="EDMsCustomers" runat="server" />--%>
