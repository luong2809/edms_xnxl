﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services.CostContract;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.CostContract
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ContractEditForm : Page
    {
        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService projectService;

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly ContractService contractService;

        private readonly UserService userService;

        private readonly ProcurementRequirementService prService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ContractEditForm()
        {
            this.documentService = new DocumentService();
            this.projectService = new ScopeProjectService();
            this.contractService = new ContractService();
            this.userService = new UserService();
            this.prService = new ProcurementRequirementService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////this.LoadComboData();
                if (!UserSession.Current.User.Role.IsAdmin.GetValueOrDefault() && !UserSession.Current.User.Role.IsUpdate.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["projId"]))
                {
                    var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                    var projObj = this.projectService.GetById(projectId);
                    this.txtProjectName.Text = projObj.Name;
                    this.LoadComboData(projectId);
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["contractId"]))
                {
                    this.CreatedInfo.Visible = true;
                    var contractId = Convert.ToInt32(this.Request.QueryString["contractId"]);
                    var contractObj = this.contractService.GetById(contractId);
                    if (contractObj != null)
                    {
                        this.LoadComboData(contractObj.ProjectID.GetValueOrDefault());
                        this.txtProjectName.Text = contractObj.ProjectName;

                        this.LoadDocInfo(contractObj);

                        var createdUser = this.userService.GetByID(contractObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + contractObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (contractObj.UpdatedBy != null && contractObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(contractObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + contractObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid && !string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var projObj = this.projectService.GetById(projectId);
                Contract contractObj;
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    contractObj = this.contractService.GetById(docId);
                    if (contractObj != null)
                    {
                        
                        this.CollectData(ref contractObj, projObj);

                        contractObj.UpdatedBy = UserSession.Current.User.Id;
                        contractObj.UpdatedDate = DateTime.Now;
                        this.contractService.Update(contractObj);
                    }
                }
                else
                {
                    contractObj = new Contract()
                    {
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                    };

                    this.CollectData(ref contractObj, projObj);
                    this.contractService.Insert(contractObj);

                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseWindow", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            ////if(this.txtName.Text.Trim().Length == 0)
            ////{
            ////    this.fileNameValidator.ErrorMessage = "Please enter file name.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = false;
            ////}
            ////else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            ////{
            ////    var docId = Convert.ToInt32(Request.QueryString["docId"]);
            ////    this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = true; ////!this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            ////}
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                
                if(this.documentService.IsDocumentExist(folderId, fileName))
                {
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData(int projectId)
        {
            var listPRInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                ? this.prService.GetAllByProject(projectId)
                    .OrderBy(t => t.ID)
                    .ToList()
                : this.prService.GetAllPRHaveFullPermission(UserSession.Current.User.Id, projectId)
                    .OrderBy(t => t.ID).ToList();
            this.ddlPR.DataSource = listPRInPermission;
            this.ddlPR.DataTextField = "Name";
            this.ddlPR.DataValueField = "ID";
            this.ddlPR.DataBind();
        }

        private void CollectData(ref Contract contractObj, ScopeProject projObj)
        {
            contractObj.ProjectID = projObj.ID;
            contractObj.ProjectName = projObj.Name;
            contractObj.ProcurementRequirementID = this.ddlPR.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlPR.SelectedValue)
                                        : 0;
            contractObj.ProcurementRequirementName = this.ddlPR.SelectedItem != null
                                          ? this.ddlPR.SelectedItem.Text
                                          : string.Empty;

            contractObj.Number = this.txtContractNumber.Text.Trim();
            contractObj.ContractorSelectedName = this.txtContractor.Text;
            contractObj.EffectedDate = this.txtEffectedDate.SelectedDate;
            contractObj.EndDateActual = this.txtEndDateActual.SelectedDate;

            contractObj.ContractValueVND = this.txtContractValueVND.Value;
            contractObj.ContractValueUSD = this.txtContractValueUSD.Value;
            contractObj.EndDate = this.txtEndDate.SelectedDate;

            contractObj.ContractAdjustmentValueUSD = this.txtContractAdjustmentValueUSD.Value;
            contractObj.EndDateAdjustment = this.txtEndDateAdjustment.SelectedDate;

            contractObj.CompleteCurrent = this.txtCompleteCurrent.Value;
            contractObj.CompleteFromStartDate = this.txtCompleteFromStartDate.Value;
            contractObj.CompleteFromStartYear = this.txtCompleteFromStartYear.Value;

            contractObj.PaymentValueCurrentVND = this.txtPaymentValueCurrentVND.Value;
            contractObj.PaymentValueFromStartDateVND = this.txtPaymentValueFromStartDateVND.Value;
            contractObj.PaymentValueFromStartYearVND = this.txtPaymentValueFromStartYearVND.Value;

            contractObj.PaymentValueCurrentUSD = this.txtPaymentValueCurrentUSD.Value;
            contractObj.PaymentValueFromStartDateUSD = this.txtPaymentValueFromStartDateUSD.Value;
            contractObj.PaymentValueFromStartYearUSD = this.txtPaymentValueFromStartYearUSD.Value;
        }

        private void LoadDocInfo(Contract contractObj)
        {
            this.ddlPR.SelectedValue = contractObj.ProcurementRequirementID.ToString();
            this.txtContractNumber.Text = contractObj.Number;
            this.txtContractor.Text = contractObj.ContractorSelectedName;
            this.txtEffectedDate.SelectedDate = contractObj.EffectedDate;
            this.txtEndDate.SelectedDate = contractObj.EndDate;

            this.txtContractValueVND.Value = contractObj.ContractValueVND;
            this.txtContractValueUSD.Value = contractObj.ContractValueUSD;

            this.txtEndDateActual.SelectedDate = contractObj.EndDateActual;
            this.txtContractAdjustmentValueUSD.Value = contractObj.ContractAdjustmentValueUSD;
            this.txtEndDateAdjustment.SelectedDate = contractObj.EndDateAdjustment;

            this.txtCompleteCurrent.Value = contractObj.CompleteCurrent;
            this.txtCompleteFromStartDate.Value = contractObj.CompleteFromStartDate;
            this.txtCompleteFromStartYear.Value = contractObj.CompleteFromStartYear;

            this.txtPaymentValueCurrentVND.Value = contractObj.PaymentValueCurrentVND;
            this.txtPaymentValueFromStartDateVND.Value = contractObj.PaymentValueFromStartDateVND;
            this.txtPaymentValueFromStartYearVND.Value = contractObj.PaymentValueFromStartYearVND;

            this.txtPaymentValueCurrentUSD.Value = contractObj.PaymentValueCurrentUSD;
            this.txtPaymentValueFromStartDateUSD.Value = contractObj.PaymentValueFromStartDateUSD;
            this.txtPaymentValueFromStartYearUSD.Value = contractObj.PaymentValueFromStartYearUSD;
        }
    }
}