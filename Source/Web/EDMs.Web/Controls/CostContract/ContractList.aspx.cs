﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.CostContract;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.CostContract
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ContractList : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly ProcurementRequirementService prService = new ProcurementRequirementService();

        private readonly ScopeProjectService projectService = new ScopeProjectService();

        private readonly ContractService contractService = new ContractService();

        private readonly PermissionContractService permissionContractService = new PermissionContractService();

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.Page.IsPostBack)
            {
                var ddlProject = (DropDownList)this.CustomerMenu.Items[2].FindControl("ddlProject");
                if (ddlProject != null)
                {
                    var projectList = this.projectService.GetAll();
                    ddlProject.DataSource = projectList;
                    ddlProject.DataTextField = "Name";
                    ddlProject.DataValueField = "ID";
                    ddlProject.DataBind();

                    this.lblProjectId.Value = ddlProject.SelectedValue;
                }

                this.LoadCostContractPanel();
                this.LoadScopePanel();
                this.LoadSystemPanel();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var ddlProject = (DropDownList)this.CustomerMenu.Items[2].FindControl("ddlProject");
            if (ddlProject != null)
            {
                if (UserSession.Current.User.Role.IsAdmin.GetValueOrDefault())
                {
                    var listPRId = this.prService.GetAllByProject(Convert.ToInt32(ddlProject.SelectedValue))
                            .Select(t => t.ID)
                            .ToList();
                    var contractList = this.contractService.GetAllByPR(listPRId).OrderBy(t => t.Number).ToList();
                    this.grdDocument.DataSource = contractList;
                }
                else
                {
                    var listPRId = this.prService.GetAllPRInPermission(UserSession.Current.User.Id, !string.IsNullOrEmpty(ddlProject.SelectedValue) ? Convert.ToInt32(ddlProject.SelectedValue) : 0).Select(t => t.ID).ToList();
                    var contractList = this.contractService.GetAllByPR(listPRId).OrderBy(t => t.Number).ToList();

                    var contractIdListInPermission =
                        this.permissionContractService.GetAllByUser(UserSession.Current.User.Id)
                            .Select(t => t.ContractID)
                            .ToList();
                    this.grdDocument.DataSource = contractList.Where(t => contractIdListInPermission.Contains(t.ID));
                }
            }
            
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format(
                    "return ShowEditForm('{0}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"]);
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var disId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.prService.Delete(disId);

            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            //AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"~/Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    //if (item.Text == "Originator")
                    //{
                    //    item.Selected = true;
                    //}
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void LoadCostContractPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CostContractID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "QUẢN LÝ GÓI THẦU" });

                this.radPbCostContract.DataSource = permissions;
                this.radPbCostContract.DataFieldParentID = "ParentId";
                this.radPbCostContract.DataFieldID = "Id";
                this.radPbCostContract.DataValueField = "Id";
                this.radPbCostContract.DataTextField = "MenuName";
                this.radPbCostContract.DataBind();
                this.radPbCostContract.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbCostContract.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    if (item.Text == "Danh Sách Hợp Đồng")
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        private void LoadScopePanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ScopeID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SCOPE OF WORK" });

                this.radPbScope.DataSource = permissions;
                this.radPbScope.DataFieldParentID = "ParentId";
                this.radPbScope.DataFieldID = "Id";
                this.radPbScope.DataValueField = "Id";
                this.radPbScope.DataTextField = "MenuName";
                this.radPbScope.DataBind();
                this.radPbScope.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbScope.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        protected void ddlProject_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblProjectId.Value = ((DropDownList) sender).SelectedValue;
            this.grdDocument.Rebind();
        }

        protected void btnExport_OnClick(object sender, EventArgs e)
        {
            var ddlProject = (DropDownList)this.CustomerMenu.Items[2].FindControl("ddlProject");
            if (ddlProject != null && ddlProject.SelectedItem != null)
            {
                var filePath = Server.MapPath("~/Exports") + @"\";
                var workbook = new Workbook(filePath + @"Template\TenderPackageManagementTemplate.xlsm");
                var pb8Sheet = workbook.Worksheets[0];
                var pb9Sheet = workbook.Worksheets[1];
                var pb10Sheet = workbook.Worksheets[2];

                // Import pr data
                var prCount = 1;
                var prList = this.prService.GetAllByProject(Convert.ToInt32(ddlProject.SelectedValue));
                var listPRId = prList.Select(t => t.ID).ToList();
                var prDataTable = new DataTable();
                prDataTable.Columns.AddRange(new[]
                {
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("Name", typeof (String)),
                    new DataColumn("BiddingPrice", typeof (Double)),
                    new DataColumn("BiddingFormalityName", typeof (String)),
                    new DataColumn("BiddingMethodName", typeof (String)),
                    new DataColumn("ContractFormalityName", typeof (String)),
                    new DataColumn("ContractProcessTime", typeof (object)),
                    new DataColumn("BiddingProcessTimePlan", typeof (object)),
                    new DataColumn("BiddingProcessTimeActual", typeof (object)),
                    new DataColumn("BidPrice", typeof (Double)),
                    new DataColumn("BidWinnerPrice", typeof (Double)),
                    new DataColumn("BidPriceDifference", typeof (Double)),
                    new DataColumn("ContractorBidWinnerName", typeof (String)),
                });

                foreach (var prItem in prList)
                {
                    var dtRow = prDataTable.NewRow();
                    dtRow["NoIndex"] = prCount.ToString();
                    dtRow["Name"] = prItem.Name;
                    dtRow["BiddingPrice"] = prItem.BiddingPrice;
                    dtRow["BiddingFormalityName"] = prItem.BiddingFormalityName;
                    dtRow["BiddingMethodName"] = prItem.BiddingMethodName;
                    dtRow["ContractFormalityName"] = prItem.ContractFormalityName;
                    dtRow["ContractProcessTime"] = prItem.ContractProcessTime;
                    dtRow["BiddingProcessTimePlan"] = prItem.BiddingProcessTimePlan;
                    dtRow["BiddingProcessTimeActual"] = prItem.BiddingProcessTimeActual;
                    dtRow["BidPrice"] = prItem.BidPrice;
                    dtRow["BidWinnerPrice"] = prItem.BidWinnerPrice;
                    dtRow["BidPriceDifference"] = prItem.BidPriceDifference;
                    dtRow["ContractorBidWinnerName"] = prItem.ContractorBidWinnerName;


                    prDataTable.Rows.Add(dtRow);
                    prCount += 1;
                }

                pb8Sheet.Cells.ImportDataTable(prDataTable, false, 5, 0, prDataTable.Rows.Count, prDataTable.Columns.Count, true);

                // Import Contract data
                var contractCount = 1;
                var contractList = this.contractService.GetAllByPR(listPRId).OrderBy(t => t.Number).ToList();
                var contractDataTable = new DataTable();
                contractDataTable.Columns.AddRange(new[]
                {
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("ProcurementRequirementName", typeof (String)),
                    new DataColumn("Number", typeof (String)),
                    new DataColumn("ContractorSelectedName", typeof (String)),
                    new DataColumn("EffectedDate", typeof (Object)),
                    new DataColumn("ContractValueVND", typeof (Double)),
                    new DataColumn("ContractValueUSD", typeof (Double)),
                    new DataColumn("EndDate", typeof (Object)),
                    new DataColumn("ContractAdjustmentValueUSD", typeof (Double)),
                    new DataColumn("EndDateAdjustment", typeof (Object)),
                    new DataColumn("EndDateActual", typeof (Object)),
                    new DataColumn("CompleteCurrent", typeof (Double)),
                    new DataColumn("CompleteFromStartYear", typeof (Double)),
                    new DataColumn("CompleteFromStartDate", typeof (Double)),
                    new DataColumn("PaymentValueCurrentVND", typeof (Double)),
                    new DataColumn("PaymentValueCurrentUSD", typeof (Double)),
                    new DataColumn("PaymentValueFromStartYearVND", typeof (Double)),
                    new DataColumn("PaymentValueFromStartYearUSD", typeof (Double)),
                    new DataColumn("PaymentValueFromStartDateVND", typeof (Double)),
                    new DataColumn("PaymentValueFromStartDateUSD", typeof (Double)),
                });

                foreach (var contractItem in contractList)
                {
                    var dtRow = contractDataTable.NewRow();
                    dtRow["NoIndex"] = contractCount.ToString();
                    dtRow["ProcurementRequirementName"] = contractItem.ProcurementRequirementName;
                    dtRow["Number"] = contractItem.Number;
                    dtRow["ContractorSelectedName"] = contractItem.ContractorSelectedName;
                    dtRow["EffectedDate"] = contractItem.EffectedDate;
                    dtRow["ContractValueVND"] = contractItem.ContractValueVND;
                    dtRow["ContractValueUSD"] = contractItem.ContractValueUSD;
                    dtRow["EndDate"] = contractItem.EndDate;
                    dtRow["ContractAdjustmentValueUSD"] = contractItem.ContractAdjustmentValueUSD;
                    dtRow["EndDateAdjustment"] = contractItem.EndDateAdjustment;
                    dtRow["EndDateActual"] = contractItem.EndDateActual;
                    dtRow["CompleteCurrent"] = contractItem.CompleteCurrent.GetValueOrDefault() / 100;
                    dtRow["CompleteFromStartYear"] = contractItem.CompleteFromStartYear.GetValueOrDefault() / 100;
                    dtRow["CompleteFromStartDate"] = contractItem.CompleteFromStartDate.GetValueOrDefault() / 100;
                    dtRow["PaymentValueCurrentVND"] = contractItem.PaymentValueCurrentVND.GetValueOrDefault();
                    dtRow["PaymentValueCurrentUSD"] = contractItem.PaymentValueCurrentUSD.GetValueOrDefault();
                    dtRow["PaymentValueFromStartYearVND"] = contractItem.PaymentValueFromStartYearVND.GetValueOrDefault();
                    dtRow["PaymentValueFromStartYearUSD"] = contractItem.PaymentValueFromStartYearUSD.GetValueOrDefault();
                    dtRow["PaymentValueFromStartDateVND"] = contractItem.PaymentValueFromStartDateVND.GetValueOrDefault();
                    dtRow["PaymentValueFromStartDateUSD"] = contractItem.PaymentValueFromStartDateUSD.GetValueOrDefault();
                    contractDataTable.Rows.Add(dtRow);
                    contractCount += 1;
                }

                pb9Sheet.Cells.ImportDataTable(contractDataTable, false, 8, 0, contractDataTable.Rows.Count, contractDataTable.Columns.Count, true);

                var filename = "TenderPackageReport" +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                workbook.Save(filePath + filename);
                this.Download_File(filePath + filename);

            }
        }

        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }
    }
}

