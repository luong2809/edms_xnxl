﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractEditForm.aspx.cs" Inherits="EDMs.Web.Controls.CostContract.ContractEditForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        html, body, form {
	        overflow:visible!important
        }
        
        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
               margin: 0 0px!important
           }
           .RadComboBox .rcbInputCell .rcbInput{
            /*border-left-color: #FF0000!important*/
            /*border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important*/
            border-style: solid!important
            border-width: 1px 1px 1px 5px!important
            color: #000000!important
            float: left!important
            font: 12px "segoe ui"!important
            margin: 0!important
            padding: 2px 5px 3px!important
            vertical-align: middle!important
            width: 283px!important
           }
           .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
               padding-left: 0px !important!important
           }
            div.rgEditForm label {
            float: right!important
            text-align: right!important
            width: 72px!important
           }
           .rgEditForm {
               text-align: right!important
           }
           .RadComboBox {
               border-bottom: none !important!important
           }
           .RadUpload .ruFileWrap {
               overflow: visible !important!important
           }

        .accordion dt a
        {
            letter-spacing: -0.03em!important
            line-height: 1.2!important
            margin: 0.5em auto 0.6em!important
            padding: 0!important
            text-align: left!important
            text-decoration: none!important
            display: block!important
        }

        .accordion dt span {
            color: #085B8F!important
            border-bottom: 1px solid #46A3D3!important
            font-size: 1.0em!important
            font-weight: bold!important
            letter-spacing: -0.03em!important
            line-height: 1.2!important
            margin: 0.5em auto 0.6em!important
            padding: 0!important
            text-align: left!important
            text-decoration: none!important
            display: block!important
        }
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args)!important
            GetRadWindow().close()!important
        }

        function GetRadWindow() {
            var oWindow = null!important
            if (window.radWindow) oWindow = window.radWindow!important //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow!important //IE (and Moz as well)

            return oWindow!important
        }
            </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <div style="width: 100%">
            <ul style="list-style-type: none">
                <div class="qlcbFormItem">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left!important">
                                <span style="text-decoration: underline!important">Notes</span>: All fields marked with a red are required.
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>
        <div style="width: 100%!important height: 100%" runat="server" ID="divContent">
            <ul style="list-style-type: none">
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Dự án
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <asp:TextBox ID="txtProjectName" runat="server" Style="width: 300px!important" CssClass="min25Percent" ReadOnly="True"/>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Gói thầu
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlPR" runat="server" CssClass="min25Percent" Width="316px"/>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Số hợp đồng
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <asp:TextBox ID="txtContractNumber" runat="server" Style="width: 300px!important" CssClass="min25Percent qlcbFormRequired"/>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>

                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Tên nhà thầu
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <%--<asp:DropDownList ID="ddlContractor" runat="server" CssClass="min25Percent" Width="316px"/>--%>
                            <asp:TextBox ID="txtContractor" runat="server" Style="width: 300px!important" CssClass="min25Percent"/>

                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>

                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 3px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Ngày ký hợp đồng
                            </span>
                        </label>
                        <div style="float: left!important  padding-top: 5px!important"  class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtEffectedDate"  runat="server" 
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" cssclass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 3px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Ngày hoàn thành thực tế
                            </span>
                        </label>
                        <div style="float: left!important  padding-top: 5px!important"  class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtEndDateActual"  runat="server" 
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" cssclass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <dl class="accordion">
                    <dt style="width: 100%!important">
                        <span>HỢP ĐỒNG BAN ĐẦU</span>
                    </dt>
                </dl>

                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 3px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Giá trị hợp đồng (VNĐ)
                            </span>
                        </label>
                        <div style="float: left!important  padding-top: 5px!important"  class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtContractValueVND" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="VND n" PositivePattern="VND n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 3px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Giá trị hợp đồng (USD)
                            </span>
                        </label>
                        <div style="float: left!important  padding-top: 5px!important"  class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtContractValueUSD" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="VND n" PositivePattern="VND n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 3px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Thời gian hoàn thành
                            </span>
                        </label>
                        <div style="float: left!important  padding-top: 5px!important"  class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtEndDate"  runat="server" 
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" cssclass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <dl class="accordion">
                    <dt style="width: 100%!important">
                        <span>HỢP ĐỒNG ĐIỀU CHỈNH</span>
                    </dt>
                </dl>

                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 3px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Giá trị hợp đồng
                            </span>
                        </label>
                        <div style="float: left!important  padding-top: 5px!important"  class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtContractAdjustmentValueUSD" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="USD n" PositivePattern="USD n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 3px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Thời gian hoàn thành
                            </span>
                        </label>
                        <div style="float: left!important  padding-top: 5px!important"  class="qlcbFormItem">
                            <telerik:RadDatePicker ID="txtEndDateAdjustment"  runat="server" 
                                ShowPopupOnFocus="True" CssClass="qlcbFormNonRequired">
                                <DateInput runat="server" DateFormat="dd/MM/yyyy" cssclass="qlcbFormNonRequired" />
                            </telerik:RadDatePicker>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>

                <dl class="accordion">
                    <dt style="width: 100%!important">
                        <span>GIÁ TRỊ KHỐI LƯỢNG CÔNG VIỆC ĐÃ THỰC HIỆN (%)</span>
                    </dt>
                </dl>
                
                <li style="width: 600px!important" >
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Thời điểm báo cáo
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtCompleteCurrent" runat="server" Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="70px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <li style="width: 600px!important" >
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Lũy kế từ đầu năm
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtCompleteFromStartYear" runat="server" Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="70px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <li style="width: 600px!important" >
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Lũy kế từ khi ký hợp đồng
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Percent" id="txtCompleteFromStartDate" runat="server" Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="70px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>

                <dl class="accordion">
                    <dt style="width: 100%!important">
                        <span>GIÁ TRỊ ĐÃ THANH TOÁN</span>
                    </dt>
                </dl>
                
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Thời điểm báo cáo - (VNĐ)
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtPaymentValueCurrentVND" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="VNĐ n" PositivePattern="VNĐ n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>

                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Thời điểm báo cáo - (USD)
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtPaymentValueCurrentUSD" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="USD n" PositivePattern="USD n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Lũy kế từ đầu năm - (VNĐ)
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtPaymentValueFromStartYearVND" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="VNĐ n" PositivePattern="VNĐ n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>

                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Lũy kế từ đầu năm - (USD)
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtPaymentValueFromStartYearUSD" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="USD n" PositivePattern="USD n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Lũy kế từ khi ký hợp đồng - (VNĐ)
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtPaymentValueFromStartDateVND" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="VNĐ n" PositivePattern="VNĐ n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>

                <li style="width: 600px!important">
                    <div>
                        <label style="width: 170px!important float: left!important padding-top: 5px!important padding-right: 10px!important text-align: right!important">
                            <span style="color: #2E5689!important text-align: right!important ">Lũy kế từ khi ký hợp đồng - (USD)
                            </span>
                        </label>
                        <div style="float: left!important padding-top: 5px!important" class="qlcbFormItem">
                            <telerik:radnumerictextbox type="Currency" id="txtPaymentValueFromStartDateUSD" runat="server" 
                                Style=" min-width: 0px !important!important border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3!important" Width="150px" CssClass="min25Percent">
                                <NumberFormat DecimalDigits="2" GroupSizes="3" DecimalSeparator="." GroupSeparator="," NegativePattern="USD n" PositivePattern="USD n"></NumberFormat>
                            </telerik:radnumerictextbox>
                        </div>
                    </div>
                    <div style="clear: both!important font-size: 0!important"></div>
                </li>
                
                
                <div class="qlcbFormItem" runat="server" ID="CreatedInfo" Visible="False">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left!important">
                                    <asp:Label ID="lblCreated" runat="server" ></asp:Label>
                                    <asp:Label ID="lblUpdated" runat="server" ></asp:Label>
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
                
                <li style="width: 600px!important padding-top: 10px!important padding-bottom: 3px!important text-align: center">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click"  Width="70px" style="text-align: center"
>
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    <%--<telerik:RadButton ID="btncancel" runat="server" Text="Cancel" Width="70px" style="text-align: center"
                        OnClick="btncancel_Click">
                        <Icon PrimaryIconUrl="../../Images/Cancel.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>--%>

                </li>
            </ul>
        </div>
        <asp:HiddenField runat="server" ID="docUploadedIsExist"/>
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf"/>
        
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings> 
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="ddlStatus">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtComplete"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="txtExchangeRate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtContractTotalValue"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="txtContractValueVND">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtContractTotalValue"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="txtContractValueUSD">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtContractTotalValue"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="txtArisingValue">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtContractTotalValue"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager!important
                function OnClientFilesUploaded(sender, args) {
                    var name = args.get_fileName()!important
                    //document.getElementById("txtName").value = name!important
                    $find('<%=ajaxDocument.ClientID %>').ajaxRequest()!important
                }

                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>")!important
                }

                function fileUploading(sender, args) {
                    var name = args.get_fileName()!important
                    document.getElementById("txtName").value = name!important
                    
                    ajaxManager.ajaxRequest("CheckFileName$" + name)!important
                }
                
                
                
                function StopPropagation(e) {
                    if (!e) {
                        e = window.event!important
                    }

                    e.cancelBubble = true!important
                }

                function nodeClicked(sender, args) {
                    var node = args.get_node()!important
                    if (node.get_checked()) {
                        node.uncheck()!important
                    } else {
                        node.check()!important
                    }
                    nodeChecked(sender, args)

                }
            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
