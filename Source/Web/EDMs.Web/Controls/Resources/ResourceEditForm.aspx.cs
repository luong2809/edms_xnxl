﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The Resource edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Resources
{
    using System;
    using System.Web.UI;
    using Business.Services;

    /// <summary>
    /// The Resource edit form.
    /// </summary>
    public partial class ResourceEditForm : Page
    {

        /// <summary>
        /// The Resource service
        /// </summary>
        private ResourceService _ResourceService;
        public ResourceEditForm()
        {     
            _ResourceService =new ResourceService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadDataResourceStatus();

                if (!string.IsNullOrEmpty(Request.QueryString["ResourceId"]))
                {
                    var Resource = this._ResourceService.GetByID(Convert.ToInt32(Request.QueryString["ResourceId"]));
                    if (Resource != null)
                    {
                        txtTen.Text = Resource.LastName;
                        txtHoTenLot.Text = Resource.FirstName;
                        ddlGioiTinh.SelectedValue = Resource.Sex == "Nam" ? "1" : "0";
                        txtNgaySinh.SelectedDate = Resource.DateOfBirth;
                        ddlTinhTrangHonNhan.SelectedValue = Resource.MarialStatus;
                        txtCMND.Text = Resource.IdentityCard;
                        txtDiaChi.Text = Resource.Address1;
                        txtSoDienThoai.Text = Resource.CellPhone;
                        txtNgheNghiep.Text = Resource.Occupation;
                        txtEmail.Text = Resource.Email;
                        ddlTinhTrang.SelectedValue = Resource.ResourceGroupId.ToString();
                        txtMaKhachHang.Text = Resource.SSN;
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            Resource Resource;
            if (!string.IsNullOrEmpty(Request.QueryString["ResourceId"]))
            {
                Resource = this._ResourceService.GetByID(Convert.ToInt32(Request.QueryString["ResourceId"]));
                Resource.LastName = txtTen.Text;
                Resource.FirstName = txtHoTenLot.Text;
                Resource.FullName = txtHoTenLot.Text + " " + txtTen.Text;
                Resource.Sex = ddlGioiTinh.SelectedValue == "1" ? "Nam" : "Nữ";
                Resource.DateOfBirth = txtNgaySinh.SelectedDate;
                Resource.MarialStatus = ddlTinhTrangHonNhan.SelectedValue;
                Resource.IdentityCard = txtCMND.Text;
                Resource.Address1 = txtDiaChi.Text;
                Resource.CellPhone = txtSoDienThoai.Text;
                Resource.Occupation = txtNgheNghiep.Text;
                Resource.Email = txtEmail.Text;
                Resource.ResourceGroupId = string.IsNullOrEmpty(ddlTinhTrang.SelectedValue) ? 0 : Convert.ToInt32(ddlTinhTrang.SelectedValue);
                Resource.SSN = txtMaKhachHang.Text;
                Resource.LastUpdate = DateTime.Now;    

                this._ResourceService.Update(Resource);
                
            }
            else
            {
                Resource = new Resource();
                Resource.LastName = txtTen.Text;
                Resource.FirstName = txtHoTenLot.Text;
                Resource.FullName = txtHoTenLot.Text + " " + txtTen.Text;
                Resource.Sex = ddlGioiTinh.SelectedValue == "1" ? "Nam" : "Nữ";
                Resource.DateOfBirth = txtNgaySinh.SelectedDate;
                Resource.MarialStatus = ddlTinhTrangHonNhan.SelectedValue;
                Resource.IdentityCard = txtCMND.Text;
                Resource.Address1 = txtDiaChi.Text;
                Resource.CellPhone = txtSoDienThoai.Text;
                Resource.Occupation = txtNgheNghiep.Text;
                Resource.Email = txtEmail.Text;
                Resource.ResourceGroupId = string.IsNullOrEmpty(ddlTinhTrang.SelectedValue) ? 0 : Convert.ToInt32(ddlTinhTrang.SelectedValue);
                Resource.SSN = txtMaKhachHang.Text;
                Resource.LastUpdate = DateTime.Now;    

                this._ResourceService.Insert(Resource);
            }
            
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// Load data for ddlTinhTrang
        /// </summary>
        private void LoadDataResourceStatus()
        {
            ResourceGroupService resourceGroupService = new ResourceGroupService();
            ddlTinhTrang.DataSource = resourceGroupService.GetAll();
            ddlTinhTrang.DataValueField = "Id";
            ddlTinhTrang.DataTextField = "Name";
            ddlTinhTrang.DataBind();
        }
    }
}