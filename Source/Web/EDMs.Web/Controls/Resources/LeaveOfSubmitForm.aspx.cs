﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeaveOfSubmitForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Resources
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using Data.Entities;
    using Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class LeaveOfSubmitForm : Page
    {
        /// <summary>
        /// Appointment service
        /// </summary>
        private AppointmentService appointmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeaveOfSubmitForm"/> class.
        /// </summary>
        public LeaveOfSubmitForm()
        {
            this.appointmentService = new AppointmentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var resourceService = new ResourceService();
                var listResources = new List<Resource>();
                Resource resource = resourceService.GetByID(UserSession.Current.User.ResourceId.GetValueOrDefault());
                if (UserSession.Current.User.IsAdmin.GetValueOrDefault() || UserSession.Current.User.CanApprove.GetValueOrDefault())
                {
                    listResources = resourceService.GetAll();
                }
                else if (resource != null)
                {
                    if (resource.IsResource.GetValueOrDefault())
                    {
                        listResources = resourceService.GetAll().Where(t => t.IsResource.GetValueOrDefault()).ToList();
                    }
                    else if (!resource.IsResource.GetValueOrDefault() && resource.IsFulltime.GetValueOrDefault())
                    {
                        listResources =
                            resourceService.GetAll().Where(
                                t => t.IsResource.GetValueOrDefault() || t.IsFulltime.GetValueOrDefault()).ToList();
                    }
                }

                ddlResource.DataSource = listResources;
                ddlResource.DataTextField = "FullName";
                ddlResource.DataValueField = "Id";
                ddlResource.DataBind();
                if (string.IsNullOrEmpty(Request.QueryString["appId"]))
                {
                    if (resource != null)
                    {
                        ddlResource.SelectedValue = resource.Id.ToString(CultureInfo.InvariantCulture);
                        txtEmail.Text = resource.Email;
                        txtSoDienThoai.Text = resource.CellPhone;
                        txtDiaChi.Text = resource.Address1;
                        lblRemainDayLeaveOf.Text = resource.RemainDayLeaveOf.ToString();

                        if (resource.IsFulltime.GetValueOrDefault())
                        {
                            txtTuNgay.TimeView.Interval = new TimeSpan(4, 0, 0);
                            txtDenNgay.TimeView.Interval = new TimeSpan(4, 0, 0);
                        }
                        else
                        {
                            txtTuNgay.TimeView.Interval = new TimeSpan(1, 0, 0);
                            txtDenNgay.TimeView.Interval = new TimeSpan(1, 0, 0);
                        }
                    }
                }
                else
                {
                    var appointment = this.appointmentService.GetByID(Convert.ToInt32(Request.QueryString["appId"]));
                    if (appointment != null)
                    {
                        var resourceApp = resourceService.GetByID(appointment.ResourceId.GetValueOrDefault());
                        if (resourceApp != null)
                        {
                            ddlResource.SelectedValue = resourceApp.Id.ToString(CultureInfo.InvariantCulture);
                            txtEmail.Text = resourceApp.Email;
                            txtSoDienThoai.Text = resourceApp.CellPhone;
                            txtDiaChi.Text = resourceApp.Address1;
                            lblRemainDayLeaveOf.Text = resourceApp.RemainDayLeaveOf.ToString();
                            ddlResource.Enabled = false;

                            if (resourceApp.IsFulltime.GetValueOrDefault())
                            {
                                txtTuNgay.TimeView.Interval = new TimeSpan(4, 0, 0);
                                txtDenNgay.TimeView.Interval = new TimeSpan(4, 0, 0);
                            }
                            else
                            {
                                txtTuNgay.TimeView.Interval = new TimeSpan(1, 0, 0);
                                txtDenNgay.TimeView.Interval = new TimeSpan(1, 0, 0);
                            }
                        }

                        txtLydo.Text = appointment.Description;
                        txtTuNgay.SelectedDate = appointment.Start;
                        txtDenNgay.SelectedDate = appointment.End;

                        txtLydo.ReadOnly = appointment.IsApprove.GetValueOrDefault();
                        ////txtTuNgay.DateInput.ReadOnly = appointment.IsApprove.GetValueOrDefault();
                        ////txtTuNgay.DatePopupButton.Visible = appointment.IsApprove.GetValueOrDefault();
                        ////txtTuNgay.TimePopupButton.Visible = appointment.IsApprove.GetValueOrDefault();
                        
                        ////txtDenNgay.DateInput.ReadOnly = appointment.IsApprove.GetValueOrDefault();
                        ////txtDenNgay.DatePopupButton.Visible = appointment.IsApprove.GetValueOrDefault();
                        ////txtDenNgay.TimePopupButton.Visible = appointment.IsApprove.GetValueOrDefault();

                        ////btnCapNhat.Visible = appointment.IsApprove.GetValueOrDefault();
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (DurationValidator.IsValid)
            {
                if (string.IsNullOrEmpty(Request.QueryString["appId"]))
                {
                    var appointment = new Appointment
                                          {
                                              SetDisplayName =
                                                  "Nghỉ phép: " + ddlResource.Items[ddlResource.SelectedIndex].Text,
                                              Description = txtLydo.Text,
                                              AppointmentDate = txtTuNgay.SelectedDate,
                                              Start = txtTuNgay.SelectedDate,
                                              End = txtDenNgay.SelectedDate,
                                              CreatedBy = UserSession.Current.User.Id,
                                              UpdateBy = UserSession.Current.User.Id,
                                              LastUpdate = DateTime.Now,
                                              IsBlock = true,
                                              ResourceId = Convert.ToInt32(ddlResource.SelectedValue)
                                          };

                    this.appointmentService.Insert(appointment);
                }
                else
                {
                    var appointment = this.appointmentService.GetByID(Convert.ToInt32(Request.QueryString["appId"]));
                    if (appointment != null)
                    {
                        appointment.Description = txtLydo.Text;
                        appointment.AppointmentDate = txtTuNgay.SelectedDate;
                        appointment.Start = txtTuNgay.SelectedDate;
                        appointment.End = txtDenNgay.SelectedDate;
                        appointment.UpdateBy = UserSession.Current.User.Id;
                        appointment.LastUpdate = DateTime.Now;

                        this.appointmentService.Update(appointment);
                    }
                }

                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The duration validator_ on server validate.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        protected void CheckValidDuration(object source, ServerValidateEventArgs args)
        {
            args.IsValid = DateTime.Compare(txtTuNgay.SelectedDate.Value, DateTime.Now) > 0 && 
                DateTime.Compare(txtTuNgay.SelectedDate.Value, txtDenNgay.SelectedDate.Value) < 0;
        }

        /// <summary>
        /// The ddl resource_ selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void ddlResource_SelectedIndexChanged(object sender, EventArgs e)
        {
            var resourceService = new ResourceService();
            Resource resource = resourceService.GetByID(Convert.ToInt32(ddlResource.SelectedValue));
            if (resource != null)
            {
                txtEmail.Text = resource.Email;
                txtSoDienThoai.Text = resource.CellPhone;
                txtDiaChi.Text = resource.Address1;
            }
        }
    }
}