﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using Telerik.Web.Zip;

namespace EDMs.Web
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class EMDR : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService = new RevisionService();

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService = new FolderService();
        private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly UserService userService = new UserService();

        private readonly AttachFilesPackageService attachFileService = new AttachFilesPackageService();

        private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly PackageService packageService = new PackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly DisciplineService disciplineService = new DisciplineService();

        private readonly RoleService roleService = new RoleService();

        private readonly TemplateManagementService templateManagementService = new TemplateManagementService();


        private readonly ContractorService contractorService = new ContractorService();


        //  private readonly DocRoleMatrixService docRoleMatrixService = new DocRoleMatrixService();

        // private readonly DistributionMatrixDetailByUserService dmDetailByUserService = new DistributionMatrixDetailByUserService();

        private readonly DocumentAssignedWorkflowService docAssignedWorkflowService = new DocumentAssignedWorkflowService();

        //  private readonly DocumentAssignedUserService docAssignedUserService = new DocumentAssignedUserService();

        //   private readonly WorkflowDetailService wfDetailService = new WorkflowDetailService();

        //   private readonly WorkflowStepService wfStepService = new WorkflowStepService();

        //  private readonly DistributionMatrixService dmService = new DistributionMatrixService();


        //  private readonly WorkflowService wfService = new WorkflowService();
        //   private readonly MarkupFileService markupFileService = new MarkupFileService();

        private readonly ProjectUserService projectUserService = new ProjectUserService();

        private readonly ProjectDepartmentService projectDepartmentService = new ProjectDepartmentService();

        private readonly AutoDistributionMatriceService autoDistributionMatriceService = new AutoDistributionMatriceService();

        private readonly StatusService statusService = new StatusService();
        private readonly CodeService docCodeService = new CodeService();

        //    private readonly DocumentStatusProcessService docStatusProcessService = new DocumentStatusProcessService();

        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        /// <summary>
        /// The list folder id.
        /// </summary>
        private List<int> listFolderId = new List<int>();

        //private List<DocumentPackage> DocListInWFProccess
        //{
        //    get { return this.documentPackageService.GetAllDocInWFProccess(); }
        //}

        //private List<DocumentAssignedUser> WorkAssigned
        //{
        //    get { return this.docAssignedUserService.GetAllIncompleteByUser(UserSession.Current.User.Id); }
        //}

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");

            if (!Page.IsPostBack)
            {
                this.LoadObjectTree();
                Session.Add("IsListAll", false);
                var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
                if (projectObj != null)
                {
                    this.InitControl(projectObj);
                }
                else
                {
                    this.RadPane3.Visible = false;
                }
            }
        }

        private void InitControl(ScopeProject projectObj)
        {
            // Get all User have permission with selected project
            var dcCUserPermission = this.projectUserService.GetAllSharedDCRole(projectObj.ID).Select(t => t.UserId.GetValueOrDefault()).ToList();
            var managerUserPermission = this.projectUserService.GetAllSharedManagerRole(projectObj.ID).Select(t => t.UserId.GetValueOrDefault()).ToList();
            if (UserSession.Current.User.Id == projectObj.DCId
                || UserSession.Current.User.Id == projectObj.PMId)
            {
                dcCUserPermission.Add(UserSession.Current.User.Id);
            }

            if (UserSession.Current.IsManager)
            {
                managerUserPermission.Add(UserSession.Current.User.Id);
            }

            Session.Add("DCUserPermission", dcCUserPermission);
            Session.Add("ManagerUserPermission", managerUserPermission);
            // ------------------------------------------------------------------------------------------
            if (!UserSession.Current.IsAdmin
                && !dcCUserPermission.Contains(UserSession.Current.User.Id)
                && !managerUserPermission.Contains(UserSession.Current.User.Id))
            {
                // Hide Add new menu
                this.CustomerMenu.Items[0].Visible = false;
                this.CustomerMenu.Items[1].Visible = false;
                // ------------------------------------------

                // Hide export distribution matrix
                // this.CustomerMenu.Items[4].Visible = false;
                // this.CustomerMenu.Items[5].Visible = false;
                // --------------------------------------------

                // Hide all admin function
                foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                {
                    if (item.Value == "Adminfunc")
                    {
                        item.Visible = false;
                    }
                }
                // ------------------------------------------------------------------------------------------
                ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons[0].Visible = false;
                ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons[2].Visible = false;
                ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons[3].Visible = false;
                ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons[4].Visible = false;
                ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons[5].Visible = false;
                ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons[6].Visible = false;

                // this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
            }
            //if (this.ddlProject.SelectedValue != "51")
            //{
            //    this.CustomerMenu.Items[6].Visible = false;
            //}
            //else
            //{
            //    this.CustomerMenu.Items[6].Visible = true;
            //}

            // this.grdDocument.MasterTableView.GetColumn("DocStatus").Visible = projectObj.ProjectTypeId == 2;
            this.IsFullPermission.Value = "true";
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false, bool isListAll = false)
        {
            var cbShowAll = (CheckBox)this.CustomerMenu.Items[6].FindControl("ckbShowAll");
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);

            var fullDocList = new List<DocumentPackage>();
            if (projectObj != null)
            {

                var DocOfProject = this.documentPackageService.GetAllOfProject(projectObj.ID, 1, cbShowAll.Checked);
                // Show document depend on selected Discipline
                if (this.rtvDiscipline.SelectedNode != null)
                {
                    fullDocList = DocOfProject.Where(t => t.DisciplineId == Convert.ToInt32(this.rtvDiscipline.SelectedValue)).ToList();//this.documentPackageService.GetAllByDiscipline(Convert.ToInt32(this.rtvDiscipline.SelectedValue),cbShowAll.Checked);
                    //fullDocList = fullDocList.Where(t => t.TypeId == 1).ToList();

                    //if (projectObj.IsAutoDistribution.GetValueOrDefault())
                    //{
                    //    docListAutoDistribution = fullDocList.Where(t => t.HasAttachFile.GetValueOrDefault()).ToList();
                    //}

                    //if (UserSession.Current.IsAdmin
                    //    || dcUserPermission.Contains(UserSession.Current.UserId)
                    //    || UserSession.Current.UserId == projectObj.PMId)
                    //{
                    //    docList = fullDocList;
                    //}
                    //else if (managerUserPermission.Contains(UserSession.Current.UserId)
                    //    || UserSession.Current.IsDC)
                    //{
                    //    var docRoleMatrix = this.docRoleMatrixService.GetAllDocRoleMatrixByGroup(UserSession.Current.RoleId, projectObj.ID);
                    //    if (docRoleMatrix.Count > 0)
                    //    {
                    //        var docIds = docRoleMatrix.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
                    //        docList = fullDocList.Where(t => docIds.Contains(t.ID)).ToList();
                    //    }
                    //}
                    //else if (UserSession.Current.IsLead || UserSession.Current.IsEngineer)
                    //else
                    //{
                    //    var dmDetailByUserList = this.dmDetailByUserService.GetAllDMDetailByUser(UserSession.Current.User.Id);
                    //    if (dmDetailByUserList.Count > 0)
                    //    {
                    //        var docIds = dmDetailByUserList.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
                    //        docList = fullDocList.Where(t => docIds.Contains(t.ID) || docIds.Contains(t.ParentId.GetValueOrDefault())).ToList();
                    //    }
                    //}
                    //finalDocList = docList;//.Union(docListAutoDistribution).Distinct().ToList();
                }
                // Show document of project
                else
                {
                    fullDocList = DocOfProject;// this.documentPackageService.GetAllRev(projectObj.ID)
                                               //: DocOfProject.Where(t=> t.IsLeaf.GetValueOrDefault()).ToList();//this.documentPackageService.GetAllLatestRev(projectObj.ID);

                    //fullDocList = fullDocList.Where(t => t.TypeId == 1).ToList();
                    //if (projectObj.IsAutoDistribution.GetValueOrDefault())
                    //{
                    //    docListAutoDistribution = fullDocList.Where(t => t.HasAttachFile.GetValueOrDefault()).ToList();
                    //}

                    //if (UserSession.Current.IsAdmin
                    //    || dcUserPermission.Contains(UserSession.Current.UserId)
                    //    || UserSession.Current.UserId == projectObj.PMId)
                    //{
                    //    docList = fullDocList;
                    //}
                    //else if (managerUserPermission.Contains(UserSession.Current.UserId)
                    //    || UserSession.Current.IsDC)
                    //{
                    //    var docRoleMatrix = this.docRoleMatrixService.GetAllDocRoleMatrixByGroup(UserSession.Current.RoleId, projectObj.ID);
                    //    var docIds = docRoleMatrix.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
                    //    docList = fullDocList.Where(t => docIds.Contains(t.ID)).ToList();
                    //}
                    //else //if (UserSession.Current.IsLead || UserSession.Current.IsEngineer)
                    //{
                    //    var dmDetailByUserList = this.dmDetailByUserService.GetAllDMDetailByUser(UserSession.Current.User.Id);
                    //    if (dmDetailByUserList.Count > 0)
                    //    {
                    //        var docIds = dmDetailByUserList.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
                    //        docList = fullDocList.Where(t => docIds.Contains(t.ID) || docIds.Contains(t.ParentId.GetValueOrDefault())).ToList();
                    //    }
                    //}

                    //finalDocList = docList; //.Union(docListAutoDistribution).Distinct().ToList();
                }
            }

            this.grdDocument.DataSource = fullDocList;
            Session.Add("EMDRDocumentList", fullDocList);
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }


            else if (e.Argument == "ExportEMDRReport_New")
            {
                this.ExportEMDRReportNew();
            }
            else if (e.Argument == "ExportEMDRReport_Selected")
            {
                this.ExportEMDRReportSelectedDocuments();
            }
            //else if (e.Argument == "ExportProgressReport")
            //{
            //    this.ExportProgressReport();
            //}
            else if (e.Argument == "Nextweekissue")
            {
                this.ExportEMDRNextWeekIssue();
            }
            else if (e.Argument == "DeleteAllDoc")
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        if (docObj.ParentId == null)
                        {
                            docObj.IsDelete = true;
                            docObj.UpdatedBy = UserSession.Current.User.Id;
                            docObj.UpdatedDate = DateTime.Now;
                            this.documentPackageService.Update(docObj);
                        }
                        else
                        {
                            var listRelateDoc =
                                this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                            if (listRelateDoc != null)
                            {
                                foreach (var objDoc in listRelateDoc)
                                {
                                    objDoc.IsDelete = true;
                                    objDoc.UpdatedBy = UserSession.Current.User.Id;
                                    objDoc.UpdatedDate = DateTime.Now;
                                    this.documentPackageService.Update(objDoc);
                                }
                            }
                        }
                    }
                }

                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ClearEMDRData")
            {
                var listDocPack = this.documentPackageService.GetAll();
                foreach (var documentPackage in listDocPack)
                {
                    this.documentPackageService.Delete(documentPackage);
                }

                var attachFilePackage = this.attachFilesPackageService.GetAll();
                foreach (var attachFilesPackage in attachFilePackage)
                {
                    var filePath = Server.MapPath(attachFilesPackage.FilePath);
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    this.attachFilesPackageService.Delete(attachFilesPackage);
                }

                this.grdDocument.Rebind();
            }
            //else if (e.Argument == "ExportDistributionMatrix")
            //{
            //    this.ExportDistributionMatrix();
            //}
            //else if (e.Argument == "ExportDMDetail")
            //{
            //    this.ExportDistributionMatrixDetail();
            //}
            //else if (e.Argument == "ExportDMLead")
            //{
            //    this.ExportDistributionMatrixLead();
            //}
            //else if (e.Argument == "ExportDMEngineer")
            //{
            //    this.ExportDistributionMatrixEngineer();
            //}
            else if (e.Argument.Split('_')[0] == "ExportEMDROverDueReport")
            {
                var dueration = Convert.ToInt32(e.Argument.Split('_')[1]);
                this.ExportEMDROverdureReport(dueration);
            }
            else if (e.Argument.Split('_')[0] == "ExportEMDROverDueResponseReport")
            {
                var dueration = Convert.ToInt32(e.Argument.Split('_')[1]);
                this.ExportEMDROverdueResponseReport(dueration);
            }
            else if (e.Argument == "ExportUpdateEDMR")
            {
                this.ExportUpdateEDMR();
            }
            else if (e.Argument == "ExportMasterList")
            {
                this.ExportMasterListTemplate();
            }
            else if (e.Argument == "ExportEMDRReport")
            {
                var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.ddlProject.SelectedValue);
                var contractorList = this.contractorService.GetAllByProject(projectID);
                dtFull.Columns.AddRange(new[]
                {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Department", typeof (String)),
                    new DataColumn("Start", typeof (String)),
                    new DataColumn("Planned", typeof (String)),
                    new DataColumn("RevName", typeof (String)),
                    new DataColumn("RevPlanned", typeof (String)),
                    new DataColumn("RevActual", typeof (String)),
                    new DataColumn("RevCommentCode", typeof (String)),
                    new DataColumn("Complete", typeof (Double)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("OutgoingNo", typeof (String)),
                    new DataColumn("OutgoingDate", typeof (String)),
                    new DataColumn("IncomingNo", typeof (String)),
                    new DataColumn("IncomingDate", typeof (String)),
                    new DataColumn("ICANo", typeof (String)),
                    new DataColumn("ICADate", typeof (String)),
                    new DataColumn("ICAReviewCode", typeof (String)),
                    new DataColumn("Notes", typeof (String)),
                    new DataColumn("IsEMDR", typeof (String)),
                    new DataColumn("HasAttachFile", typeof (String)),
                });

                foreach (var contractor in contractorList)
                {

                }

                var dtDiscipline = new DataTable();
                dtDiscipline.Columns.AddRange(new[]
                {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Start", typeof (DateTime)),
                    new DataColumn("RevName", typeof (String)),
                    new DataColumn("RevPlanned", typeof (DateTime)),
                    new DataColumn("RevActual", typeof (DateTime)),
                    new DataColumn("Complete", typeof (Double)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("Department", typeof (String)),
                    new DataColumn("Notes", typeof (String)),
                    new DataColumn("IsEMDR", typeof (String)),
                    new DataColumn("HasAttachFile", typeof (String)),
                    new DataColumn("OutgoingNo", typeof (String)),
                    new DataColumn("OutgoingDate", typeof (String)),
                });
                projectName = this.ddlProject.SelectedItem.Text;

                if (this.rtvDiscipline.SelectedNode != null)
                {

                    var templateManagement = this.templateManagementService.GetSpecial(1, projectID);
                    if (templateManagement != null)
                    {
                        workbook.Open(Server.MapPath(templateManagement.FilePath));

                        var sheets = workbook.Worksheets;

                        var DisciplineId = Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value);
                        var Discipline = this.disciplineService.GetById(DisciplineId);
                        if (Discipline != null)
                        {
                            sheets[0].Name = Discipline.Name;
                            sheets[0].Cells["C7"].PutValue(Discipline.Name);
                            sheets[0].Cells["B7"].PutValue(this.ddlProject.SelectedValue + "," + DisciplineId);
                            sheets[0].Cells["M4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            docList =
                                this.documentPackageService.GetAllEMDRByDiscipline(DisciplineId, false)
                                    .OrderBy(t => t.DocNo)
                                    .ToList();

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();
                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType =
                                    this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                var dataRow = dtDiscipline.NewRow();
                                dataRow["DocId"] = -1;
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtDiscipline.Rows.Add(dataRow);

                                var listDocByDocType =
                                    docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtDiscipline.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;
                                    dataRow["Start"] = (object)document.StartDate ?? DBNull.Value;
                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["RevPlanned"] = (object)document.RevisionPlanedDate ?? DBNull.Value;
                                    dataRow["RevActual"] = (object)document.RevisionActualDate ?? DBNull.Value;
                                    dataRow["Complete"] = document.Complete / 100;
                                    dataRow["Weight"] = document.Weight / 100;
                                    dataRow["Department"] = document.DeparmentName;
                                    dataRow["Notes"] = document.Notes;
                                    dataRow["IsEMDR"] = document.IsEMDR.GetValueOrDefault() ? "x" : string.Empty;
                                    dataRow["HasAttachFile"] = document.HasAttachFile.GetValueOrDefault()
                                        ? "x"
                                        : string.Empty;
                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    count += 1;
                                    dtDiscipline.Rows.Add(dataRow);
                                }
                            }

                            sheets[0].Cells["A7"].PutValue(dtDiscipline.Rows.Count);

                            sheets[0].Cells.ImportDataTable(dtDiscipline, false, 7, 1, dtDiscipline.Rows.Count, 16,
                                true);
                            sheets[1].Cells.ImportDataTable(dtDiscipline, false, 7, 1, dtDiscipline.Rows.Count, 16,
                                true);

                            sheets[0].Cells[7 + dtDiscipline.Rows.Count, 2].PutValue("Total");

                            var txtDisciplineComplete =
                                this.CustomerMenu.Items[3].FindControl("txtDisciplineComplete") as RadNumericTextBox;
                            var txtDisciplineWeight =
                                this.CustomerMenu.Items[3].FindControl("txtDisciplineWeight") as RadNumericTextBox;
                            if (txtDisciplineComplete != null)
                            {
                                sheets[0].Cells[7 + dtDiscipline.Rows.Count, 9].PutValue(
                                    txtDisciplineComplete.Value / 100);
                            }

                            if (txtDisciplineWeight != null)
                            {
                                sheets[0].Cells[7 + dtDiscipline.Rows.Count, 10].PutValue(txtDisciplineWeight.Value /
                                                                                          100);
                            }
                            sheets[0].AutoFitRows();
                            sheets[1].IsVisible = false;

                            var filename = projectName + " - " + Discipline.Name + " EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.DownloadByWriteByte(filePath + filename, filename, true);

                        }
                    }
                }
                else
                {
                    var listDisciplineInPermission = UserSession.Current.User.Id == 1
                        ? this.disciplineService.GetAllDisciplineOfProject(
                            Convert.ToInt32(this.ddlProject.SelectedValue))
                            .OrderBy(t => t.ID)
                            .ToList()
                        : this.disciplineService.GetAllDisciplineInPermission(UserSession.Current.User.Id,
                            !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                : 0)
                            .OrderBy(t => t.ID).ToList();

                    if (listDisciplineInPermission.Count > 0)
                    {
                        var templateManagement = this.templateManagementService.GetSpecial(2,
                            projectID);
                        if (templateManagement != null)
                        {
                            workbook.Open(Server.MapPath(templateManagement.FilePath));

                            var totalDoc = 0;
                            var totalDocIssues = 0;
                            var totalDocRev0Issues = 0;
                            var totalDocRev1Issues = 0;
                            var totalDocRev2Issues = 0;
                            var totalDocRev3Issues = 0;
                            var totalDocRev4Issues = 0;
                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;

                            var totalDocDontIssues = 0;

                            var sheets = workbook.Worksheets;
                            var wsSummary = sheets[0];
                            wsSummary.Cells.InsertRows(7, listDisciplineInPermission.Count - 1);

                            for (int i = 0; i < listDisciplineInPermission.Count; i++)
                            {
                                dtFull.Rows.Clear();

                                sheets.AddCopy(1);

                                sheets[i + 2].Name = listDisciplineInPermission[i].Name;
                                sheets[i + 2].Cells["V4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                                sheets[i + 2].Cells["C7"].PutValue(listDisciplineInPermission[i].Name);

                                // Add hyperlink
                                var linkName = listDisciplineInPermission[i].Name;
                                wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                                wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1,
                                    "'" + listDisciplineInPermission[i].Name + "'" + "!D7");

                                docList =
                                    this.documentPackageService.GetAllEMDRByDiscipline(
                                        listDisciplineInPermission[i].ID, false).OrderBy(t => t.DocNo).ToList();
                                var docListHasAttachFile = docList.Where(t => t.HasAttachFile.GetValueOrDefault());
                                var wgDoc = docList.Count;
                                var wgDocIssues = docListHasAttachFile.Count();
                                var wgDocRev0Issues = docListHasAttachFile.Count(t => t.RevisionName == "0");
                                var wgDocRev1Issues = docListHasAttachFile.Count(t => t.RevisionName == "1");
                                var wgDocRev2Issues = docListHasAttachFile.Count(t => t.RevisionName == "2");
                                var wgDocRev3Issues = docListHasAttachFile.Count(t => t.RevisionName == "3");
                                var wgDocRev4Issues = docListHasAttachFile.Count(t => t.RevisionName == "4");
                                var wgDocRev5Issues = docListHasAttachFile.Count(t => t.RevisionName == "5");
                                var wgTotalDocRev = wgDocRev0Issues + wgDocRev1Issues + wgDocRev2Issues +
                                                    wgDocRev3Issues + wgDocRev4Issues + wgDocRev5Issues;
                                var wgDocDontIssues = wgDoc - wgDocIssues;

                                totalDoc += wgDoc;
                                totalDocIssues += wgDocIssues;
                                totalDocRev0Issues += wgDocRev0Issues;
                                totalDocRev1Issues += wgDocRev1Issues;
                                totalDocRev2Issues += wgDocRev2Issues;
                                totalDocRev3Issues += wgDocRev3Issues;
                                totalDocRev4Issues += wgDocRev4Issues;
                                totalDocRev5Issues += wgDocRev5Issues;
                                totalDocRevIssues += wgTotalDocRev;
                                totalDocDontIssues = totalDoc - totalDocIssues;

                                wsSummary.Cells["C" + (7 + i)].PutValue(wgDoc);
                                wsSummary.Cells["D" + (7 + i)].PutValue(wgDocDontIssues);
                                wsSummary.Cells["E" + (7 + i)].PutValue(wgDocRev0Issues);
                                wsSummary.Cells["F" + (7 + i)].PutValue(wgDocRev1Issues);
                                wsSummary.Cells["G" + (7 + i)].PutValue(wgDocRev2Issues);
                                wsSummary.Cells["H" + (7 + i)].PutValue(wgDocRev3Issues);
                                wsSummary.Cells["I" + (7 + i)].PutValue(wgDocRev4Issues);
                                wsSummary.Cells["J" + (7 + i)].PutValue(wgDocRev5Issues);
                                wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                                wsSummary.Cells["L" + (7 + i)].PutValue(wgTotalDocRev);


                                var count = 1;

                                var listDocumentTypeId =
                                    docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                                double? complete = 0;
                                double? weight = 0;

                                foreach (var documentTypeId in listDocumentTypeId)
                                {
                                    var documentType =
                                        this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                    var dataRow = dtFull.NewRow();
                                    dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                    dtFull.Rows.Add(dataRow);

                                    var listDocByDocType =
                                        docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                    foreach (var document in listDocByDocType)
                                    {
                                        dataRow = dtFull.NewRow();
                                        dataRow["DocId"] = document.ID;
                                        dataRow["NoIndex"] = count;
                                        dataRow["DocNo"] = document.DocNo;
                                        dataRow["DocTitle"] = document.DocTitle;
                                        dataRow["Department"] = document.DeparmentName;
                                        dataRow["Start"] = document.StartDate != null
                                            ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["Planned"] = document.PlanedDate != null
                                            ? document.PlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevName"] = document.RevisionName;
                                        dataRow["RevPlanned"] = document.RevisionPlanedDate != null
                                            ? document.RevisionPlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevActual"] = document.RevisionActualDate != null
                                            ? document.RevisionActualDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevCommentCode"] = document.RevisionCommentCode;
                                        dataRow["Complete"] = document.Complete / 100;
                                        dataRow["Weight"] = document.Weight / 100;
                                        dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                        dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                            ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["IncomingNo"] = document.IncomingTransNo;
                                        dataRow["IncomingDate"] = document.IncomingTransDate != null
                                            ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                        dataRow["ICADate"] = document.ICAReviewReceivedDate != null
                                            ? document.ICAReviewReceivedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                        dataRow["Notes"] = document.Notes;
                                        dataRow["IsEMDR"] = document.IsEMDR.GetValueOrDefault() ? "x" : string.Empty;
                                        dataRow["HasAttachFile"] = document.HasAttachFile.GetValueOrDefault()
                                            ? "x"
                                            : string.Empty;

                                        count += 1;
                                        dtFull.Rows.Add(dataRow);

                                        complete += (document.Complete / 100) * (document.Weight / 100);
                                        weight += document.Weight / 100;
                                    }
                                }

                                sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                                sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 23, true);

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 12].PutValue(complete);
                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 13].PutValue(weight);

                            }

                            wsSummary.Cells["H4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));

                            wsSummary.Cells["C" + (7 + listDisciplineInPermission.Count)].PutValue(totalDoc);
                            wsSummary.Cells["D" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocDontIssues);
                            wsSummary.Cells["E" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev0Issues);
                            wsSummary.Cells["F" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev1Issues);
                            wsSummary.Cells["G" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev2Issues);
                            wsSummary.Cells["H" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev3Issues);
                            wsSummary.Cells["I" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev4Issues);
                            wsSummary.Cells["J" + (7 + listDisciplineInPermission.Count)].PutValue(
                                totalDocRev5Issues);
                            wsSummary.Cells["K" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRevIssues);
                            wsSummary.Cells["L" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRevIssues);

                            sheets[1].IsVisible = false;

                            var filename = projectName + " - " + "EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.DownloadByWriteByte(filePath + filename, filename, true);
                        }
                    }
                }
            }
            else if (e.Argument == "UpdatePackageStatus")
            {
                var txtPackageComplete =
                    this.CustomerMenu.Items[2].FindControl("txtPackageComplete") as RadNumericTextBox;
                var txtPackageWeight =
                    this.CustomerMenu.Items[2].FindControl("txtPackageWeight") as RadNumericTextBox;

                var packageobj = this.packageService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                if (packageobj != null)
                {
                    if (txtPackageComplete != null)
                    {
                        packageobj.Complete = txtPackageComplete.Value.GetValueOrDefault();
                    }

                    if (txtPackageWeight != null)
                    {
                        packageobj.Weight = txtPackageWeight.Value.GetValueOrDefault();
                    }

                    this.packageService.Update(packageobj);
                }
            }
            else if (e.Argument.Contains("DeleteRev"))
            {
                string st = e.Argument.ToString();
                int docId = Convert.ToInt32(st.Replace("DeleteRev_", string.Empty));

                var docObj = this.documentPackageService.GetById(docId);
                var listRelateDoc =
                    this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                if (docObj != null && listRelateDoc.Count > 1)
                {

                    docObj.IsDelete = true;
                    docObj.UpdatedBy = UserSession.Current.User.Id;
                    docObj.UpdatedDate = DateTime.Now;
                    docObj.IsLeaf = false;
                    this.documentPackageService.Update(docObj);
                    docId = 0;
                    listRelateDoc =
                        this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            if (docId < objDoc.ID)
                            {
                                docId = objDoc.ID;
                                docObj = objDoc;
                            }
                        }
                    }
                    if (docId != 0)
                    {
                        docObj.IsLeaf = true;
                        this.documentPackageService.Update(docObj);
                        this.grdDocument.Rebind();
                    }
                }
                else
                {
                    Response.Write(
                        "<script>window.alert('Can not be reduced, because this document is only one version.')</script>");
                }
            }
            else if (e.Argument == "DownloadMulti")
            {
                var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.rar");
                var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    //var name = (Label)selectedItem["DocNo"].FindControl("lblDocNo");
                    //var serverDocPackPath = Server.MapPath("~/Exports/DocPack/" + name.Text);
                    var attachFiles = this.attachFileService.GetAllDocId(docId);
                    var attachFilesConstruction = attachFiles.Where(t => t.FilePath.Contains("02.Latest Revision")).ToList();
                    //var temp = Directory.CreateDirectory(serverDocPackPath);
                    if (attachFilesConstruction.Count > 0)
                    {
                        foreach (var attachFile in attachFilesConstruction)
                        {
                            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                            {
                                //File.Copy(Server.MapPath(attachFile.FilePath), serverDocPackPath);
                                docPack.Add(Server.MapPath(attachFile.FilePath));
                            }
                        }
                    }
                    else
                    {
                        if (attachFiles.Count > 0)
                        {
                            foreach (var attachFile in attachFiles)
                            {
                                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                                {
                                    //File.Copy(Server.MapPath(attachFile.FilePath), serverDocPackPath);
                                    docPack.Add(Server.MapPath(attachFile.FilePath));
                                }
                            }
                        }
                    }
                }
                this.DownloadByWriteByte(serverTotalDocPackPath, "DocumentPackage.rar", true);
                //MemoryStream memStream = new MemoryStream();
                //int count = 0;
                //using (ZipArchive archive = new ZipArchive(memStream, ZipArchiveMode.Create, true, null))
                //{
                //    foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                //    {
                //        var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                //        var attachFiles = this.attachFileService.GetAllDocId(docId);
                //        var attachFilesConstruction = attachFiles.Where(t => t.FilePath.Contains("02.Latest Revision")).ToList();
                //        if (attachFilesConstruction.Count > 0)
                //        {
                //            foreach (var attachFile in attachFilesConstruction)
                //            {
                //                if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //                {
                //                    count++;
                //                    var key = count + "_" + attachFile.FileName;
                //                    using (ZipArchiveEntry entry = archive.CreateEntry(key))
                //                    {
                //                        using (var inputFile = File.Open(Server.MapPath(attachFile.FilePath), FileMode.Open))
                //                        {
                //                            Stream entryStream = entry.Open();
                //                            inputFile.CopyTo(entryStream);
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //        else
                //        {
                //            if (attachFiles.Count > 0)
                //            {
                //                foreach (var attachFile in attachFiles)
                //                {
                //                    if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //                    {
                //                        count++;
                //                        var key = count + "_" + attachFile.FileName;
                //                        using (ZipArchiveEntry entry = archive.CreateEntry(key))
                //                        {
                //                            using (var inputFile = File.Open(Server.MapPath(attachFile.FilePath), FileMode.Open))
                //                            {
                //                                Stream entryStream = entry.Open();
                //                                inputFile.CopyTo(entryStream);
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                //SendZipToClient(memStream, serverTotalDocPackPath, "DocumentPackage.zip", true);
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "SendNotification")
            {
                var listDisciplineId = new List<int>();
                var listSelectedDoc = new List<Document>();
                var count = 0;
                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                {
                    var cboxSelected = (CheckBox)item["IsSelected"].FindControl("IsSelected");
                    if (cboxSelected.Checked)
                    {
                        count += 1;
                        var docItem = new Document();
                        var disciplineId = item["DisciplineID"].Text != @"&nbsp;"
                            ? item["DisciplineID"].Text
                            : string.Empty;
                        if (!string.IsNullOrEmpty(disciplineId) && disciplineId != "0")
                        {
                            listDisciplineId.Add(Convert.ToInt32(disciplineId));

                            docItem.ID = count;
                            docItem.DocumentNumber = item["DocumentNumber"].Text != @"&nbsp;"
                                ? item["DocumentNumber"].Text
                                : string.Empty;
                            docItem.Title = item["Title"].Text != @"&nbsp;"
                                ? item["Title"].Text
                                : string.Empty;
                            docItem.RevisionName = item["Revision"].Text != @"&nbsp;"
                                ? item["Revision"].Text
                                : string.Empty;
                            docItem.FilePath = item["FilePath"].Text != @"&nbsp;"
                                ? item["FilePath"].Text
                                : string.Empty;
                            docItem.DisciplineID = Convert.ToInt32(disciplineId);
                            listSelectedDoc.Add(docItem);
                        }
                    }
                }

                listDisciplineId = listDisciplineId.Distinct().ToList();

                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials =
                        Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials =
                        new NetworkCredential(UserSession.Current.User.Email,
                            Utility.Decrypt(UserSession.Current.User.HashCode))
                };

                foreach (var disciplineId in listDisciplineId)
                {
                    var notificationRule = this.notificationRuleService.GetAllByDiscipline(disciplineId);

                    if (notificationRule != null)
                    {
                        var message = new MailMessage();
                        message.From = new MailAddress(UserSession.Current.User.Email,
                            UserSession.Current.User.FullName);
                        message.Subject = "Test send notification from EDMs";
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = @"******<br/>
                                        Dear users,<br/><br/>

                                        Please be informed that the following documents are now available on the BDPOC Document Library System for your information.<br/><br/>

                                        <table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:350px'>Document number</th>
		                                        <th style='text-align:center; width:350px'>Document title</th>
		                                        <th style='text-align:center; width:60px'>Revision</th>
	                                        </tr>";

                        if (!string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listUserId =
                                notificationRule.ReceiverListId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            foreach (var userId in listUserId)
                            {
                                var user = this.userService.GetByID(userId);
                                if (user != null)
                                {
                                    message.To.Add(new MailAddress(user.Email));
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(notificationRule.ReceiveGroupId) &&
                                 string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listGroupId =
                                notificationRule.ReceiveGroupId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            var listUser = this.userService.GetSpecialListUser(listGroupId);
                            foreach (var user in listUser)
                            {
                                message.To.Add(new MailAddress(user.Email));
                            }
                        }

                        var subBody = string.Empty;
                        foreach (var document in listSelectedDoc)
                        {
                            var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                            if (document.DisciplineID == disciplineId)
                            {
                                subBody += @"<tr>
                                <td>" + document.ID + @"</td>
                                <td><a href='http://" + Server.MachineName +
                                           (!string.IsNullOrEmpty(port) ? ":" + port : string.Empty)
                                           + document.FilePath + "' download='" + document.DocumentNumber + "'>"
                                           + document.DocumentNumber + @"</a></td>
                                <td>"
                                           + document.Title + @"</td>
                                <td>"
                                           + document.RevisionName + @"</td>";
                            }
                        }


                        message.Body += subBody + @"</table>
                                        <br/><br/>
                                        Thanks and regards,<br/>
                                        ******";

                        smtpClient.Send(message);
                    }
                }
            }
        }

        private void ExportEMDROverdureReport(int dueration)
        {
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var temp = string.Empty;
            var docList = new List<DocumentPackage>();
            var docListAutoDistribution = new List<DocumentPackage>();
            var nearday = DateTime.Now.Date.AddDays(dueration);
            if (projectObj != null)
            {
                //var fullDocList = this.documentPackageService.GetAllRev(projectObj.ID);

                //fullDocList = fullDocList.Where(t => t.TypeId == 1).ToList();
                // var cbShowAll = (CheckBox)this.CustomerMenu.Items[6].FindControl("ckbShowAll");
                var fullDocList = this.documentPackageService.GetAllOfProject(projectObj.ID, 1, true).Where(t => string.IsNullOrEmpty(t.IncomingHardCopyTransNo)).ToList(); ;

                if (fullDocList.Count > 0)
                {
                    var doclistnearoverdue = fullDocList.Where(t => t.DeadlineComment != null && t.DeadlineComment.GetValueOrDefault().Date == nearday && t.OutgoingTransDate == null).ToList();
                    var overdure = fullDocList.Where(t => t.DeadlineComment != null && t.DeadlineComment.GetValueOrDefault().Date < DateTime.Now.Date && t.OutgoingTransDate == null).ToList();
                    docList = doclistnearoverdue.Union(overdure).Distinct().ToList();
                    var docListGroupByDisciplineList = docList.GroupBy(t => t.DisciplineName).ToList();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\EMDRReportTemplateOverDure.xlsm");

                    var workSheets = workbook.Worksheets;
                    var summarySheet = workSheets[0];
                    summarySheet.Cells["E1"].PutValue(summarySheet.Cells["E1"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name));


                    summarySheet.Cells["H4"].PutValue(DateTime.Now.ToString("dd-MM-yyyy"));
                    summarySheet.Cells["A2"].PutValue(docListGroupByDisciplineList.Count);


                    //   workSheets[1].Cells["E4"].PutValue("Document List Date: " + DateTime.Now.ToString("dd-MM-yyyy"));


                    for (int i = 0; i < docListGroupByDisciplineList.Count; i++)
                    {
                        var total = docListGroupByDisciplineList[i].Count();

                        // Fill data to Summary sheet
                        summarySheet.Cells[7 + i, 2].PutValue(i + 1);
                        summarySheet.Cells[7 + i, 3].PutValue(docListGroupByDisciplineList[i].Key);
                        summarySheet.Cells[7 + i, 4].PutValue(doclistnearoverdue.Count);



                        summarySheet.Cells[7 + i, 5].PutValue(overdure.Count());

                        summarySheet.Hyperlinks.Add("D" + (8 + i), 1, 1, "'" + docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-') + "'!C7");

                        // Fill data for discipline sheet
                        var count = 1;
                        var docCount = 1;

                        workSheets.AddCopy("DataSheet");
                        workSheets[i + 2].Name = docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-');
                        workSheets[i + 2].Cells["E1"].PutValue("Document List Date: " + DateTime.Now.ToString("dd-MM-yyyy"));

                        var docListGroupByDocTypeList = docListGroupByDisciplineList[i].GroupBy(t => t.DocumentTypeName);
                        foreach (var docListGroupByDocType in docListGroupByDocTypeList)
                        {
                            workSheets[i + 2].Cells[6 + count, 3].PutValue(docListGroupByDocType.Key);
                            workSheets[i + 2].Cells[6 + count, 1].PutValue(0);

                            count += 1;

                            foreach (var document in docListGroupByDocType)
                            {
                                workSheets[i + 2].Cells[6 + count, 0].PutValue(overdure.Count(t => t.ID == document.ID) > 0 ? "OV" : "");
                                workSheets[i + 2].Cells[6 + count, 1].PutValue(document.ID);
                                workSheets[i + 2].Cells[6 + count, 2].PutValue(docCount);
                                workSheets[i + 2].Cells[6 + count, 3].PutValue(document.DocNo);
                                workSheets[i + 2].Cells[6 + count, 4].PutValue(document.DocTitle);
                                workSheets[i + 2].Cells[6 + count, 5].PutValue(document.RevisionName);
                                workSheets[i + 2].Cells[6 + count, 6].PutValue(document.IncomingTransDate != null ? document.IncomingTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 7].PutValue(document.DeadlineComment != null ? document.DeadlineComment.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);

                                workSheets[i + 2].Cells[6 + count, 8].PutValue(document.Notes);
                                docCount += 1;
                                count += 1;
                            }
                        }

                        workSheets[i + 2].Cells["A2"].PutValue(count);
                    }

                    summarySheet.Cells["D" + (8 + docListGroupByDisciplineList.Count)].PutValue("TOTAL");
                    summarySheet.Cells["E" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(E8:E" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["F" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(F8:F" + (7 + docListGroupByDisciplineList.Count) + ")";
                    var style = new Aspose.Cells.Style();
                    var flag = new StyleFlag();
                    style = summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].Style;
                    style.Font.IsBold = true;
                    //style.BackgroundColor = Color.Orange;
                    flag.FontBold = true;

                    summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].ApplyStyle(style, flag);

                    workSheets.RemoveAt(1);

                    var filename = Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "_" + "EMDR OverDue Report.xlsm";
                    workbook.Save(filePath + filename);
                    //this.Download_File(filePath + filename);
                    this.DowloadAndDeleteFile(filePath + filename, filename);
                }
            }
        }
        private void ExportEMDROverdueResponseReport(int dueration)
        {
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var temp = string.Empty;
            var docList = new List<DocumentPackage>();
            var docListAutoDistribution = new List<DocumentPackage>();
            var nearday = DateTime.Now.Date.AddDays(dueration);
            if (projectObj != null)
            {
                //var fullDocList = this.documentPackageService.GetAllRev(projectObj.ID);

                //fullDocList = fullDocList.Where(t => t.TypeId == 1).ToList();
                // var cbShowAll = (CheckBox)this.CustomerMenu.Items[6].FindControl("ckbShowAll");
                var fullDocList = this.documentPackageService.GetAllOfProject(projectObj.ID, 1, true).Where(t => string.IsNullOrEmpty(t.IncomingHardCopyTransNo)).ToList(); ;

                if (fullDocList.Count > 0)
                {
                    var doclistnearoverdue = fullDocList.Where(t => t.DeadlineResponse != null && t.DeadlineResponse.GetValueOrDefault().Date == nearday && t.EnddateResponse != null && t.OutgoingTransDate == null).ToList();
                    var overdure = fullDocList.Where(t => t.DeadlineResponse != null && t.DeadlineResponse.GetValueOrDefault().Date < DateTime.Now.Date && t.EnddateResponse != null && t.OutgoingTransDate == null).ToList();
                    docList = doclistnearoverdue.Union(overdure).Distinct().ToList();
                    var docListGroupByDisciplineList = docList.GroupBy(t => t.DisciplineName).ToList();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\EMDRReportTemplateOverDure.xlsm");

                    var workSheets = workbook.Worksheets;
                    var summarySheet = workSheets[0];
                    summarySheet.Cells["E1"].PutValue(summarySheet.Cells["E1"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name));


                    summarySheet.Cells["H4"].PutValue(DateTime.Now.ToString("dd-MM-yyyy"));
                    summarySheet.Cells["A2"].PutValue(docListGroupByDisciplineList.Count);


                    //   workSheets[1].Cells["E4"].PutValue("Document List Date: " + DateTime.Now.ToString("dd-MM-yyyy"));


                    for (int i = 0; i < docListGroupByDisciplineList.Count; i++)
                    {
                        var total = docListGroupByDisciplineList[i].Count();

                        // Fill data to Summary sheet
                        summarySheet.Cells[7 + i, 2].PutValue(i + 1);
                        summarySheet.Cells[7 + i, 3].PutValue(docListGroupByDisciplineList[i].Key);
                        summarySheet.Cells[7 + i, 4].PutValue(doclistnearoverdue.Count);



                        summarySheet.Cells[7 + i, 5].PutValue(overdure.Count());

                        summarySheet.Hyperlinks.Add("D" + (8 + i), 1, 1, "'" + docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-') + "'!C7");

                        // Fill data for discipline sheet
                        var count = 1;
                        var docCount = 1;

                        workSheets.AddCopy("DataSheet");
                        workSheets[i + 2].Name = docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-');
                        workSheets[i + 2].Cells["E1"].PutValue("Document List Date: " + DateTime.Now.ToString("dd-MM-yyyy"));

                        var docListGroupByDocTypeList = docListGroupByDisciplineList[i].GroupBy(t => t.DocumentTypeName);
                        foreach (var docListGroupByDocType in docListGroupByDocTypeList)
                        {
                            workSheets[i + 2].Cells[6 + count, 3].PutValue(docListGroupByDocType.Key);
                            workSheets[i + 2].Cells[6 + count, 1].PutValue(0);

                            count += 1;

                            foreach (var document in docListGroupByDocType)
                            {
                                workSheets[i + 2].Cells[6 + count, 0].PutValue(overdure.Count(t => t.ID == document.ID) > 0 ? "OV" : "");
                                workSheets[i + 2].Cells[6 + count, 1].PutValue(document.ID);
                                workSheets[i + 2].Cells[6 + count, 2].PutValue(docCount);
                                workSheets[i + 2].Cells[6 + count, 3].PutValue(document.DocNo);
                                workSheets[i + 2].Cells[6 + count, 4].PutValue(document.DocTitle);
                                workSheets[i + 2].Cells[6 + count, 5].PutValue(document.RevisionName);
                                workSheets[i + 2].Cells[6 + count, 6].PutValue(document.IncomingTransDate != null ? document.IncomingTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 7].PutValue(document.DeadlineComment != null ? document.DeadlineComment.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);

                                workSheets[i + 2].Cells[6 + count, 8].PutValue(document.Notes);
                                docCount += 1;
                                count += 1;
                            }
                        }

                        workSheets[i + 2].Cells["A2"].PutValue(count);
                    }

                    summarySheet.Cells["D" + (8 + docListGroupByDisciplineList.Count)].PutValue("TOTAL");
                    summarySheet.Cells["E" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(E8:E" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["F" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(F8:F" + (7 + docListGroupByDisciplineList.Count) + ")";
                    var style = new Aspose.Cells.Style();
                    var flag = new StyleFlag();
                    style = summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].Style;
                    style.Font.IsBold = true;
                    //style.BackgroundColor = Color.Orange;
                    flag.FontBold = true;

                    summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].ApplyStyle(style, flag);

                    workSheets.RemoveAt(1);

                    var filename = Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "_" + "EMDR OverDue Response Report.xlsm";
                    workbook.Save(filePath + filename);
                    //this.Download_File(filePath + filename);
                    this.DowloadAndDeleteFile(filePath + filename, filename);
                }
            }
        }

        private void ExportMasterListTemplate()
        {
            var projectId = this.ddlProject.SelectedItem != null
                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            var docStatusList = this.statusService.GetAllByProject(projectId).ToList();
            var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId).OrderBy(t => t.FullName).ToList();

            var filePath = Server.MapPath("Exports") + @"\";
            if (disciplineList.Count > 0)
            {
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\MasterListTemplate_New.xls");
                var revisionList = this.revisionService.GetAll();
                var docTypeList = this.documentTypeService.GetAllByProject(this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0);
                var departmentList = this.roleService.GetAll(false);

                var sheets = workbook.Worksheets;
                var tempDataSheet = sheets[0];
                var initSheet = sheets[1];
                var rangeRevisionList = tempDataSheet.Cells.CreateRange("A1",
                    "A" + (revisionList.Count == 0 ? 1 : revisionList.Count));
                var rangeDocTypeList = tempDataSheet.Cells.CreateRange("B1",
                    "B" + (docTypeList.Count == 0 ? 1 : docTypeList.Count));
                var rangeDepartmentList = tempDataSheet.Cells.CreateRange("C1",
                    "C" + (departmentList.Count == 0 ? 1 : departmentList.Count));
                //var rangeRefFromList = tempDataSheet.Cells.CreateRange("B1", "B2");

                rangeRevisionList.Name = "RevisionList";
                rangeDocTypeList.Name = "DocTypeList";
                rangeDepartmentList.Name = "DepartmentList";
                //rangeRefFromList.Name = "RefFormList";

                for (int j = 0; j < revisionList.Count; j++)
                {
                    rangeRevisionList[j, 0].PutValue(revisionList[j].Name);
                }
                for (int j = 0; j < docTypeList.Count; j++)
                {
                    rangeDocTypeList[j, 0].PutValue(docTypeList[j].Name);
                }
                for (int j = 0; j < departmentList.Count; j++)
                {
                    rangeDepartmentList[j, 0].PutValue(departmentList[j].Name);
                }

                // Fill doc status into template for input plan date
                for (int i = 0; i < docStatusList.Count; i++)
                {
                    initSheet.Cells[3, 9 + i].PutValue(docStatusList[i].Name + Environment.NewLine + "(" + docStatusList[i].PercentCompleteDefault + "%)");

                }

                if (docStatusList.Count > 0)
                {
                    initSheet.Cells.Merge(2, 8, 1, docStatusList.Count, false, false);
                    initSheet.Cells["I3"].PutValue("Document Status Plan (dd/MM/yyyy)");
                }

                for (int i = 0; i < disciplineList.Count; i++)
                {
                    sheets.AddCopy("Sheet1");
                    sheets[i + 2].Name = disciplineList[i].FullName.Replace('/', '-').Replace('\\', '-');
                    sheets[i + 2].Cells["A1"].PutValue(this.ddlProject.SelectedValue);
                    sheets[i + 2].Cells["A2"].PutValue(disciplineList[i].ID);
                    sheets[i + 2].Cells["D1"].PutValue(
                        sheets[i + 2].Cells["D1"].Value.ToString()
                            .Replace("<ProjectName>", this.ddlProject.SelectedItem.Text));
                    sheets[i + 2].Cells["D3"].PutValue(disciplineList[i].FullName);

                    var validations = sheets[i + 2].Validations;
                    this.CreateValidation(rangeRevisionList.Name, validations, 2, 5000, 5, 5);
                    this.CreateValidation(rangeDepartmentList.Name, validations, 2, 5000, 6, 6);
                    this.CreateValidation(rangeDocTypeList.Name, validations, 2, 5000, 7, 7);
                }

                workbook.Worksheets.RemoveAt(1);
                workbook.Worksheets[0].IsVisible = false;

                var filename = this.ddlProject.SelectedItem.Text + "$" + "MasterListTemplate.xls";
                workbook.Save(filePath + filename);
                this.DownloadByWriteByte(filePath + filename, filename, true);
            }
        }


        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            this.LoadDocuments(false, isListAll);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var docObj = this.documentPackageService.GetById(docId);
            if (docObj != null)
            {
                if (docObj.ParentId == null)
                {
                    docObj.IsDelete = true;
                    docObj.UpdatedBy = UserSession.Current.User.Id;
                    docObj.UpdatedDate = DateTime.Now;
                    this.documentPackageService.Update(docObj);

                    // Delete Attach file
                    var attachDocList = this.attachFilesPackageService.GetAllDocId(docObj.ID);
                    foreach (var attachDoc in attachDocList)
                    {
                        var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                        if (File.Exists(attachFilePath))
                        {
                            File.Delete(attachFilePath);
                        }
                    }
                }
                else
                {
                    var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            objDoc.IsDelete = true;
                            objDoc.UpdatedBy = UserSession.Current.User.Id;
                            objDoc.UpdatedDate = DateTime.Now;
                            this.documentPackageService.Update(objDoc);
                        }
                    }

                    // Delete Attach file
                    var docRelatedList = this.documentPackageService.GetAllRevDoc(docObj.ParentId.Value).Where(t => t.ID != docObj.ID).Select(t => t.ID).ToList();
                    var attachDocList = this.attachFilesPackageService.GetAllDocId(docRelatedList);
                    foreach (var attachDoc in attachDocList)
                    {
                        var attachFilePath = Path.Combine(Server.MapPath("../.." + attachDoc.FilePath.Replace("01.All Revision", "02.Latest Revision")));
                        if (File.Exists(attachFilePath))
                        {
                            File.Delete(attachFilePath);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {

            }
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                this.CustomerMenu.Items[3].Visible = false;
                this.rtvDiscipline.UnselectAllNodes();
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {

            }
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if (e.Item is GridFilteringItem)
            //{
            ////Populate Filters by binding the combo to datasource
            //var filteringItem = (GridFilteringItem)e.Item;
            //var myRadComboBox = (RadComboBox)filteringItem.FindControl("RadComboBoxCustomerProgramDescription");

            //myRadComboBox.DataSource = myDataSet;
            //myRadComboBox.DataTextField = "CustomerProgramDescription";
            //myRadComboBox.DataValueField = "CustomerProgramDescription";
            //myRadComboBox.ClearSelection();
            //myRadComboBox.DataBind();
            //}
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item["No."].BackColor = Color.Aqua;// ColorTranslator.FromHtml("#00608f");// Color.Aqua;
                    item["No."].BorderColor = Color.Aqua;// ColorTranslator.FromHtml("#00608f"); //
                }
            }
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var lbldocNo = item.FindControl("lbldocNo") as Label;
                var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
                var ddlDepartment = item.FindControl("ddlDepartment") as RadComboBox;
                var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
                var txtPlanedDate = item.FindControl("txtPlanedDate") as RadDatePicker;

                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var txtRevisionPlanedDate = item.FindControl("txtRevisionPlanedDate") as RadDatePicker;
                var txtRevisionActualDate = item.FindControl("txtRevisionActualDate") as RadDatePicker;
                var txtRevisionCommentCode = item.FindControl("txtRevisionCommentCode") as TextBox;

                var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
                var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

                var txtOutgoingTransNo = item.FindControl("txtOutgoingTransNo") as TextBox;
                var txtOutgoingTransDate = item.FindControl("txtOutgoingTransDate") as RadDatePicker;

                var txtIncomingTransNo = item.FindControl("txtIncomingTransNo") as TextBox;
                var txtIncomingTransDate = item.FindControl("txtIncomingTransDate") as RadDatePicker;

                var txtICAReviewOutTransNo = item.FindControl("txtICAReviewOutTransNo") as TextBox;
                var txtICAReviewReceivedDate = item.FindControl("txtICAReviewReceivedDate") as RadDatePicker;
                var txtICAReviewCode = item.FindControl("txtICAReviewCode") as TextBox;

                var cbIsEMDR = item.FindControl("cbIsEMDR") as CheckBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentPackageService.GetById(docId);

                var currentRevision = objDoc.RevisionId;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                var department = this.roleService.GetByID(Convert.ToInt32(ddlDepartment.SelectedValue));

                var projectid = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectname = this.ddlProject.SelectedItem.Text;

                if (newRevision > currentRevision)
                {
                    var docObjNew = new DocumentPackage();
                    docObjNew.ProjectId = projectid;
                    docObjNew.ProjectName = projectname;
                    docObjNew.DisciplineId = objDoc.DisciplineId;
                    docObjNew.DisciplineName = objDoc.DisciplineName;
                    docObjNew.DocNo = lbldocNo.Text;
                    docObjNew.DocTitle = txtDocTitle.Text.Trim();
                    docObjNew.DeparmentId = objDoc.DeparmentId;
                    docObjNew.DeparmentName = objDoc.DeparmentName;
                    docObjNew.StartDate = txtStartDate.SelectedDate;

                    docObjNew.PlanedDate = txtPlanedDate.SelectedDate;
                    docObjNew.RevisionId = newRevision;
                    docObjNew.RevisionName = ddlRevision.SelectedItem.Text;
                    docObjNew.RevisionActualDate = txtRevisionActualDate.SelectedDate;
                    docObjNew.RevisionCommentCode = txtRevisionCommentCode.Text.Trim();
                    docObjNew.RevisionPlanedDate = txtRevisionPlanedDate.SelectedDate;
                    docObjNew.Complete = txtComplete.Value.GetValueOrDefault();
                    docObjNew.Weight = txtWeight.Value.GetValueOrDefault();
                    docObjNew.OutgoingTransNo = txtOutgoingTransNo.Text.Trim();
                    docObjNew.OutgoingTransDate = txtOutgoingTransDate.SelectedDate;
                    docObjNew.IncomingTransNo = txtIncomingTransNo.Text.Trim();
                    docObjNew.IncomingTransDate = txtIncomingTransDate.SelectedDate;
                    docObjNew.ICAReviewOutTransNo = txtICAReviewOutTransNo.Text.Trim();
                    docObjNew.ICAReviewCode = txtICAReviewCode.Text.Trim();
                    docObjNew.ICAReviewReceivedDate = txtICAReviewReceivedDate.SelectedDate;

                    docObjNew.DocumentTypeId = objDoc.DocumentTypeId;
                    docObjNew.DocumentTypeName = objDoc.DocumentTypeName;
                    docObjNew.DisciplineId = objDoc.DisciplineId;
                    docObjNew.DisciplineName = objDoc.DisciplineName;
                    docObjNew.PackageId = objDoc.PackageId;
                    docObjNew.PackageName = objDoc.PackageName;
                    docObjNew.Notes = string.Empty;
                    docObjNew.PlatformId = objDoc.PlatformId;
                    docObjNew.PlatformName = objDoc.PlatformName;

                    docObjNew.IsLeaf = true;
                    docObjNew.IsEMDR = cbIsEMDR.Checked;
                    docObjNew.ParentId = objDoc.ParentId ?? objDoc.ID;
                    docObjNew.CreatedBy = UserSession.Current.User.Id;
                    docObjNew.CreatedDate = DateTime.Now;

                    this.documentPackageService.Insert(docObjNew);

                    objDoc.IsLeaf = false;
                }
                else
                {
                    ////objDoc.DocNo = lbldocNo.Text;
                    objDoc.DocTitle = txtDocTitle.Text.Trim();
                    ////objDoc.DeparmentName = department != null ? department.FullName : string.Empty;
                    ////objDoc.DeparmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
                    objDoc.StartDate = txtStartDate.SelectedDate;
                    objDoc.PlanedDate = txtPlanedDate.SelectedDate;
                    objDoc.RevisionId = newRevision;
                    objDoc.RevisionName = ddlRevision.SelectedItem.Text;
                    objDoc.RevisionActualDate = txtRevisionActualDate.SelectedDate;
                    objDoc.RevisionCommentCode = txtRevisionCommentCode.Text.Trim();
                    objDoc.RevisionPlanedDate = txtRevisionPlanedDate.SelectedDate;
                    objDoc.Complete = txtComplete.Value.GetValueOrDefault();
                    objDoc.Weight = txtWeight.Value.GetValueOrDefault();
                    ////objDoc.OutgoingTransNo = txtOutgoingTransNo.Text.Trim();
                    ////objDoc.OutgoingTransDate = txtOutgoingTransDate.SelectedDate;
                    ////objDoc.IncomingTransNo = txtIncomingTransNo.Text.Trim();
                    ////objDoc.IncomingTransDate = txtIncomingTransDate.SelectedDate;
                    ////objDoc.ICAReviewOutTransNo = txtICAReviewOutTransNo.Text.Trim();
                    ////objDoc.ICAReviewCode = txtICAReviewCode.Text.Trim();
                    ////objDoc.ICAReviewReceivedDate = txtICAReviewReceivedDate.SelectedDate;

                    objDoc.IsEMDR = cbIsEMDR.Checked;
                }

                objDoc.UpdatedBy = UserSession.Current.User.Id;
                objDoc.UpdatedDate = DateTime.Now;

                this.documentPackageService.Update(objDoc);
            }
        }

        protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The get all child folder id.
        /// </summary>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<int> GetAllChildFolderId(int parentId, List<int> folderPermission)
        {
            if (!this.listFolderId.Contains(parentId))
            {
                this.listFolderId.Add(parentId);
            }


            var listFolder = this.folderService.GetAllByParentId(parentId, folderPermission);
            foreach (var folder in listFolder)
            {
                this.listFolderId.Add(folder.ID);
                this.GetAllChildFolderId(folder.ID, folderPermission);
            }

            return this.listFolderId;
        }

        /// <summary>
        /// The custom folder tree.
        /// </summary>
        /// <param name="radTreeView">
        /// The rad tree view.
        /// </param>
        private void CustomFolderTree(RadTreeNode radTreeView)
        {
            foreach (var node in radTreeView.Nodes)
            {
                var nodetemp = (RadTreeNode)node;
                if (nodetemp.Nodes.Count > 0)
                {
                    this.CustomFolderTree(nodetemp);
                }

                nodetemp.ImageUrl = "Images/folderdir16.png";
            }
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";

            // Set the error message.
            validation.ErrorMessage = "Please select item from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }

        private void SendZipToClient(MemoryStream memStream, string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            memStream.Seek(0, SeekOrigin.Begin);
            if (memStream != null && memStream.Length > 0)
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + strDownloadName);
                Response.ContentType = "application/zip";
                Response.BinaryWrite(memStream.ToArray());
                if (DeleteOriginalFile)
                {
                    if (File.Exists(strFileName))
                    {
                        File.SetAttributes(strFileName, FileAttributes.Normal);
                        File.Delete(strFileName);
                    }
                }
                Response.End();
            }
        }

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private bool DowloadAndDeleteFile(string strfilename, string strDownloadName)
        {
            try
            {
                ////down load va xoa nguon
                Response.ClearContent();
                Response.Clear();
                Response.ContentType = "application/octet-stream";// "application/zip";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName + ";");
                Response.TransmitFile(strfilename);

                HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
                cookie.Value = "Flag";
                cookie.Expires = DateTime.Now.AddDays(1);
                Response.AppendCookie(cookie);

                Response.Flush();
                System.IO.File.Delete(strfilename);
                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        private void LoadObjectTree()
        {
            var projectAutoDistribution = this.autoDistributionMatriceService.GetAllByUser(UserSession.Current.UserId).Select(t => t.ProjectId.GetValueOrDefault()).ToList();

            var projectList = new List<ScopeProject>();

            if (UserSession.Current.IsAdmin)
            {
                projectList = this.scopeProjectService.GetAll();
            }
            else if (UserSession.Current.IsManager)
            {
                //var projectIdsInPermission = this.docRoleMatrixService.GetProjectIdsInPermissionByRole(UserSession.Current.RoleId);
                var projectIdsInPermission = this.projectUserService.GetAllByUser(UserSession.Current.User.Id).Select(t => t.ProjectId.GetValueOrDefault());
                projectList = this.scopeProjectService.GetAll().Where(t => projectIdsInPermission.Contains(t.ID) || projectAutoDistribution.Contains(t.ID)).ToList();
            }
            else if (UserSession.Current.IsEngineer || UserSession.Current.IsLead)
            {
                //var projectIdsInPermission = this.dmDetailByUserService.GetProjectIdListInPermission(UserSession.Current.User.Id);
                var projectIdsInPermission = this.projectUserService.GetAllByUser(UserSession.Current.User.Id).Select(t => t.ProjectId.GetValueOrDefault());

                projectList = this.scopeProjectService.GetAll().Where(t => projectIdsInPermission.Contains(t.ID) || projectAutoDistribution.Contains(t.ID)).ToList();
            }
            else
            {
                projectList = this.scopeProjectService.GetAllByUser(UserSession.Current.User.Id, projectAutoDistribution);
            }

            if (projectList.Any())
            {
                this.ddlProject.DataSource = projectList;
                this.ddlProject.DataTextField = "FullName";
                this.ddlProject.DataValueField = "ID";
                this.ddlProject.DataBind();
                this.ddlProject.SelectedIndex = 0;
                if (this.ddlProject.SelectedItem != null)
                {
                    var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                    var projectObj = this.scopeProjectService.GetById(projectId);

                    this.lblProjectId.Value = projectId.ToString();
                    this.ProjectFolderPath.Value = projectObj.ProjectPath;
                    var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId);

                    this.rtvDiscipline.DataSource = disciplineList.OrderBy(t => t.FullName);
                    this.rtvDiscipline.DataTextField = "FullName";
                    this.rtvDiscipline.DataValueField = "ID";
                    this.rtvDiscipline.DataFieldID = "ID";
                    this.rtvDiscipline.DataBind();
                }
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (this.ddlProject.SelectedItem != null)
            {
                //if (this.ddlProject.SelectedValue != "51")
                //{
                //    this.CustomerMenu.Items[6].Visible = false;
                //}
                //else
                //{
                //    this.CustomerMenu.Items[6].Visible = true;
                //}
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var projectObj = this.scopeProjectService.GetById(projectId);
                this.lblProjectId.Value = projectId.ToString();
                this.ProjectFolderPath.Value = projectObj.ProjectPath;
                var disciplineList = this.disciplineService.GetAllDisciplineOfProject(this.ddlProject.SelectedItem != null
                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                : 0);
                this.CustomerMenu.Items[3].Visible = false;

                this.rtvDiscipline.DataSource = disciplineList;
                this.rtvDiscipline.DataTextField = "FullName";
                this.rtvDiscipline.DataValueField = "ID";
                this.rtvDiscipline.DataFieldID = "ID";
                this.rtvDiscipline.DataBind();

                var dcCUserPermission = this.projectUserService.GetAllSharedDCRole(projectObj.ID).Select(t => t.UserId.GetValueOrDefault()).ToList();
                var managerUserPermission = this.projectUserService.GetAllSharedManagerRole(projectObj.ID).Select(t => t.UserId.GetValueOrDefault()).ToList();

                if (UserSession.Current.User.Id == projectObj.DCId
                        || UserSession.Current.User.Id == projectObj.PMId)
                {
                    dcCUserPermission.Add(UserSession.Current.User.Id);
                }

                if (UserSession.Current.IsManager)
                {
                    managerUserPermission.Add(UserSession.Current.User.Id);
                }

                // ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons[8].Visible = projectObj.ProjectTypeId == 2;


                Session.Add("DCUserPermission", dcCUserPermission);
                Session.Add("ManagerUserPermission", managerUserPermission);
                refeshMenu(projectObj);

                // ------------------------------------------------------------------------------------------
            }

            this.grdDocument.Rebind();
        }

        private void refeshMenu(ScopeProject projectObj)
        {
            // Get all User have permission with selected project
            var dcUserPermission = (List<int>)Session["DCUserPermission"];
            var managerUserPermission = (List<int>)Session["ManagerUserPermission"];
            // ------------------------------------------------------------------------------------------
            if (!UserSession.Current.User.IsAdmin.GetValueOrDefault()
                   && !dcUserPermission.Contains(UserSession.Current.User.Id)
                   && !managerUserPermission.Contains(UserSession.Current.User.Id))
            {
                this.CustomerMenu.Items[0].Visible = false;
                this.CustomerMenu.Items[1].Visible = false;
                foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                {
                    if (item.Value == "Adminfunc")
                    {
                        item.Visible = false;
                    }
                }

                //this.CustomerMenu.Items[4].Visible = false;
                // this.CustomerMenu.Items[5].Visible = false;
                //this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
            }
            else
            {
                this.CustomerMenu.Items[0].Visible = true;
                this.CustomerMenu.Items[1].Visible = true;
                foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
                {
                    if (item.Value == "Adminfunc")
                    {
                        item.Visible = true;
                    }
                }
                // this.CustomerMenu.Items[4].Visible = true;
                // this.CustomerMenu.Items[5].Visible = true;
                //this.CustomerMenu.Items[3].Visible = false;

                this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = true;
                //this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = true;
            }

        }
        protected void grdDocument_Init(object sender, EventArgs e)
        {
        }

        protected void grdDocument_DataBound(object sender, EventArgs e)
        {
        }

        protected void rtvDiscipline_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
                var filterItem = (GridFilteringItem)e.Item;
                var selectedProperty = new List<string>();

                var ddlFilterRev = (RadComboBox)filterItem.FindControl("ddlFilterRev");
            }
        }

        protected DateTime? SetPublishDate(GridItem item)
        {
            if (item.OwnerTableView.GetColumn("Index27").CurrentFilterValue == string.Empty)
            {
                return new DateTime?();
            }
            else
            {
                return DateTime.Parse(item.OwnerTableView.GetColumn("Index27").CurrentFilterValue);
            }
        }

        protected void rtvDiscipline_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/discipline.png";
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void InitGridContextMenu(int projectId)
        {
            var contractorList = this.contractorService.GetAllByProject(projectId).OrderBy(t => t.TypeID).ThenBy(t => t.Name);
            foreach (var contractor in contractorList)
            {
                if (contractor.TypeID == 1)
                {
                    this.grdDocument.MasterTableView.ColumnGroups[0].HeaderText = contractor.Name + " - VSP";
                }

                this.radMenu.Items.Add(new RadMenuItem()
                {
                    Text = (contractor.TypeID == 1 ? "Response from " : "Comment from ") + contractor.Name,
                    ImageUrl = "~/Images/comment1.png",
                    Value = contractor.TypeID == 1 ? "Response_" + contractor.ID : "Comment_" + contractor.ID,
                    //NavigateUrl = "~/Controls/Document/CommentResponseForm.aspx?contId=" + contractor.ID
                });
            }
        }

        private void ExportEMDRReportNew()
        {
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var temp = string.Empty;
            var docList = new List<DocumentPackage>();
            var docListAutoDistribution = new List<DocumentPackage>();
            var dcUserPermission = (List<int>)Session["DCUserPermission"];
            // var managerUserPermission = (List<int>)Session["ManagerUserPermission"];
            if (projectObj != null)
            {
                var cbShowAll = (CheckBox)this.CustomerMenu.Items[6].FindControl("ckbShowAll");
                var fullDocList = this.documentPackageService.GetAllOfProject(projectObj.ID, 1, cbShowAll.Checked); //this.documentPackageService.GetAllRev(projectObj.ID);
                docList = fullDocList.OrderBy(t => t.DocNo).ToList();
                if (docList.Count > 0)
                {
                    var docListGroupByDisciplineList = docList.GroupBy(t => t.DisciplineName).ToList();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\EMDRReportTemplateNew.xlsm");

                    var workSheets = workbook.Worksheets;
                    var summarySheet = workSheets[0];
                    summarySheet.Cells["E1"].PutValue(summarySheet.Cells["E1"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name));
                    summarySheet.Cells["E3"].PutValue(summarySheet.Cells["E3"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name) + "\n" + temp);

                    summarySheet.Cells["N4"].PutValue(DateTime.Now.ToString("dd-MM-yyyy"));
                    summarySheet.Cells["A2"].PutValue(docListGroupByDisciplineList.Count);


                    workSheets[1].Cells["E1"].PutValue(workSheets[1].Cells["E1"].Value.ToString()
                                .Replace("<ProjectName>", projectObj.Name));
                    workSheets[1].Cells["E3"].PutValue(workSheets[1].Cells["E3"].Value + "\n" + temp);
                    workSheets[1].Cells["P4"].PutValue("Cut - off: " + DateTime.Now.ToString("dd-MM-yyyy"));


                    for (int i = 0; i < docListGroupByDisciplineList.Count; i++)
                    {
                        var total = docListGroupByDisciplineList[i].Count();

                        // Fill data to Summary sheet
                        summarySheet.Cells[7 + i, 2].PutValue(i + 1);
                        summarySheet.Cells[7 + i, 3].PutValue(docListGroupByDisciplineList[i].Key);
                        summarySheet.Cells[7 + i, 4].PutValue(total);
                        //  summarySheet.Cells[7 + i, 5].PutValue(docListGroupByDisciplineList[i].Count(t => !string.IsNullOrEmpty(t.IncomingHardCopyTransNo)));
                        //  summarySheet.Cells[7 + i, 6].PutValue(Math.Round((double)docListGroupByDisciplineList[i].Count(t => !string.IsNullOrEmpty(t.IncomingHardCopyTransNo)) / (double)total, 2));

                        summarySheet.Cells[7 + i, 7].PutValue(docListGroupByDisciplineList[i].Count(t => !string.IsNullOrEmpty(t.IncomingTransNo)));

                        summarySheet.Cells[7 + i, 8].PutValue(Math.Round((double)docListGroupByDisciplineList[i].Count(t => !string.IsNullOrEmpty(t.IncomingTransNo)) / (double)total, 2));

                        summarySheet.Cells[7 + i, 9].PutValue(docListGroupByDisciplineList[i].Count(t => (t.CommentCode != null) && (t.CommentCode.Contains("1"))));
                        summarySheet.Cells[7 + i, 10].PutValue(docListGroupByDisciplineList[i].Count(t => (t.CommentCode != null) && t.CommentCode.Contains("2")));

                        summarySheet.Cells[7 + i, 11].PutValue(docListGroupByDisciplineList[i].Count(t => (t.CommentCode != null) && t.CommentCode.Contains("3")));

                        var overdure = docListGroupByDisciplineList[i].Where(t => t.DeadlineComment != null && t.DeadlineComment.GetValueOrDefault().Date < DateTime.Now.Date && t.OutgoingTransDate == null).ToList();

                        summarySheet.Cells[7 + i, 12].PutValue(overdure.Count());

                        summarySheet.Hyperlinks.Add("D" + (8 + i), 1, 1, "'" + docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-') + "'!C7");

                        // Fill data for discipline sheet
                        var count = 1;
                        var docCount = 1;
                        var countHardcopy = 0;
                        var docListGroupByDis = docListGroupByDisciplineList[i].ToList();
                        workSheets.AddCopy("DataSheet");
                        workSheets[i + 2].Name = docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-');
                        workSheets[i + 2].Cells["E4"].PutValue("Discipline: " + docListGroupByDisciplineList[i].Key);

                        var docListGroupByDocTypeList = docListGroupByDis.GroupBy(t => t.DocumentTypeName);
                        foreach (var docListGroupByDocType in docListGroupByDocTypeList)
                        {
                            workSheets[i + 2].Cells[6 + count, 3].PutValue(docListGroupByDocType.Key);
                            workSheets[i + 2].Cells[6 + count, 1].PutValue(0);

                            count += 1;

                            foreach (var document in docListGroupByDocType)
                            {
                                workSheets[i + 2].Cells[6 + count, 0].PutValue(overdure.Count(t => t.ID == document.ID) > 0 ? "OV" : "");
                                workSheets[i + 2].Cells[6 + count, 1].PutValue(document.ID);
                                workSheets[i + 2].Cells[6 + count, 2].PutValue(docCount);
                                workSheets[i + 2].Cells[6 + count, 3].PutValue(document.DocNo);
                                workSheets[i + 2].Cells[6 + count, 4].PutValue(document.DocTitle);
                                workSheets[i + 2].Cells[6 + count, 5].PutValue(document.RevisionName);
                                workSheets[i + 2].Cells[6 + count, 6].PutValue(document.StatusName);
                                workSheets[i + 2].Cells[6 + count, 7].PutValue(document.PlanedDate != null ? document.PlanedDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 8].PutValue(document.DeadlineComment != null ? document.DeadlineComment.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 9].PutValue(document.CommentCode);
                                workSheets[i + 2].Cells[6 + count, 10].PutValue(document.DeadlineResponse != null ? document.DeadlineResponse.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 11].PutValue(document.IncomingTransNo);
                                workSheets[i + 2].Cells[6 + count, 12].PutValue(document.IncomingTransDate != null ? document.IncomingTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 13].PutValue(document.OutgoingTransNo);
                                workSheets[i + 2].Cells[6 + count, 14].PutValue(document.OutgoingTransDate != null ? document.OutgoingTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 15].PutValue(document.IncomingHardCopyTransNo);
                                workSheets[i + 2].Cells[6 + count, 16].PutValue(document.IncomingHardCopyTransDate != null ? document.IncomingHardCopyTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                                workSheets[i + 2].Cells[6 + count, 17].PutValue(document.IsLeaf.GetValueOrDefault() ? "TRUE" : "FALSE");

                                countHardcopy += fullDocList.Where(t => ((t.ID == document.ID || t.ParentId == document.ID)) && !string.IsNullOrEmpty(t.IncomingHardCopyTransNo)).Any() ? 1 : 0;

                                docCount += 1;
                                count += 1;
                            }
                        }
                        summarySheet.Cells[7 + i, 5].PutValue(countHardcopy);
                        summarySheet.Cells[7 + i, 6].PutValue(Math.Round((double)countHardcopy / (double)total, 2));
                        workSheets[i + 2].Cells["A2"].PutValue(count);
                    }

                    summarySheet.Cells["D" + (8 + docListGroupByDisciplineList.Count)].PutValue("TOTAL");
                    summarySheet.Cells["E" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(E8:E" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["F" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(F8:F" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["J" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(J8:J" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["H" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(H8:H" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["K" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(K8:K" + (7 + docListGroupByDisciplineList.Count) + ")";

                    summarySheet.Cells["L" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(L8:L" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["M" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(M8:M" + (7 + docListGroupByDisciplineList.Count) + ")";
                    summarySheet.Cells["N" + (8 + docListGroupByDisciplineList.Count)].Formula = "=SUM(N8:N" + (7 + docListGroupByDisciplineList.Count) + ")";

                    var style = new Aspose.Cells.Style();
                    var flag = new StyleFlag();
                    style = summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].Style;
                    style.Font.IsBold = true;
                    //style.BackgroundColor = Color.Orange;
                    flag.FontBold = true;

                    summarySheet.Cells.Rows[7 + docListGroupByDisciplineList.Count].ApplyStyle(style, flag);

                    workSheets.RemoveAt(1);
                    var filename = Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "_" + "EMDR Report" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xlsm";
                    workbook.Save(filePath + filename);
                    //  this.Download_File(filePath + filename);
                    this.DowloadAndDeleteFile(filePath + filename, filename);
                }
            }
        }
        private void ExportEMDRReportSelectedDocuments()
        {
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var temp = string.Empty;
            var docList = new List<DocumentPackage>();
            var count = 1;
            var docCount = 1;
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\EMDRReporSelectedDocuments.xlsm");

            var workSheets = workbook.Worksheets;
            var summarySheet = workSheets[0];

            var listSelectedDocId = new List<int>();

            summarySheet.Cells["E1"].PutValue(summarySheet.Cells["E1"].Value.ToString()
                             .Replace("<ProjectName>", projectObj.Name));

            summarySheet.Cells["M4"].PutValue(summarySheet.Cells["M4"].Value.ToString()
                        .Replace("<Date>", DateTime.Now.ToString("dd-MM-yyyy")));

            foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
            {
                var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                var docObj = this.documentPackageService.GetById(docId);
                if (docObj.ParentId != null && docObj.ParentId != 0)
                {
                    listSelectedDocId.Add(docObj.ParentId.GetValueOrDefault());
                }
                listSelectedDocId.Add(docId);

                //selectedItem.Selected = false;
            }
            docList = this.documentPackageService.GetSpecialList(listSelectedDocId);
            var docListGroupByDocTypeList = docList.GroupBy(t => t.DocumentTypeName);
            foreach (var docListGroupByDocType in docListGroupByDocTypeList)
            {
                summarySheet.Cells[6 + count, 3].PutValue(docListGroupByDocType.Key);
                summarySheet.Cells[6 + count, 1].PutValue(0);

                count += 1;

                foreach (var document in docListGroupByDocType)
                {
                    summarySheet.Cells[6 + count, 1].PutValue(document.ID);
                    summarySheet.Cells[6 + count, 2].PutValue(docCount);
                    summarySheet.Cells[6 + count, 3].PutValue(document.DocNo);
                    summarySheet.Cells[6 + count, 4].PutValue(document.DocTitle);
                    summarySheet.Cells[6 + count, 5].PutValue(document.RevisionName);
                    summarySheet.Cells[6 + count, 6].PutValue(document.PlanedDate);
                    summarySheet.Cells[6 + count, 7].PutValue(document.Complete.GetValueOrDefault() / 100);
                    summarySheet.Cells[6 + count, 8].PutValue(document.DeparmentName);
                    summarySheet.Cells[6 + count, 9].PutValue(document.CommentCode);
                    summarySheet.Cells[6 + count, 10].PutValue(document.IncomingTransNo);
                    summarySheet.Cells[6 + count, 11].PutValue(document.IncomingTransDate);
                    summarySheet.Cells[6 + count, 12].PutValue(document.IncomingHardCopyTransNo);
                    summarySheet.Cells[6 + count, 13].PutValue(document.IncomingHardCopyTransDate);
                    summarySheet.Cells[6 + count, 14].PutValue(document.IsLeaf.GetValueOrDefault() ? "TRUE" : "FALSE");

                    docCount += 1;
                    count += 1;
                }
            }

            //summarySheet.AutoFilter.Filter(14, "TRUE");

            summarySheet.Cells["A2"].PutValue(count);

            var filename = Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "_" + "EMDR Selected Report.xlsm";
            workbook.Save(filePath + filename);
            this.Download_File(filePath + filename);

            this.grdDocument.Rebind();
        }

        protected void ExportEMDRNextWeekIssue()
        {
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var temp = string.Empty;
            var docList = new List<DocumentPackage>();

            var docCount = 1;
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\EMDRReporNextIssue.xlsm");

            var workSheets = workbook.Worksheets;
            var summarySheet = workSheets[0];

            var listSelectedDocId = new List<int>();
            var monday = this.Layngaythu2(DateTime.Today.Date);
            DateTime fromdate = monday.AddDays(7);
            DateTime todate = fromdate.AddDays(6);

            summarySheet.Cells["E1"].PutValue(summarySheet.Cells["E1"].Value.ToString()
                             .Replace("<ProjectName>", projectObj.Name));

            summarySheet.Cells["G4"].PutValue(DateTime.Now.ToString("dd-MM-yyyy"));
            summarySheet.Cells["E4"].PutValue("(From:" + fromdate.Date.ToString("dd-MM-yyyy") + " To :" + todate.Date.ToString("dd-MM-yyyy") + ")");

            workSheets[1].Cells["E1"].PutValue(summarySheet.Cells["E1"].Value.ToString()
                            .Replace("<ProjectName>", projectObj.Name));

            workSheets[1].Cells["H4"].PutValue("Cut_Off:" + DateTime.Now.ToString("dd-MM-yyyy"));

            docList = this.documentPackageService.GetAllLatestRev(projectObj.ID).Where(t => t.PlanedDate != null && t.PlanedDate.Value.Date >= fromdate.Date && t.PlanedDate.Value.Date <= todate.Date).ToList();

            if (docList.Count > 0)
            {
                var docListGroupByDisciplineList = docList.GroupBy(t => t.DisciplineName).ToList();
                summarySheet.Cells["A2"].PutValue(docListGroupByDisciplineList.Count);
                for (int i = 0; i < docListGroupByDisciplineList.Count; i++)
                {
                    var total = docListGroupByDisciplineList[i].Count();
                    summarySheet.Cells[6 + i, 2].PutValue(i + 1);
                    summarySheet.Cells[6 + i, 3].PutValue(docListGroupByDisciplineList[i].Key);
                    summarySheet.Cells[6 + i, 4].PutValue(total);
                    var docListGroupByDis = docListGroupByDisciplineList[i].ToList();
                    var count = 1;
                    workSheets.AddCopy("DataSheet");
                    workSheets[i + 2].Name = docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-');
                    workSheets[i + 2].Cells["E4"].PutValue("Discipline: " + docListGroupByDisciplineList[i].Key);

                    var docListGroupByDocTypeList = docListGroupByDis.GroupBy(t => t.DocumentTypeName);
                    foreach (var docListGroupByDocType in docListGroupByDocTypeList)
                    {
                        workSheets[i + 2].Cells[6 + count, 3].PutValue(docListGroupByDocType.Key);
                        workSheets[i + 2].Cells[6 + count, 1].PutValue(0);

                        count += 1;

                        foreach (var document in docListGroupByDocType)
                        {
                            workSheets[i + 2].Cells[6 + count, 1].PutValue(document.ID);
                            workSheets[i + 2].Cells[6 + count, 2].PutValue(docCount);
                            workSheets[i + 2].Cells[6 + count, 3].PutValue(document.DocNo);
                            workSheets[i + 2].Cells[6 + count, 4].PutValue(document.DocTitle);
                            workSheets[i + 2].Cells[6 + count, 5].PutValue(document.RevisionName);
                            workSheets[i + 2].Cells[6 + count, 6].PutValue(document.PlanedDate.Value.ToString("dd/MM/yyyy"));
                            docCount += 1;
                            count += 1;
                        }
                    }
                    workSheets[i + 2].Cells["A2"].PutValue(count);
                }



                summarySheet.Cells["D" + (7 + docListGroupByDisciplineList.Count)].PutValue("TOTAL");
                summarySheet.Cells["E" + (7 + docListGroupByDisciplineList.Count)].Formula = "=SUM(E7:E" + (7 + docListGroupByDisciplineList.Count) + ")";


                //summarySheet.AutoFilter.Filter(14, "TRUE");

                var style = new Aspose.Cells.Style();
                var flag = new StyleFlag();
                style = summarySheet.Cells.Rows[6 + docListGroupByDisciplineList.Count].Style;
                style.Font.IsBold = true;
                //style.BackgroundColor = Color.Orange;
                flag.FontBold = true;

                summarySheet.Cells.Rows[6 + docListGroupByDisciplineList.Count].ApplyStyle(style, flag);

                workSheets.RemoveAt(1);
            }

            var filename = Utility.RemoveSpecialCharacterForFolder(projectObj.Name) + "_" + "EMDR next week issue Report.xlsm";
            workbook.Save(filePath + filename);
            //  this.Download_File(filePath + filename);
            this.DowloadAndDeleteFile(filePath + filename, filename);
        }

        protected void ckbShowAll_CheckedChange(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }
        #region Hiden export matrix
        //private void ExportProgressReport()
        //{
        //    var docList = (List<DocumentPackage>)Session["EMDRDocumentList"];
        //    var projectId = this.ddlProject.SelectedItem != null
        //        ? Convert.ToInt32(this.ddlProject.SelectedValue)
        //        : 0;
        //    var projectObj = this.scopeProjectService.GetById(projectId);
        //    var docStatusList = this.statusService.GetAllByProject(projectObj.ID).OrderBy(t => t.PercentCompleteDefault).ToList();

        //    var filePath = Server.MapPath("~/Exports") + @"\";
        //    var workbook = new Workbook();
        //    workbook.Open(filePath + @"Template\ProgressReportTemplate.xlsm");
        //    var dataSheet = workbook.Worksheets[0];

        //    dataSheet.Cells["E1"].PutValue(dataSheet.Cells["E1"].Value.ToString()
        //        .Replace("<ProjectName>", projectObj.FullName));

        //    for (int i = 0; i < docStatusList.Count; i++)
        //    {
        //        dataSheet.Cells[2, 6 + (3 * i)].PutValue(docStatusList[i].FullNameWithWeight);
        //        dataSheet.Cells[3, 6 + (3 * i)].PutValue("Plan Date");
        //        dataSheet.Cells[3, 7 + (3 * i)].PutValue("Actual Date");
        //        dataSheet.Cells[3, 8 + (3 * i)].PutValue("Comment Code");

        //        dataSheet.Cells.Merge(2, 6 + (3 * i), 1, 3);
        //    }

        //    for (int i = 0; i < docList.Count; i++)
        //    {
        //        var docStatusProcessList = this.docStatusProcessService.GetAllByDoc(docList[i].ID);

        //        dataSheet.Cells[5 + i, 2].PutValue(i + 1);
        //        dataSheet.Cells[5 + i, 3].PutValue(docList[i].DocNo);
        //        dataSheet.Cells[5 + i, 4].PutValue(docList[i].DocTitle);
        //        dataSheet.Cells[5 + i, 5].PutValue(docList[i].RevisionName);
        //        for (int j = 0; j < docStatusProcessList.Count; j++)
        //        {
        //            dataSheet.Cells[5 + i, 6 + 3 * j].PutValue(docStatusProcessList[j].PlantDate);
        //            dataSheet.Cells[5 + i, 7 + 3 * j].PutValue(docStatusProcessList[j].ActualDate);
        //            dataSheet.Cells[5 + i, 8 + 3 * j].PutValue(docStatusProcessList[j].CommentCode);
        //        }
        //    }

        //    var filename = this.ddlProject.SelectedItem.Text + "$" + "DocumentProgressReport.xlsm";
        //    workbook.Save(filePath + filename);
        //    this.Download_File(filePath + filename);
        //}
        //private void ExportDistributionMatrixDetail()
        //{
        //    if (this.ddlProject.SelectedItem != null)
        //    {
        //        var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
        //        var projectName = this.ddlProject.SelectedItem.Text;
        //        var groupList = this.roleService.GetAll(false);
        //        var filePath = Server.MapPath("Exports") + @"\";
        //        var workbook = new Workbook();
        //        workbook.Open(filePath + @"Template\DocDistributionmatrixDetail_Template.xlsm");
        //        var sheets = workbook.Worksheets;
        //        for (int i = 0; i < groupList.Count; i++)
        //        {
        //            sheets.AddCopy("DistributionMatrix");
        //            sheets[i + 1].Name = groupList[i].Name;

        //            var docRoleMatrix = this.docRoleMatrixService.GetAllDocRoleMatrixByGroup(groupList[i].Id, projectId);
        //            if (docRoleMatrix.Count > 0)
        //            {
        //                var docIds = docRoleMatrix.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
        //                var userList = this.userService.GetAllByRoleId(groupList[i].Id);
        //                var leadUsers = userList.Where(t => t.IsLeader.GetValueOrDefault()).OrderBy(t => t.FullName).ToList();
        //                var engUsers = userList.Where(t => t.IsEngineer.GetValueOrDefault()).OrderBy(t => t.FullName).ToList();

        //                var dtFull = new DataTable();
        //                dtFull.Columns.AddRange(new[]
        //                {
        //                    new DataColumn("DocID", typeof (String)),
        //                    new DataColumn("NoIndex", typeof (String)),
        //                    new DataColumn("DocNo", typeof (String)),
        //                    new DataColumn("DocTitle", typeof (String)),
        //                    new DataColumn("DocRev", typeof (String)),
        //                });

        //                for (int j = 0; j < leadUsers.Count; j++)
        //                {
        //                    sheets[i + 1].Cells[2, 5 + j].PutValue(leadUsers[j].FullName);
        //                    sheets[i + 1].Cells[3, 5 + j].PutValue(j + 1);
        //                    sheets[i + 1].Cells[4, 5 + j].PutValue(leadUsers[j].Id);

        //                    // Add dynamic matrix column
        //                    var colGroup = new DataColumn("Col_" + leadUsers[j].Id, typeof(String));
        //                    dtFull.Columns.Add(colGroup);
        //                }

        //                sheets[i + 1].Cells.Merge(1, 5, 1, leadUsers.Count);
        //                sheets[i + 1].Cells[1, 5].PutValue("Lead");

        //                for (int j = 0; j < engUsers.Count; j++)
        //                {
        //                    sheets[i + 1].Cells[2, 5 + j + leadUsers.Count].PutValue(engUsers[j].FullName);
        //                    sheets[i + 1].Cells[3, 5 + j + leadUsers.Count].PutValue(j + 1 + leadUsers.Count);
        //                    sheets[i + 1].Cells[4, 5 + j + leadUsers.Count].PutValue(engUsers[j].Id);

        //                    // Add dynamic matrix column
        //                    var colGroup = new DataColumn("Col_" + engUsers[j].Id, typeof(String));
        //                    dtFull.Columns.Add(colGroup);
        //                }

        //                sheets[i + 1].Cells.Merge(1, 5 + leadUsers.Count, 1, engUsers.Count);
        //                sheets[i + 1].Cells[1, 5 + leadUsers.Count].PutValue("Engineer");

        //                sheets[i + 1].Cells["D1"].PutValue(sheets[i + 1].Cells["D1"].Value.ToString()
        //                    .Replace("<ProjectName>", projectName));

        //                var docListFull = this.documentPackageService.GetSpecialList(docIds, 1);
        //                var count = 1;

        //                var docGroupBydocType = docListFull.GroupBy(t => t.DocumentTypeName);
        //                foreach (var docListOfDocType in docGroupBydocType)
        //                {
        //                    var dataRow = dtFull.NewRow();
        //                    dataRow["DocID"] = 0;
        //                    dataRow["DocNo"] = docListOfDocType.Key;
        //                    dtFull.Rows.Add(dataRow);
        //                    //count += 1;

        //                    var docList = docListOfDocType.OrderBy(t => t.DocNo).ToList();
        //                    foreach (var document in docList)
        //                    {
        //                        dataRow = dtFull.NewRow();
        //                        dataRow["DocID"] = document.ID;
        //                        dataRow["NoIndex"] = count;
        //                        dataRow["DocNo"] = document.DocNo;
        //                        dataRow["DocTitle"] = document.DocTitle;
        //                        dataRow["DocRev"] = document.RevisionName;

        //                        var dmDetailList = this.dmDetailByUserService.GetAllByDoc(document.ID);
        //                        foreach (var dmDetailItem in dmDetailList)
        //                        {
        //                            if (leadUsers.Exists(t => t.Id == dmDetailItem.UserId)
        //                                || engUsers.Exists(t => t.Id == dmDetailItem.UserId))
        //                            {
        //                                dataRow["Col_" + dmDetailItem.UserId] = "X";
        //                            }
        //                        }

        //                        dtFull.Rows.Add(dataRow);

        //                        count += 1;
        //                    }
        //                }

        //                sheets[i + 1].Cells.ImportDataTable(dtFull, false, 5, 0, dtFull.Rows.Count, dtFull.Columns.Count,
        //                    true);
        //                sheets[i + 1].Cells["D2"].PutValue("Dept: " + groupList[i].FullName);
        //                sheets[i + 1].Cells["A1"].PutValue("DMDetail");
        //                sheets[i + 1].Cells["A2"].PutValue(dtFull.Rows.Count);
        //                sheets[i + 1].Cells["A3"].PutValue(leadUsers.Count + engUsers.Count);
        //            }
        //        }
        //        sheets.RemoveAt(0);
        //        var filename = this.ddlProject.SelectedItem.Text + "$" + "DocDistributionMatrix_Detail.xlsm";
        //        workbook.Save(filePath + filename);
        //        this.Download_File(filePath + filename);
        //    }
        //}

        //private void ExportDistributionMatrixLead()
        //{
        //    if (this.ddlProject.SelectedItem != null)
        //    {
        //        var roleId = UserSession.Current.RoleId;
        //        var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
        //        var projectName = this.ddlProject.SelectedItem.Text;
        //        var docRoleMatrix = this.docRoleMatrixService.GetAllDocRoleMatrixByGroup(roleId, projectId);
        //        if (docRoleMatrix.Count > 0)
        //        {
        //            var docIds = docRoleMatrix.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
        //            var userList = this.userService.GetAllByRoleId(roleId);
        //            var leadUsers = userList.Where(t => t.IsLeader.GetValueOrDefault()).OrderBy(t => t.FullName).ToList();

        //            var filePath = Server.MapPath("Exports") + @"\";
        //            var workbook = new Workbook();
        //            workbook.Open(filePath + @"Template\DocDistributionmatrixByLead_SingleProject_Template.xlsm");
        //            var dataSheet = workbook.Worksheets[0];
        //            var tempDataSheet = workbook.Worksheets[1];

        //            var dtFull = new DataTable();
        //            dtFull.Columns.AddRange(new[]
        //            {
        //                new DataColumn("DocID", typeof (String)),
        //                new DataColumn("NoIndex", typeof (String)),
        //                new DataColumn("DocNo", typeof (String)),
        //                new DataColumn("DocTitle", typeof (String)),
        //                new DataColumn("DocRev", typeof (String)),
        //            });

        //            for (int i = 0; i < leadUsers.Count; i++)
        //            {
        //                dataSheet.Cells[2, 6 + i].PutValue(leadUsers[i].FullName);
        //                dataSheet.Cells[3, 6 + i].PutValue(i + 1);
        //                dataSheet.Cells[4, 6 + i].PutValue(leadUsers[i].Id);

        //                // Add dynamic matrix column
        //                var colGroup = new DataColumn("Col_" + leadUsers[i].Id, typeof(String));
        //                dtFull.Columns.Add(colGroup);
        //            }

        //            dataSheet.Cells.Merge(1, 6, 1, leadUsers.Count);
        //            dataSheet.Cells[1, 6].PutValue("Lead");

        //            dataSheet.Cells["E1"].PutValue(dataSheet.Cells["E1"].Value.ToString()
        //                .Replace("<ProjectName>", projectName));

        //            var docListFull = this.documentPackageService.GetSpecialList(docIds, 1);
        //            var count = 1;

        //            var docGroupBydocType = docListFull.GroupBy(t => t.DocumentTypeName);
        //            foreach (var docListOfDocType in docGroupBydocType)
        //            {
        //                var dataRow = dtFull.NewRow();
        //                dataRow["DocID"] = 0;
        //                dataRow["DocNo"] = docListOfDocType.Key;
        //                dtFull.Rows.Add(dataRow);
        //                //count += 1;

        //                var docList = docListOfDocType.OrderBy(t => t.DocNo).ToList();
        //                foreach (var document in docList)
        //                {
        //                    dataRow = dtFull.NewRow();
        //                    dataRow["DocID"] = document.ID;
        //                    dataRow["NoIndex"] = count;
        //                    dataRow["DocNo"] = document.DocNo;
        //                    dataRow["DocTitle"] = document.DocTitle;
        //                    dataRow["DocRev"] = document.RevisionName;

        //                    var dmDetailList = this.dmDetailByUserService.GetAllByDoc(document.ID);
        //                    foreach (var dmDetailItem in dmDetailList)
        //                    {
        //                        if (leadUsers.Exists(t => t.Id == dmDetailItem.UserId))
        //                        {
        //                            dataRow["Col_" + dmDetailItem.UserId] = "X";
        //                        }
        //                    }

        //                    dtFull.Rows.Add(dataRow);

        //                    count += 1;
        //                }
        //            }

        //            dataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
        //            tempDataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
        //            dataSheet.Cells["E2"].PutValue("Dept: " + UserSession.Current.User.Role.FullName);
        //            dataSheet.Cells["A1"].PutValue("DMLead");
        //            dataSheet.Cells["A2"].PutValue(dtFull.Rows.Count);
        //            dataSheet.Cells["A3"].PutValue(leadUsers.Count);
        //            dataSheet.Cells["A4"].PutValue(projectId);

        //            tempDataSheet.IsVisible = false;

        //            var filename = this.ddlProject.SelectedItem.Text + "_" + UserSession.Current.User.Role.Name + "$" + "DocDistributionMatrix_ForLead.xlsm";
        //            workbook.Save(filePath + filename);
        //            this.Download_File(filePath + filename);
        //        }
        //    }
        //}

        //private void ExportDistributionMatrixEngineer()
        //{
        //    if (this.ddlProject.SelectedItem != null)
        //    {
        //        var roleId = UserSession.Current.RoleId;
        //        var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
        //        var projectName = this.ddlProject.SelectedItem.Text;

        //        var dmDetailByUserList = this.dmDetailByUserService.GetAllDMDetailByUser(UserSession.Current.User.Id, projectId);
        //        if (dmDetailByUserList.Count > 0)
        //        {
        //            var docIds = dmDetailByUserList.Select(t => t.DocId.GetValueOrDefault()).Distinct().ToList();
        //            var userList = this.userService.GetAllByRoleId(roleId);
        //            var engUsers = userList.Where(t => t.IsEngineer.GetValueOrDefault()).OrderBy(t => t.FullName).ToList();

        //            var filePath = Server.MapPath("Exports") + @"\";
        //            var workbook = new Workbook();
        //            workbook.Open(filePath + @"Template\DocDistributionmatrixByUser_OneProject_Template.xlsm");
        //            var dataSheet = workbook.Worksheets[0];
        //            var tempdataSheet = workbook.Worksheets[1];

        //            var dtFull = new DataTable();
        //            dtFull.Columns.AddRange(new[]
        //            {
        //                new DataColumn("DocID", typeof (String)),
        //                new DataColumn("NoIndex", typeof (String)),
        //                new DataColumn("DocNo", typeof (String)),
        //                new DataColumn("DocTitle", typeof (String)),
        //                new DataColumn("DocRev", typeof (String)),
        //            });

        //            for (int i = 0; i < engUsers.Count; i++)
        //            {
        //                dataSheet.Cells[2, 6 + i].PutValue(engUsers[i].FullName);
        //                dataSheet.Cells[3, 6 + i].PutValue(i + 1);
        //                dataSheet.Cells[4, 6 + i].PutValue(engUsers[i].Id);

        //                // Add dynamic matrix column
        //                var colGroup = new DataColumn("Col_" + engUsers[i].Id, typeof(String));
        //                dtFull.Columns.Add(colGroup);
        //            }

        //            dataSheet.Cells.Merge(1, 6, 1, engUsers.Count);
        //            dataSheet.Cells[1, 6].PutValue("Engineer");

        //            dataSheet.Cells["E1"].PutValue(dataSheet.Cells["E1"].Value.ToString()
        //                .Replace("<ProjectName>", projectName));

        //            var docListFull = this.documentPackageService.GetSpecialList(docIds, 1);
        //            var count = 1;

        //            var docGroupBydocType = docListFull.GroupBy(t => t.DocumentTypeName);
        //            foreach (var docListOfDocType in docGroupBydocType)
        //            {
        //                var dataRow = dtFull.NewRow();
        //                dataRow["DocID"] = 0;
        //                dataRow["DocNo"] = docListOfDocType.Key;
        //                dtFull.Rows.Add(dataRow);
        //                //count += 1;

        //                var docList = docListOfDocType.OrderBy(t => t.DocNo).ToList();
        //                foreach (var document in docList)
        //                {
        //                    dataRow = dtFull.NewRow();
        //                    dataRow["DocID"] = document.ID;
        //                    dataRow["NoIndex"] = count;
        //                    dataRow["DocNo"] = document.DocNo;
        //                    dataRow["DocTitle"] = document.DocTitle;
        //                    dataRow["DocRev"] = document.RevisionName;

        //                    var dmDetailList = this.dmDetailByUserService.GetAllByDoc(document.ID);
        //                    foreach (var dmDetailItem in dmDetailList)
        //                    {
        //                        if (engUsers.Exists(t => t.Id == dmDetailItem.UserId))
        //                        {
        //                            dataRow["Col_" + dmDetailItem.UserId] = "X";
        //                        }
        //                    }

        //                    dtFull.Rows.Add(dataRow);

        //                    count += 1;
        //                }
        //            }

        //            dataSheet.Cells.ImportDataTable(dtFull, false, 5, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);
        //            tempdataSheet.Cells.ImportDataTable(dtFull, false, 5, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);
        //            dataSheet.Cells["E2"].PutValue("Dept: " + UserSession.Current.User.Role.FullName);
        //            dataSheet.Cells["A1"].PutValue("DMEngineer");
        //            dataSheet.Cells["A2"].PutValue(dtFull.Rows.Count);
        //            dataSheet.Cells["A3"].PutValue(engUsers.Count);
        //            dataSheet.Cells["A4"].PutValue(projectId);

        //            tempdataSheet.IsVisible = false;

        //            var filename = this.ddlProject.SelectedItem.Text + "_" + UserSession.Current.User.Username + "-" + UserSession.Current.User.Role.Name + "$" + "DocDistributionMatrix_ForEngineer.xlsm";
        //            workbook.Save(filePath + filename);
        //            this.Download_File(filePath + filename);
        //        }
        //    }
        //}

        //private void ExportDistributionMatrix()
        //{
        //    if (this.ddlProject.SelectedItem != null)
        //    {
        //        // Get department joined in project
        //        var departmentJoinProject = this.projectDepartmentService.GetAllByProject(Convert.ToInt32(this.ddlProject.SelectedValue)).Select(t => t.DepartmentId.GetValueOrDefault()).ToList();

        //        var groupList = new List<Role>();

        //        if (departmentJoinProject.Count > 0)
        //        {
        //            groupList = this.roleService.GetAll(false).Where(t => departmentJoinProject.Contains(t.Id)).ToList();
        //        }
        //        else
        //        {
        //            groupList = this.roleService.GetAll(false);
        //        }
        //        // --------------------------------------------------------------


        //        var filePath = Server.MapPath("Exports") + @"\";
        //        var workbook = new Workbook();
        //        workbook.Open(filePath + @"Template\DocDistributionmatrix_SingleProject_Template.xlsm");
        //        var dataSheet = workbook.Worksheets[0];
        //        var tempDataSheet = workbook.Worksheets[1];

        //        var dtFull = new DataTable();
        //        dtFull.Columns.AddRange(new[]
        //            {
        //                new DataColumn("DocID", typeof (String)),
        //                new DataColumn("NoIndex", typeof (String)),
        //                new DataColumn("DocNo", typeof (String)),
        //                new DataColumn("DocTitle", typeof (String)),
        //                new DataColumn("DocRev", typeof (String)),
        //            });

        //        for (int i = 0; i < groupList.Count; i++)
        //        {
        //            dataSheet.Cells[2, 6 + i].PutValue(groupList[i].FullName);
        //            dataSheet.Cells[3, 6 + i].PutValue(i + 1);
        //            dataSheet.Cells[4, 6 + i].PutValue(groupList[i].Id);

        //            // Add dynamic matrix column
        //            var colGroup = new DataColumn("Col_" + groupList[i].Id, typeof(String));
        //            dtFull.Columns.Add(colGroup);
        //        }

        //        dataSheet.Cells.Merge(1, 6, 1, groupList.Count);
        //        dataSheet.Cells[1, 6].PutValue("Dept.");
        //        dataSheet.Cells["E1"].PutValue(dataSheet.Cells["E1"].Value.ToString()
        //                        .Replace("<ProjectName>", this.ddlProject.SelectedItem.Text));

        //        var docListFull = this.documentPackageService.GetAll(Convert.ToInt32(this.ddlProject.SelectedValue), 1);
        //        var count = 1;

        //        var docGroupBydocType = docListFull.GroupBy(t => t.DocumentTypeName);
        //        foreach (var docListOfDocType in docGroupBydocType)
        //        {
        //            var dataRow = dtFull.NewRow();
        //            dataRow["DocID"] = 0;
        //            dataRow["DocNo"] = docListOfDocType.Key;
        //            dtFull.Rows.Add(dataRow);
        //            //count += 1;

        //            var docList = docListOfDocType.ToList();
        //            foreach (var document in docList)
        //            {
        //                dataRow = dtFull.NewRow();
        //                dataRow["DocID"] = document.ID;
        //                dataRow["NoIndex"] = count;
        //                dataRow["DocNo"] = document.DocNo;
        //                dataRow["DocTitle"] = document.DocTitle;
        //                dataRow["DocRev"] = document.RevisionName;

        //                var distributionMatrixList = this.docRoleMatrixService.GetAllByDoc(document.ID);
        //                foreach (var dmItem in distributionMatrixList)
        //                {
        //                    if (departmentJoinProject.Count == 0 || departmentJoinProject.Contains(dmItem.RoleId.GetValueOrDefault()))
        //                    {
        //                        dataRow["Col_" + dmItem.RoleId] = "X";
        //                    }

        //                }

        //                dtFull.Rows.Add(dataRow);

        //                count += 1;
        //            }
        //        }

        //        dataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
        //        tempDataSheet.Cells.ImportDataTable(dtFull, false, 5, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
        //        dataSheet.Cells["A1"].PutValue("DMRole");
        //        dataSheet.Cells["A2"].PutValue(dtFull.Rows.Count);
        //        dataSheet.Cells["A3"].PutValue(groupList.Count);
        //        dataSheet.Cells["A4"].PutValue(Convert.ToInt32(this.ddlProject.SelectedValue));

        //        tempDataSheet.IsVisible = false;

        //        var filename = this.ddlProject.SelectedItem.Text + "$" + "DocDistributionMatrix_Department.xlsm";
        //        workbook.Save(filePath + filename);
        //        this.Download_File(filePath + filename);
        //    }

        //}
        #endregion
        private void ExportUpdateEDMR()
        {
            var cbShowAll = (CheckBox)this.CustomerMenu.Items[6].FindControl("ckbShowAll");
            var projectObj = this.scopeProjectService.GetById(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            var docList = new List<DocumentPackage>();
            if (projectObj != null)
            {
                if (this.rtvDiscipline.SelectedNode != null)
                {
                    var fullDocList = this.documentPackageService.GetAllByDiscipline(Convert.ToInt32(this.rtvDiscipline.SelectedValue), cbShowAll.Checked);
                    fullDocList = fullDocList.Where(t => t.TypeId == 1).ToList();
                    docList = fullDocList;
                }
                else
                {
                    var fullDocList = this.documentPackageService.GetAllRev(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, cbShowAll.Checked);

                    fullDocList = fullDocList.Where(t => t.TypeId == 1).ToList();
                    docList = fullDocList;
                }

                if (docList.Count > 0)
                {
                    var docListGroupByDisciplineList = docList.GroupBy(t => t.DisciplineName).ToList();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\UpdateEMDRTemplate.xlsm");
                    var revisionList = this.revisionService.GetAll();
                    var docTypeList = this.documentTypeService.GetAllByProject(this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0);

                    var projectId = this.ddlProject.SelectedItem != null
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0;
                    var departmentList = this.roleService.GetAll(false);
                    var codeList = this.docCodeService.GetAllByProject(projectId).ToList();
                    var statusList = this.statusService.GetAllByProject(projectId).ToList();

                    var sheets = workbook.Worksheets;
                    var tempDataSheet = sheets[0];

                    // Create rang data for validation
                    var rangeRevisionList = tempDataSheet.Cells.CreateRange("A1",
                        "A" + (revisionList.Count == 0 ? 1 : revisionList.Count));
                    var rangeDocTypeList = tempDataSheet.Cells.CreateRange("B1",
                        "B" + (docTypeList.Count == 0 ? 1 : docTypeList.Count));
                    var rangeDepartmentList = tempDataSheet.Cells.CreateRange("C1",
                        "C" + (departmentList.Count == 0 ? 1 : departmentList.Count));
                    var rgangStatusList = tempDataSheet.Cells.CreateRange("D1", "D" + (statusList.Count == 0 ? 1 : statusList.Count));
                    var rangeCodeList = tempDataSheet.Cells.CreateRange("E1", "E" + (codeList.Count == 0 ? 1 : codeList.Count));

                    rangeRevisionList.Name = "RevisionList";
                    rangeDocTypeList.Name = "DocTypeList";
                    rangeDepartmentList.Name = "DepartmentList";
                    rgangStatusList.Name = "StatusList";
                    rangeCodeList.Name = "CodeList";

                    for (int j = 0; j < revisionList.Count; j++)
                    {
                        rangeRevisionList[j, 0].PutValue(revisionList[j].Name);
                    }
                    for (int j = 0; j < docTypeList.Count; j++)
                    {
                        rangeDocTypeList[j, 0].PutValue(docTypeList[j].FullName);
                    }
                    for (int j = 0; j < departmentList.Count; j++)
                    {
                        rangeDepartmentList[j, 0].PutValue(departmentList[j].FullName);
                    }
                    for (int j = 0; j < statusList.Count; j++)
                    {
                        rgangStatusList[j, 0].PutValue(statusList[j].FullName);
                    }
                    for (int j = 0; j < codeList.Count; j++)
                    {
                        rangeCodeList[j, 0].PutValue(codeList[j].Name);
                    }
                    // -------------------------------------------------------------------------------

                    for (int i = 0; i < docListGroupByDisciplineList.Count; i++)
                    {
                        var docListGroupByDis = docListGroupByDisciplineList[i].ToList();
                        //sheets.AddCopy("Sheet1");
                        // Add init info for datasheet
                        sheets[i + 1].Name = docListGroupByDisciplineList[i].Key.Replace('/', '-').Replace('\\', '-');
                        sheets[i + 1].Cells["A1"].PutValue(this.ddlProject.SelectedValue);
                        sheets[i + 1].Cells["A2"].PutValue(docListGroupByDis[0].DisciplineId);
                        sheets[i + 2].Cells["A3"].PutValue(docListGroupByDis.Count());

                        sheets[i + 1].Cells["D1"].PutValue(sheets[i + 2].Cells["D1"].Value.ToString()
                                .Replace("<ProjectName>", this.ddlProject.SelectedItem.Text));
                        sheets[i + 1].Cells["D3"].PutValue(docListGroupByDis[0].DisciplineName);
                        sheets[i + 1].Cells["J2"].PutValue(DateTime.Now);
                        //var docStatusList = new List<Status>();
                        /// if (projectObj.ProjectTypeId == 2)
                        //{
                        //    docStatusList = this.statusService.GetAllByProject(projectObj.ID).OrderBy(t => t.PercentCompleteDefault).ToList();
                        //    for (int j = 0; j < docStatusList.Count; j++)
                        //    {
                        //        sheets[i + 1].Cells[4, 15 + j].PutValue(docStatusList[j].Name + Environment.NewLine +
                        //                                                "(" + docStatusList[j].PercentCompleteDefault +
                        //                                                "%)");
                        //    }

                        //    if (docStatusList.Count > 0)
                        //    {
                        //        sheets[i + 1].Cells.Merge(3, 15, 1, docStatusList.Count, false, false);
                        //        sheets[i + 1].Cells["P4"].PutValue("Document Status Plan (dd/MM/yyyy)");
                        //    }
                        //}
                        // ---------------------------------------------------------------------------

                        // Create validation
                        var validations = sheets[i + 1].Validations;
                        this.CreateValidation(rangeRevisionList.Name, validations, 5, docListGroupByDis.Count() + 5, 4, 4);
                        this.CreateValidation(rangeDepartmentList.Name, validations, 5, docListGroupByDis.Count() + 5, 6, 6);
                        this.CreateValidation(rangeDocTypeList.Name, validations, 5, docListGroupByDis.Count() + 5, 7, 7);
                        this.CreateValidation(rgangStatusList.Name, validations, 5, docListGroupByDis.Count() + 5, 12, 12);
                        this.CreateValidation(rangeCodeList.Name, validations, 5, docListGroupByDis.Count() + 5, 13, 13);
                        // ----------------------------------------------------------------------------

                        // Fill doc data
                        for (int j = 0; j < docListGroupByDis.Count; j++)
                        {
                            var docObj = docListGroupByDis[j];

                            sheets[i + 1].Cells[5 + j, 1].PutValue(docObj.ID);
                            sheets[i + 1].Cells[5 + j, 2].PutValue(docObj.DocNo);
                            sheets[i + 1].Cells[5 + j, 3].PutValue(docObj.DocTitle);
                            sheets[i + 1].Cells[5 + j, 4].PutValue(docObj.RevisionName);
                            sheets[i + 1].Cells[5 + j, 5].PutValue(docObj.PlanedDate != null ? docObj.PlanedDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 6].PutValue(docObj.DeparmentName);
                            sheets[i + 1].Cells[5 + j, 7].PutValue(docObj.DocumentTypeName);
                            sheets[i + 1].Cells[5 + j, 8].PutValue(docObj.IncomingTransNo);
                            sheets[i + 1].Cells[5 + j, 9].PutValue(docObj.IncomingTransDate != null ? docObj.IncomingTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 10].PutValue(docObj.IncomingHardCopyTransNo);
                            sheets[i + 1].Cells[5 + j, 11].PutValue(docObj.IncomingHardCopyTransDate != null ? docObj.IncomingHardCopyTransDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 12].PutValue(docObj.StatusName);
                            sheets[i + 1].Cells[5 + j, 13].PutValue(docObj.CommentCode);
                            sheets[i + 1].Cells[5 + j, 14].PutValue(docObj.DeadlineComment != null ? docObj.DeadlineComment.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            sheets[i + 1].Cells[5 + j, 15].PutValue(docObj.DeadlineResponse != null ? docObj.DeadlineResponse.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty);
                            //if (projectObj.ProjectTypeId == 2)
                            //{
                            //    //var docStatusProcessPlan = this.docStatusProcessService.GetAllByDoc(docObj.ID);
                            //    //for (int x = 0; x < docStatusList.Count; x++)
                            //    //{
                            //    //    sheets[i + 1].Cells[5 + j, 15 + x].PutValue(docStatusProcessPlan.First(t => t.StatusId == docStatusList[x].ID).PlantDate);
                            //    //}
                            //}
                        }
                        // -----------------------------------------------------------------------------------------
                    }

                    // Remove temp sheet not use
                    for (int i = docListGroupByDisciplineList.Count; i < 40; i++)
                    {
                        workbook.Worksheets.RemoveAt(docListGroupByDisciplineList.Count + 1);
                    }
                    // ---------------------------------------------------------------------------
                    workbook.Worksheets[0].IsVisible = false;

                    var filename = this.ddlProject.SelectedItem.Text + "$" + UserSession.Current.User.Username + "_UpdateEMDR.xlsm";
                    workbook.Save(filePath + filename);
                    // this.Download_File(filePath + filename);
                    this.DowloadAndDeleteFile(filePath + filename, filename);
                }
            }

        }



        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }
        protected DateTime Layngaythu2(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;

            if (dayOfWeek == DayOfWeek.Sunday)
            {
                //xét chủ nhật là đầu tuần thì thứ 2 là ngày kế tiếp nên sẽ tăng 1 ngày  
                //return date.AddDays(1);  

                // nếu xét chủ nhật là ngày cuối tuần  
                return date.AddDays(-6);
            }

            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            return date.AddDays(-offset);
        }

    }
}