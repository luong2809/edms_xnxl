﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Site.Master.cs" company="">
//   
// </copyright>
// <summary>
//   The site master.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Web.Security;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Security;
    using EDMs.Web.Utilities.Enums;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The site master.
    /// </summary>
    public partial class SiteMaster : MasterPage
    {
        #region Fields

        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        private PermissionService _permissionService;
        private MenuService _menuService;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the role id on session.
        /// </summary>
        /// <value>
        /// The role id.
        /// </value>
        public int RoleId
        {
            get { return UserSession.Current.User.RoleId != null ? UserSession.Current.User.RoleId.Value : -1; }
        }

        #endregion

        #region Methods

        public SiteMaster()
        {
            _permissionService = new PermissionService();
            _menuService = new MenuService();
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!UserSession.Current.IsAvailable)
            {
                if (Request.RawUrl != "/Controls/System/Login.aspx")
                {
                    Session.Add("ReturnURL", Request.RawUrl);
                }

                Response.Redirect("~/Controls/System/Login.aspx");
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        /// <summary>
        /// Handles the PreLoad event of the master_Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.InvalidOperationException">Validation of Anti-XSRF token failed.</exception>
        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? string.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? string.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.RadSearchBox.SearchContext.Items.Add(new SearchContextItem("EMDR", "1"));
                this.RadSearchBox.SearchContext.Items.Add(new SearchContextItem("Shop Drawing", "2"));
                this.RadSearchBox.SearchContext.ShowDefaultItem = false;

                if (UserSession.Current.IsAvailable)
                {
                    var lblFullName = this.menu.Items[0].FindControl("lblFullName") as Label;
                    var lblCurrentUserOnline = this.menu.Items[0].FindControl("lblCurrentUserOnline") as Label;

                    if (lblFullName != null)
                    {
                        lblFullName.Text = "Welcome, " + UserSession.Current.User.FullName + ".";
                    }

                    if (lblCurrentUserOnline != null)
                    {
                        lblCurrentUserOnline.Text = "Current license online: " + Application.Get("userLoginCount");
                    }

                    LoadLeftMenu();
                    LoadMainMenu();
                }
            }
        }

        /// <summary>
        /// Handles the OnItemClick event of the menu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadMenuEventArgs"/> instance containing the event data.</param>
        protected void menu_OnItemClick(object sender, RadMenuEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "LogoutCommand":
                    Session.Clear();
                    Session.Abandon();
                    FormsAuthentication.SignOut();

                    UserSession.DestroySession();
                    Response.Redirect("~/Controls/System/Login.aspx");
                    break;
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Loads the main menu.
        /// </summary>
        private void LoadMainMenu()
        {
            var menus = this._menuService.GetAllRelatedPermittedMenuItems(RoleId, (int)MenuType.TopMenu).Where(t => t.Active == true).OrderBy(t => t.Priority).ToList();

            if (!UserSession.Current.IsAdmin && !UserSession.Current.IsDC && !UserSession.Current.IsManager)
            {
                menus = menus.Where(t => t.Id != 63).ToList();
            }

            //phan quyen cho emdr phong thuong mai
            List<int> userTM = new List<int>() {581, 735, 586, 484, 451, 465, 637, 734, 460, 609, 578, 551, 495, 597, 654,
                653, 733, 732, 592, 731, 591, 730, 729, 461, 631, 463, 554, 704, 555, 557, 576, 575, 526, 573, 523, 478,
                667, 565, 736, 569, 527, 656, 564, 563, 652, 660, 668, 500, 452, 544, 502, 503, 512, 510, 508, 506, 504,
                536, 479, 538, 501, 584, 496, 610, 728, 723, 727, 677 };
            if (userTM.Contains(UserSession.Current.UserId))
            {
                var menuTM = this._menuService.GetByID(119);
                menus.Add(menuTM);
                menus = menus.OrderBy(t => t.Priority).ToList();
            }

            this.MainMenu.DataSource = menus;
            this.MainMenu.DataTextField = "Description";
            this.MainMenu.DataNavigateUrlField = "Url";
            this.MainMenu.DataFieldID = "Id";
            this.MainMenu.DataValueField = "Id";
            this.MainMenu.DataFieldParentID = "ParentId";

            this.MainMenu.DataBind();

            this.SetIcon(this.MainMenu.Items, menus);

            ////foreach (RadMenuItem menuItem in MainMenu.Items)
            ////{
            ////    if (!string.IsNullOrEmpty(menuItem.Value))
            ////    {
            ////        var tempMenu = menus.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
            ////        if (tempMenu != null)
            ////        {
            ////            menuItem.ImageUrl = tempMenu.Icon;
            ////        }
            ////    }
            ////}
        }

        private void SetIcon(RadMenuItemCollection menu, List<EDMs.Data.Entities.Menu> menuData)
        {
            foreach (RadMenuItem menuItem in menu)
            {
                if (!string.IsNullOrEmpty(menuItem.Value))
                {
                    var tempMenu = menuData.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
                    if (tempMenu != null)
                    {
                        menuItem.ImageUrl = tempMenu.Icon;
                    }
                }

                if (menuItem.Items.Count > 0)
                {
                    this.SetIcon(menuItem.Items, menuData);
                }
            }
        }

        /// <summary>
        /// Loads the left menu.
        /// </summary>
        private void LoadLeftMenu()
        {
            var menus = _menuService.GetAllRelatedPermittedMenuItems(RoleId, (int)MenuType.LeftMenu);
            if (menus == null)
            {
                //bottomLeftPane.Collapsed = true; 
                return;
            }

            //Gets root parents only
            menus = menus.Where(x => x.ParentId == null).ToList();

            if (menus.Count == 0)
            {
                //bottomLeftPane.Collapsed = true;
                return;
            }
            //LeftMenu.DataSource = menus;
            //LeftMenu.DataTextField = "Description";
            //LeftMenu.DataNavigateUrlField = "Url";
            //LeftMenu.DataFieldID = "Id";
            //LeftMenu.DataBind();
        }

        #endregion

        protected void RadSearchBox_Search(object sender, SearchBoxEventArgs e)
        {
            if (this.lblSearchContext.Value == "1")
            {
                Response.Redirect("~/Search.aspx?text=" + e.Text);
            }
            else
            {
                Response.Redirect("~/SearchShopDrawing.aspx?text=" + e.Text);
            }
        }
    }
}
