﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Document;
     using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using System.Net;
   // using SautinSoft;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ViewerFilePDF : Page
    {
        private readonly DocumentService documentServie;
        private readonly AttachFilesPackageService attachfilepackageService;
        public ViewerFilePDF()
        {
            this.documentServie = new DocumentService();
            this.attachfilepackageService = new AttachFilesPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["DocId"]))
            {
                var id = Convert.ToInt32(this.Request.QueryString["DocId"]);
                if (this.Request.QueryString["File"] == "0")
                {
                 var docfile = this.documentServie.GetById(id);
                this.PDFIframe.Attributes["src"] = docfile.FilePath.ToString() + "#toolbar=0&navpanes=0";
                }
                else if (this.Request.QueryString["File"] == "1")
                {
                    var docfile = this.attachfilepackageService.GetById(id);
                    this.PDFIframe.Attributes["src"] = docfile.FilePath.ToString() + "#toolbar=0&navpanes=0";

                }
            } 
            
        }

       
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}