﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DownloadFile.ashx.cs" company="">
//   
// </copyright>
// <summary>
//   Summary description for DownloadFile
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.IO;
    using System.Web;

    /// <summary>
    /// Summary description for DownloadFile
    /// </summary>
    public class DownloadFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string mediaName = "abc.rar"; // 600MB in file size
            if (string.IsNullOrEmpty(mediaName))
            {
                return;
            }

            string destPath = context.Server.MapPath("~/Exports/" + mediaName);
            // Check to see if file exist
            FileInfo fi = new FileInfo(destPath);

            try
            {
                if (fi.Exists)
                {
                    HttpContext.Current.Response.ClearHeaders();
                    HttpContext.Current.Response.ClearContent();
                    HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fi.Name);
                    HttpContext.Current.Response.AppendHeader("Content-Length", fi.Length.ToString());
                    HttpContext.Current.Response.ContentType = "application/octet-stream";
                    HttpContext.Current.Response.TransmitFile(fi.FullName);
                    HttpContext.Current.Response.Flush();
                }
            }
            catch (Exception exception)
            {
                HttpContext.Current.Response.ContentType = "text/plain";
                HttpContext.Current.Response.Write(exception.Message);
            }
            finally
            {
                HttpContext.Current.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
 
    }
}